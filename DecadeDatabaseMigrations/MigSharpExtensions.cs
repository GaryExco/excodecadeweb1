﻿using System;
using System.Data;
using System.Linq.Expressions;
using MigSharp;



namespace DecadeDatabaseMigrations
{
    public static class MigSharpExtensions
    {
        public static ExistingTableWithType<T> GetTable<T>( this IDatabase db )
        {
            return new ExistingTableWithType<T>( db.Tables[typeof (T).Name] );
        }


        public static IExistingColumn GetColumn<T>( this ExistingTableWithType<T> table, Expression<Func<T, object>> action ) where T : class
        {
            var exp = (MemberExpression) action.Body;
            return table.Table.Columns[exp.Member.Name];
        }
    }


    public class ExistingTableWithType<T>
    {
        public IExistingTable Table { get; internal set; }


        public ExistingTableWithType( IExistingTable table )
        {
            Table = table;
        }


        public IExistingTableWithAddedColumn AddNotNullableColumn( string columnName, DbType columnType )
        {
            return Table.AddNotNullableColumn(
                columnName,
                columnType );
        }


        public IExistingTableWithAddedColumn AddNullableColumn( string columnName, DbType columnType )
        {
            return Table.AddNullableColumn(
                columnName,
                columnType );
        }


        public IAddedForeignKey AddForeignKeyTo( string referencedTableName, string constraintName )
        {
            return Table.AddForeignKeyTo(
                referencedTableName,
                constraintName );
        }


        public IAddedIndex AddIndex( string indexName )
        {
            return Table.AddIndex( indexName );
        }


        public IAddedPrimaryKey AddPrimaryKey( string constraintName )
        {
            return Table.AddPrimaryKey( constraintName );
        }


        public IAddedUniqueConstraint AddUniqueConstraint( string constraintName )
        {
            return Table.AddUniqueConstraint( constraintName );
        }


        public void Drop()
        {
            Table.Drop();
        }


        public IExistingColumnCollection Columns
        {
            get { return Table.Columns; }
        }


        public IForeignKeyCollection ForeignKyes
        {
            get { return Table.ForeignKeys; }
        }


        public IIndexesCollection Indexes
        {
            get { return Table.Indexes; }
        }


        public IExistingPrimaryKey PrimaryKey( string constraintName )
        {
            return Table.PrimaryKey( constraintName );
        }


        public void Rename( string newName )
        {
            Table.Rename( newName );
        }


        public IUniqueConstraintCollection UniqueConstraints
        {
            get { return Table.UniqueConstraints; }
        }
    }
}