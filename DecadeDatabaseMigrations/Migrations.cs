﻿using System.Data;
using MigSharp;



// ReSharper disable InconsistentNaming

namespace DecadeDatabaseMigrations
{


    #region Add mincharge/maxcharge to d_pricelistitemcharges
    [MigrationExport( Tag = "Add mincharge/maxcharge to d_pricelistitemcharges" )]
    public class Migrations_00000001 : IMigration
    {
        public void Up( IDatabase db )
        {
            // Adds mincharge/maxcharge to d_pricelistitemcharges for new pricing changes for Brazil (Will be used at other facilities in the future).
            db.Tables["d_pricelistitemcharges"] //.GetTable<d_pricelistitemcharges>()
                // .Table
                .AddNotNullableColumn(
                    "mincharge",
                    DbType.Decimal )
                .OfSize(
                    18,
                    4 )
                .HavingDefault( -1 )
                .AddNotNullableColumn(
                    "maxcharge",
                    DbType.Decimal )
                .OfSize(
                    18,
                    4 )
                .HavingDefault( -1 );

            // Console.WriteLine();
        }
    }
    #endregion


    #region Add mincharge/maxcharge to d_pricelistitemcharges
    /*
    [MigrationExport(Tag = "Start changes for multi-plant database / Cleanup old unused tables")]
    public class Migrations_00000002 : IMigration
    {
        public void Up(IDatabase db)
        {
            db.CreateTable("d_plant")
                .WithNotNullableColumn("plantid", DbType.UInt16)
                .WithNotNullableColumn("name", DbType.AnsiString)
                    .OfSize(50)
                .WithNotNullableColumn("shortname", DbType.AnsiStringFixedLength)
                    .OfSize(2)
                .WithNotNullableColumn("locale", DbType.AnsiString)
                    .OfSize(5)
                .WithNotNullableColumn("defaultcurrency", DbType.AnsiStringFixedLength)
                    .OfSize(3)
                .WithNotNullableColumn("yearopen", DbType.UInt16);

            db.CreateTable("d_permissionname")
                .WithNotNullableColumn("name", DbType.AnsiString)
                    .OfSize(155);

            db.CreateTable("d_userpermissions")
                .WithNotNullableColumn("employeenumber", DbType.UInt32)
                .WithNotNullableColumn("plantid", DbType.UInt16)
                .WithNotNullableColumn("permissionname", DbType.AnsiString);

            db.GetTable<d_orderitem>()
                .AddPrimaryKey("PK")
                    .OnColumn("ordernumber")
                    .OnColumn("line");

            db.Tables.
            db.GetTable<d_order>()
                .AddNullableColumn("shelfdate", DbType.DateTime);

            // TODO: Migrate data from d_shelf into d_order.shelfdate

            // Remove extra tables
            db.Tables["d_itemprice"]
                 .Drop();

            db.Tables["d_news"]
                 .Drop();

            db.Tables["d_noteclass"]
                 .Drop();

            db.Tables["d_notedef"]
                 .Drop();

            db.Tables["d_notes"]
                 .Drop();

            db.Tables["d_notestations"]
                 .Drop();

            db.Tables["d_orderexported"]
                 .Drop();

            db.Tables["d_shelf"]
                 .Drop();

            db.Tables["d_ship"]
                 .Drop();

            db.GetTable<d_order>()
                .Columns["shelf"]
                .Drop();
        }
    }
    */
    #endregion
}

// ReSharper restore InconsistentNaming