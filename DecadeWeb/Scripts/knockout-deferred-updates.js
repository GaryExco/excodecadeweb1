﻿/*
 Deferred Updates plugin for Knockout http://knockoutjs.com/
 (c) Michael Best, Steven Sanderson
 License: MIT (http://www.opensource.org/licenses/mit-license.php)
 Version 2.3.1
*/
(function(b) { "function" === typeof require && "object" === typeof exports && "object" === typeof module ? b(require("knockout")) : "function" === typeof define && define.amd ? define(["knockout"], b) : b(ko) })(function(b, B) {
    function w(a, b) { for (var c in a) if (a.hasOwnProperty(c) && 0 <= a[c].toString().indexOf(b)) return c }

    function r(a, b) { for (var c in a) if (a.hasOwnProperty(c) && a[c] === b) return c }

    function Q(a, b, c, s) {
        var d = a.subscribe(b, null, "dirty", !1, s), h = a.subscribe(c, null, "change", !1, s);
        return {
            dispose: function() {
                d.dispose();
                h.dispose();
            },
            target: a
        };
    }

    var C = "object" === typeof global && global ? global : window;
    b.tasks = function() {
        function a(c) {
            var a = 0;
            try {
                for (var b = c; b = b._next;) e = b, b._done || (b._done = !0, b._func.apply(b.object, b.args || []), a++);
            } finally {
                if (c !== h) c._next = null, n = c;
                else {
                    g = [];
                    h._next = null;
                    f = n = h;
                    if (m) C[d](m);
                    m = B;
                }
                e = B;
            }
            return a++;
        }

        function J() { if (!e) return a(h) }

        var c, d;
        C.setImmediate ? (c = "setImmediate", d = "clearImmediate") : (c = "setTimeout", d = "clearTimeout");
        var m,
            h = {},
            n = h,
            g = [],
            e,
            f = h,
            p = {
                processImmediate: function(c, b, e) {
                    g.push(f);
                    f = n;
                    try {
                        return c.apply(b, e || []);
                    } finally {
                        try {
                            f._next && a(f);
                        } finally {
                            f = g.pop() || h;
                        }
                    }
                },
                processDelayed: function(b, a, d) {
                    if (a = a || a === B)
                        a: {
                            a = e || f;
                            for (var s; s = a._next; a = s)
                                if (s._func === b && !s._done) {
                                    a._next = s._next;
                                    a._next || (n = a);
                                    a = !0;
                                    break a;
                                }
                            a = !1;
                        }
                    d = d || {};
                    d._func = b;
                    n = n._next = d;
                    g.length || m || (m = C[c](J));
                    return !a;
                },
                makeProcessedCallback: function(a) { return function() { return p.processImmediate(a, this, arguments) } }
            };
        b.processDeferredBindingUpdatesForNode = b.processAllDeferredBindingUpdates = function() {
            for (var a = h; a = a._next;)
                a.node &&
                    !a._done && (a._done = !0, a._func.call());
        };
        b.processAllDeferredUpdates = J;
        b.evaluateAsynchronously = function(a, b) { return setTimeout(p.makeProcessedCallback(a), b) };
        return p;
    }();
    b.utils.objectForEach || (b.utils.objectForEach = function(a, b) { for (var c in a) a.hasOwnProperty(c) && b(c, a[c]) });
    b.utils.objectMap || (b.utils.objectMap = function(a, b) {
        if (!a) return a;
        var c = {}, d;
        for (d in a) a.hasOwnProperty(d) && (c[d] = b(a[d], d, a));
        return c;
    });
    var f = function(a, b) { for (var c in a) if (a.hasOwnProperty(c) && a[c] && a[c][b]) return a[c] }(b,
            "end"),
        R = w(f, ".apply(") || "ignore",
        H = w(f, ".push({"),
        K = w(f, ".length"),
        l = b.computed,
        S = r(b, l),
        E = r(l.fn, l),
        d = b.computed(function() {}),
        T = r(d, d.peek) || "peek",
        U = r(d, d.isActive) || "isActive",
        V = r(d, d.getDependenciesCount),
        L = r(d, !1),
        W = r(d, d.dispose),
        M = "disposeWhenNodeIsRemoved",
        N = "disposeWhen";
    if ("hasWriteFunction" != L) {
        var d = l.toString(), y;
        if (y = d.match(/.\.disposeWhenNodeIsRemoved\|\|.\.([^|]+)\|\|/)) M = y[1];
        if (d = d.match(/.\.disposeWhen\|\|.\.([^|]+)\|\|/)) N = d[1];
    }
    var X = w(b.utils, "ocument)"), d = b.subscribable.fn;
    y = w(d, ".bind(");
    var z = (new b.subscribable).subscribe(), O = z.dispose, P = z.constructor.prototype, Y = r(P, O);
    z.dispose();
    var z = null, u = [], Z = 0;
    f[H] = function(a) { u.push({ callback: a, deps: {} }) };
    f.end = function() { u.pop() };
    f[K] = function(a) {
        if (!b.isSubscribable(a)) throw Error("Only subscribable things can act as dependencies");
        if (0 < u.length) {
            var d = u[u.length - 1], c = a._id = a._id || ++Z;
            d && !d.deps[c] && (d.deps[c] = !0, d.callback(a, c));
        }
    };
    b.ignoreDependencies = f[R] = function(a, b, c) {
        try {
            return u.push(null), a.apply(b, c || []);
        } finally {
            u.pop();
        }
    };
    var $ = d[y];
    d[y] = function(a, d, c, f, m) {
        c = c || "change";
        var h;
        if (m) h = function(b) { a(b, c) }, "change" == c && (this.dependents = this.dependents || [], this.dependents.push(m));
        else {
            var n = function(b) { a.call(d, b, c) };
            h = "change" != c ? n : function(a) { v.deferUpdates && !1 !== g.deferUpdates || g.deferUpdates ? b.tasks.processDelayed(n, !0, { args: [a] }) : n(a) };
        }
        var g = $.call(this, h, null, c);
        g.target = this;
        g.event = c;
        g.dependent = m;
        g.deferUpdates = f;
        return g;
    };
    var I = d.notifySubscribers, q;
    d.notifySubscribers = function(a, b) {
        if ("change" === b || "dirty" ===
            b || b === B)
            if (q) q.push({ object: this, value: a, event: b });
            else
                try {
                    if (q = [], I.call(this, a, b), q.length) for (var c = 0, d; d = q[c]; c++) I.call(d.object, d.value, d.event);
                } finally {
                    q = null;
                }
        else I.call(this, a, b);
    };
    d.getDependents = function() { return this.dependents ? [].concat(this.dependents) : [] };
    P[Y] = function() {
        O.call(this);
        this.dependent && "change" == this.event && b.utils.arrayRemoveItem(this.target.dependents, this.dependent);
    };
    var v = function(a, d, c) {
        function s() {
            b.utils.objectForEach(A, function(b, a) { a.dispose() });
            b.utils.arrayForEach(w,
                function(a) { a.dispose() });
            A = {};
            q = 0;
            w = [];
            t = k = !1;
        }

        function m(a, c) {
            var d = "dirty" == c, f = d && !t && !k;
            d ? t = !0 : k = !0;
            (d = e.throttleEvaluation) && 0 <= d ? (clearTimeout(y), y = b.evaluateAsynchronously(g, d)) : v.deferUpdates && !1 !== e.deferUpdates || e.deferUpdates ? f = b.tasks.processDelayed(g, !0, { node: D }) : k && (f = g());
            f && e.notifySubscribers && (e.notifySubscribers(p, "dirty"), !t && d && clearTimeout(y));
        }

        function h(a, b) { t || k ? k = !0 : m(a, b) }

        function n(a, b) {
            var c;
            c = a[E] === v ? Q(a, m, h, e) : a.subscribe(m, null, "change", !1, e);
            A[b] = c;
            q++;
        }

        function g(a) {
            if (l ||
                !k && !0 !== a) return t = k, !1;
            if (F && F()) return e.dispose(), !1;
            l = !0;
            try {
                var c = b.utils.objectMap(A, function() { return !0 });
                f[H](function(a, b) { b in c ? c[b] = B : n(a, b) });
                var g = x.call(d);
                b.utils.objectForEach(c, function(a, b) { b && (A[a].dispose(), delete A[a], q--) });
                t = k = !1;
                e.equalityComparer && e.equalityComparer(p, g) || (e.notifySubscribers(p, "beforeChange"), p = g, e._latestValue = p, e.notifySubscribers(p));
            } finally {
                f.end(), t = k = l = !1;
            }
            return !0;
        }

        function e() {
            if (0 < arguments.length) {
                if ("function" === typeof u) {
                    var a = e.deferUpdates;
                    e.deferUpdates = !1;
                    try {
                        u.apply(d, arguments);
                    } finally {
                        e.deferUpdates = a;
                    }
                } else throw Error('Cannot write a value to a ko.computed unless you specify a "write" option. If you wish to read the current value, don\'t pass any parameters.');
                return this;
            }
            (k || t) && g(!0);
            f[K](e);
            return p;
        }

        function r() { return k || t || 0 < q }

        var p, t = !1, k = !0, l = !1, x = a;
        x && "object" == typeof x ? (c = x, x = c.read) : (c = c || {}, x || (x = c.read));
        if ("function" != typeof x) throw Error("Pass a function that returns the value of the ko.computed");
        var u = c.write;
        d || (d = c.owner);
        var A = {}, q = 0, w = [], y = null, z, D = c[M] || c.disposeWhenNodeIsRemoved || null;
        if (!0 !== c.deferEvaluation) {
            l = !0;
            try {
                f[H](n), e._latestValue = p = x.call(d);
            } finally {
                f.end(), k = l = !1;
            }
        }
        var G = s, F = c[N] || c.disposeWhen || function() { return !1 };
        if (D && r()) {
            G = function() {
                b.utils.domNodeDisposal.removeDisposeCallback(D, G);
                s();
            };
            b.utils.domNodeDisposal.addDisposeCallback(D, G);
            var C = F, F = function() { return !b.utils[X](D) || C() };
        }
        b.subscribable.call(e);
        b.utils.extend(e, v.fn);
        e[T] = e.peek = function() {
            (k || t) && g(!0);
            return p;
        };
        e[V] =
            e.getDependenciesCount = function() { return q };
        e[L] = e.hasWriteFunction = "function" === typeof u;
        e[W] = e.dispose = function() { G() };
        e[U] = e.isActive = r;
        e.activeWhen = function(a) {
            z || (z = b.computed(function() {
                l = !a();
                !l && k && m(B, "change");
            }), z.deferUpdates = !1, w.push(z));
        };
        e.getDependencies = function() {
            var a = [];
            b.utils.objectForEach(A, function(b, c) { a.push(c.target) });
            return a;
        };
        return e;
    };
    v[E] = l[E];
    v.fn = l.fn;
    v.fn[E] = v;
    v.deferUpdates = !0;
    b[S] = b.computed = b.dependentObservable = v;
    l = d = null;
    b.extenders.throttle = function(a, d) {
        if (b.isWriteableObservable(a)) {
            var c =
                null;
            return b.computed({
                read: a,
                write: function(f) {
                    clearTimeout(c);
                    c = b.evaluateAsynchronously(function() { a(f) }, d);
                }
            });
        }
        a.throttleEvaluation = d;
        return a;
    };
    b.extenders.deferred = function(a, b) { a.deferUpdates = b };
    return b;
});