ko.validation.rules['greaterThan'] = {
    validator: function (val, otherVal) {
        return val > otherVal;
    },
    message: "The field must be greater than {0}"
};

ko.validation.rules['isFloat'] = {
    validator: function (val) {
        return !isNaN(Globalize.parseFloat(val));
    },
    message: "The field must be a float."
};

ko.validation.rules['isInt'] = {
    validator: function (val) {
        return !isNaN(Globalize.parseInt(val));
    },
    message: "The field must be an integer."
};

ko.validation.configure({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessaged: true,
    parseInputAttributes: true,
    messageTemplate: null
});

((function () {
    var valueHandler = ko.bindingHandlers.value;
    var getInjectValueUpdate = function (allBindingsAccessor) {
        var AFTERKEYDOWN = 'afterkeydown';
        return function () {
            var allBindings = ko.utils.extend({}, allBindingsAccessor()), valueUpdate = allBindings.valueUpdate;

            if (valueUpdate === undefined) {
                return ko.utils.extend(allBindings, { valueUpdate: AFTERKEYDOWN });
            } else if (typeof valueUpdate === 'string' && valueUpdate !== AFTERKEYDOWN) {
                return ko.utils.extend(allBindings, { valueUpdate: [valueUpdate, AFTERKEYDOWN] });
            } else if (typeof valueUpdate === 'array' && ko.utils.arrayIndexOf(valueUpdate, AFTERKEYDOWN) === -1) {
                valueUpdate = ko.utils.arrayPushAll([AFTERKEYDOWN], valueUpdate);
                return ko.utils.extend(allBindings, { valueUpdate: valueUpdate });
            }
            return allBindings;
        };
    };
    ko.bindingHandlers.value = {
        'init': function (element, valueAccessor, allBindingsAccessor) {
            allBindingsAccessor = getInjectValueUpdate(allBindingsAccessor);
            return valueHandler.init(element, valueAccessor, allBindingsAccessor);
        },
        'update': valueHandler.update
    };
})());
