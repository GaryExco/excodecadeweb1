/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\typings\globalize\globalize.d.ts" />
/// <reference path="..\typings\ExcoGlobals.ts" />
/// <reference path="DecadeClasses.ts" />
// Regex to Test for Valid SO
var validOrdernumber = /^[0-9]{6}$/;
var validCustomer = /^[A-Z0-9]{1}[0-9]{5}$/;
var validEmployee = /^[0-9]{5}$/;
function IsValid(val, checkEmpty) {
    if (val !== undefined && val !== null) {
        if (checkEmpty === undefined || (checkEmpty && val !== ''))
            return true;
    }
    return false;
}
// Parse JSON Objects returned from ASP.Net (Doesn't look like this is needed with MVC)
function ParseJSON(msg) {
    if (msg.hasOwnProperty("d"))
        msg = $.parseJSON(msg["d"]);
    else
        msg = $.parseJSON(msg);
    return msg;
}
function DisplayError(type, title, message, clear) {
    if (clear === undefined || clear)
        $.pnotify_remove_all();
    $.pnotify({
        title: title,
        text: message,
        type: type
    });
}
// Read JSON For Errors Secion and Display them in the Destination ID
function DisplayErrors(msg, destId) {
    var rtnVal = false;
    if (msg["errors"].length > 0) {
        for (var i = 0; i < msg["errors"].length; i++) {
            var htmlStr = '<div class="alert ' + msg["errors"][i]["errortype"] + ' fade in">' +
                '<a href="#" class="close" data-dismiss="alert">x</a>' +
                msg["errors"][i]["errormsg"] +
                '</div>';
            $(htmlStr).hide().appendTo($("#" + destId)).fadeIn();
            htmlStr = "";
        }
        rtnVal = true;
    }
    return rtnVal;
}
String.prototype.format = String.prototype.f = function () {
    var args = typeof arguments[0] === 'object' ? arguments[0] : Array.prototype.slice.call(arguments, 0);
    return this.replace(/\{\{|\}\}|}|{(\w+)\}/g, function (m, n) {
        if (m == "{{") {
            return "{";
        }
        if (m == "}}") {
            return "}";
        }
        return args[n];
    });
};
//# sourceMappingURL=ExcoCommon.js.map