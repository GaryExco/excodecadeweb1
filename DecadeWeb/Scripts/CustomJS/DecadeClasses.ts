//// This file was Auto-Generated and could be overwritten at anytime,
////   use the DecadeExtensions. file to extend functionality.
/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\typings\knockout\knockout.d.ts" />
/// <reference path="..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\typings\globalize\globalize.d.ts" />
/// <reference path="..\typings\ExcoGlobals.ts" />
//#region Classes

class d_budget {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public estsales: KnockoutObservable<number> = ko.observable<number>();
    public yearmonth: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_custgroup {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public name: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_custgroupitems {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public groupid: KnockoutObservable<number> = ko.observable<number>();
    public customercode: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_customer {
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public name: KnockoutObservable<string> = ko.observable<string>();
    public onhold: KnockoutObservable<boolean> = ko.observable<boolean>();
    public creditlimit: KnockoutObservable<number> = ko.observable<number>();
    public gst: KnockoutObservable<boolean> = ko.observable<boolean>();
    public territory: KnockoutObservable<string> = ko.observable<string>();
    public contact: KnockoutObservable<string> = ko.observable<string>();
    public accountset: KnockoutObservable<string> = ko.observable<string>();
    public terms: KnockoutObservable<string> = ko.observable<string>();
    public freightcost: KnockoutObservable<number> = ko.observable<number>();
    public fax: KnockoutObservable<string> = ko.observable<string>();
    public faxconfirm: KnockoutObservable<boolean> = ko.observable<boolean>();
    public nitridecost: KnockoutObservable<number> = ko.observable<number>();
    public email: KnockoutObservable<string> = ko.observable<string>();
    public steelcost: KnockoutObservable<number> = ko.observable<number>();
    public ResPri: KnockoutObservable<number> = ko.observable<number>();
    public ResSec: KnockoutObservable<number> = ko.observable<number>();
    public ResTri: KnockoutObservable<number> = ko.observable<number>();
    public combineprices: KnockoutObservable<boolean> = ko.observable<boolean>();
    public mastercustomer: KnockoutObservable<string> = ko.observable<string>();
    public ftpremium: KnockoutObservable<number> = ko.observable<number>();
    public rank: KnockoutObservable<number> = ko.observable<number>();
    public weighttype: KnockoutObservable<number> = ko.observable<number>();
    public surcharge: KnockoutObservable<number> = ko.observable<number>();
    public minnitridecost: KnockoutObservable<number> = ko.observable<number>();
    public nitridemineffectivedate: KnockoutObservable<Date> = ko.observable<Date>();
    public surchargedetaildisplay: KnockoutObservable<number> = ko.observable<number>();
    public maxnitridecost: KnockoutObservable<number> = ko.observable<number>();
    public lang: KnockoutObservable<string> = ko.observable<string>();
    public gstlicense: KnockoutObservable<string> = ko.observable<string>();
    public suppliercode: KnockoutObservable<string> = ko.observable<string>();
    public active: KnockoutObservable<boolean> = ko.observable<boolean>();
    public pricelist: KnockoutObservable<string> = ko.observable<string>();
    public forceupdate: KnockoutObservable<boolean> = ko.observable<boolean>();
    public lastupdated: KnockoutObservable<Date> = ko.observable<Date>();
    public taxgroup: KnockoutObservable<string> = ko.observable<string>();
    public taxcode: KnockoutObservable<string> = ko.observable<string>();
    public surchargecode: KnockoutObservable<string> = ko.observable<string>();
    public roundtotals: KnockoutObservable<boolean> = ko.observable<boolean>();
    public confPriceBreakDown: KnockoutObservable<boolean> = ko.observable<boolean>();
    public confIncludeFreight: KnockoutObservable<boolean> = ko.observable<boolean>();
    public confAttachFile: KnockoutObservable<boolean> = ko.observable<boolean>();
    public pricelistMultiplier: KnockoutObservable<number> = ko.observable<number>();
    public currencyMultiplier: KnockoutObservable<number> = ko.observable<number>();
    public taxOnFreight: KnockoutObservable<boolean> = ko.observable<boolean>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_customer_order_count_total {
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public ordercount: KnockoutObservable<number> = ko.observable<number>();
    public ordertotal: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_customeraddress {
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public name: KnockoutObservable<string> = ko.observable<string>();
    public address1: KnockoutObservable<string> = ko.observable<string>();
    public address2: KnockoutObservable<string> = ko.observable<string>();
    public address3: KnockoutObservable<string> = ko.observable<string>();
    public address4: KnockoutObservable<string> = ko.observable<string>();
    public postalcode: KnockoutObservable<string> = ko.observable<string>();
    public phone: KnockoutObservable<string> = ko.observable<string>();
    public country: KnockoutObservable<string> = ko.observable<string>();
    public stateprov: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_customermapping {
    public cmsNumber: KnockoutObservable<string> = ko.observable<string>();
    public decadeCode: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_customership {
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public shipvia: KnockoutObservable<string> = ko.observable<string>();
    public defaultship: KnockoutObservable<boolean> = ko.observable<boolean>();
    public id: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_customerwatch {
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_dailylimits {
    public dietype: KnockoutObservable<string> = ko.observable<string>();
    public maxitems: KnockoutObservable<number> = ko.observable<number>();
    public id: KnockoutObservable<number> = ko.observable<number>();
    public name: KnockoutObservable<string> = ko.observable<string>();
    public ftable: KnockoutObservable<string> = ko.observable<string>();
    public ffield: KnockoutObservable<string> = ko.observable<string>();
    public flogic: KnockoutObservable<number> = ko.observable<number>();
    public fvalue: KnockoutObservable<string> = ko.observable<string>();
    public limit: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_dailytotals {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public orderdate: KnockoutObservable<Date> = ko.observable<Date>();
    public shipdate: KnockoutObservable<Date> = ko.observable<Date>();
    public invoicedate: KnockoutObservable<Date> = ko.observable<Date>();
    public sales: KnockoutObservable<number> = ko.observable<number>();
    public nitride: KnockoutObservable<number> = ko.observable<number>();
    public steelsurcharge: KnockoutObservable<number> = ko.observable<number>();
    public discountamount: KnockoutObservable<number> = ko.observable<number>();
    public ncr: KnockoutObservable<boolean> = ko.observable<boolean>();
    public ncrs: KnockoutObservable<boolean> = ko.observable<boolean>();
    public freight: KnockoutObservable<number> = ko.observable<number>();
    public gst: KnockoutObservable<number> = ko.observable<number>();
    public prefix: KnockoutObservable<string> = ko.observable<string>();
    public territory: KnockoutObservable<string> = ko.observable<string>();
    public accountset: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_department {
    public ID: KnockoutObservable<number> = ko.observable<number>();
    public name: KnockoutObservable<string> = ko.observable<string>();
    public finishedtaskcode: KnockoutObservable<string> = ko.observable<string>();
    public OrderInProcess: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_dietype {
    public prefix: KnockoutObservable<string> = ko.observable<string>();
    public dietype: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_exchangerates {
    public basecurrency: KnockoutObservable<string> = ko.observable<string>();
    public destcurrency: KnockoutObservable<string> = ko.observable<string>();
    public factor: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_hotlist {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public orderdate: KnockoutObservable<Date> = ko.observable<Date>();
    public shopdate: KnockoutObservable<Date> = ko.observable<Date>();
    public expshipdate: KnockoutObservable<Date> = ko.observable<Date>();
    public hotListDate: KnockoutObservable<Date> = ko.observable<Date>();
    public name: KnockoutObservable<string> = ko.observable<string>();
    public notes: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_mailinglist {
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public displayname: KnockoutObservable<string> = ko.observable<string>();
    public email: KnockoutObservable<string> = ko.observable<string>();
    public confirmations: KnockoutObservable<boolean> = ko.observable<boolean>();
    public forrecords: KnockoutObservable<boolean> = ko.observable<boolean>();
    public openorders: KnockoutObservable<boolean> = ko.observable<boolean>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_openorders_inc_ship {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();
    public orderdate: KnockoutObservable<Date> = ko.observable<Date>();
    public invoicedate: KnockoutObservable<Date> = ko.observable<Date>();
    public shopdate: KnockoutObservable<Date> = ko.observable<Date>();
    public noshipbefore: KnockoutObservable<Date> = ko.observable<Date>();
    public expshipdate: KnockoutObservable<Date> = ko.observable<Date>();
    public expreceiveddate: KnockoutObservable<Date> = ko.observable<Date>();
    public shipdate: KnockoutObservable<Date> = ko.observable<Date>();
    public shipwithorder: KnockoutObservable<number> = ko.observable<number>();
    public invoicenumber: KnockoutObservable<number> = ko.observable<number>();
    public onhold: KnockoutObservable<boolean> = ko.observable<boolean>();
    public customerpo: KnockoutObservable<string> = ko.observable<string>();
    public customerreq: KnockoutObservable<string> = ko.observable<string>();
    public note: KnockoutObservable<string> = ko.observable<string>();
    public defaultfreight: KnockoutObservable<boolean> = ko.observable<boolean>();
    public defaultgst: KnockoutObservable<boolean> = ko.observable<boolean>();
    public sales: KnockoutObservable<number> = ko.observable<number>();
    public freight: KnockoutObservable<number> = ko.observable<number>();
    public gst: KnockoutObservable<number> = ko.observable<number>();
    public parts: KnockoutObservable<number> = ko.observable<number>();
    public baddress1: KnockoutObservable<string> = ko.observable<string>();
    public baddress2: KnockoutObservable<string> = ko.observable<string>();
    public baddress3: KnockoutObservable<string> = ko.observable<string>();
    public baddress4: KnockoutObservable<string> = ko.observable<string>();
    public bpostalcode: KnockoutObservable<string> = ko.observable<string>();
    public saddress1: KnockoutObservable<string> = ko.observable<string>();
    public saddress2: KnockoutObservable<string> = ko.observable<string>();
    public saddress3: KnockoutObservable<string> = ko.observable<string>();
    public saddress4: KnockoutObservable<string> = ko.observable<string>();
    public spostalcode: KnockoutObservable<string> = ko.observable<string>();
    public stock: KnockoutObservable<boolean> = ko.observable<boolean>();
    public fasttrack: KnockoutObservable<boolean> = ko.observable<boolean>();
    public priority: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shipvia: KnockoutObservable<string> = ko.observable<string>();
    public defaultnitride: KnockoutObservable<boolean> = ko.observable<boolean>();
    public nitride: KnockoutObservable<number> = ko.observable<number>();
    public ncr: KnockoutObservable<boolean> = ko.observable<boolean>();
    public wr: KnockoutObservable<boolean> = ko.observable<boolean>();
    public defaultsteelsurcharge: KnockoutObservable<boolean> = ko.observable<boolean>();
    public steelsurcharge: KnockoutObservable<number> = ko.observable<number>();
    public steelrate: KnockoutObservable<number> = ko.observable<number>();
    public poscanned: KnockoutObservable<boolean> = ko.observable<boolean>();
    public polink: KnockoutObservable<string> = ko.observable<string>();
    public DWGName: KnockoutObservable<string> = ko.observable<string>();
    public Sent4Records: KnockoutObservable<boolean> = ko.observable<boolean>();
    public combineprices: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shelf: KnockoutObservable<boolean> = ko.observable<boolean>();
    public zeroprice: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shelforder: KnockoutObservable<boolean> = ko.observable<boolean>();
    public cor: KnockoutObservable<boolean> = ko.observable<boolean>();
    public ncrs: KnockoutObservable<boolean> = ko.observable<boolean>();
    public nitrideweight: KnockoutObservable<number> = ko.observable<number>();
    public nitridecharge: KnockoutObservable<number> = ko.observable<number>();
    public roughweight: KnockoutObservable<number> = ko.observable<number>();
    public freightweight: KnockoutObservable<number> = ko.observable<number>();
    public freightcharge: KnockoutObservable<number> = ko.observable<number>();
    public fasttrackcharge: KnockoutObservable<number> = ko.observable<number>();
    public fasttrackpercentage: KnockoutObservable<number> = ko.observable<number>();
    public total: KnockoutObservable<number> = ko.observable<number>();
    public subtotal: KnockoutObservable<number> = ko.observable<number>();
    public steel: KnockoutObservable<number> = ko.observable<number>();
    public shipvendorid: KnockoutObservable<number> = ko.observable<number>();
    public shippingvendor: KnockoutObservable<string> = ko.observable<string>();
    public trackingnumber: KnockoutObservable<string> = ko.observable<string>();
    public discountpercentage: KnockoutObservable<number> = ko.observable<number>();
    public discountamount: KnockoutObservable<number> = ko.observable<number>();
    public discountontotal: KnockoutObservable<boolean> = ko.observable<boolean>();
    public lastchange: KnockoutObservable<Date> = ko.observable<Date>();
    public hasnitridecomputedline: KnockoutObservable<boolean> = ko.observable<boolean>();
    public importedorder: KnockoutObservable<number> = ko.observable<number>();
    public ForApproval: KnockoutObservable<boolean> = ko.observable<boolean>();
    public htdate: KnockoutObservable<Date> = ko.observable<Date>();
    public hotListPriority: KnockoutObservable<number> = ko.observable<number>();
    public hotListDate: KnockoutObservable<Date> = ko.observable<Date>();
    public DesignDone: KnockoutObservable<boolean> = ko.observable<boolean>();
    public DesignType: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_openorders_no_ship {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();
    public orderdate: KnockoutObservable<Date> = ko.observable<Date>();
    public invoicedate: KnockoutObservable<Date> = ko.observable<Date>();
    public shopdate: KnockoutObservable<Date> = ko.observable<Date>();
    public noshipbefore: KnockoutObservable<Date> = ko.observable<Date>();
    public expshipdate: KnockoutObservable<Date> = ko.observable<Date>();
    public expreceiveddate: KnockoutObservable<Date> = ko.observable<Date>();
    public shipdate: KnockoutObservable<Date> = ko.observable<Date>();
    public shipwithorder: KnockoutObservable<number> = ko.observable<number>();
    public invoicenumber: KnockoutObservable<number> = ko.observable<number>();
    public onhold: KnockoutObservable<boolean> = ko.observable<boolean>();
    public customerpo: KnockoutObservable<string> = ko.observable<string>();
    public customerreq: KnockoutObservable<string> = ko.observable<string>();
    public note: KnockoutObservable<string> = ko.observable<string>();
    public defaultfreight: KnockoutObservable<boolean> = ko.observable<boolean>();
    public defaultgst: KnockoutObservable<boolean> = ko.observable<boolean>();
    public sales: KnockoutObservable<number> = ko.observable<number>();
    public freight: KnockoutObservable<number> = ko.observable<number>();
    public gst: KnockoutObservable<number> = ko.observable<number>();
    public parts: KnockoutObservable<number> = ko.observable<number>();
    public baddress1: KnockoutObservable<string> = ko.observable<string>();
    public baddress2: KnockoutObservable<string> = ko.observable<string>();
    public baddress3: KnockoutObservable<string> = ko.observable<string>();
    public baddress4: KnockoutObservable<string> = ko.observable<string>();
    public bpostalcode: KnockoutObservable<string> = ko.observable<string>();
    public saddress1: KnockoutObservable<string> = ko.observable<string>();
    public saddress2: KnockoutObservable<string> = ko.observable<string>();
    public saddress3: KnockoutObservable<string> = ko.observable<string>();
    public saddress4: KnockoutObservable<string> = ko.observable<string>();
    public spostalcode: KnockoutObservable<string> = ko.observable<string>();
    public stock: KnockoutObservable<boolean> = ko.observable<boolean>();
    public fasttrack: KnockoutObservable<boolean> = ko.observable<boolean>();
    public priority: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shipvia: KnockoutObservable<string> = ko.observable<string>();
    public defaultnitride: KnockoutObservable<boolean> = ko.observable<boolean>();
    public nitride: KnockoutObservable<number> = ko.observable<number>();
    public ncr: KnockoutObservable<boolean> = ko.observable<boolean>();
    public wr: KnockoutObservable<boolean> = ko.observable<boolean>();
    public defaultsteelsurcharge: KnockoutObservable<boolean> = ko.observable<boolean>();
    public steelsurcharge: KnockoutObservable<number> = ko.observable<number>();
    public steelrate: KnockoutObservable<number> = ko.observable<number>();
    public poscanned: KnockoutObservable<boolean> = ko.observable<boolean>();
    public polink: KnockoutObservable<string> = ko.observable<string>();
    public DWGName: KnockoutObservable<string> = ko.observable<string>();
    public Sent4Records: KnockoutObservable<boolean> = ko.observable<boolean>();
    public combineprices: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shelf: KnockoutObservable<boolean> = ko.observable<boolean>();
    public zeroprice: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shelforder: KnockoutObservable<boolean> = ko.observable<boolean>();
    public cor: KnockoutObservable<boolean> = ko.observable<boolean>();
    public ncrs: KnockoutObservable<boolean> = ko.observable<boolean>();
    public nitrideweight: KnockoutObservable<number> = ko.observable<number>();
    public nitridecharge: KnockoutObservable<number> = ko.observable<number>();
    public roughweight: KnockoutObservable<number> = ko.observable<number>();
    public freightweight: KnockoutObservable<number> = ko.observable<number>();
    public freightcharge: KnockoutObservable<number> = ko.observable<number>();
    public fasttrackcharge: KnockoutObservable<number> = ko.observable<number>();
    public fasttrackpercentage: KnockoutObservable<number> = ko.observable<number>();
    public total: KnockoutObservable<number> = ko.observable<number>();
    public subtotal: KnockoutObservable<number> = ko.observable<number>();
    public steel: KnockoutObservable<number> = ko.observable<number>();
    public shipvendorid: KnockoutObservable<number> = ko.observable<number>();
    public shippingvendor: KnockoutObservable<string> = ko.observable<string>();
    public trackingnumber: KnockoutObservable<string> = ko.observable<string>();
    public discountpercentage: KnockoutObservable<number> = ko.observable<number>();
    public discountamount: KnockoutObservable<number> = ko.observable<number>();
    public discountontotal: KnockoutObservable<boolean> = ko.observable<boolean>();
    public lastchange: KnockoutObservable<Date> = ko.observable<Date>();
    public hasnitridecomputedline: KnockoutObservable<boolean> = ko.observable<boolean>();
    public importedorder: KnockoutObservable<number> = ko.observable<number>();
    public ForApproval: KnockoutObservable<boolean> = ko.observable<boolean>();
    public htdate: KnockoutObservable<Date> = ko.observable<Date>();
    public hotListPriority: KnockoutObservable<number> = ko.observable<number>();
    public hotListDate: KnockoutObservable<Date> = ko.observable<Date>();
    public DesignDone: KnockoutObservable<boolean> = ko.observable<boolean>();
    public DesignType: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_order {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();
    public lastemployeenumber: KnockoutObservable<number> = ko.observable<number>();
    public orderdate: KnockoutObservable<Date> = ko.observable<Date>();
    public invoicedate: KnockoutObservable<Date> = ko.observable<Date>();
    public shopdate: KnockoutObservable<Date> = ko.observable<Date>();
    public noshipbefore: KnockoutObservable<Date> = ko.observable<Date>();
    public expshipdate: KnockoutObservable<Date> = ko.observable<Date>();
    public adjustedexpship: KnockoutObservable<Date> = ko.observable<Date>();
    public expreceiveddate: KnockoutObservable<Date> = ko.observable<Date>();
    public shipdate: KnockoutObservable<Date> = ko.observable<Date>();
    public shipwithorder: KnockoutObservable<number> = ko.observable<number>();
    public invoicenumber: KnockoutObservable<number> = ko.observable<number>();
    public onhold: KnockoutObservable<boolean> = ko.observable<boolean>();
    public customerpo: KnockoutObservable<string> = ko.observable<string>();
    public customerreq: KnockoutObservable<string> = ko.observable<string>();
    public note: KnockoutObservable<string> = ko.observable<string>();
    public defaultfreight: KnockoutObservable<boolean> = ko.observable<boolean>();
    public defaultgst: KnockoutObservable<boolean> = ko.observable<boolean>();
    public sales: KnockoutObservable<number> = ko.observable<number>();
    public freight: KnockoutObservable<number> = ko.observable<number>();
    public gst: KnockoutObservable<number> = ko.observable<number>();
    public parts: KnockoutObservable<number> = ko.observable<number>();
    public baddress1: KnockoutObservable<string> = ko.observable<string>();
    public baddress2: KnockoutObservable<string> = ko.observable<string>();
    public baddress3: KnockoutObservable<string> = ko.observable<string>();
    public baddress4: KnockoutObservable<string> = ko.observable<string>();
    public bpostalcode: KnockoutObservable<string> = ko.observable<string>();
    public saddress1: KnockoutObservable<string> = ko.observable<string>();
    public saddress2: KnockoutObservable<string> = ko.observable<string>();
    public saddress3: KnockoutObservable<string> = ko.observable<string>();
    public saddress4: KnockoutObservable<string> = ko.observable<string>();
    public spostalcode: KnockoutObservable<string> = ko.observable<string>();
    public stock: KnockoutObservable<boolean> = ko.observable<boolean>();
    public fasttrack: KnockoutObservable<boolean> = ko.observable<boolean>();
    public priority: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shipvia: KnockoutObservable<string> = ko.observable<string>();
    public defaultnitride: KnockoutObservable<boolean> = ko.observable<boolean>();
    public nitride: KnockoutObservable<number> = ko.observable<number>();
    public ncr: KnockoutObservable<boolean> = ko.observable<boolean>();
    public wr: KnockoutObservable<boolean> = ko.observable<boolean>();
    public defaultsteelsurcharge: KnockoutObservable<boolean> = ko.observable<boolean>();
    public steelsurcharge: KnockoutObservable<number> = ko.observable<number>();
    public steelrate: KnockoutObservable<number> = ko.observable<number>();
    public poscanned: KnockoutObservable<boolean> = ko.observable<boolean>();
    public polink: KnockoutObservable<string> = ko.observable<string>();
    public DWGName: KnockoutObservable<string> = ko.observable<string>();
    public Sent4Records: KnockoutObservable<boolean> = ko.observable<boolean>();
    public combineprices: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shelf: KnockoutObservable<boolean> = ko.observable<boolean>();
    public zeroprice: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shelforder: KnockoutObservable<boolean> = ko.observable<boolean>();
    public cor: KnockoutObservable<boolean> = ko.observable<boolean>();
    public ncrs: KnockoutObservable<boolean> = ko.observable<boolean>();
    public nitrideweight: KnockoutObservable<number> = ko.observable<number>();
    public nitridecharge: KnockoutObservable<number> = ko.observable<number>();
    public roughweight: KnockoutObservable<number> = ko.observable<number>();
    public freightweight: KnockoutObservable<number> = ko.observable<number>();
    public freightcharge: KnockoutObservable<number> = ko.observable<number>();
    public fasttrackcharge: KnockoutObservable<number> = ko.observable<number>();
    public fasttrackpercentage: KnockoutObservable<number> = ko.observable<number>();
    public total: KnockoutObservable<number> = ko.observable<number>();
    public subtotal: KnockoutObservable<number> = ko.observable<number>();
    public steel: KnockoutObservable<number> = ko.observable<number>();
    public shipvendorid: KnockoutObservable<number> = ko.observable<number>();
    public shippingvendor: KnockoutObservable<string> = ko.observable<string>();
    public trackingnumber: KnockoutObservable<string> = ko.observable<string>();
    public discountpercentage: KnockoutObservable<number> = ko.observable<number>();
    public defaultdiscountamount: KnockoutObservable<boolean> = ko.observable<boolean>();
    public discountamount: KnockoutObservable<number> = ko.observable<number>();
    public discountontotal: KnockoutObservable<boolean> = ko.observable<boolean>();
    public lastchange: KnockoutObservable<Date> = ko.observable<Date>();
    public hasnitridecomputedline: KnockoutObservable<boolean> = ko.observable<boolean>();
    public importedorder: KnockoutObservable<number> = ko.observable<number>();
    public ForApproval: KnockoutObservable<boolean> = ko.observable<boolean>();
    public htdate: KnockoutObservable<Date> = ko.observable<Date>();
    public hotListPriority: KnockoutObservable<number> = ko.observable<number>();
    public hotListDate: KnockoutObservable<Date> = ko.observable<Date>();
    public DesignDone: KnockoutObservable<boolean> = ko.observable<boolean>();
    public DesignType: KnockoutObservable<number> = ko.observable<number>();
    public canceled: KnockoutObservable<boolean> = ko.observable<boolean>();
    public countasorder: KnockoutObservable<boolean> = ko.observable<boolean>();
    public pricelistmultiplier: KnockoutObservable<number> = ko.observable<number>();
    public currencymultiplier: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_order_changelog {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public revision: KnockoutObservable<number> = ko.observable<number>();
    public revisiontime: KnockoutObservable<Date> = ko.observable<Date>();
    public propertyname: KnockoutObservable<string> = ko.observable<string>();
    public oldvalue: KnockoutObservable<string> = ko.observable<string>();
    public newvalue: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_orderchecklist {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public dienumber: KnockoutObservable<string> = ko.observable<string>();
    public feeder: KnockoutObservable<string> = ko.observable<string>();
    public backer: KnockoutObservable<string> = ko.observable<string>();
    public bolster: KnockoutObservable<string> = ko.observable<string>();
    public press: KnockoutObservable<string> = ko.observable<string>();
    public pressnumber: KnockoutObservable<string> = ko.observable<string>();
    public alloy: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_orderitem {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public line: KnockoutObservable<number> = ko.observable<number>();
    public prefix: KnockoutObservable<string> = ko.observable<string>();
    public suffix: KnockoutObservable<string> = ko.observable<string>();
    public qty: KnockoutObservable<number> = ko.observable<number>();
    public description: KnockoutObservable<string> = ko.observable<string>();
    public location: KnockoutObservable<string> = ko.observable<string>();
    public dienumber: KnockoutObservable<string> = ko.observable<string>();
    public note: KnockoutObservable<string> = ko.observable<string>();
    public price: KnockoutObservable<number> = ko.observable<number>();
    public chargenitride: KnockoutObservable<boolean> = ko.observable<boolean>();
    public steelcost: KnockoutObservable<number> = ko.observable<number>();
    public roughweight: KnockoutObservable<number> = ko.observable<number>();
    public freightweight: KnockoutObservable<number> = ko.observable<number>();
    public nitrideweight: KnockoutObservable<number> = ko.observable<number>();
    public steelsurchargeweight: KnockoutObservable<number> = ko.observable<number>();
    public baseprice: KnockoutObservable<number> = ko.observable<number>();
    public hasnitride: KnockoutObservable<boolean> = ko.observable<boolean>();
    public tariffcode: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_orderitemcharges {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public line: KnockoutObservable<number> = ko.observable<number>();
    public chargeline: KnockoutObservable<number> = ko.observable<number>();
    public chargetype: KnockoutObservable<string> = ko.observable<string>();
    public chargename: KnockoutObservable<string> = ko.observable<string>();
    public chargeprice: KnockoutObservable<number> = ko.observable<number>();
    public chargepercentage: KnockoutObservable<number> = ko.observable<number>();
    public qty: KnockoutObservable<number> = ko.observable<number>();
    public price: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_ordernotes {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public notes: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_ordertaxes {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public taxline: KnockoutObservable<number> = ko.observable<number>();
    public taxtype: KnockoutObservable<string> = ko.observable<string>();
    public rate: KnockoutObservable<number> = ko.observable<number>();
    public taxamount: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_orderwatch {
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public emailalert: KnockoutObservable<boolean> = ko.observable<boolean>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_partsteelcut {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public part: KnockoutObservable<string> = ko.observable<string>();
    public NumID: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_pricelist {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public plcode: KnockoutObservable<string> = ko.observable<string>();
    public description: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_pricelistbaseitems {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public plcode: KnockoutObservable<string> = ko.observable<string>();
    public prefix: KnockoutObservable<string> = ko.observable<string>();
    public suffix: KnockoutObservable<string> = ko.observable<string>();
    public price: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_pricelistholecharges {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public plcode: KnockoutObservable<string> = ko.observable<string>();
    public prefix: KnockoutObservable<string> = ko.observable<string>();
    public suffix: KnockoutObservable<string> = ko.observable<string>();
    public holeqty: KnockoutObservable<number> = ko.observable<number>();
    public holeprice: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_pricelistitemcharges {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public plcode: KnockoutObservable<string> = ko.observable<string>();
    public prefix: KnockoutObservable<string> = ko.observable<string>();
    public chargetype: KnockoutObservable<string> = ko.observable<string>();
    public chargename: KnockoutObservable<string> = ko.observable<string>();
    public chargeprice: KnockoutObservable<number> = ko.observable<number>();
    public chargepercentage: KnockoutObservable<number> = ko.observable<number>();
    public minqty: KnockoutObservable<number> = ko.observable<number>();
    public maxqty: KnockoutObservable<number> = ko.observable<number>();
    public mincharge: KnockoutObservable<number> = ko.observable<number>();
    public maxcharge: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_scheduling {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public customername: KnockoutObservable<string> = ko.observable<string>();
    public orderdate: KnockoutObservable<Date> = ko.observable<Date>();
    public shopdate: KnockoutObservable<Date> = ko.observable<Date>();
    public expreceiveddate: KnockoutObservable<Date> = ko.observable<Date>();
    public hotlistdate: KnockoutObservable<Date> = ko.observable<Date>();
    public hotlistpriority: KnockoutObservable<number> = ko.observable<number>();
    public onhold: KnockoutObservable<boolean> = ko.observable<boolean>();
    public awaitingapproval: KnockoutObservable<boolean> = ko.observable<boolean>();
    public nitride: KnockoutObservable<boolean> = ko.observable<boolean>();
    public diedia: KnockoutObservable<number> = ko.observable<number>();
    public dietype: KnockoutObservable<string> = ko.observable<string>();
    public dienumber: KnockoutObservable<string> = ko.observable<string>();
    public isinvoiced: KnockoutObservable<boolean> = ko.observable<boolean>();
    public oldShopDate: KnockoutObservable<Date> = ko.observable<Date>();
    public ShopDateChanged: KnockoutObservable<Date> = ko.observable<Date>();
    public LastChanged: KnockoutObservable<Date> = ko.observable<Date>();
    public Ignore: KnockoutObservable<boolean> = ko.observable<boolean>();
    public process: KnockoutObservable<string> = ko.observable<string>();
    public forcerefresh: KnockoutObservable<boolean> = ko.observable<boolean>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_schedulingcustomer {
    public customercode: KnockoutObservable<string> = ko.observable<string>();
    public HotListLevel: KnockoutObservable<number> = ko.observable<number>();
    public DateExpires: KnockoutObservable<Date> = ko.observable<Date>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_schedulingevent {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public partcode: KnockoutObservable<string> = ko.observable<string>();
    public deporder: KnockoutObservable<number> = ko.observable<number>();
    public department: KnockoutObservable<string> = ko.observable<string>();
    public duedate: KnockoutObservable<Date> = ko.observable<Date>();
    public completed: KnockoutObservable<boolean> = ko.observable<boolean>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_schedulingitem {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public partcode: KnockoutObservable<string> = ko.observable<string>();
    public reqdepartments: KnockoutObservable<string> = ko.observable<string>();
    public LastDep: KnockoutObservable<string> = ko.observable<string>();
    public LastStation: KnockoutObservable<string> = ko.observable<string>();
    public LastEmployee: KnockoutObservable<string> = ko.observable<string>();
    public LastTrackTime: KnockoutObservable<Date> = ko.observable<Date>();
    public LastScrapTime: KnockoutObservable<Date> = ko.observable<Date>();
    public LastChanged: KnockoutObservable<Date> = ko.observable<Date>();
    public Ignore: KnockoutObservable<boolean> = ko.observable<boolean>();
    public StartDate: KnockoutObservable<Date> = ko.observable<Date>();
    public LeadTime: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_shelf {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public shelved: KnockoutObservable<boolean> = ko.observable<boolean>();
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();
    public shelftime: KnockoutObservable<Date> = ko.observable<Date>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_ship {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public shipped: KnockoutObservable<boolean> = ko.observable<boolean>();
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();
    public readytoship: KnockoutObservable<boolean> = ko.observable<boolean>();
    public shiptime: KnockoutObservable<Date> = ko.observable<Date>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_shipdatechanged {
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public changedate: KnockoutObservable<Date> = ko.observable<Date>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_shippingvendors {
    public shipvendorid: KnockoutObservable<number> = ko.observable<number>();
    public name: KnockoutObservable<string> = ko.observable<string>();
    public trackingpage: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_solsync {
    public Name: KnockoutObservable<string> = ko.observable<string>();
    public LastOccurrence: KnockoutObservable<Date> = ko.observable<Date>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_task {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();
    public task: KnockoutObservable<string> = ko.observable<string>();
    public part: KnockoutObservable<string> = ko.observable<string>();
    public subpart: KnockoutObservable<string> = ko.observable<string>();
    public station: KnockoutObservable<string> = ko.observable<string>();
    public tasktime: KnockoutObservable<Date> = ko.observable<Date>();
    public flags: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_taskcode {
    public task: KnockoutObservable<string> = ko.observable<string>();
    public taskname: KnockoutObservable<string> = ko.observable<string>();
    public feeder: KnockoutObservable<number> = ko.observable<number>();
    public solidplate: KnockoutObservable<number> = ko.observable<number>();
    public solidbacker: KnockoutObservable<number> = ko.observable<number>();
    public mandrel: KnockoutObservable<number> = ko.observable<number>();
    public hollowplate: KnockoutObservable<number> = ko.observable<number>();
    public hollowbacker: KnockoutObservable<number> = ko.observable<number>();
    public bolster: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_taskcodemapping {
    public DepartmentID: KnockoutObservable<number> = ko.observable<number>();
    public task: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_taxinfo {
    public taxgroup: KnockoutObservable<string> = ko.observable<string>();
    public taxratecode: KnockoutObservable<string> = ko.observable<string>();
    public taxtype: KnockoutObservable<string> = ko.observable<string>();
    public sequence: KnockoutObservable<number> = ko.observable<number>();
    public description: KnockoutObservable<string> = ko.observable<string>();
    public rate: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_territories {
    public code: KnockoutObservable<string> = ko.observable<string>();
    public name: KnockoutObservable<string> = ko.observable<string>();
    public orderinlist: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class d_user {
    public employeenumber: KnockoutObservable<number> = ko.observable<number>();
    public lastname: KnockoutObservable<string> = ko.observable<string>();
    public firstname: KnockoutObservable<string> = ko.observable<string>();
    public trackpassword: KnockoutObservable<string> = ko.observable<string>();
    public salt: KnockoutObservable<string> = ko.observable<string>();
    public hashpassword: KnockoutObservable<string> = ko.observable<string>();
    public a_vieworders: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_modorders: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_viewcustomers: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_modcustomers: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_adminreports: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_shopreports: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_tracking: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_admin: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_god: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_ship: KnockoutObservable<boolean> = ko.observable<boolean>();
    public startapp: KnockoutObservable<string> = ko.observable<string>();
    public a_scrap: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_steel: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_steelpricing: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_cadnoteedit: KnockoutObservable<boolean> = ko.observable<boolean>();
    public email: KnockoutObservable<string> = ko.observable<string>();
    public a_deltasks: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_deltrack: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_hotlistview: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_hotlistadmin: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_htschedview: KnockoutObservable<boolean> = ko.observable<boolean>();
    public a_depschedadmin: KnockoutObservable<boolean> = ko.observable<boolean>();
    public secondaryemail: KnockoutObservable<string> = ko.observable<string>();
    public printername: KnockoutObservable<string> = ko.observable<string>();
    public debugmode: KnockoutObservable<boolean> = ko.observable<boolean>();
    public disabled: KnockoutObservable<boolean> = ko.observable<boolean>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class inv_steel {
    public ID: KnockoutObservable<number> = ko.observable<number>();
    public TrueDia: KnockoutObservable<number> = ko.observable<number>();
    public Length: KnockoutObservable<number> = ko.observable<number>();
    public PrevLength: KnockoutObservable<number> = ko.observable<number>();
    public NumID: KnockoutObservable<string> = ko.observable<string>();
    public DateIn: KnockoutObservable<Date> = ko.observable<Date>();
    public DateFin: KnockoutObservable<Date> = ko.observable<Date>();
    public lastchange: KnockoutObservable<Date> = ko.observable<Date>();
    public BarInf: KnockoutObservable<number> = ko.observable<number>();
    public lbPrice: KnockoutObservable<number> = ko.observable<number>();
    public vendorid: KnockoutObservable<number> = ko.observable<number>();
    public heatnumber: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class inv_steelinf {
    public ID: KnockoutObservable<number> = ko.observable<number>();
    public Material: KnockoutObservable<string> = ko.observable<string>();
    public Dia: KnockoutObservable<number> = ko.observable<number>();
    public MinStock: KnockoutObservable<number> = ko.observable<number>();
    public MaxStock: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class inv_vendors {
    public VendorID: KnockoutObservable<number> = ko.observable<number>();
    public VendorName: KnockoutObservable<string> = ko.observable<string>();
    public Address1: KnockoutObservable<string> = ko.observable<string>();
    public Address2: KnockoutObservable<string> = ko.observable<string>();
    public Address3: KnockoutObservable<string> = ko.observable<string>();
    public Address4: KnockoutObservable<string> = ko.observable<string>();
    public PostalCode: KnockoutObservable<string> = ko.observable<string>();
    public Phone: KnockoutObservable<string> = ko.observable<string>();
    public Fax: KnockoutObservable<string> = ko.observable<string>();
    public OurAcctNum: KnockoutObservable<string> = ko.observable<string>();
    public SteelVendor: KnockoutObservable<boolean> = ko.observable<boolean>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class scrapdes {
    public id: KnockoutObservable<number> = ko.observable<number>();
    public department: KnockoutObservable<string> = ko.observable<string>();
    public scrapdesc: KnockoutObservable<string> = ko.observable<string>();
    public errorcode: KnockoutObservable<string> = ko.observable<string>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class scraplog {
    public date: KnockoutObservable<Date> = ko.observable<Date>();
    public part: KnockoutObservable<string> = ko.observable<string>();
    public department: KnockoutObservable<string> = ko.observable<string>();
    public sonum: KnockoutObservable<number> = ko.observable<number>();
    public Errorcode: KnockoutObservable<string> = ko.observable<string>();
    public empnum: KnockoutObservable<number> = ko.observable<number>();
    public problem: KnockoutObservable<string> = ko.observable<string>();
    public correctioncode: KnockoutObservable<string> = ko.observable<string>();
    public ID: KnockoutObservable<number> = ko.observable<number>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}

class SteelCuts {
    public ID: KnockoutObservable<number> = ko.observable<number>();
    public ordernumber: KnockoutObservable<number> = ko.observable<number>();
    public PieceName: KnockoutObservable<string> = ko.observable<string>();
    public SteelGrade: KnockoutObservable<string> = ko.observable<string>();
    public Thickness: KnockoutObservable<number> = ko.observable<number>();
    public Diameter: KnockoutObservable<string> = ko.observable<string>();
    public Plotted: KnockoutObservable<boolean> = ko.observable<boolean>();
    public Deleted: KnockoutObservable<boolean> = ko.observable<boolean>();
    public PlotTime: KnockoutObservable<Date> = ko.observable<Date>();

    toJSON() {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    }
}
//#endregion