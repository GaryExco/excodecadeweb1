/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
class CustomerAutoComplete {
    constructor(textBoxName: string) {
        $(document).ready(function() {
            var custCache = {};
            $("#" + textBoxName).autocomplete({
                source: function(request, response) {
                    var term = request.term;
                    if (term in custCache) {
                        response(custCache[term]);
                        return;
                    }

                    $.getJSON("/api/customer/GetCustomerByAny", request, function(data, status, xhr) {
                        if (data == undefined || data == null || data == "")
                            return;

                        data = $.parseJSON(data);
                        custCache[term] = data;
                        response($.map(data, function(item) {
                            return {
                                label: item.customercode + " - " + item.name,
                                value: item.customercode
                            };
                        }));
                    });
                },
                select: function(e, ui) {
                    $("#" + textBoxName).val(ui.item.value);
                    $("#" + textBoxName).trigger("change");
                },
                minLength: 1,
                open: function() { $("#" + textBoxName).autocomplete("widget").width(300); }
            });
        });
    }
}