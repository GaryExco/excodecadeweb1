/// <reference path="..\typings\jquery\jquery.d.ts" />

function OrderIsValid(ordernumber: string): boolean {
    if (/^([0-9]){5,6}$/.exec(ordernumber)) {
        return true;
    }

    return false;
}