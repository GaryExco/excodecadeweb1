/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
function ClearOnDoubleClick(selector) {
    $(selector).dblclick(function () {
        $(this).val("");
        $(this).change();
        if ($(this).hasClass("datepicker")) {
            $(this).datepicker("hide");
        }
    });
}
//# sourceMappingURL=ClearOnDoubleClick.js.map