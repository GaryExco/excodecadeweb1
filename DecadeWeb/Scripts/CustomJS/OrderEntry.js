var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\typings\knockout\knockout.d.ts" />
/// <reference path="..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\typings\globalize\globalize.d.ts" />
/// <reference path="..\typings\ExcoGlobals.ts" />
/// <reference path="DecadeClasses.ts" />
var Order = (function (_super) {
    __extends(Order, _super);
    function Order() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Order;
}(d_order));
// Main Order Entry Screen
function showOrderForm(orderToDisp) {
    if (orderToDisp != null) {
        window.location.href = "/OrderEntry/OrderInfo.aspx?ordernumber=" + orderToDisp;
    }
    else {
        window.location.href = "/OrderEntry/OrderInfo.aspx";
    }
}
function GetFirstSelectedOrder() {
    var rtnVal = "";
    var checkboxes = $("#orders input:checkbox");
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            rtnVal = $(checkboxes[i]).attr("id").toString().replace("chk", "");
            break;
        }
    }
    return rtnVal;
}
function GetAllSelectedOrders() {
    var rtnVal = new Array();
    var checkboxes = $("#orders input:checkbox");
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            rtnVal[rtnVal.length] = $(checkboxes[i]).attr("id").toString().replace("chk", "");
        }
    }
    return rtnVal;
}
//# sourceMappingURL=OrderEntry.js.map