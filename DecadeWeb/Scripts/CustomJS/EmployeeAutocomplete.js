/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
var UserAutoComplete = (function () {
    function UserAutoComplete(textBoxName) {
        $(document).ready(function () {
            var custCache = {};
            $("#" + textBoxName).autocomplete({
                source: function (request, response) {
                    var term = request.term;
                    if (term in custCache) {
                        response(custCache[term]);
                        return;
                    }
                    $.getJSON("/api/user/GetUserByAny", request, function (data, status, xhr) {
                        data = JSON.parse(data);
                        custCache[term] = data;
                        response($.map(data, function (item) {
                            return {
                                label: item.employeenumber + " - " + item.name,
                                value: item.employeenumber
                            };
                        }));
                    });
                },
                select: function (e, ui) {
                    $("#" + textBoxName).val(ui.item.value).change();
                    $("#" + textBoxName).trigger("change");
                },
                minLength: 1
            });
        });
    }
    return UserAutoComplete;
}());
//# sourceMappingURL=EmployeeAutocomplete.js.map