/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
/// <reference path="..\ExcoCommon.ts" />
class SurchargeCalculatorViewModel {
    public customercode: KnockoutObservable<string> = ko.observable("").extend({ required: true });
    public diameter: KnockoutObservable<string> = ko.observable("").extend({ required: true, isFloat: true });
    public thickness: KnockoutObservable<string> = ko.observable("").extend({ required: true, isFloat: true });
    public piececount: KnockoutObservable<string> = ko.observable("").extend({ required: true, isInt: true });
    public dietype: KnockoutObservable<string> = ko.observable("").extend({ required: true });
    public mode: KnockoutObservable<string> = ko.observable("").extend({ required: true });
    public customrate: KnockoutObservable<string> = ko.observable("").extend({ isFloat: true });

    public defaultRate: KnockoutComputed<number>;
    public rate: KnockoutComputed<number>;
    public weight: KnockoutObservable<string> = ko.observable("");
    public fixedWeight: KnockoutComputed<string>;
    public total: KnockoutComputed<number>;

    public errors: KnockoutValidationErrors;

    private loadWeightTimeout;
    private loadRateTimeout;

    public customerInfo: KnockoutObservable<any> = ko.observable();

    constructor() {
        var self = this;

        // Used to disable form submission (which reloads the page)
        $("#surchargeform input").keypress(function(e) {
            return e.keyCode !== 13;
        });

        self.errors = ko.validation.group(self);

        self.customercode.subscribe(function() {
            self.LoadRate();
            self.Submit();
        });

        self.diameter.subscribe(function() {
            self.Submit();
        });

        self.thickness.subscribe(function() {
            self.Submit();
        });

        self.piececount.subscribe(function() {
            self.Submit();
        });

        self.dietype.subscribe(function() {
            self.Submit();
        });

        self.mode.subscribe(function() {
            self.Submit();
        });

        self.customrate.subscribe(function() {
            self.Submit();
        });

        self.defaultRate = ko.computed<number>({
            read: function() {
                if (self.customerInfo() === undefined || self.customerInfo() === null)
                    return 0;

                if (self.mode() === "freight")
                    return self.customerInfo().freightcost;

                return self.customerInfo().steelcost;
            }
        });

        self.rate = ko.computed({
            read: function() {
                if (IsValid(self.customrate(), true) && !isNaN(Number(self.customrate())))
                    return Number(self.customrate());

                return Number(self.defaultRate());
            }
        });

        self.fixedWeight = ko.computed({
            read: function() {
                return RoundNumber(Number(self.weight()), 2) + " lbs.";
            }
        });

        self.total = ko.computed({
            read: function() {
                return Number(self.rate()) * Number(self.weight());
            }
        });

        ko.applyBindings(self);
    }

    public LoadRate(): void {
        var self = this;

        self.customerInfo(null);

        if (!self.customercode.isValid)
            return;

        if (!validCustomer.test(self.customercode()))
            return;

        if (IsValid(self.loadRateTimeout))
            clearTimeout(self.loadRateTimeout);

        self.loadRateTimeout = setTimeout(function() {
            $.ajax({
                    type: "GET",
                    url: "/api/Customer/Get",
                    data: ko.mapping.toJS({ id: self.customercode() })
                })
                .done(function(data) {

                    if (!data.isValid) {
                        DisplayError("error", "Error", "Error loading rate data (" + data.message + ").");
                        return;
                    }

                    self.customerInfo(data.Customer);
                })
                .fail(function() {
                    DisplayError("error", "Error", "Error loading rate data");
                });
        }, 250);
    }

    public Submit(): void {
        var self = this;

        if (self.errors().length > 0)
            return;

        if (IsValid(self.loadWeightTimeout))
            clearTimeout(self.loadWeightTimeout);

        self.loadWeightTimeout = setTimeout(function() {
            $.ajax({
                    type: "GET",
                    url: "/api/OrderEntry/GetWeight",
                    data: ko.mapping.toJS({
                        weightMode: self.mode(),
                        customercode: self.customercode(),
                        prefix: self.dietype(),
                        diameter: self.diameter(),
                        thickness: self.thickness(),
                        pieceCount: self.piececount()
                    })
                })
                .done(function(data) {

                    if (!data.isValid) {
                        DisplayError("error", "Error", "Error getting weight.");
                        return;
                    }

                    self.weight(data.weight);

                })
                .fail(function() {
                    DisplayError("error", "Error processing", "message");
                })
                .always(function() {
                    self.errors.showAllMessages(false);
                });
        }, 200);
    }
}