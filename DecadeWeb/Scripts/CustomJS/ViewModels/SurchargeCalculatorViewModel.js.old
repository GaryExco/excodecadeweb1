/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
/// <reference path="..\ExcoCommon.ts" />
var SurchargeCalculatorViewModel = (function () {
    function SurchargeCalculatorViewModel() {
        this.customercode = ko.observable("").extend({ required: true });
        this.diameter = ko.observable("").extend({ required: true, isFloat: true });
        this.thickness = ko.observable("").extend({ required: true, isFloat: true });
        this.piececount = ko.observable("").extend({ required: true, isInt: true });
        this.dietype = ko.observable("").extend({ required: true });
        this.mode = ko.observable("").extend({ required: true });
        this.customrate = ko.observable("").extend({ isFloat: true });
        this.weight = ko.observable("");
        this.customerInfo = ko.observable();
        var self = this;

        // Used to disable form submission (which reloads the page)
        $("#surchargeform input").keypress(function (e) {
            return e.keyCode !== 13;
        });

        self.errors = ko.validation.group(self);

        self.customercode.subscribe(function () {
            self.LoadRate();
            self.Submit();
        });

        self.diameter.subscribe(function () {
            self.Submit();
        });

        self.thickness.subscribe(function () {
            self.Submit();
        });

        self.piececount.subscribe(function () {
            self.Submit();
        });

        self.dietype.subscribe(function () {
            self.Submit();
        });

        self.mode.subscribe(function () {
            self.Submit();
        });

        self.customrate.subscribe(function () {
            self.Submit();
        });

        self.defaultRate = ko.computed({
            read: function () {
                if (self.customerInfo() === undefined || self.customerInfo() === null)
                    return 0;

                if (self.mode() === "freight")
                    return self.customerInfo().freightcost;

                return self.customerInfo().steelcost;
            }
        });

        self.rate = ko.computed({
            read: function () {
                if (IsValid(self.customrate(), true) && !isNaN(Number(self.customrate())))
                    return Number(self.customrate());

                return Number(self.defaultRate());
            }
        });

        self.fixedWeight = ko.computed({
            read: function () {
                return RoundNumber(Number(self.weight()), 2) + " lbs.";
            }
        });

        self.total = ko.computed({
            read: function () {
                return Number(self.rate()) * Number(self.weight());
            }
        });

        ko.applyBindings(self);
    }
    SurchargeCalculatorViewModel.prototype.LoadRate = function () {
        var self = this;

        self.customerInfo(null);

        if (!self.customercode.isValid)
            return;

        if (!validCustomer.test(self.customercode()))
            return;

        if (IsValid(self.loadRateTimeout))
            clearTimeout(self.loadRateTimeout);

        self.loadRateTimeout = setTimeout(function () {
            $.ajax({
                type: "GET",
                url: "/api/Customer/Get",
                data: ko.mapping.toJS({ id: self.customercode() })
            }).done(function (data) {
                if (!data.isValid) {
                    DisplayError("error", "Error", "Error loading rate data (" + data.message + ").");
                    return;
                }

                self.customerInfo(data.Customer);
            }).fail(function () {
                DisplayError("error", "Error", "Error loading rate data");
            });
        }, 250);
    };

    SurchargeCalculatorViewModel.prototype.Submit = function () {
        var self = this;

        if (self.errors().length > 0)
            return;

        if (IsValid(self.loadWeightTimeout))
            clearTimeout(self.loadWeightTimeout);

        self.loadWeightTimeout = setTimeout(function () {
            $.ajax({
                type: "GET",
                url: "/api/OrderEntry/GetWeight",
                data: ko.mapping.toJS({
                    weightMode: self.mode(),
                    customercode: self.customercode(),
                    prefix: self.dietype(),
                    diameter: self.diameter(),
                    thickness: self.thickness(),
                    pieceCount: self.piececount()
                })
            }).done(function (data) {
                if (!data.isValid) {
                    DisplayError("error", "Error", "Error getting weight.");
                    return;
                }

                self.weight(data.weight);
            }).fail(function () {
                DisplayError("error", "Error processing", "message");
            }).always(function () {
                self.errors.showAllMessages(false);
            });
        }, 200);
    };
    return SurchargeCalculatorViewModel;
})();
//# sourceMappingURL=SurchargeCalculatorViewModel.js.map
