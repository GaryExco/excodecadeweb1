var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
/// <reference path="..\ExcoCommon.ts" />
//#region Add Bars
var SteelInventoryAddBar = (function (_super) {
    __extends(SteelInventoryAddBar, _super);
    //#endregion
    function SteelInventoryAddBar() {
        var _this = _super.call(this) || this;
        //#region Variables
        // Public
        _this.Material = ko.observable("").extend({ required: true });
        _this.Diameter = ko.observable("").extend({ required: true, isFloat: true });
        // Setup Validation
        var self = _this;
        self.TrueDia.extend({ required: true, isFloat: true, greaterThan: self.Diameter });
        self.Length.extend({ required: true, isFloat: true });
        self.vendorid.extend({ required: true });
        self.lbPrice.extend({ required: true, isFloat: true });
        self.heatnumber.extend({ required: true });
        self.NumID(" ");
        self.errors = ko.validation.group(self);
        return _this;
    }
    SteelInventoryAddBar.prototype.toJSON = function () {
        var self = this;
        if (IsValid(self.errors) && self.errors.length > 0)
            return null;
        var copy = ko.toJS(self);
        return copy;
    };
    return SteelInventoryAddBar;
}(inv_steel));
var SteelInventoryAddBarViewModel = (function () {
    function SteelInventoryAddBarViewModel() {
        this.Bars = ko.observableArray();
        for (var i = 0; i < 10; i++)
            this.AddBar();
        ko.applyBindings(this);
    }
    SteelInventoryAddBarViewModel.prototype.AddBar = function () {
        this.Bars.push(new SteelInventoryAddBar());
        this.errors = ko.validation.group(this, { deep: true });
    };
    return SteelInventoryAddBarViewModel;
}());
//#endregion
//#region Edit Bars
var SteelInventoryEditBar = (function (_super) {
    __extends(SteelInventoryEditBar, _super);
    //#endregion
    function SteelInventoryEditBar() {
        var _this = _super.call(this) || this;
        //#region Variables
        // Public
        _this.NewLength = ko.observable("").extend({ required: true, isFloat: true });
        // Setup Validation
        var self = _this;
        self.NumID.extend({
            required: true,
            minLength: 7,
            ajax: '/api/SteelInventory/CheckBarID'
        });
        self.NewLength.extend({
            required: true,
            ajax: {
                url: '/api/SteelInventory/CheckBarLength',
                val: { "val": self.NumID, "newLength": self.NewLength }
            }
        });
        /*
        self.NewLength.extend({
            validation: {
                async: true,
                request: new Array(),
                timeouts: new Array(),
                validator: function (val, parms, callback) {
                    var self2 = this;
                    var ajaxKey = 'CheckBarLength';

                    if (parms.field != null) {
                        if (parms.id != null) {
                            ajaxKey = parms.field + parms.id;
                        } else {
                            ajaxKey = parms.field + '_new';
                        }
                    }

                    if (self2.request[ajaxKey] != null) {
                        self2.request[ajaxKey].abort();
                    }

                    var ajaxDefaults = {
                        url: '/api/SteelInventory/CheckBarLength',
                        type: 'GET',
                        success: callback,
                        data: { "id": self.NumID(), "newLength": val }
                    };

                    var options = $.extend(ajaxDefaults, parms);

                    clearTimeout(self2.timeouts[ajaxKey]);
                    self2.timeouts[ajaxKey] = setTimeout(function () {
                        if (self2.request[ajaxKey] != null) {
                            self2.request[ajaxKey].abort();
                        }
                        self2.request[ajaxKey] = $.ajax(options);
                    }, 200);

                    // $.ajax(options);
                },
                message: 'Invalid Length'
            }
        });
        */
        self.errors = ko.validation.group(self);
        return _this;
    }
    return SteelInventoryEditBar;
}(inv_steel));
var SteelInventoryEditBarViewModel = (function () {
    function SteelInventoryEditBarViewModel() {
        this.Bars = ko.observableArray();
        for (var i = 0; i < 1; i++)
            this.AddBar();
        ko.applyBindings(this);
    }
    SteelInventoryEditBarViewModel.prototype.AddBar = function () {
        this.Bars.push(new SteelInventoryEditBar());
        this.errors = ko.validation.group(this, { deep: true });
    };
    return SteelInventoryEditBarViewModel;
}());
//#endregion 
//# sourceMappingURL=SteelInventory.js.map