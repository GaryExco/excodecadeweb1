/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
/// <reference path="..\ExcoCommon.ts" />
//#region Add Bars
class SteelInventoryAddBar extends inv_steel {
    //#region Variables
    // Public
    public Material: KnockoutSubscribable<string> = ko.observable("").extend({ required: true });
    public Diameter: KnockoutSubscribable<string> = ko.observable("").extend({ required: true, isFloat: true });

    public errors: KnockoutValidationErrors;

//#endregion

    constructor() {
        super();
        // Setup Validation
        var self = this;

        self.TrueDia.extend({ required: true, isFloat: true, greaterThan: self.Diameter });
        self.Length.extend({ required: true, isFloat: true });
        self.vendorid.extend({ required: true });
        self.lbPrice.extend({ required: true, isFloat: true });
        self.heatnumber.extend({ required: true });
        self.NumID(" ");

        self.errors = ko.validation.group(self);
    }

    toJSON() {
        var self = this;

        if (IsValid(self.errors) && self.errors.length > 0)
            return null;

        var copy = ko.toJS(self);

        return copy;
    }
}

class SteelInventoryAddBarViewModel {
    public Bars: KnockoutObservableArray<SteelInventoryAddBar> = ko.observableArray<SteelInventoryAddBar>();
    private errors: KnockoutValidationErrors;

    constructor() {
        for (var i = 0; i < 10; i++)
            this.AddBar();

        ko.applyBindings(this);
    }

    AddBar(): void {
        this.Bars.push(new SteelInventoryAddBar());
        this.errors = ko.validation.group(this, { deep: true });
    }
}

//#endregion


//#region Edit Bars
class SteelInventoryEditBar extends inv_steel {
    //#region Variables
    // Public
    public NewLength: KnockoutSubscribable<string> = ko.observable("").extend({ required: true, isFloat: true });

    public errors: KnockoutValidationErrors;

//#endregion

    constructor() {
        super();
        // Setup Validation
        var self = this;
        self.NumID.extend({
            required: true,
            minLength: 7,
            ajax: '/api/SteelInventory/CheckBarID'
        });

        self.NewLength.extend({
            required: true,
            ajax: {
                url: '/api/SteelInventory/CheckBarLength',
                val: { "val": self.NumID, "newLength": self.NewLength }
            }
        });

        /*
        self.NewLength.extend({
            validation: {
                async: true,
                request: new Array(),
                timeouts: new Array(),
                validator: function (val, parms, callback) {
                    var self2 = this;
                    var ajaxKey = 'CheckBarLength';

                    if (parms.field != null) {
                        if (parms.id != null) {
                            ajaxKey = parms.field + parms.id;
                        } else {
                            ajaxKey = parms.field + '_new';
                        }
                    }

                    if (self2.request[ajaxKey] != null) {
                        self2.request[ajaxKey].abort();
                    }

                    var ajaxDefaults = {
                        url: '/api/SteelInventory/CheckBarLength',
                        type: 'GET',
                        success: callback,
                        data: { "id": self.NumID(), "newLength": val }
                    };

                    var options = $.extend(ajaxDefaults, parms);

                    clearTimeout(self2.timeouts[ajaxKey]);
                    self2.timeouts[ajaxKey] = setTimeout(function () {
                        if (self2.request[ajaxKey] != null) {
                            self2.request[ajaxKey].abort();
                        }
                        self2.request[ajaxKey] = $.ajax(options);
                    }, 200);

                    // $.ajax(options);
                },
                message: 'Invalid Length'
            }
        });
        */

        self.errors = ko.validation.group(self);
    }
}

class SteelInventoryEditBarViewModel {
    public Bars: KnockoutObservableArray<SteelInventoryEditBar> = ko.observableArray<SteelInventoryEditBar>();
    private errors: KnockoutValidationErrors;

    constructor() {
        for (var i = 0; i < 1; i++)
            this.AddBar();

        ko.applyBindings(this);
    }

    AddBar(): void {
        this.Bars.push(new SteelInventoryEditBar());
        this.errors = ko.validation.group(this, { deep: true });
    }
}
//#endregion