//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
//#endregion

// ReSharper disable InconsistentNaming
class DOrder extends d_order {
    public OrderNumber: KnockoutComputed<any>;
    public Customer: KnockoutObservable<DCustomer> = ko.observable<DCustomer>();
    public CustomerName: KnockoutObservable<string> = ko.observable<string>();
    public OrderCopies: KnockoutObservable<number> = ko.observable<number>();
    public CheckList: KnockoutObservable<d_orderchecklist>;
    public OriginalOrderDate: KnockoutObservable<Date> = ko.observable<Date>(); // Used to track time on order in case of date changes
    public TempOrderDate: KnockoutObservable<Date> = ko.observable<Date>();

    public BillingAddress: KnockoutObservable<string> = ko.observable<string>();
    public ShippingAddress: KnockoutObservable<string> = ko.observable<string>();

    public OrderItems: KnockoutObservableArray<DOrderItem> = ko.observableArray<DOrderItem>();
    public TaxItems: KnockoutObservableArray<d_ordertaxes> = ko.observableArray<d_ordertaxes>();

    public ChargeType: KnockoutObservable<string> = ko.observable("0");

    public SubTotal: KnockoutComputed<number>;

    public DieType: KnockoutComputed<string>;

    public errors: KnockoutValidationErrors;

    public CurrentOrderItem: KnockoutObservable<DOrderItem>;

    public DiscountInput: KnockoutObservable<string> = ko.observable("");

    public CurrentlySaving: KnockoutObservable<boolean> = ko.observable(false);

    public IsValid: KnockoutComputed<boolean>;

    public Symbol: KnockoutObservable<string> = ko.observable("$");

    // Override Computeables
    /*
    public Sales: KnockoutComputed<number>;
    public DiscountOnTotal: KnockoutComputed<boolean>;
    public DiscountPercentage: KnockoutComputed<number>;
    public DiscountAmount: KnockoutComputed<number>;
    public SteelSurcharge: KnockoutComputed<number>;
    public Freight: KnockoutComputed<number>;
    public FastTrackCharge: KnockoutComputed<number>;
    public Total: KnockoutComputed<number>;
    public OrderDate: KnockoutComputed<Date>;
    public CountAsOrder: KnockoutComputed<boolean>;
    public FreightWeight: KnockoutComputed<number>;
    public RoughWeight: KnockoutComputed<number>;
    public NitrideWeight: KnockoutComputed<number>;
    public Price: KnockoutComputed<number>;
    public GST: KnockoutComputed<number>;
    */

    // Override Text Holders
    public Override_steelsurcharge: KnockoutObservable<string> = ko.observable("");
    public Override_discountamount: KnockoutObservable<string> = ko.observable("");
    public Override_freight: KnockoutObservable<string> = ko.observable("");

    // Enabled Helpers
    public FieldsEnabled: KnockoutComputed<boolean>;
    public CustomerEnabled: KnockoutComputed<boolean>;
    public InvoiceEnabled: KnockoutComputed<boolean>;
    public CanClearInvoice: KnockoutObservable<boolean> = ko.observable(false);

    // Used in Change Address
    public TempAddressKey: KnockoutObservable<string> = ko.observable("");
    public TempAddress1: KnockoutObservable<string> = ko.observable("");
    public TempAddress2: KnockoutObservable<string> = ko.observable("");
    public TempAddress3: KnockoutObservable<string> = ko.observable("");
    public TempAddress4: KnockoutObservable<string> = ko.observable("");
    public TempAddress: KnockoutComputed<string>;
    public TempAddressPostalCode: KnockoutObservable<string> = ko.observable("");
    public TempAddressType: KnockoutObservable<string> = ko.observable("");

    // Used for Custom Charge
    public CustomCharge: KnockoutObservable<PriceListItemCharge> = ko.observable(new PriceListItemCharge());

    public ShipVendors: KnockoutObservableArray<d_shippingvendors> = ko.observableArray<d_shippingvendors>();

    // The first possible day this job can ship (No Nitride = Shop Date / Nitride = Shop Date + 1 Day)
    public FirstShipDate: KnockoutComputed<Date>;

    constructor(options?: any) {
        super();

        var self = this;

        self.OrderNumber = ko.computed({
            read: () => {
                if (IsValid(self.ordernumber()) && !isNaN(self.ordernumber()) && self.ordernumber() !== 0)
                    return self.ordernumber().toString();

                return "<Auto>";
            }
        });

        //#region Load Default Objects
        self.Customer = ko.observable(new DCustomer());
        self.CheckList = ko.observable(new d_orderchecklist());
        self.defaultdiscountamount(true);
        self.discountamount(0);
        self.shipvendorid(0);

        self.CurrentOrderItem = ko.observable(new DOrderItem(self));
        //#endregion

        ko.mapping.fromJS(vendors, {}, self.ShipVendors);

        if (options) {
            ko.mapping.fromJS(options.data, {
                'ignore': ['DieType'],
                'Customer': {
                    create: cOptions => new DCustomer(cOptions)
                },
                'OrderItems': {
                    create: oiOptions => new DOrderItem(this, oiOptions)
                },
                'TaxItems': {
                    create: taxOptions => new DOrderTaxes(this, taxOptions)
                }
            }, self);

            // Set Overrides
            self.Override_discountamount(options.data.discountamount.toString());
            self.Override_freight(options.data.freight.toString());
            self.Override_steelsurcharge(options.data.steelsurcharge.toString());
            self.shipvendorid(options.data.shipvendorid);
            self.OriginalOrderDate(Globalize.parseDate(options.data.orderdate, "yyyy-MM-ddTHH:mm:ss"));

            if (self.ncr()) {
                self.ChargeType("1");
            } else if (self.ncrs()) {
                self.ChargeType("2");
            } else if (self.zeroprice()) {
                self.ChargeType("3");
            } else {
                self.ChargeType("0");
            }

            setTimeout(() => {
                $("#in_discountpercentage").attr('placeholder', self.discountpercentage() + (self.discountontotal() ? "T" : "")).change();
            }, 0);

            /*
            setTimeout(()=> {
                $("#in_discountpercentage").attr('placeholder', self.DiscountPercentage() + (self.DiscountOnTotal() ? "T" : "")).change();
            }, 0);
            */
        }

        //#region Subscriptions
        self.OrderItems.subscribe(() => {
            self.UpdateLineNumbers();
        });

        //#region Load Customer Information
        self.customercode.subscribe(newValue => {
            if (validCustomer.test(newValue)) {
                $.get('/api/customer/Get', { id: newValue }).done(data => {
                    // Map Customer Object from Server
                    if (!data.isValid) {
                        DisplayError("error", "No Customer Found", "No valid customer was found.");
                        return;
                    }

                    ko.mapping.fromJS(data.Customer, { 'ShipVia': { create: svOptions => new DCustomerShip(svOptions) }, 'Addresses': { create: addrOptions => new DCustomerAddress(addrOptions) } }, self.Customer);
                    self.Customer().UpdateCustomerSettings(self, true);
                });
            }
        });
        //#endregion

        //#region Load Schedule
        self.shopdate.subscribe(newValue => {
            if (newValue instanceof Date) {
                $.get('/api/OrderEntry/Schedule', { shopdate: Globalize.format(newValue, "yyyy-MM-dd") }).done(data => {
                    var schedule = self.GetScheduleLine("S", data.Solid.Count, data.Solid.Limit) +
                        self.GetScheduleLine("H", data.Hollow.Count, data.Hollow.Limit);

                    if (data.Bolster.Count > 0)
                        schedule += self.GetScheduleLine("B", data.Bolster.Count, data.Bolster.Limit);

                    if (data.Ring.Count > 0)
                        schedule += self.GetScheduleLine("R", data.Ring.Count, data.Ring.Limit);

                    $("#sched").html(schedule);
                });
            }
        });
        //#endregion

        //#region Design Type
        self.DesignType.subscribe(newValue => {
            // Show/Hide Checklist on DesignType Change
            var isShown = $("#collapseChecklist").hasClass("in");
            switch (Number(newValue)) {
            case 2: // Repeat
                if (isShown)
                    $("#collapseChecklist").collapse("hide");
                break;

            default: // New & Re-Design
                if (!isShown)
                    $("#collapseChecklist").collapse("show");
                break;
            }
        });
        //#endregion

        //#region ChargeType
        self.ChargeType.subscribe((newValue: any) => {
            var val = Globalize.parseInt(newValue.toString());
            if (val == 1) {
                self.ncr(true);
                self.ncrs(false);
                self.zeroprice(false);
            } else if (val == 2) {
                self.ncr(false);
                self.ncrs(true);
                self.zeroprice(false);
            } else if (val == 3) {
                self.ncr(false);
                self.ncrs(false);
                self.zeroprice(true);
            } else {
                self.ncr(false);
                self.ncrs(false);
                self.zeroprice(false);
            }
        });
        //#endregion
        //#endregion

        //#region Computeds
        self.DieType = ko.computed({
            read: () => {
                var rtnVal: string = "Solid";

                ko.utils.arrayForEach(self.OrderItems(), (item: DOrderItem) => {
                    if (item.prefix() == "HO") {
                        rtnVal = "Hollow";
                        return;
                    }
                });

                return rtnVal;
            },
            write: () => {
                // Do Nothing
            }
        });

        self.CustomerEnabled = ko.computed({
            read: () => {
                if (!self.invoicedate())
                    return true;
                return false;
            }
        });

        self.FieldsEnabled = ko.computed({
            read: () => {
                if (!self.invoicedate() && self.Customer().IsValid())
                    return true;
                return false;
            }
        });

        self.InvoiceEnabled = ko.computed({
            read: () => {
                if (self.Customer().IsValid() && self.CanClearInvoice())
                    return true;
                return false;
            }
        });

        /// TODO: These are what need to change if this doesn't work.
        self.sales = ko.computed({
            read: () => {
                var total = 0;
                ko.utils.arrayForEach(self.OrderItems(), (item: DOrderItem) => {
                    total += item.ExtendedPrice();
                });
                return RoundNumber(total, OrderViewModel.DecimalPlaces);
            },
            write: () => {
                // Do Nothing
            }
        });

        self.discountontotal = ko.computed({
            read: () => {
                if (self.DiscountInput().toLowerCase().indexOf('t') > -1)
                    return true;

                return false;
            },
            write: () => {
                // Do Nothing
            }
        });

        self.discountpercentage = ko.computed({
            read: () => Globalize.parseFloat(self.DiscountInput().toLowerCase().replace('t', '')),
            write: () => {
                // Do Nothing
            }
        });

        self.discountamount = ko.computed({
            read: () => {
                var val;

                if (self.defaultdiscountamount()) {
                    var total = 0;
                    ko.utils.arrayForEach(self.OrderItems(), (item: DOrderItem) => {
                        if (self.discountontotal())
                            total += item.ExtendedPrice();
                        else {
                            total += item.baseprice() * item.qty();
                            ko.utils.arrayForEach(item.Charges(), (charge: DItemCharge) => {
                                if (!isNaN(charge.chargepercentage()) && charge.chargepercentage() !== 0) {
                                    total += charge.ExtendedPrice();
                                }
                            });
                        }
                    });

                    val = total * self.discountpercentage() / 100;
                } else {
                    val = Globalize.parseFloat(self.Override_discountamount().toString());
                }

                return RoundNumber(val, OrderViewModel.DecimalPlaces);
            },
            write: (value: any) => {
                // Do Nothing
                self.Override_discountamount(value.toString());
            },
            owner: self,
            deferEvaluation: true
        });

        self.steelsurcharge = ko.computed({
            read: () => {
                var val;
                if (self.defaultsteelsurcharge()) {
                    var weight = 0;
                    ko.utils.arrayForEach(self.OrderItems(), (item: DOrderItem) => {
                        weight += item.steelsurchargeweight();
                    });

                    val = weight * self.steelrate();
                } else {
                    val = Globalize.parseFloat(self.Override_steelsurcharge().toString());
                }

                return RoundNumber(val, OrderViewModel.DecimalPlaces);
            },
            write: (value: any) => {
                self.Override_steelsurcharge(value);
            },
            owner: self,
            deferEvaluation: true
        });

        self.freight = ko.computed({
            read: () => {
                var val;
                if (self.defaultfreight()) {
                    var weight = 0;
                    ko.utils.arrayForEach(self.OrderItems(), (item: DOrderItem) => {
                        weight += item.freightweight();
                    });

                    if (IsValid(self.freightcharge()))
                        val = weight * self.freightcharge();
                    else
                        val = 0;
                } else {
                    val = Globalize.parseFloat(self.Override_freight().toString());
                }

                return RoundNumber(val, OrderViewModel.DecimalPlaces);
            },
            write: (value: any) => {
                // Do Nothing
                self.Override_freight(value);
            },
            owner: self,
            deferEvaluation: true
        });

        self.fasttrackcharge = ko.computed({
            read: () => {
                var val = 0;

                if (self.fasttrack()) {
                    val = self.sales() * self.fasttrackpercentage() / 100;
                }

                return RoundNumber(val, OrderViewModel.DecimalPlaces);
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.SubTotal = ko.computed({
            read: () => {
                var val = self.sales() - self.discountamount() + self.steelsurcharge() + self.fasttrackcharge();

                // Trigger the GST field to recalculate.
                self.TaxItems.valueHasMutated();

                return RoundNumber(val, OrderViewModel.DecimalPlaces);
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.gst = ko.computed({
            read: () => {
                var val: number = 0;

                ko.utils.arrayForEach(self.TaxItems(), (tax: DOrderTaxes) => { val += tax.taxamount(); });

                return val;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.total = ko.computed({
            read: () => {
                var val = self.SubTotal() + self.freight() + self.gst();
                return RoundNumber(val, OrderViewModel.DecimalPlaces);
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.TempAddressKey.subscribe(newValue => {
            var addr = $.grep(self.Customer().Addresses(), item => {
                if (item.name() === newValue)
                    return true;
                return false;
            });

            if (addr.length == 1) {
                self.TempAddress1(addr[0].address1());
                self.TempAddress2(addr[0].address2());
                self.TempAddress3(addr[0].address3());
                self.TempAddress4(addr[0].address4());
                self.TempAddressPostalCode(addr[0].postalcode());
            }
        });

        self.TempAddress = ko.computed({
            read: () => (IsValid(self.TempAddress1()) ? self.TempAddress1() : '') + "\n" +
            (IsValid(self.TempAddress2()) ? self.TempAddress2() : '') + "\n" +
            (IsValid(self.TempAddress3()) ? self.TempAddress3() : '') + "\n" +
            (IsValid(self.TempAddress4()) ? self.TempAddress4() : '')
        });

        self.FirstShipDate = ko.computed<Date>({
            read: () => {
                if (!IsValid(self.shopdate()))
                    return self.orderdate();

                var hasNitride: boolean = false;

                var tmp: DOrderItem = ko.utils.arrayFirst(self.OrderItems(), (item: DOrderItem) => item.hasnitride());

                if (IsValid(tmp) && tmp.hasnitride())
                    hasNitride = true;

                if (hasNitride) {
                    var tmpDate = JSON.parse(ko.toJSON(self.shopdate()));

                    if (!(tmpDate instanceof Date)) {
                        tmpDate = Globalize.parseDate(<any>(tmpDate).toString().substr(0, 10), 'yyyy-MM-dd');
                    }

                    tmpDate.setDate(tmpDate.getDate() + 1);
                    return tmpDate;
                }

                return self.shopdate();
            },
            owner: self,
            deferEvaluation: true
        });

        if (IsValid(self.orderdate()))
            self.OriginalOrderDate(Globalize.parseDate((<any>self.orderdate()).toString().substr(0, 19), "yyyy-MM-ddTHH:mm:ss"));

        self.orderdate = ko.computed({
            read: () => {
                var cur = self.TempOrderDate(),
                    def = self.OriginalOrderDate();

                if (cur instanceof Date && def instanceof Date) {
                    if (cur.getFullYear() !== def.getFullYear() ||
                        cur.getMonth() !== def.getMonth() ||
                        cur.getDate() !== def.getDate())
                        return self.TempOrderDate();
                }

                return self.OriginalOrderDate();
            },
            write: (value: Date) => {
                self.TempOrderDate(value);
                // self.OrderDate(value);
            },
            owner: self,
            deferEvaluation: true
        });

        self.countasorder = ko.computed({
            read: () => {
                var rtnVal: boolean = false;

                if (IsValid(self.OrderItems())) {
                    var tmp: DOrderItem = ko.utils.arrayFirst(self.OrderItems(), (item: DOrderItem) => item.prefix() !== "MI" && item.baseprice() !== 0);

                    if (IsValid(tmp) && tmp.prefix() != "MI" && tmp.baseprice() !== 0)
                        rtnVal = true;
                }

                // Don't count Credit Notes
                if (IsValid(self.total()) && self.total() < 0)
                    rtnVal = false;

                return rtnVal;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.freightweight = ko.computed({
            read: () => {
                var rtnVal = 0;

                ko.utils.arrayForEach(self.OrderItems(), (item: DOrderItem) => {
                    rtnVal += item.freightweight();
                });

                return rtnVal;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.roughweight = ko.computed({
            read: () => {
                var rtnVal = 0;

                ko.utils.arrayForEach(self.OrderItems(), (item: DOrderItem) => {
                    rtnVal += item.steelsurchargeweight();
                });

                return rtnVal;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.nitrideweight = ko.computed({
            read: () => {
                var rtnVal = 0;

                ko.utils.arrayForEach(self.OrderItems(), (item: DOrderItem) => {
                    if (item.hasnitride())
                        rtnVal += item.nitrideweight();
                });

                return rtnVal;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        //#endregion

        if (self.invoicedate() == undefined || self.invoicedate() == null)
            self.CanClearInvoice(true);

        self.InitValidation();
        self.errors = ko.validation.group(self);

        // This has to be declared after errors has been setup
        ko.computed({
            read: () => { return 1; },
            write: () => {},
            owner: self,
            deferEvaluation: true
        });
        self.IsValid = ko.computed({
            read: () => {
                return self.errors().length === 0;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        setTimeout(() => {
            var errorIcon = $("#erroricon");

            self.IsValid.subscribe((val: boolean) => {
                if (!val) {
                    var errorList = "";
                    ko.utils.arrayForEach(self.errors(), (error: any) => {
                        errorList += "<li>" + error + "</li>";
                    });

                    errorList = "<ul>" + errorList + "</ul>";

                    errorIcon.data('popover').options.content = errorList;

                    errorIcon.popover("enable");
                    errorIcon.stop(true);

                    for (var i = 0; i < 3; i++) {
                        errorIcon.animate({ opacity: "1" }, 1000);
                        errorIcon.animate({ opacity: "-=0.5" }, 1000);
                    }

                    errorIcon.animate({ opacity: "1" }, 1000);
                } else {
                    errorIcon.popover("disable").popover("hide");

                    errorIcon.stop(true);
                    errorIcon.animate({ opacity: "0" }, 1000);
                }
            });
        }, 10);
    }

/// TODO: This is the end of what needs to be changed.

    InitValidation(): void {
        var self = this;

        //#region Dates
        self.noshipbefore.extend({
            validation: {
                validator: (val: any) => {
                    var hasNoShipBefore = IsValid(self.noshipbefore());

                    if (!hasNoShipBefore)
                        return true;

                    return (val - <any>self.FirstShipDate()) >= 0;
                },
                message: "No ship before must be greater than shop date."
            }
        });

        self.expshipdate.extend({
            validation: {
                validator: (val: any) => {
                    var firstShipDate = self.FirstShipDate();

                    if (IsValid(self.noshipbefore(), true))
                        firstShipDate = self.noshipbefore();

                    return (val - <any>firstShipDate) >= 0;
                },
                message: "Exp. ship date must be greater than shop date."
            }
        });

        self.expreceiveddate.extend({
            validation: {
                validator: (val: any) => {
                    var hasNoShipBefore = IsValid(self.noshipbefore());
                    return (val - <any>self.expshipdate()) >= 0 && (hasNoShipBefore && (val - <any>self.noshipbefore()) >= 0 || !hasNoShipBefore);
                },
                message: "Exp. received date must be greater than exp. ship date."
            }
        });

        self.invoicedate.extend({
            validation: [
                {
                    validator: (val: any) => {
                        if (val === undefined || val === null)
                            return true;

                        var tmpOrderDate = new Date(self.orderdate().getTime());
                        tmpOrderDate.setHours(0, 0, 0, 0);

                        return (val - <any>tmpOrderDate) >= 0;
                    },
                    message: "Invoice date must be greater than order date."
                },
                {
                    validator: (val: any) => {
                        if (!self.canceled() && !IsValid(self.shipdate(), true) && IsValid(val, true))
                            return false;
                        return true;
                    },
                    message: "Ship date cannot be empty when invoicing."
                }
            ]
        });
        //#endregion

        self.OrderItems.extend({
            minLength: {
                params: 1,
                message: "Must have at least 1 order item."
            }
        });

        self.total.extend({
            validation: {
                validator: (val: number) => IsValid(val) && !isNaN(val),
                message: "Total must be a number"
            }
        });

        self.shipwithorder.extend({
            validation: [
                {
                    validator: (val: number) => {
                        if (val === undefined || val === null || <any>val == '' || validOrdernumber.test(<any>val))
                            return true;

                        return false;
                    },
                    message: "Invalid Order"
                },
                {
                    async: true,
                    validator: (val: number, otherVal, callback) => {
                        if (val === undefined || val === null || <any>val == '' || !validOrdernumber.test(val.toString()) || IsValid(self.shipdate())) {
                            setTimeout(() => { callback({ "isValid": true }); }, 10);
                            return;
                        }

                        $.get('/api/OrderEntry/ValidateShipWithOrder', { customercode: self.customercode(), shipwithorder: val })
                            .done((response) => {
                                if (response.isValid) {
                                    $("#expshipdate").datepicker("setDate", Globalize.parseDate(response.expshipdate, 'yyyy-MM-dd'));
                                    $("#exprecvdate").datepicker("setDate", Globalize.parseDate(response.expreceiveddate, 'yyyy-MM-dd'));
                                    $("#shipvia").val(response.shipvia);
                                }
                                callback(response);
                            })
                            .fail((xhr) => {
                                callback({ isValid: false, message: xhr.responseText });
                            });
                    },
                    message: "Temp Message"
                }
            ]
        });

        var testShipWith = () => {
            var shipWithHasValue = IsValid(self.shipwithorder(), true) && self.shipwithorder() !== 0;
            if (!self.FieldsEnabled() || shipWithHasValue && self.shipwithorder.isValid() && !IsValid(self.shipdate())) {
                $("#expshipdate").attr("disabled", "disabled");
                $("#exprecvdate").attr("disabled", "disabled");
                $("#shipvia").attr("disabled", "disabled");
            } else {
                $("#expshipdate").removeAttr("disabled").val("");
                $("#exprecvdate").removeAttr("disabled").val("");
                $("#shipvia").removeAttr("disabled").val("");
            }
        };
        self.shipwithorder.subscribe(() => {
            testShipWith();
        });

        self.shipwithorder.isValid.subscribe(() => {
            testShipWith();
        });

        self.shipdate.subscribe(() => {
            testShipWith();
        });

        // If the Order is currently being saved, then disable the Save feature.
        self.CurrentlySaving.extend({
            equal: false
        });
    }

    GetScheduleLine(key: string, count: number, limit: number): string {
        var percentDone = count / limit;
        var color;

        if (percentDone >= .95)
            color = "F00";
        else if (percentDone >= .75)
            color = "C00";
        else if (percentDone >= .5)
            color = "900";
        else
            color = "000";

        return '<div style="display: inline; color: #' + color + '">' + key + ':' + count + '</div> ';
    }

    UpdateLineNumbers(): void {
        var self = this;

        setTimeout(() => {
            // Re-Number Lines
            var i: number = 1;
            $("#orderinfo > tbody.dragable").each(function() {
                var context = ko.contextFor(this);
                self.OrderItems()[context.$index()].line(i++);
            });
        }, 0);
    }

    toJSON() {
        var self = this;

        var validFields: string[] = [
            'ordernumber', 'customercode', 'lastemployeenumber', 'orderdate', 'invoicedate', 'shopdate', 'noshipbefore',
            'expshipdate', 'expreceiveddate', 'shipdate', 'shipwithorder', 'invoicenumber', 'customerpo', 'customerreq', 'note',
            'defaultfreight', 'defaultgst', 'sales', 'freight', 'gst', 'parts', 'baddress1', 'baddress2', 'baddress3', 'baddress4', 'bpostalcode',
            'saddress1', 'saddress2', 'saddress3', 'saddress4', 'spostalcode', 'stock', 'fasttrack', 'priority', 'shipvia', 'defaultnitride', 'nitride',
            'ncr', 'wr', 'defaultsteelsurcharge', 'steelsurcharge', 'steelrate', 'combineprices', 'shelf', 'zeroprice', 'shelforder', 'cor', 'ncrs',
            'nitrideweight', 'nitridecharge', 'roughweight', 'freightcharge', 'fasttrackcharge', 'fasttrackpercentage', 'total', 'subtotal', 'steel',
            'shipvendorid', 'shippingvendor', 'trackingnumber', 'discountpercentage', 'defaultdiscountamount', 'discountamount', 'discountontotal',
            'hasnitridecomputedline', 'importedorder', 'DesignType', 'canceled', 'OrderItems', 'OrderCopies', 'freightweight', 'countasorder', 'onhold',
            'CheckList', 'pricelistmultiplier', 'currencymultiplier', 'TaxItems'
        ];

        var copy = ko.toJS(self);

        for (var prop in copy) {
            if (copy.hasOwnProperty(prop)) {
                if ($.inArray(prop, validFields) < 0) {
                    delete copy[prop];
                } else if (copy[prop] instanceof Date) {
                    // Fix up the date so that times don't get offset on the server
                    copy[prop] = moment(copy[prop]).format("YYYY-MM-DD HH:mm:ss");
                }
            }
        }

        return copy;
    }
}
// ReSharper restore InconsistentNaming