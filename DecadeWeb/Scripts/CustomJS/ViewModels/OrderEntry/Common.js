/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\..\typings\bootbox\bootbox.d.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
// Open an Order for Editing
function GoToEditOrder(order, displayOnNone) {
    if (order === void 0) { order = -1; }
    if (displayOnNone === void 0) { displayOnNone = false; }
    if (order != null && order != -1) {
        window.location.href = "/OrderEntry/Edit/" + order;
        return true;
    }
    else {
        if (displayOnNone) {
            window.location.href = "/OrderEntry/Add/";
            return true;
        }
        else {
            return false;
        }
    }
}
function GoToTrackOrder(order) {
    if (validOrdernumber.test(String(order))) {
        window.open("/Tracking/" + order, "Tracking", "");
        return true;
    }
    else {
        return false;
    }
}
function GoToSchedule(dietype, date) {
    if (typeof (date) != typeof (new Date())) {
        var tmp = date;
        date = Globalize.parseDate(tmp);
    }
    window.open("/Schedule/Admin/" + dietype + "/Die/" + Globalize.format(date, "yyyy-MM-dd"), "Schedule", "");
}
function GetSONumber(promptMessage, fn) {
    bootbox.prompt(promptMessage, fn);
}
function GetCustomerCode(promptMessage, fn) {
    bootbox.prompt(promptMessage, fn);
}
//# sourceMappingURL=Common.js.map