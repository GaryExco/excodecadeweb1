//#region Typing References
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
//#endregion
// ReSharper disable InconsistentNaming
var PressInfo = (function () {
    function PressInfo(options) {
        this.CustNum = ko.observable();
        this.PressName = ko.observable();
        this.PressNameCAD = ko.observable();
        if (options) {
            ko.mapping.fromJS(options.data, {}, self);
        }
    }
    return PressInfo;
}());
// ReSharper restore InconsistentNaming 
//# sourceMappingURL=PressInfo.js.map