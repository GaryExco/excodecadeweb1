//#region Typing References
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
//#endregion


// ReSharper disable InconsistentNaming
class PressInfo {
    public CustNum: KnockoutObservable<number> = ko.observable<number>();
    public PressName: KnockoutObservable<string> = ko.observable<string>();
    public PressNameCAD: KnockoutObservable<string> = ko.observable<string>();

    constructor(options?: any) {
        if (options) {
            ko.mapping.fromJS(options.data, {}, self);
        }
    }
}
// ReSharper restore InconsistentNaming