//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// ReSharper disable InconsistentNaming
var PriceListItemCharge = (function (_super) {
    __extends(PriceListItemCharge, _super);
    function PriceListItemCharge(options) {
        var _this = _super.call(this) || this;
        _this.IsChecked = ko.observable(false);
        _this.Quantity = ko.observable(0);
        _this.ChargeValue = ko.observable(0);
        _this.ChargeType = ko.observable("Fixed");
        var self = _this;
        if (options) {
            ko.mapping.fromJS(options.data, {}, self);
        }
        self.IsChecked.subscribe(function (newValue) {
            if (newValue)
                self.Quantity(1);
            else
                self.Quantity(0);
        });
        self.ChargeText = ko.computed({
            read: function () {
                if (self.chargepercentage() != 0) {
                    return self.chargepercentage() + "%";
                }
                var culture = ko.toJS(Globalize.culture());
                culture.numberFormat.currency.symbol = VM.Order.Symbol();
                var val = Globalize.format(self.chargeprice(), "c", culture);
                return val;
            },
            write: function () {
                // Do Nothing
            },
            owner: self
        });
        self.ChargePercentage = ko.computed({
            read: function () {
                if (self.ChargeType().toLowerCase() === "percent") {
                    return self.ChargeValue();
                }
                else {
                    return 0;
                }
            },
            write: function () {
                // Do Nothing
            },
            owner: self
        });
        self.ChargePrice = ko.computed({
            read: function () {
                if (self.ChargeType().toLowerCase() === "percent") {
                    return 0;
                }
                else {
                    return self.ChargeValue();
                }
            },
            write: function () {
                // Do Nothing
            },
            owner: self
        });
        return _this;
    }
    return PriceListItemCharge;
}(d_pricelistitemcharges));
// ReSharper restore InconsistentNaming 
//# sourceMappingURL=PriceListItemCharge.js.map