//#region Typing References
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
//#endregion


// ReSharper disable InconsistentNaming
class DCustomerAddress extends d_customeraddress {
    constructor(options?: any) {
        super();

        var self = this;

        if (IsValid(options)) {
            ko.mapping.fromJS(options.data, {}, self);
        }
    }
}
// ReSharper restore InconsistentNaming