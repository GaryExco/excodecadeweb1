//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion


// ReSharper disable InconsistentNaming
class PriceListItemCharge extends d_pricelistitemcharges {
    public IsChecked: KnockoutObservable<boolean> = ko.observable(false);
    public Quantity: KnockoutObservable<number> = ko.observable(0);
    public ChargeText: KnockoutComputed<string>;

    public ChargePercentage: KnockoutComputed<number>;
    public ChargePrice: KnockoutComputed<number>;
    public ChargeValue: KnockoutObservable<number> = ko.observable(0);
    public ChargeType: KnockoutObservable<string> = ko.observable("Fixed");

    constructor(options?: any) {
        super();

        var self = this;

        if (options) {
            ko.mapping.fromJS(options.data, {}, self);
        }

        self.IsChecked.subscribe(newValue => {
            if (newValue)
                self.Quantity(1);
            else
                self.Quantity(0);
        });

        self.ChargeText = ko.computed({
            read: () => {
                if (self.chargepercentage() != 0) {
                    return self.chargepercentage() + "%";
                }

                var culture = ko.toJS(Globalize.culture());
                culture.numberFormat.currency.symbol = VM.Order.Symbol();

                var val: string = Globalize.format(self.chargeprice(), "c", culture);

                return val;
            },
            write: () => {
                // Do Nothing
            },
            owner: self
        });

        self.ChargePercentage = ko.computed({
            read: () => {
                if (self.ChargeType().toLowerCase() === "percent") {
                    return self.ChargeValue();
                } else {
                    return 0;
                }
            },
            write: () => {
                // Do Nothing
            },
            owner: self
        });

        self.ChargePrice = ko.computed({
            read: () => {
                if (self.ChargeType().toLowerCase() === "percent") {
                    return 0;
                } else {
                    return self.ChargeValue();
                }
            },
            write: () => {
                // Do Nothing
            },
            owner: self
        });
    }
}
// ReSharper restore InconsistentNaming