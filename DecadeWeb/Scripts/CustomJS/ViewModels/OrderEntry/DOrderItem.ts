//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion

// ReSharper disable InconsistentNaming
class DOrderItem extends d_orderitem {
    public DieType: KnockoutComputed<string>;
    public Diameter: KnockoutObservable<number> = ko.observable<number>();
    public Thickness: KnockoutObservable<number> = ko.observable<number>();
    public Cavities: KnockoutObservable<number> = ko.observable<number>();
    public InnerDiameter: KnockoutObservable<number> = ko.observable<number>();
    public CavID: KnockoutObservable<number> = ko.observable<number>();

    public SubTotal: KnockoutComputed<number>;
    public ExtendedPrice: KnockoutComputed<number>;

    public Charges: KnockoutObservableArray<DItemCharge> = ko.observableArray<DItemCharge>();
    public _order: DOrder;

    // Charges from the Server
    public ChargeList: KnockoutObservableArray<PriceListItemCharge> = ko.observableArray<PriceListItemCharge>();

    // Search Variables
    public _ItemsMatchingQuery: KnockoutComputed<PriceListItemCharge[]>;
    public _ItemsToDisplay: KnockoutComputed<PriceListItemCharge[]>;
    public _ItemsPerPageOptions: KnockoutObservableArray<number> = ko.observableArray([10, 25, 50, 75, 100]);
    public _ItemsPerPage: KnockoutObservable<number> = ko.observable(10);
    public _FirstPage: KnockoutObservable<number> = ko.observable(1);
    public _CurrentPage: KnockoutObservable<number> = ko.observable(1);
    public _LastPage: KnockoutComputed<number>;
    public _SearchText: KnockoutObservable<string> = ko.observable("");

    // Price Lookup Variables
    public _LoadPrice: KnockoutObservable<boolean> = ko.observable(false);
    private LookupPriceTimeout = null;

    public CanSave: KnockoutComputed<boolean>;

    constructor(order: DOrder, options?: any) {
        super();

        var self = this;
        self._order = order;

        self.qty(1);
        self.roughweight(0);

        if (options) {
            self._LoadPrice(false);
            ko.mapping.fromJS(options.data, { 'Charges': { create: chargeOptions => new DItemCharge(this, chargeOptions) } }, self);

            if (self.prefix() === "RI")
                self.CavID(self.InnerDiameter());
            else
                self.CavID(self.Cavities());
        }

        self.InitSubscriptions();

        self.DieType = ko.computed({
            read: () => {
                if (self.prefix() == undefined || self.prefix() == null || self.prefix() === "")
                    return "";

                var result: any = $.grep(OrderViewModel._DieTypes, (e: any) => e.Prefix === self.prefix());
                var tmp: string;

                if (result.length == 1)
                    tmp = result[0].Name;
                else
                    tmp = "Misc";

                return tmp;
            },
            write: (value: string) => {
                var result: any = $.grep(OrderViewModel._DieTypes, (e: any) => e.Name === value);

                if (result.length == 1) {
                    self.prefix(result[0].Prefix);

                    // Change the suffix to an empty string since it doesn't allow nulls
                    if (result[0].Prefix === "MI")
                        self.suffix("");
                }
            },
            owner: self,
            deferEvaluation: true
        });

        self.CanSave = ko.computed({
            read: () => {
                if (!IsValid(self.baseprice()) || isNaN(self.baseprice()))
                    return false;

                if (IsValid(self.prefix()) && self.prefix() == "MI")
                    return true;

                if (IsValid(self.prefix()) && IsValid(self.Diameter()) && IsValid(self.Thickness()) && IsValid(self.CavID())) {
                    if (!isNaN(self.Diameter()) && !isNaN(self.Thickness()) && !isNaN(self.CavID())) {
                        return true;
                    }
                }

                return false;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.prefix.subscribe(newValue => {
            if (newValue === "MI") {
                self.freightweight(0);
                self.nitrideweight(0);
                self.steelsurchargeweight(0);
            }

            self.LoadPriceInfo();
        });

        self.Diameter.subscribe(() => {
            self.LoadPriceInfo();
        });

        self.Thickness.subscribe(() => {
            self.LoadPriceInfo();
        });

        self.CavID.subscribe(newValue => {
            if (self.prefix() === "RI")
                self.InnerDiameter(newValue);
            else
                self.Cavities(newValue);
            self.LoadPriceInfo();
        });

        //#region Search Helpers
        self._ItemsMatchingQuery = ko.computed({
            read: () => {
                var rtnVal: PriceListItemCharge[] = self.ChargeList().slice(0, self.ChargeList().length);

                if (self._SearchText() != '') {
                    rtnVal = $.grep(rtnVal, value => value.chargename().toLowerCase().indexOf(self._SearchText().toLowerCase()) > -1);
                }

                return rtnVal;
            },
            write: () => {
                // Do Nothing
            }
        });

        self._ItemsToDisplay = ko.computed({
            read: () => {
                var rtnVal: PriceListItemCharge[] = self._ItemsMatchingQuery();

                var startIndex = (self._CurrentPage() - 1) * self._ItemsPerPage();
                rtnVal = rtnVal.slice(startIndex, startIndex + self._ItemsPerPage());

                return rtnVal;
            },
            write: () => {
                // Do Nothing
            }
        });

        self._LastPage = ko.computed({
            read: () => Math.ceil(self._ItemsMatchingQuery().length / self._ItemsPerPage()),
            write: () => {
                // Do Nothing
            }
        });

        self._SearchText.subscribe(() => {
            self._CurrentPage(1);
        });

        self._ItemsPerPage.subscribe(() => {
            self._CurrentPage(1);
        });
        //#endregion
    }

    InitSubscriptions(): void {
        var self = this;

        self.prefix.subscribe(() => {
            self.LoadCharges();
        });

        self.SubTotal = ko.computed({
            read: () => {
                var tmpTotal = 0;

                ko.utils.arrayForEach(self.Charges(), (item: DItemCharge) => {
                    if (item.hasOwnProperty('ExtendedPrice'))
                        tmpTotal += item.ExtendedPrice();
                });

                return RoundNumber(self.baseprice() + tmpTotal, OrderViewModel.DecimalPlaces);
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.ExtendedPrice = ko.computed({
            read: () => {
                var val = self.baseprice() * self.qty();

                ko.utils.arrayForEach(self.Charges(), (charge: DItemCharge) => {
                    if (charge.hasOwnProperty('ExtendedPrice'))
                        val += charge.ExtendedPrice();
                });

                // self.price(RoundNumber(val, OrderViewModel.DecimalPlaces));

                return RoundNumber(val, OrderViewModel.DecimalPlaces);
            },
            write: () => {
                // Do Nothing
                // self.price(RoundNumber(value, OrderViewModel.DecimalPlaces));
            },
            owner: self,
            deferEvaluation: true
        });

        self.price = ko.computed({
            read: () => {
                return self.baseprice();
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
    }

    LoadCharges(): void {
        var self = this;

        self.ChargeList([]);

        if (self.prefix() == undefined || self.prefix() == null || self.prefix() == '')
            return;

        $.get('/api/OrderEntry/GetCharges',
        {
            'plcode': self._order.Customer().pricelist(),
            'prefix': self.prefix(),
            'pricelistMultiplier': self._order.pricelistmultiplier(),
            'currencyMultiplier': self._order.currencymultiplier()
        }).done(data => {
            ko.mapping.fromJS(data, { create: options => new PriceListItemCharge(options) }, self.ChargeList);

            for (var i = 0; i < self.Charges().length; i++) {
                if (self.Charges()[i].chargetype().toLowerCase() == "custom") {
                    // Custom Charge
                    var charge: DItemCharge = self.Charges()[i];
                    var tmpCharge: PriceListItemCharge = new PriceListItemCharge();
                    tmpCharge.chargename(charge.chargename());
                    tmpCharge.chargepercentage(charge.chargepercentage());
                    tmpCharge.chargeprice(charge.chargeprice());
                    tmpCharge.chargetype(charge.chargetype());
                    tmpCharge.Quantity(charge.qty());
                    tmpCharge.maxqty(9999);

                    self.ChargeList().push(tmpCharge);
                } else {
                    // Non-Custom Charge
                    var result: any = $.grep(self.ChargeList(), (e: PriceListItemCharge) => e.chargetype() == self.Charges()[i].chargetype() && e.chargename() == self.Charges()[i].chargename() && e.chargeprice() == self.Charges()[i].chargeprice());

                    if (result.length == 1) {
                        if (result[0].maxqty() === 1) {
                            result[0].IsChecked(self.Charges()[i].qty() > 0);
                        }
                        result[0].Quantity(self.Charges()[i].qty());
                    }
                }
            }

            // Only Reload Charges if a Custom charge exists
            // if (ChargeListChanged)
            self.ChargeList.valueHasMutated();
        });
    }

    LoadPriceInfo(): void {
        var self = this;

        if (!self._LoadPrice())
            return;

        if (!IsValid(self.prefix(), true)
            || !IsValid(self.Diameter()) || isNaN(self.Diameter())
            || !IsValid(self.Thickness()) || isNaN(self.Thickness())
            || !IsValid(self.CavID()) || isNaN(self.CavID()))
            return;

        if (IsValid(self.LookupPriceTimeout)) {
            clearTimeout(self.LookupPriceTimeout);
        }

        self.LookupPriceTimeout = setTimeout(() => {
            $.get('/api/OrderEntry/GetPriceInfo',
            {
                'customercode': self._order.customercode(),
                'plcode': self._order.Customer().pricelist(),
                'prefix': self.prefix(),
                'diameter': self.Diameter(),
                'thickness': self.Thickness(),
                'cavid': self.CavID(),
                'pricelistMultiplier': self._order.pricelistmultiplier(),
                'currencyMultiplier': self._order.currencymultiplier()
            }).done(data => {
                if (!data.isValid)
                    return;

                if (!data.PriceFound) {
                    DisplayError("error", "No Price Found", "No price was found");
                    data.price = 0;
                } else if (data.price === 0) {
                    DisplayError("error", "No Price Found", "Price found was $0.00");
                }

                if (data.UsedEstimated) {
                    DisplayError("warning", "Estimated Pricing", "Used estimated pricing");
                }

                self.suffix(data.suffix);
                self.description(data.Description);
                self.baseprice(data.price);
                self.freightweight(data.freightWeight);
                self.nitrideweight(data.nitrideWeight);
                self.steelsurchargeweight(data.steelSurchargeWeight);
                self.tariffcode(data.TariffCode);

                if (!IsValid(self.suffix()))
                    self.suffix("");
            });
        }, 250);
    }

    // Add Functionality
    GetCustomWeight(): void {

    }

    UpdateSubTotal(): void {
        var self = this;
        var tmpTotal = 0;

        ko.utils.arrayForEach(self.Charges(), (item: DItemCharge) => {
            tmpTotal += item.ExtendedPrice();
        });

        self.SubTotal(self.baseprice() + tmpTotal);
    }

    UpdateExtended(): void {
        var self = this;

        self.ExtendedPrice(self.SubTotal() * self.qty());
    }

    //#region Pagination
    NextPage(): void {
        var self = this;

        var curPage = self._CurrentPage();

        if (curPage < self._LastPage())
            self._CurrentPage(curPage + 1);
        else
            self._CurrentPage(self._LastPage());
    }

    GoToPage(parent, data): void {
        parent._CurrentPage(data);
    }

    LastPage(): void {
        var self = this;

        var curPage = self._CurrentPage();

        if (curPage > 1)
            self._CurrentPage(curPage - 1);
        else
            self._CurrentPage(1);
    }

    UpdateDisplay(): void {
        var self = this;

        var rtnVal: PriceListItemCharge[] = self.ChargeList().slice(0, self.ChargeList().length - 1);

        if (self._SearchText() != '') {
            rtnVal = $.grep(rtnVal, value => value.chargename().toLowerCase().indexOf(self._SearchText().toLowerCase()) > -1);
        }

        var startIndex = (self._CurrentPage() - 1) * self._ItemsPerPage();
        rtnVal = rtnVal.slice(startIndex, startIndex + self._ItemsPerPage());

        self._ItemsToDisplay(rtnVal);
    }

//#endregion

    toJSON() {
        var self = this;

        var validFields: string[] = [
            'ordernumber', 'line', 'prefix', 'suffix', 'qty', 'description', 'location', 'dienumber', 'note',
            'price', 'chargenitride', 'steelcost', 'roughweight', 'freightweight', 'nitrideweight', 'steelsurchargeweight', 'baseprice', 'hasnitride',
            'Charges', 'tariffcode'
        ];

        var copy = ko.toJS(self);

        for (var prop in copy) {
            if (copy.hasOwnProperty(prop)) {
                if ($.inArray(prop, validFields) < 0) {
                    delete copy[prop];
                }
            }
        }

        return copy;
    }
}
// ReSharper restore InconsistentNaming