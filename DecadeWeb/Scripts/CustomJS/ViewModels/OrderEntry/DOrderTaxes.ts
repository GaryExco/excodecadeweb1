//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion


// ReSharper disable InconsistentNaming
class DOrderTaxes extends d_ordertaxes {
    public Order: DOrder;

    constructor(order: DOrder, options?: any) {
        super();

        var self = this;
        self.Order = order;

        if (IsValid(options)) {

            ko.mapping.fromJS(options.data, {}, self);
        }

        self.taxamount = ko.computed({
            read: () => {
                var taxableTotal = self.Order.SubTotal();

                if (order.Customer().taxOnFreight())
                    taxableTotal += self.Order.freight();

                return RoundNumber(taxableTotal * self.rate() / 100, OrderViewModel.DecimalPlaces);
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
    }

    toJSON() {
        var self = this;

        var validFields: string[] = ['ordernumber', 'taxline', 'taxamount', 'taxtype', 'rate'];

        var copy = ko.toJS(self);

        for (var prop in copy) {
            if (copy.hasOwnProperty(prop)) {
                if ($.inArray(prop, validFields) < 0) {
                    delete copy[prop];
                }
            }
        }

        return copy;
    }
}
// ReSharper restore InconsistentNaming