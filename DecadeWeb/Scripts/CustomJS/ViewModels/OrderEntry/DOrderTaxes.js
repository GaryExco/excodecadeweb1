//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// ReSharper disable InconsistentNaming
var DOrderTaxes = (function (_super) {
    __extends(DOrderTaxes, _super);
    function DOrderTaxes(order, options) {
        var _this = _super.call(this) || this;
        var self = _this;
        self.Order = order;
        if (IsValid(options)) {
            ko.mapping.fromJS(options.data, {}, self);
        }
        self.taxamount = ko.computed({
            read: function () {
                var taxableTotal = self.Order.SubTotal();
                if (order.Customer().taxOnFreight())
                    taxableTotal += self.Order.freight();
                return RoundNumber(taxableTotal * self.rate() / 100, OrderViewModel.DecimalPlaces);
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        return _this;
    }
    DOrderTaxes.prototype.toJSON = function () {
        var self = this;
        var validFields = ['ordernumber', 'taxline', 'taxamount', 'taxtype', 'rate'];
        var copy = ko.toJS(self);
        for (var prop in copy) {
            if (copy.hasOwnProperty(prop)) {
                if ($.inArray(prop, validFields) < 0) {
                    delete copy[prop];
                }
            }
        }
        return copy;
    };
    return DOrderTaxes;
}(d_ordertaxes));
// ReSharper restore InconsistentNaming 
//# sourceMappingURL=DOrderTaxes.js.map