//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// ReSharper disable InconsistentNaming
var DItemCharge = (function (_super) {
    __extends(DItemCharge, _super);
    function DItemCharge(item, options) {
        var _this = _super.call(this) || this;
        var self = _this;
        self._item = item;
        if (options) {
            ko.mapping.fromJS(options.data, { 'ignore': ['_item'] }, self);
        }
        self.IsNitride = ko.computed({
            read: function () {
                if (IsValid(self.chargetype()) && self.chargetype().toLowerCase() === "nitride")
                    return true;
                return false;
            },
            write: function () {
                // Do Nothing
            },
            owner: self
        });
        self.IsNitridePlateOnly = ko.computed({
            read: function () {
                if (self.IsNitride() && IsValid(self.chargename()) && self.chargename().toLowerCase().indexOf("plate only") !== -1)
                    return true;
                return false;
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        self.Quantity = ko.computed({
            read: function () {
                if (self.IsNitride())
                    return self._item.nitrideweight();
                return self.qty();
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        self.ChargePrice = ko.computed({
            read: function () {
                var isNitride = self.IsNitride(), nitrideCharge = self._item._order.nitridecharge(), // VM.Order.nitridecharge(),
                normalCharge = self.chargeprice();
                if (self.IsNitridePlateOnly()) {
                    // Only charge 40% if die plate only
                    nitrideCharge *= 0.40;
                }
                return isNitride ? nitrideCharge : normalCharge;
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        self.Price = ko.computed({
            read: function () {
                var val;
                var tmpDecimalPlaces = OrderViewModel.DecimalPlaces;
                if (self.IsNitride()) {
                    val = self.Quantity() * self.ChargePrice();
                    // Check sanity of nitride to be within customer nitride range
                    if (val < self._item._order.Customer().minnitridecost())
                        val = self._item._order.Customer().minnitridecost();
                    else if (val > self._item._order.Customer().maxnitridecost())
                        val = self._item._order.Customer().maxnitridecost();
                    if (self.IsNitridePlateOnly()) {
                        // Round to nearest dollar value
                        tmpDecimalPlaces = 0;
                    }
                }
                else if (self.chargepercentage() !== 0)
                    val = self._item.baseprice() * (self.chargepercentage() / 100) * self.Quantity();
                else
                    val = self.ChargePrice() * self.Quantity();
                return RoundNumber(val, tmpDecimalPlaces);
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        self.ChargeText = ko.computed({
            read: function () {
                var val, format, suffix = "";
                if (self.IsNitride()) {
                    val = self.ChargePrice();
                    format = "c";
                    suffix = "/lb.";
                }
                else if (self.chargepercentage() != 0) {
                    val = self.chargepercentage() / 100;
                    format = "p";
                }
                else {
                    val = self.ChargePrice();
                    format = "c";
                }
                var culture = ko.toJS(Globalize.culture());
                culture.numberFormat.currency.symbol = self._item._order.Symbol();
                if (IsValid(val))
                    return Globalize.format(val, format, culture) + suffix;
                return "";
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        self.ExtendedPrice = ko.computed({
            read: function () {
                var rtnVal = self.Price();
                return RoundNumber(rtnVal, OrderViewModel.DecimalPlaces);
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        return _this;
    }
    DItemCharge.prototype.toJSON = function () {
        var self = this;
        var validFields = ['ordernumber', 'line', 'chargeline', 'chargetype', 'chargename', 'chargeprice', 'chargepercentage', 'qty', 'price'];
        var copy = ko.toJS(self);
        for (var prop in copy) {
            if (copy.hasOwnProperty(prop)) {
                if ($.inArray(prop, validFields) < 0) {
                    delete copy[prop];
                }
            }
        }
        return copy;
    };
    return DItemCharge;
}(d_orderitemcharges));
// ReSharper restore InconsistentNaming 
//# sourceMappingURL=DItemCharge.js.map