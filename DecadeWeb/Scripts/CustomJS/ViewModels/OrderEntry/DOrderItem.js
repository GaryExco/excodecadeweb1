//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// ReSharper disable InconsistentNaming
var DOrderItem = (function (_super) {
    __extends(DOrderItem, _super);
    function DOrderItem(order, options) {
        var _this = _super.call(this) || this;
        _this.Diameter = ko.observable();
        _this.Thickness = ko.observable();
        _this.Cavities = ko.observable();
        _this.InnerDiameter = ko.observable();
        _this.CavID = ko.observable();
        _this.Charges = ko.observableArray();
        // Charges from the Server
        _this.ChargeList = ko.observableArray();
        _this._ItemsPerPageOptions = ko.observableArray([10, 25, 50, 75, 100]);
        _this._ItemsPerPage = ko.observable(10);
        _this._FirstPage = ko.observable(1);
        _this._CurrentPage = ko.observable(1);
        _this._SearchText = ko.observable("");
        // Price Lookup Variables
        _this._LoadPrice = ko.observable(false);
        _this.LookupPriceTimeout = null;
        var self = _this;
        self._order = order;
        self.qty(1);
        self.roughweight(0);
        if (options) {
            self._LoadPrice(false);
            ko.mapping.fromJS(options.data, { 'Charges': { create: function (chargeOptions) { return new DItemCharge(_this, chargeOptions); } } }, self);
            if (self.prefix() === "RI")
                self.CavID(self.InnerDiameter());
            else
                self.CavID(self.Cavities());
        }
        self.InitSubscriptions();
        self.DieType = ko.computed({
            read: function () {
                if (self.prefix() == undefined || self.prefix() == null || self.prefix() === "")
                    return "";
                var result = $.grep(OrderViewModel._DieTypes, function (e) { return e.Prefix === self.prefix(); });
                var tmp;
                if (result.length == 1)
                    tmp = result[0].Name;
                else
                    tmp = "Misc";
                return tmp;
            },
            write: function (value) {
                var result = $.grep(OrderViewModel._DieTypes, function (e) { return e.Name === value; });
                if (result.length == 1) {
                    self.prefix(result[0].Prefix);
                    // Change the suffix to an empty string since it doesn't allow nulls
                    if (result[0].Prefix === "MI")
                        self.suffix("");
                }
            },
            owner: self,
            deferEvaluation: true
        });
        self.CanSave = ko.computed({
            read: function () {
                if (!IsValid(self.baseprice()) || isNaN(self.baseprice()))
                    return false;
                if (IsValid(self.prefix()) && self.prefix() == "MI")
                    return true;
                if (IsValid(self.prefix()) && IsValid(self.Diameter()) && IsValid(self.Thickness()) && IsValid(self.CavID())) {
                    if (!isNaN(self.Diameter()) && !isNaN(self.Thickness()) && !isNaN(self.CavID())) {
                        return true;
                    }
                }
                return false;
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        self.prefix.subscribe(function (newValue) {
            if (newValue === "MI") {
                self.freightweight(0);
                self.nitrideweight(0);
                self.steelsurchargeweight(0);
            }
            self.LoadPriceInfo();
        });
        self.Diameter.subscribe(function () {
            self.LoadPriceInfo();
        });
        self.Thickness.subscribe(function () {
            self.LoadPriceInfo();
        });
        self.CavID.subscribe(function (newValue) {
            if (self.prefix() === "RI")
                self.InnerDiameter(newValue);
            else
                self.Cavities(newValue);
            self.LoadPriceInfo();
        });
        //#region Search Helpers
        self._ItemsMatchingQuery = ko.computed({
            read: function () {
                var rtnVal = self.ChargeList().slice(0, self.ChargeList().length);
                if (self._SearchText() != '') {
                    rtnVal = $.grep(rtnVal, function (value) { return value.chargename().toLowerCase().indexOf(self._SearchText().toLowerCase()) > -1; });
                }
                return rtnVal;
            },
            write: function () {
                // Do Nothing
            }
        });
        self._ItemsToDisplay = ko.computed({
            read: function () {
                var rtnVal = self._ItemsMatchingQuery();
                var startIndex = (self._CurrentPage() - 1) * self._ItemsPerPage();
                rtnVal = rtnVal.slice(startIndex, startIndex + self._ItemsPerPage());
                return rtnVal;
            },
            write: function () {
                // Do Nothing
            }
        });
        self._LastPage = ko.computed({
            read: function () { return Math.ceil(self._ItemsMatchingQuery().length / self._ItemsPerPage()); },
            write: function () {
                // Do Nothing
            }
        });
        self._SearchText.subscribe(function () {
            self._CurrentPage(1);
        });
        self._ItemsPerPage.subscribe(function () {
            self._CurrentPage(1);
        });
        return _this;
        //#endregion
    }
    DOrderItem.prototype.InitSubscriptions = function () {
        var self = this;
        self.prefix.subscribe(function () {
            self.LoadCharges();
        });
        self.SubTotal = ko.computed({
            read: function () {
                var tmpTotal = 0;
                ko.utils.arrayForEach(self.Charges(), function (item) {
                    if (item.hasOwnProperty('ExtendedPrice'))
                        tmpTotal += item.ExtendedPrice();
                });
                return RoundNumber(self.baseprice() + tmpTotal, OrderViewModel.DecimalPlaces);
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
        self.ExtendedPrice = ko.computed({
            read: function () {
                var val = self.baseprice() * self.qty();
                ko.utils.arrayForEach(self.Charges(), function (charge) {
                    if (charge.hasOwnProperty('ExtendedPrice'))
                        val += charge.ExtendedPrice();
                });
                // self.price(RoundNumber(val, OrderViewModel.DecimalPlaces));
                return RoundNumber(val, OrderViewModel.DecimalPlaces);
            },
            write: function () {
                // Do Nothing
                // self.price(RoundNumber(value, OrderViewModel.DecimalPlaces));
            },
            owner: self,
            deferEvaluation: true
        });
        self.price = ko.computed({
            read: function () {
                return self.baseprice();
            },
            write: function () {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
    };
    DOrderItem.prototype.LoadCharges = function () {
        var self = this;
        self.ChargeList([]);
        if (self.prefix() == undefined || self.prefix() == null || self.prefix() == '')
            return;
        $.get('/api/OrderEntry/GetCharges', {
            'plcode': self._order.Customer().pricelist(),
            'prefix': self.prefix(),
            'pricelistMultiplier': self._order.pricelistmultiplier(),
            'currencyMultiplier': self._order.currencymultiplier()
        }).done(function (data) {
            ko.mapping.fromJS(data, { create: function (options) { return new PriceListItemCharge(options); } }, self.ChargeList);
            for (var i = 0; i < self.Charges().length; i++) {
                if (self.Charges()[i].chargetype().toLowerCase() == "custom") {
                    // Custom Charge
                    var charge = self.Charges()[i];
                    var tmpCharge = new PriceListItemCharge();
                    tmpCharge.chargename(charge.chargename());
                    tmpCharge.chargepercentage(charge.chargepercentage());
                    tmpCharge.chargeprice(charge.chargeprice());
                    tmpCharge.chargetype(charge.chargetype());
                    tmpCharge.Quantity(charge.qty());
                    tmpCharge.maxqty(9999);
                    self.ChargeList().push(tmpCharge);
                }
                else {
                    // Non-Custom Charge
                    var result = $.grep(self.ChargeList(), function (e) { return e.chargetype() == self.Charges()[i].chargetype() && e.chargename() == self.Charges()[i].chargename() && e.chargeprice() == self.Charges()[i].chargeprice(); });
                    if (result.length == 1) {
                        if (result[0].maxqty() === 1) {
                            result[0].IsChecked(self.Charges()[i].qty() > 0);
                        }
                        result[0].Quantity(self.Charges()[i].qty());
                    }
                }
            }
            // Only Reload Charges if a Custom charge exists
            // if (ChargeListChanged)
            self.ChargeList.valueHasMutated();
        });
    };
    DOrderItem.prototype.LoadPriceInfo = function () {
        var self = this;
        if (!self._LoadPrice())
            return;
        if (!IsValid(self.prefix(), true)
            || !IsValid(self.Diameter()) || isNaN(self.Diameter())
            || !IsValid(self.Thickness()) || isNaN(self.Thickness())
            || !IsValid(self.CavID()) || isNaN(self.CavID()))
            return;
        if (IsValid(self.LookupPriceTimeout)) {
            clearTimeout(self.LookupPriceTimeout);
        }
        self.LookupPriceTimeout = setTimeout(function () {
            $.get('/api/OrderEntry/GetPriceInfo', {
                'customercode': self._order.customercode(),
                'plcode': self._order.Customer().pricelist(),
                'prefix': self.prefix(),
                'diameter': self.Diameter(),
                'thickness': self.Thickness(),
                'cavid': self.CavID(),
                'pricelistMultiplier': self._order.pricelistmultiplier(),
                'currencyMultiplier': self._order.currencymultiplier()
            }).done(function (data) {
                if (!data.isValid)
                    return;
                if (!data.PriceFound) {
                    DisplayError("error", "No Price Found", "No price was found");
                    data.price = 0;
                }
                else if (data.price === 0) {
                    DisplayError("error", "No Price Found", "Price found was $0.00");
                }
                if (data.UsedEstimated) {
                    DisplayError("warning", "Estimated Pricing", "Used estimated pricing");
                }
                self.suffix(data.suffix);
                self.description(data.Description);
                self.baseprice(data.price);
                self.freightweight(data.freightWeight);
                self.nitrideweight(data.nitrideWeight);
                self.steelsurchargeweight(data.steelSurchargeWeight);
                self.tariffcode(data.TariffCode);
                if (!IsValid(self.suffix()))
                    self.suffix("");
            });
        }, 250);
    };
    // Add Functionality
    DOrderItem.prototype.GetCustomWeight = function () {
    };
    DOrderItem.prototype.UpdateSubTotal = function () {
        var self = this;
        var tmpTotal = 0;
        ko.utils.arrayForEach(self.Charges(), function (item) {
            tmpTotal += item.ExtendedPrice();
        });
        self.SubTotal(self.baseprice() + tmpTotal);
    };
    DOrderItem.prototype.UpdateExtended = function () {
        var self = this;
        self.ExtendedPrice(self.SubTotal() * self.qty());
    };
    //#region Pagination
    DOrderItem.prototype.NextPage = function () {
        var self = this;
        var curPage = self._CurrentPage();
        if (curPage < self._LastPage())
            self._CurrentPage(curPage + 1);
        else
            self._CurrentPage(self._LastPage());
    };
    DOrderItem.prototype.GoToPage = function (parent, data) {
        parent._CurrentPage(data);
    };
    DOrderItem.prototype.LastPage = function () {
        var self = this;
        var curPage = self._CurrentPage();
        if (curPage > 1)
            self._CurrentPage(curPage - 1);
        else
            self._CurrentPage(1);
    };
    DOrderItem.prototype.UpdateDisplay = function () {
        var self = this;
        var rtnVal = self.ChargeList().slice(0, self.ChargeList().length - 1);
        if (self._SearchText() != '') {
            rtnVal = $.grep(rtnVal, function (value) { return value.chargename().toLowerCase().indexOf(self._SearchText().toLowerCase()) > -1; });
        }
        var startIndex = (self._CurrentPage() - 1) * self._ItemsPerPage();
        rtnVal = rtnVal.slice(startIndex, startIndex + self._ItemsPerPage());
        self._ItemsToDisplay(rtnVal);
    };
    //#endregion
    DOrderItem.prototype.toJSON = function () {
        var self = this;
        var validFields = [
            'ordernumber', 'line', 'prefix', 'suffix', 'qty', 'description', 'location', 'dienumber', 'note',
            'price', 'chargenitride', 'steelcost', 'roughweight', 'freightweight', 'nitrideweight', 'steelsurchargeweight', 'baseprice', 'hasnitride',
            'Charges', 'tariffcode'
        ];
        var copy = ko.toJS(self);
        for (var prop in copy) {
            if (copy.hasOwnProperty(prop)) {
                if ($.inArray(prop, validFields) < 0) {
                    delete copy[prop];
                }
            }
        }
        return copy;
    };
    return DOrderItem;
}(d_orderitem));
// ReSharper restore InconsistentNaming 
//# sourceMappingURL=DOrderItem.js.map