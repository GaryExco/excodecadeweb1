//#region Typing References
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
//#endregion


// ReSharper disable InconsistentNaming
class TaxInfo extends d_taxinfo {
    constructor(options?: any) {
        super();

        var self = this;

        if (IsValid(options)) {
            ko.mapping.fromJS(options.data, {}, self);
        }
    }
}
// ReSharper restore InconsistentNaming