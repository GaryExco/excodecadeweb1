//#region Typing References
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
//#endregion
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// ReSharper disable InconsistentNaming
var DCustomerShip = (function (_super) {
    __extends(DCustomerShip, _super);
    function DCustomerShip(options) {
        var _this = _super.call(this) || this;
        var self = _this;
        if (IsValid(options)) {
            ko.mapping.fromJS(options.data, {}, self);
        }
        return _this;
    }
    return DCustomerShip;
}(d_customership));
// ReSharper restore InconsistentNaming 
//# sourceMappingURL=DCustomerShip.js.map