//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion


// ReSharper disable InconsistentNaming
class DItemCharge extends d_orderitemcharges {
    public ExtendedPrice: KnockoutComputed<number>;
    public ChargeText: KnockoutComputed<string>;
    public ChargePrice: KnockoutComputed<number>;
    public IsNitride: KnockoutComputed<boolean>;
    public IsNitridePlateOnly: KnockoutComputed<boolean>;
    public Quantity: KnockoutComputed<number>;

    public Price: KnockoutComputed<number>;

    public _item: DOrderItem;

    constructor(item: DOrderItem, options?: any) {
        super();

        var self = this;
        self._item = item;

        if (options) {
            ko.mapping.fromJS(options.data, { 'ignore': ['_item'] }, self);
        }

        self.IsNitride = ko.computed({
            read: () => {
                if (IsValid(self.chargetype()) && self.chargetype().toLowerCase() === "nitride")
                    return true;
                return false;
            },
            write: () => {
                // Do Nothing
            },
            owner: self
        });

        self.IsNitridePlateOnly = ko.computed({
            read: () => {
                if (self.IsNitride() && IsValid(self.chargename()) && self.chargename().toLowerCase().indexOf("plate only") !== -1)
                    return true;
                return false;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.Quantity = ko.computed({
            read: () => {
                if (self.IsNitride())
                    return self._item.nitrideweight();
                return self.qty();
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.ChargePrice = ko.computed({
            read: () => {
                var isNitride = self.IsNitride(),
                    nitrideCharge = self._item._order.nitridecharge(), // VM.Order.nitridecharge(),
                    normalCharge = self.chargeprice();

                if (self.IsNitridePlateOnly()) {
                    // Only charge 40% if die plate only
                    nitrideCharge *= 0.40;
                }

                return isNitride ? nitrideCharge : normalCharge;
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.Price = ko.computed({
            read: () => {
                var val;
                var tmpDecimalPlaces = OrderViewModel.DecimalPlaces;

                if (self.IsNitride()) {
                    val = self.Quantity() * self.ChargePrice();

                    // Check sanity of nitride to be within customer nitride range
                    if (val < self._item._order.Customer().minnitridecost())
                        val = self._item._order.Customer().minnitridecost();
                    else if (val > self._item._order.Customer().maxnitridecost())
                        val = self._item._order.Customer().maxnitridecost();

                    if (self.IsNitridePlateOnly()) {
                        // Round to nearest dollar value
                        tmpDecimalPlaces = 0;
                    }
                } else if (self.chargepercentage() !== 0)
                    val = self._item.baseprice() * (self.chargepercentage() / 100) * self.Quantity();
                else
                    val = self.ChargePrice() * self.Quantity();

                return RoundNumber(val, tmpDecimalPlaces);
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.ChargeText = ko.computed({
            read: () => {
                var val,
                    format,
                    suffix = "";

                if (self.IsNitride()) {
                    val = self.ChargePrice();
                    format = "c";
                    suffix = "/lb.";
                } else if (self.chargepercentage() != 0) {
                    val = self.chargepercentage() / 100;
                    format = "p";
                } else {
                    val = self.ChargePrice();
                    format = "c";
                }

                var culture = ko.toJS(Globalize.culture());
                culture.numberFormat.currency.symbol = self._item._order.Symbol();

                if (IsValid(val))
                    return Globalize.format(val, format, culture) + suffix;

                return "";
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });

        self.ExtendedPrice = ko.computed({
            read: () => {
                var rtnVal = self.Price();

                return RoundNumber(rtnVal, OrderViewModel.DecimalPlaces);
            },
            write: () => {
                // Do Nothing
            },
            owner: self,
            deferEvaluation: true
        });
    }

    toJSON() {
        var self = this;

        var validFields: string[] = ['ordernumber', 'line', 'chargeline', 'chargetype', 'chargename', 'chargeprice', 'chargepercentage', 'qty', 'price'];

        var copy = ko.toJS(self);

        for (var prop in copy) {
            if (copy.hasOwnProperty(prop)) {
                if ($.inArray(prop, validFields) < 0) {
                    delete copy[prop];
                }
            }
        }

        return copy;
    }
}
// ReSharper restore InconsistentNaming