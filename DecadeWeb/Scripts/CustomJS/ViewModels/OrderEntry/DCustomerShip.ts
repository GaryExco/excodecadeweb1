//#region Typing References
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
//#endregion


// ReSharper disable InconsistentNaming
class DCustomerShip extends d_customership {
    constructor(options?: any) {
        super();

        var self = this;

        if (IsValid(options)) {
            ko.mapping.fromJS(options.data, {}, self);
        }
    }
}
// ReSharper restore InconsistentNaming