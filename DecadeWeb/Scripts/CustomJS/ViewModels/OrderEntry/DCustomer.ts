//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion


// ReSharper disable InconsistentNaming
class DCustomer extends d_customer {
    public Addresses: KnockoutObservableArray<DCustomerAddress> = ko.observableArray<DCustomerAddress>();
    public ShipVia: KnockoutObservableArray<DCustomerShip> = ko.observableArray<DCustomerShip>();
    public IsValid: KnockoutComputed<boolean>;

    public Presses: KnockoutObservableArray<PressInfo> = ko.observableArray<PressInfo>();
    public TaxInfo: KnockoutObservableArray<TaxInfo> = ko.observableArray<TaxInfo>();

    constructor(options?: any) {
        super();

        var self = this;

        if (IsValid(options)) {
            ko.mapping.fromJS(options.data, {
                'ShipVia': { create: svOptions => new DCustomerShip(svOptions) },
                'Addresses': { create: addrOptions => new DCustomerAddress(addrOptions) },
                'Presses': { create: pressOptions => new PressInfo(pressOptions) },
                'TaxInfo': { create: taxOptions => new TaxInfo(taxOptions) }
            }, self);
        }

        self.IsValid = ko.computed({
            read: () => {
                if (validCustomer.test(self.customercode()) && IsValid(self.name(), true))
                    return true;
                return false;
            }
        });
    }

    UpdateCustomerSettings(order: DOrder, loadData?: boolean): void {
        var self = this;

        // Set Placeholder Value to new Customer Code and clear the value
        $("#customercode").attr('placeholder', self.customercode()).val("");
        // Set Customer Specific Settings
        $("#in_nitriderate").attr('placeholder', Globalize.format(self.nitridecost(), "n2")).change();
        $("#in_freightrate").attr('placeholder', Globalize.format(self.freightcost(), "n2")).change();
        $("#in_steelsurchargerate").attr('placeholder', Globalize.format(self.steelcost(), "n2")).change();
        $("#in_fasttrackpercentage").attr('placeholder', Globalize.format(self.ftpremium(), "n2")).change();

        //#region Ship Via AutoComplete
        $("#shipvia").autocomplete({
            source: $.map(self.ShipVia(), item => {
                return {
                    label: item.shipvia(),
                    value: item.shipvia()
                };
            }),
            select: (e, ui) => {
                $("#shipvia").val(ui.item.value).change();
            },
            minLength: 0
        });

        //#region Set Default Value
        var curShipVia = $("#shipvia").attr('placeholder');
        if (curShipVia == undefined || curShipVia == '') { // || curShipVia == null
            for (var i = 0; i < self.ShipVia().length; i++) {
                if (i == 0 || self.ShipVia()[i].defaultship()) {
                    $("#shipvia").attr('placeholder', self.ShipVia()[i].shipvia());

                    if (!IsValid(order.shipvia()))
                        order.shipvia(self.ShipVia()[i].shipvia());
                    break;
                }
            }
        }
        //#endregion
        //#endregion

        if (self.roundtotals())
            OrderViewModel.DecimalPlaces = 0;
        else
            OrderViewModel.DecimalPlaces = 2;

        if (loadData) {
            // var results = $.grep(self.Order.CurrentOrderItem().ChargeList(), function (e: PriceListItemCharge) { return e.Quantity() > 0; });
            var billing = $.grep(self.Addresses(), (e: DCustomerAddress) => {
                if (e.name() == undefined) return false;
                return e.name().toLowerCase() === "billing";
            });
            var shipping = $.grep(self.Addresses(), (e: DCustomerAddress) => {
                if (e.name() == undefined) return false;
                return e.name().toLowerCase() === "shipping";
            });

            if (billing.length > 0)
                self.SetAddress(order, 'b', billing[0]);
            if (shipping.length > 0)
                self.SetAddress(order, 's', shipping[0]);

            // Set Order Currency/Pricelist Mutlipliers
            order.currencymultiplier(self.currencyMultiplier());
            order.pricelistmultiplier(self.pricelistMultiplier());

            // Focus the PO
            setTimeout(() => { $("#customerpo").focus(); }, 0);

            //#region Check if Taxes need to be loaded (If changed, prompt user if we should load in new taxes)
            // Only run this check on existing orders that have NOT been invoiced.
            var loadTaxes = false;

            if (order.ordernumber() == 0) {
                loadTaxes = true;
            } else if (!IsValid(order.invoicedate()) && order.ordernumber() > 0) {
                var displayTaxesDifferentPrompt: boolean = order.TaxItems().length !== self.TaxInfo().length;

                if (!displayTaxesDifferentPrompt) {
                    ko.utils.arrayForEach(order.TaxItems(), (orderTax: DOrderTaxes) => {
                        if (displayTaxesDifferentPrompt)
                            return;

                        var tmpTax: TaxInfo = ko.utils.arrayFirst(self.TaxInfo(), (taxInfo: TaxInfo) => { return orderTax.taxtype() === taxInfo.taxtype(); });

                        /*
                        if (IsValid(tmpTax)) {
                            console.log(tmpTax.rate());
                            console.log(orderTax.rate());
                            console.log(tmpTax.rate() === orderTax.rate());
                        }
                        */

                        if (!IsValid(tmpTax) || tmpTax.rate() !== orderTax.rate())
                            displayTaxesDifferentPrompt = true;
                    });
                }

                if (displayTaxesDifferentPrompt) {
                    loadTaxes = false;

                    bootbox.confirm("Taxes have changed since the order was written up. Calculate new taxes?", result => {
                        if (result)
                            self.LoadTaxes(order);
                    });
                }
            }

            // If we want to load the taxes, take the tax info from the customer and load it into the order so they can calculate.
            if (loadTaxes)
                self.LoadTaxes(order);
            //#endregion

            // Alert user that Customer is on shipping hold
            if (self.onhold())
                DisplayError("warning", "Attention!", "Customer is on shipping hold.");
        }
    }

    public LoadTaxes(order: DOrder) {
        var self = this;
        var taxLine: number = 1;

        // Empty the order taxes
        order.TaxItems([]);

        ko.utils.arrayForEach(self.TaxInfo(), (taxInfo: TaxInfo) => {
            var tax = new DOrderTaxes(order);
            tax.rate(taxInfo.rate());
            tax.taxtype(taxInfo.taxtype());
            tax.taxline(taxLine++);
            order.TaxItems().push(tax);
        });
    }

    public SetAddress(order: DOrder, addressType: string, address: DCustomerAddress): void {
        order[addressType + 'address1'](address.address1());
        order[addressType + 'address2'](address.address2());
        order[addressType + 'address3'](address.address3());
        order[addressType + 'address4'](address.address4());
        order[addressType + 'postalcode'](address.postalcode());

        var fullAddress = "";
        for (var i = 1; i <= 4; i++) {
            if (i > 1)
                fullAddress += "\n";

            // Set as empty if undefined or null
            if (!IsValid(order[addressType + "address" + i]()))
                order[addressType + "address" + i]("");

            if (IsValid(order[addressType + 'address' + i]()))
                fullAddress += order[addressType + 'address' + i]();
        }

        var key: string = "Billing";
        if (addressType == 's')
            key = "Shipping";

        order[key + "Address"](fullAddress);
    }
}
// ReSharper restore InconsistentNaming