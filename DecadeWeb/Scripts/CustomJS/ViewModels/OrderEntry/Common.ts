/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\..\typings\bootbox\bootbox.d.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
// Open an Order for Editing
function GoToEditOrder(order: number = -1, displayOnNone: boolean = false): boolean {
    if (order != null && order != -1) {
        window.location.href = "/OrderEntry/Edit/" + order;
        return true;
    } else {
        if (displayOnNone) {
            window.location.href = "/OrderEntry/Add/";
            return true;
        } else {
            return false;
        }
    }
}

function GoToTrackOrder(order: number): boolean {
    if (validOrdernumber.test(String(order))) {
        window.open("/Tracking/" + order, "Tracking", "");
        return true;
    } else {
        return false;
    }
}

function GoToSchedule(dietype: string, date: Date): void {
    if (typeof (date) != typeof (new Date())) {
        var tmp: any = date;
        date = Globalize.parseDate(tmp);
    }

    window.open("/Schedule/Admin/" + dietype + "/Die/" + Globalize.format(date, "yyyy-MM-dd"), "Schedule", "");
}

function GetSONumber(promptMessage, fn) {
    bootbox.prompt(promptMessage, fn);
}

function GetCustomerCode(promptMessage, fn) {
    bootbox.prompt(promptMessage, fn);
}