var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\jQuery.fileDownload\jQuery.fileDownload.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
var OrderEntryMenuViewModel = (function () {
    function OrderEntryMenuViewModel() {
        this.OrdersTable = $("#orders");
        this.OrdersRows = $("#orders tbody tr");
        this.lastSelectedRowIndex = -1;
        this.Orders = ko.observableArray();
        this.ordercount = ko.observable("");
        this.title = ko.observable("");
        this.numberfield = ko.observable("");
        this.datetitle = ko.observable("");
        this.datefield = ko.observable("");
        this._CurrentPage = ko.observable(1);
        this._SearchText = ko.observable("");
        this._ItemsPerPageOptions = ko.observableArray([25, 50, 100, 200]);
        this._ItemsPerPage = ko.observable(this._ItemsPerPageOptions()[0]);
        this._FirstDispPage = ko.observable(1);
        this._LastDispPage = ko.observable(1);
        this._SortingColumn = ko.observable("");
        this._SortingDirection = ko.observable(false);
        var self = this;
        self.init();
        self._CurrentPage.subscribe(function (newValue) {
            var first = newValue - 2, last = newValue + 2;
            if (first < 1) {
                first = 1;
                last = self._LastPage() > 5 ? 5 : self._LastPage();
            }
            else if (last > self._LastPage()) {
                last = self._LastPage();
                first = self._LastPage() > 5 ? self._LastPage() - 4 : 1;
            }
            self._FirstDispPage(first);
            self._LastDispPage(last);
        });
        self._ItemsPerPage.subscribe(function () {
            self._CurrentPage.valueHasMutated();
        });
        ko.applyBindings(self);
    }
    //#region Initalization Functions
    OrderEntryMenuViewModel.prototype.init = function () {
        this.initMenu();
        this.initTable();
        this.LoadData();
    };
    OrderEntryMenuViewModel.prototype.initMenu = function () {
        var self = this;
        $("#neworder").click(function () {
            GoToEditOrder(null, true);
        });
        $("#edit").click(function () {
            if (!GoToEditOrder(self.GetFirstSelectedOrder(), false)) {
                GetSONumber("Enter order number to edit: ", function (order) { GoToEditOrder(order); });
            }
        });
        $("#editbyso").click(function () {
            GetSONumber("Enter order number to edit: ", function (order) { GoToEditOrder(order); });
        });
        $("#print, #printinvoice").click(function () {
            var mode = "Order";
            if ($(this).attr("id").toString().toLowerCase() == "printinvoice")
                mode = "Invoice";
            self.PrintReport(mode);
        });
        $("#pdforder, #pdfinvoice").click(function () {
            var mode = "Order";
            if ($(this).attr("id").toString().toLowerCase() == "pdfinvoice")
                mode = "Invoice";
            self.DownloadReport(mode);
        });
        $("#track").click(function () {
            if (!GoToTrackOrder(self.GetFirstSelectedOrder())) {
                GetSONumber("Enter order number to track: ", function (order) { GoToTrackOrder(order); });
            }
        });
        $("#schedule").click(function () {
            var date = new Date();
            date.setDate(date.getDate() + 8);
            GoToSchedule("Solid", date);
        });
        $("#copyorder").click(function () {
            // Add Functionality
        });
        $("#confirmations").click(function () {
            window.open("/OrderEntry/Confirmations", "Confirmations", "");
        });
        $("#editcustomers").click(function () {
            return;
            // window.location.href = "/OrderEntry/CustomerList";
        });
        // Add handlers for filter to reload data
        $("input[type=radio][name='filtertype'], input[type=radio][name='filterduration']").change(function () {
            self.LoadData();
        });
    };
    OrderEntryMenuViewModel.prototype.initTable = function () {
        var self = this;
        //#region Table Functions
        $("#orders > tbody").on("click", "tr", function (e) {
            // Ignore the Click on the Checkbox
            if ($(e.target).prop("tagName").toUpperCase() === 'INPUT')
                return;
            if (e.shiftKey && self.lastSelectedRowIndex != -1) {
                var curIndex = $(this).closest("tr").prevAll().length;
                // Select All Rows in-between
                var start = 0;
                var stop = 0;
                if (curIndex > self.lastSelectedRowIndex) {
                    start = self.lastSelectedRowIndex + 1;
                    stop = curIndex - 1;
                }
                else {
                    start = curIndex + 1;
                    stop = self.lastSelectedRowIndex - 1;
                }
                var i = 0;
                $("#orders > tbody > tr").each(function () {
                    if (i >= start && i <= stop) {
                        var chkBox = $(this).find("input[type=checkbox]");
                        chkBox.prop("checked", "checked");
                        ko.contextFor(chkBox[0]).$data.Selected(true);
                    }
                    i++;
                });
                // Done Selecting All Rows in-between
                self.lastSelectedRowIndex = curIndex;
            }
            else {
                if ($(this).closest("tr").find(':checked').length < 1)
                    self.lastSelectedRowIndex = $(this).closest("tr").prevAll().length;
                else
                    self.lastSelectedRowIndex = -1;
            }
        });
        // Enable Double Clicking to Edit Order
        $("#orders > tbody").on("dblclick", "tr", function (e) {
            var oNum = Number($(this).attr('id').toString().replace("row", ""));
            GoToEditOrder(oNum);
        });
        //#endregion
        //#region Searchable Table
        self._ItemsMatchingQuery = ko.computed({
            read: function () {
                var rtnVal = self.Orders().slice(0, self.Orders().length);
                if (self._SearchText() != '') {
                    rtnVal = $.grep(rtnVal, function (value) {
                        var searchText = self._SearchText().toLowerCase();
                        if (value[self.numberfield()]().toString().toLowerCase().indexOf(searchText) > -1)
                            return true;
                        if (value.CustomerName().toLowerCase().indexOf(searchText) > -1)
                            return true;
                        if (value.customercode().toLowerCase().indexOf(searchText) > -1)
                            return true;
                        return false;
                    });
                }
                // Sorting
                if (IsValid(self._SortingColumn(), true)) {
                    var column = "";
                    var isAsc = self._SortingDirection();
                    var ascDir = isAsc ? -1 : 1;
                    var descDir = isAsc ? 1 : -1;
                    switch (self._SortingColumn()) {
                        default:
                            column = self.numberfield();
                            break;
                        case "code":
                            column = "customercode";
                            break;
                        case "name":
                            column = "CustomerName";
                            break;
                        case "date":
                            column = self.datefield();
                            break;
                    }
                    rtnVal.sort(function (left, right) {
                        left = left[column]();
                        right = right[column]();
                        return left === right ? 0 : left < right ? ascDir : descDir;
                    });
                }
                return rtnVal;
            },
            write: function () {
                // Do Nothing
            }
        });
        self._ItemsToDisplay = ko.computed({
            read: function () {
                var rtnVal = self._ItemsMatchingQuery();
                var startIndex = (self._CurrentPage() - 1) * self._ItemsPerPage();
                rtnVal = rtnVal.slice(startIndex, startIndex + self._ItemsPerPage());
                return rtnVal;
            },
            write: function () {
                // Do Nothing
            }
        });
        self._LastPage = ko.computed({
            read: function () {
                return Math.ceil(self._ItemsMatchingQuery().length / self._ItemsPerPage());
            },
            write: function () {
                // Do Nothing
            }
        });
        self._SearchText.subscribe(function () {
            self._CurrentPage(1);
        });
        self._ItemsPerPage.subscribe(function () {
            self._CurrentPage(1);
        });
        //#endregion
    };
    //#endregion
    //#region Pagination
    OrderEntryMenuViewModel.prototype.FirstPage = function () {
        var self = this;
        self._CurrentPage(1);
    };
    OrderEntryMenuViewModel.prototype.NextPage = function () {
        var self = this;
        var curPage = self._CurrentPage();
        if (curPage < self._LastPage())
            self._CurrentPage(curPage + 1);
        else
            self._CurrentPage(self._LastPage());
    };
    OrderEntryMenuViewModel.prototype.GoToPage = function (data) {
        var self = this;
        self._CurrentPage(data);
    };
    OrderEntryMenuViewModel.prototype.PrevPage = function () {
        var self = this;
        var curPage = self._CurrentPage();
        if (curPage > 1)
            self._CurrentPage(curPage - 1);
        else
            self._CurrentPage(1);
    };
    OrderEntryMenuViewModel.prototype.LastPage = function () {
        var self = this;
        self._CurrentPage(self._LastPage());
    };
    OrderEntryMenuViewModel.prototype.UpdateDisplay = function () {
        var self = this;
        var rtnVal = self.Orders().slice(0, self.Orders().length - 1);
        if (self._SearchText() != '') {
            rtnVal = $.grep(rtnVal, function (value) {
                return value.chargename().toLowerCase().indexOf(self._SearchText().toLowerCase()) > -1;
            });
        }
        var startIndex = (self._CurrentPage() - 1) * self._ItemsPerPage();
        rtnVal = rtnVal.slice(startIndex, startIndex + self._ItemsPerPage());
        self._ItemsToDisplay(rtnVal);
    };
    //#endregion
    OrderEntryMenuViewModel.prototype.LoadData = function () {
        // Remove Orders
        var self = this;
        self.Orders.removeAll();
        // Show Working Animation
        // $("#ordercount").html('<img src="/images/loading.gif" width="20px" height="20px" alt="Loading..." />');
        var filter = $("#orderfilter :radio:checked").attr("value"), duration = $("#durationfilter :radio:checked").attr("value");
        // Load in New Content
        $.ajax({
            type: "GET",
            url: "/api/Order/GetOrderListing",
            data: { "orderstatus": filter, "duration": duration },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                self.ordercount(data.OrderCountTitle);
                self.title(data.Title);
                self.numberfield(data.NumberField);
                self.datetitle(data.DateTitle);
                self.datefield(data.DateField);
                ko.mapping.fromJS(data.Orders, { create: function (options) { return new MenuOrder(options); } }, self.Orders);
                self._CurrentPage.valueHasMutated();
            }
        });
    };
    OrderEntryMenuViewModel.prototype.PrintReport = function (mode) {
        var self = this;
        var data = { "mode": mode, "orders": self.GetAllSelectedOrders() };
        $.ajax({
            type: "POST",
            url: "/api/OrderEntry/PrintOrders",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                // Clear Selected Orders
                console.log(msg.Orders);
                if (!msg.isValid) {
                    DisplayError("error", "Error Printing (At Server)", msg.message);
                    return;
                }
                self.ClearSelected();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                DisplayError("error", "Error Printing (At Client)", xhr.responseText);
            }
        });
    };
    OrderEntryMenuViewModel.prototype.DownloadReport = function (mode) {
        var self = this;
        var data = { "mode": mode, "orders": self.GetAllSelectedOrders() };
        $.fileDownload("/api/OrderEntry/DownloadOrders", {
            data: data
        })
            .done(function () {
            alert('File download a success!');
            self.ClearSelected();
        })
            .fail(function (xhr) {
            DisplayError("error", "Download Error", "There was an error downloading the file.  " + xhr.responseText);
        });
    };
    OrderEntryMenuViewModel.prototype.GetFirstSelectedOrder = function () {
        var self = this;
        var order = ko.utils.arrayFirst(self.Orders(), function (item) { return item.Selected(); });
        if (IsValid(order))
            return order.ordernumber();
        return -1;
    };
    OrderEntryMenuViewModel.prototype.GetAllSelectedOrders = function () {
        var self = this;
        var rtnVal = [];
        var orders = ko.utils.arrayFilter(self.Orders(), function (item) { return item.Selected(); });
        if (IsValid(orders))
            ko.utils.arrayForEach(orders, function (item) { rtnVal.push(item.ordernumber()); });
        return rtnVal;
    };
    OrderEntryMenuViewModel.prototype.ClearSelected = function () {
        var self = this;
        ko.utils.arrayForEach(self.Orders(), function (item) {
            item.Selected(false);
        });
    };
    OrderEntryMenuViewModel.prototype.SortTable = function (header) {
        var self = this;
        if (self._SortingColumn() !== header) {
            self._SortingColumn(header);
            self._SortingDirection(true);
        }
        else {
            if (self._SortingDirection())
                self._SortingDirection(false);
            else {
                self._SortingColumn("");
                self._SortingDirection(true);
            }
        }
    };
    return OrderEntryMenuViewModel;
}());
var MenuOrder = (function (_super) {
    __extends(MenuOrder, _super);
    function MenuOrder(options) {
        var _this = _super.call(this) || this;
        _this.CustomerName = ko.observable("");
        _this.Selected = ko.observable(false);
        var self = _this;
        if (IsValid(options)) {
            ko.mapping.fromJS(options.data, {}, self);
        }
        return _this;
    }
    return MenuOrder;
}(d_order));
//# sourceMappingURL=MenuViewModel.js.map