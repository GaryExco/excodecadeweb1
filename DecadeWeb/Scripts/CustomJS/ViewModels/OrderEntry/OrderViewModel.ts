//#region Typing References
/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\bootstrap\bootstrap.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\moment\moment.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path="..\..\CustomerAutocomplete.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
/// <reference path="..\..\_KnockoutValidationExtensions.ts" />
/// <reference path="DOrder.ts" />
//#endregion
// ReSharper disable InconsistentNaming


/*
                                        !!!!! WARNING !!!!!
    If Fields are being added that need to be passed to the server, adjust the toJSON() method of
    the class that the field is being added to. The CASE-SENSITIVE name needs to be added to the 
    ValidFields array in order for it to be passed to the server (This was done to ignore misc properties
    on the table that we should not control from this application, but are controled else where).
*/


// Placeholder data from the server
var data: any;

// 
var vendors: any;

// 
var VM: OrderViewModel;

class OrderViewModel {
    public Order: DOrder = new DOrder();


    private _OrderItemIndex: number = -1;


    public static DecimalPlaces = 2;


    public NCRRate: KnockoutObservable<string> = ko.observable("100T");


    public static RedirectURL = '/OrderEntry';


    public shouldSave: KnockoutObservable<boolean> = ko.observable(true);


    public static _DieTypes: Object[] = [
        { Prefix: "DI", Name: "Solid" },
        { Prefix: "HO", Name: "Hollow" },
        { Prefix: "BA", Name: "Backer" },
        { Prefix: "FE", Name: "Feeder" },
        { Prefix: "BO", Name: "Bolster" },
        { Prefix: "RI", Name: "Ring" },
        { Prefix: "MI", Name: "Misc" }
    ];


    constructor() {
        var self = this;

        self.InitMenu();
        self.InitAutoCompletes();
        self.InitEditOrderItemSection();
        self.InitOverrideCharges();
        self.InitAddressModal();
        self.InitCustomChargeModal();
        self.InitWeightModal();

        // Focus the CustomerCode if a new order
        if (!data.ordernumber)
            $("#customercode").focus();

        // Set our Round Totals Early
        if (IsValid(data.Customer.roundtotals))
            OrderViewModel.DecimalPlaces = data.Customer.roundtotals ? 0 : 2;

        self.Order = ko.mapping.fromJS(data, {
            create: options => new DOrder(options)
        });

        // data = null;
        self.InitSubscriptions();

        ko.applyBindings(self.Order);
        $("#in_discountpercentage").attr("placeholder", 0);
        self.Order.Customer().UpdateCustomerSettings(self.Order, false);

        // Swap in Press Name
        var press = self.Order.CheckList().press();
        if (IsValid(press, true)) {
            var presses = $.grep(self.Order.Customer().Presses(), (e: PressInfo) => e.PressNameCAD() == press);

            if (presses.length === 1) {
                self.Order.CheckList().press(presses[0].PressName());
                self.Order.CheckList().pressnumber(presses[0].PressNameCAD());

                $("#cl_press").attr('placeholder', self.Order.CheckList().press());
            }
        }

        //#region Setup Redirect URL Location
        var tmpReferrerHost = document.referrer.split('/')[2];
        if (IsValid(tmpReferrerHost)) {
            if (tmpReferrerHost.indexOf(':') > -1)
                tmpReferrerHost = tmpReferrerHost.split(':')[0];
            if (tmpReferrerHost === location.hostname) {
                if (document.referrer.toLowerCase().indexOf('/account') < 0)
                    OrderViewModel.RedirectURL = document.referrer;
            }
        }
        //#endregion

        // Close Checklist if necessary
        setTimeout(() => {
            if (self.Order.ordernumber() === 0) {
                $("#collapseChecklist").collapse("show");
            } else {
                $("#collapseChecklist").collapse("hide");
            }
        }, 10);

        setTimeout(() => {
            self.Order.CurrentOrderItem()._LoadPrice(true);
            self.Order.shipvendorid.valueHasMutated();

            // Don't allow order to be saved if invoiced and invoice date not cleared
            if (IsValid(self.Order.invoicedate())) {
                self.shouldSave(false);
            }

            // Close the errors window until something changes
            $("#erroricon").popover("disable").popover("hide");

            // Trigger a pricing change at the bottom so that the numbers are formatted properly
            /*
            self.Order.parts.valueHasMutated();
            self.Order.discountamount.valueHasMutated();
            self.Order.steelsurcharge.valueHasMutated();
            self.Order.fasttrackcharge.valueHasMutated();
            self.Order.freight.valueHasMutated();
            self.Order.gst.valueHasMutated();
            */
        }, 10);
    }


    InitSubscriptions(): void {
        var self = this;

        self.Order.ncr.subscribe(() => {
            self.UpdateDiscountPercentage();
        });

        self.Order.ncrs.subscribe(() => {
            self.UpdateDiscountPercentage();
        });

        self.Order.zeroprice.subscribe(() => {
            self.UpdateDiscountPercentage();
        });

        // Allow order to be saved if we clear the invoice date at all (Only affects invoiced orders)
        self.Order.invoicedate.subscribe(() => {
            self.shouldSave(true);
        });
    }


    InitMenu(): void {
        var self = this;

        //#region Edit Menu Button Presses
        $("#editmenu button").click(function() {
            switch ($(this).attr("id")) {
            case "ok":
                self.SaveOrder();
                break;

            case "cancel":
                // This will need to be changed if we're working with Quotes
                window.location.href = OrderViewModel.RedirectURL;
                break;

            case "printorder":
                self.PrintOrder();
                break;

            case "printinvoice":
                // Prompt the user to generate the invoice number
                var tmpInvoice = self.Order.invoicenumber(),
                    tmpInvoiceDate = self.Order.invoicedate();

                if (!IsValid(tmpInvoiceDate) && (!IsValid(tmpInvoice) || isNaN(tmpInvoice) || tmpInvoice === 0)) {
                    // if ((tmpInvoiceDate === undefined || tmpInvoiceDate === null) && (tmpInvoice === undefined || tmpInvoice === null || isNaN(tmpInvoice) || tmpInvoice === 0)) {
                    bootbox.confirm("No Invoice number and no invoice date selected, would you like to generate an invoice number now?", result => {
                        if (result)
                            self.PrintInvoice(true);
                    });

                    return;
                }

                var genInvoiceNumber = IsValid(tmpInvoiceDate) && (!IsValid(tmpInvoice) || isNaN(tmpInvoice) || tmpInvoice === 0);

                self.PrintInvoice(genInvoiceNumber);
                break;

            case "track":
                if (!GoToTrackOrder(self.Order.ordernumber())) {
                    GetSONumber("Enter order number to track: ", order => { GoToTrackOrder(order); });
                }
                break;

            case "schedule":
                GoToSchedule(self.Order.DieType(), self.Order.shopdate());
                break;
            }
        });
        //#endregion

        $("#erroricon").popover({
                placement: "top",
                trigger: "hover",
                title: "Errors",
                content: "",
                html: true,
                delay: 150,
            })
            .popover("disable")
            .popover("hide");
    }


    InitAutoCompletes(): void {
        var self = this;

        // Setup Autocomplete
        // var cc = new CustomerAutoComplete("customercode");

        $("#shipvia").focus(() => {
            $("#shipvia").autocomplete("search");
        });

        //#region Die Type
        $("#item_dietype").autocomplete({
            source: $.map(OrderViewModel._DieTypes, (item: any) => {
                return {
                    label: item.Name,
                    value: item.Prefix
                };
            }),
            select: (event, ui) => {
                self.Order.CurrentOrderItem().prefix(ui.item.value);

                return false;
            },
            minLength: 0
        }).focus(() => {
            $("#item_dietype").autocomplete("search");
        });
        //#endregion

        //#region Checklist
        $(".autocomplete-file").each(function() {
            var type = $(this).data('mode');
            $(this).autocomplete({
                source: (request, response) => {
                    var query = request.term;
                    if (query == undefined || query == '') // || query == null
                        return;

                    $.get('/api/OrderEntry/FileSearch', { mode: type, customercode: self.Order.customercode(), filename: query }).done((data) => {
                        if (data == undefined || data == '') // || data == null
                            return;

                        response($.map(data, item => item));
                    });
                },
                minLength: 2,
                select: function() {
                    var tmp = $(this);
                    setTimeout(() => { tmp.change(); }, 10);
                }
            });
        });

        // Press
        $("#cl_press").autocomplete({
            source: (request, response) => {
                // var query = request.term;

                response($.map(self.Order.Customer().Presses(), item => {
                    return {
                        label: item.PressName(),
                        value: item.PressName(),
                        id: item.PressNameCAD()
                    };
                }));
            },
            select: (event, ui) => {
                self.Order.CheckList().press(ui.item.label);
                self.Order.CheckList().pressnumber(ui.item.id);
            },
            minLength: 0
        }).focus(function() {
            $(this).autocomplete("search");
        });
        //#endregion
    }


    InitEditOrderItemSection(): void { // Load Item into edit section on click
        var self = this;

        $("#orderinfo").on('click', '> tbody', event => {
            event.stopPropagation();

            // Get Reference to Order Item (Not Charge Table)
            var $parent = $(event.target).closest('.dragable');
            var context = ko.contextFor($parent[0]);

            if (context.$index() == self._OrderItemIndex)
                return;

            self._OrderItemIndex = context.$index();

            var original = self.Order.OrderItems.slice(self._OrderItemIndex, self._OrderItemIndex + 1)[0];

            var tmp = ko.mapping.toJS(original);
            delete tmp['__ko_mapping__'];
            delete tmp['ChargeList'];

            self.Order.CurrentOrderItem()._LoadPrice(false);

            ko.mapping.fromJS(tmp, { 'ignore': ['constructor'], }, self.Order.CurrentOrderItem);

            self.Order.CurrentOrderItem().CavID(original.CavID());

            if (self.Order.CurrentOrderItem().qty() !== 1)
                $("#item_qty").val(self.Order.CurrentOrderItem().qty().toString());

            self.Order.CurrentOrderItem().LoadCharges();
            // Has to be in Timeout to run after initial values are set in async
            setTimeout(() => { self.Order.CurrentOrderItem()._LoadPrice(true); }, 15);

            var isShown = $("#collapseEdit").hasClass("in");
            if (!isShown) {
                $("#collapseEdit").collapse("show");
            }
        });

        $("#orderinfo").sortable({
            items: ".dragable",
            forcePlaceholderSize: true,
            axis: "y",
            distance: 7,
            placeholder: "ui-state-highlight",
            create: function() {
                var list = this;
                () => {
                    jQuery(this).css("height", "auto");
                    jQuery(this).height(jQuery(this).height());
                };
                jQuery(list).height(jQuery(list).height());
            },
            helper: (e, ui) => {
                var tmpBody = ui.children(),
                    helper = ui.clone();

                helper.children().each(function(index) {
                    $(this).width(tmpBody.eq(index).width());
                    $(this).height(tmpBody.eq(index).height());
                });
                return helper;
            },
            start: function(e, ui) {
                ui.placeholder.html('<tr><td colspan="7" style="height: 75px">&nbsp;</td></tr>');
                $(this).sortable("refreshPositions");
            },
            stop: () => {
                self.Order.OrderItems.valueHasMutated();
            }
        }).disableSelection();

        // Remove All Button Press
        $("#item_removeall").click(() => {
            self.Order.OrderItems([]);
            self.Order.OrderItems.valueHasMutated();
            self.ResetEditItem();
        });

        // Remove Single Button Press
        $("#item_remove").click(() => {
            if (self._OrderItemIndex == -1)
                return;

            var tmp = self.Order.OrderItems()[self._OrderItemIndex];

            self.Order.OrderItems.remove(tmp);
            self.ResetEditItem();
        });

        var saveOrderItem = () => {
            if ($("#item_save").is(":disabled")) {
                DisplayError("error", "Invalid Input", "Item has some missing information");
                return;
            }

            // Turn off price loading
            self.Order.CurrentOrderItem()._LoadPrice(false);

            // Adjust CurrentOrderItem.Charges()
            self.Order.CurrentOrderItem().Charges([]);
            var results = $.grep(self.Order.CurrentOrderItem().ChargeList(), (e: PriceListItemCharge) => e.Quantity() > 0);
            self.Order.CurrentOrderItem().hasnitride(false);

            for (var i = 0; i < results.length; i++) {
                var charge: PriceListItemCharge = results[i];
                var tmpCharge: DItemCharge = new DItemCharge(self.Order.CurrentOrderItem());

                // Set qty before chargetype because of computed on ExtendedPrice
                tmpCharge.qty(charge.Quantity());
                tmpCharge.chargetype(charge.chargetype());
                tmpCharge.chargename(charge.chargename());
                tmpCharge.chargepercentage(charge.chargepercentage());
                tmpCharge.chargeprice(charge.chargeprice());

                if (tmpCharge.chargetype().toLowerCase() === "nitride") {
                    tmpCharge.qty(self.Order.CurrentOrderItem().nitrideweight());
                    self.Order.CurrentOrderItem().hasnitride(true);
                } else if (tmpCharge.chargename().toLowerCase().indexOf("nitrid") > -1) {
                    self.Order.CurrentOrderItem().hasnitride(true);
                }

                self.Order.CurrentOrderItem().Charges().push(tmpCharge);
            }

            // Save Changes to Item
            if (self._OrderItemIndex == -1) {
                var tmpOrderItem = new DOrderItem(self.Order);
                ko.mapping.fromJS(ko.mapping.toJS(self.Order.CurrentOrderItem()), {
                    'ignore': ['_order', 'constructor'],
                    'Charges': {
                        create: options => new DItemCharge(tmpOrderItem, options)
                    }
                }, tmpOrderItem);

                self.Order.OrderItems.push(tmpOrderItem);
            } else {
                var tmp = ko.mapping.toJS(self.Order.CurrentOrderItem(), { 'ignore': ['_order'] });
                ko.mapping.fromJS(tmp, { 'ignore': ['constructor'], 'Charges': { create: options => new DItemCharge(self.Order.OrderItems()[self._OrderItemIndex], options) } }, self.Order.OrderItems()[self._OrderItemIndex]);
                self.Order.OrderItems.valueHasMutated();
            }

            //#region Check cavities match between items (Run Async)
            setTimeout(() => {
                var cavCnt = -1;
                for (var j = 0; j < self.Order.OrderItems().length; j++) {
                    var item = self.Order.OrderItems()[j];

                    if (item.prefix() !== 'MI') {
                        if (cavCnt === -1) {
                            cavCnt = item.CavID();
                        } else {
                            if (cavCnt !== item.CavID()) {
                                DisplayError("error", "Cavities Mismatch", "Cavities do not match between order items", false);
                                break;
                            }
                        }
                    }
                }
            }, 5);
            //#endregion

            self.ResetEditItem();
        };

        // Save edit item into OrderItems
        $("#item_save").click(() => {
            saveOrderItem();
        });

        // Reset current edit item
        $("#item_clear").click(() => {
            self.ResetEditItem();
        });

        $("#row_orderitem_edit").on("keypress", ":input", e => {
            if (e.which !== 13)
                return;

            saveOrderItem();
        });
    }


    InitOverrideCharges(): void {
        var self = this;
        // Used to Override Value
        $("#pricinginfo").on("dblclick", ".override_charge", function() {
            if (IsValid(self.Order.invoicedate()) || !IsValid(self.Order.customercode())) {
                return;
            }

            var content = this;
            var $parent = $(this).parent();

            var originalID = $(this).attr('id'),
                inputName = "VALUE_" + $(this).attr("id"),
                inputStr = '<input type="text" id="' + inputName + '" style="width: 100%;" class="input_small selectall" placeholder="" data-bind="value: Override_' + $(content).data('bind').split(' ')[1].toLowerCase() + '" />';

            ko.cleanNode($("#" + originalID)[0]);
            $parent.html(inputStr);
            ko.applyBindings(self.Order, $("#" + inputName)[0]);
            $("#" + inputName).on("blur", function() {
                var key: string = $(content).data('bind').split(' ')[1].toLowerCase().replace(",", "");
                var val: string = $(this).val();

                console.log("Value: ");
                console.log($(this).val());
                console.log("Key = " + key);

                if (val === undefined || val === "") { // || val == null
                    self.Order['default' + key](true);
                } else {
                    self.Order['default' + key](false);
                    self.Order[key](Globalize.parseFloat($(this).val()));
                }

                $parent.html(content);
                ko.applyBindings(self.Order, $("#" + originalID)[0]);
            }).on("keypress", function(e) {
                var code = (e.keyCode ? e.which : e.keyCode);
                if (code == 13 || code == 27) {
                    $(this).trigger("blur");
                }
            }).focus();
        });
    }


    InitAddressModal(): void {
        var self = this;

        // Setup Display
        $(".addr_sel").click(function() {
            if (!self.Order.FieldsEnabled())
                return false;

            self.Order.TempAddressType($(this).attr('id'));

            $("#modal_address").modal("show");
            return true;
        });

        $(".save_addr").click(() => {
            var addresses = $.grep(self.Order.Customer().Addresses(), item => {
                if (item.name() === self.Order.TempAddressKey())
                    return true;
                return false;
            });

            if (addresses.length > 0)
                self.Order.Customer().SetAddress(self.Order, self.Order.TempAddressType().substr(0, 1), addresses[0]);
        });
    }


    InitCustomChargeModal(): void { // Setup Display
        var self = this;

        $("#item_addcust").click(() => {
            if (!self.Order.FieldsEnabled())
                return false;

            var tmpCharge: PriceListItemCharge = new PriceListItemCharge();
            delete tmpCharge.ChargePercentage;
            delete tmpCharge.ChargePrice;
            ko.mapping.fromJS(ko.mapping.toJS(tmpCharge), {}, self.Order.CustomCharge);

            $("#modal_CustomCharge").modal("show");
            return true;
        });

        $(".charge_save").click(() => {
            var tmpCharge: PriceListItemCharge = new PriceListItemCharge();
            tmpCharge.chargename(self.Order.CustomCharge().chargename());
            tmpCharge.chargepercentage(Globalize.parseFloat(self.Order.CustomCharge().ChargePercentage().toString()));
            tmpCharge.chargeprice(Globalize.parseFloat(self.Order.CustomCharge().ChargePrice().toString()));
            tmpCharge.Quantity(Globalize.parseFloat(self.Order.CustomCharge().Quantity().toString()));
            tmpCharge.maxqty(tmpCharge.chargepercentage() > 0 ? 1 : 9999);
            tmpCharge.minqty(1);
            tmpCharge.IsChecked(true);
            tmpCharge.chargetype("CUSTOM");

            self.Order.CurrentOrderItem().ChargeList().push(tmpCharge);
            self.Order.CurrentOrderItem().ChargeList.valueHasMutated();
        });
    }


    InitWeightModal(): void { // Setup Display
        var self = this;

        $(".weight_calc").click(() => {
            if (!self.Order.FieldsEnabled())
                return false;

            $("#modal_weightcalc").modal("show");
            return true;
        });
    }


    SaveOrder(print?: string, generateInvoiceNumber?: boolean): void {
        var self = this;

        if (!IsValid(print))
            print = "none";

        if (!IsValid(generateInvoiceNumber))
            generateInvoiceNumber = false;

        if (!self.shouldSave()) {
            var tmpOrders = [self.Order.ordernumber()];
            if (print !== "none") {
                $.ajax({
                        type: "POST",
                        url: '/api/OrderEntry/PrintOrders',
                        data: JSON.stringify({ "orders": tmpOrders, "mode": print }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json"
                    })
                    .done((response) => {
                        if (!response.isValid) {
                            DisplayError("error", "Error Saving", response.message);
                            return;
                        }

                        if (print === undefined || print === null || print === "none") {
                            window.location.href = OrderViewModel.RedirectURL;
                            return;
                        }
                    })
                    .fail((xhr) => {
                        DisplayError("error", "Error Saving", xhr.responseText);
                    });
            } else {
                window.location.href = OrderViewModel.RedirectURL;
            }

            return;
        }

        // Disallow clicking the save button
        self.Order.CurrentlySaving(true);

        //#region Fix-up to send to server
        // Copy Computed over Observables
        /*
        self.Order.sales(self.Order.Sales());
        self.Order.discountontotal(self.Order.DiscountOnTotal());
        self.Order.discountpercentage(self.Order.DiscountPercentage());
        self.Order.discountamount(self.Order.DiscountAmount());
        self.Order.steelsurcharge(self.Order.SteelSurcharge());
        self.Order.freight(self.Order.Freight());
        self.Order.fasttrackcharge(self.Order.FastTrackCharge());
        self.Order.total(self.Order.Total());
        self.Order.orderdate(self.Order.OrderDate());
        self.Order.countasorder(self.Order.CountAsOrder());
        */


        var ordernumber = IsValid(self.Order.ordernumber()) && !isNaN(self.Order.ordernumber()) && self.Order.ordernumber() > 0 ? self.Order.ordernumber() : -1;
        var tmpSteelSurcharge = self.Order.steelsurcharge();

        for (var oi = 0; oi < self.Order.OrderItems().length; oi++) {
            var item = self.Order.OrderItems()[oi];

            item.ordernumber(ordernumber);

            //#region Fix Steel Cost
            var itemSteelSurcharge = RoundNumber(item.steelsurchargeweight() * self.Order.steelrate(), OrderViewModel.DecimalPlaces);
            if (tmpSteelSurcharge < itemSteelSurcharge || oi == self.Order.OrderItems().length - 1) {
                itemSteelSurcharge = RoundNumber(tmpSteelSurcharge, OrderViewModel.DecimalPlaces);
            }

            item.steelcost(itemSteelSurcharge);
            tmpSteelSurcharge -= itemSteelSurcharge;
            //#endregion

            for (var c = 0; c < item.Charges().length; c++) {
                var charge = item.Charges()[c];

                charge.ordernumber(ordernumber);
                charge.line(item.line());
                charge.chargeline(c + 1);
                charge.price(charge.Price());
            }
        }
        //#endregion

        // Create copy of model to send to the server
        //   (NOTE: each class has a toJSON function to remove extra properties that should NOT be sent to the server)
        var tmpOrder = $.parseJSON(ko.toJSON(self.Order));
        tmpOrder.ordernumber = ordernumber;

        // Reset Nitride Qty to 1
        for (var i = 0; i < tmpOrder.OrderItems.length; i++) {
            var tmpItem: any = tmpOrder.OrderItems[i];
            for (var j = 0; j < tmpItem.Charges.length; j++) {
                var tmpCharge = tmpItem.Charges[j];
                if (tmpCharge.chargetype.toLowerCase() === 'nitride') {
                    tmpCharge.qty = 1;
                }
            }
        }

        $.ajax({
                type: "POST",
                url: '/api/OrderEntry/SaveOrder',
                data: JSON.stringify({ "order": tmpOrder, "print": print, "geninvoicenumber": generateInvoiceNumber }),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            })
            .done((response) => {
                // Allow order to be saved again
                self.Order.CurrentlySaving(false);

                if (!response.isValid) {
                    DisplayError("error", "Error Saving", response.message);
                    return;
                }

                console.log(response);
                console.log(response.ordernumber);

                console.log('Current order number: ' + self.Order.ordernumber());

                if (self.Order.ordernumber() <= 0) {
                    self.Order.ordernumber(response.ordernumber);
                }

                if (!IsValid(self.Order.invoicenumber()) && IsValid(response.invoicenumber)) {
                    self.Order.invoicenumber(response.invoicenumber);
                }

                if (print === undefined || print === null || print === "none") {
                    window.location.href = OrderViewModel.RedirectURL;
                    return;
                }
            })
            .fail((xhr) => {
                // Allow order to be saved again
                self.Order.CurrentlySaving(false);

                DisplayError("error", "Error Saving", xhr.responseText);
            });
    }


    PrintOrder(): void {
        var self = this;
        self.SaveOrder("order");
    }


    PrintInvoice(generateInvoiceNumber: boolean): void {
        var self = this;
        self.SaveOrder("invoice", generateInvoiceNumber);
    }


    ResetEditItem(): void {
        var self = this;
        var optionsPerPage = self.Order.CurrentOrderItem()._ItemsPerPage();
        self._OrderItemIndex = -1;

        var tmpItem = ko.toJS(new DOrderItem(self.Order));

        self.Order.CurrentOrderItem()._LoadPrice(false);
        ko.mapping.fromJS(tmpItem, { 'ignore': ['_order', 'DieType', 'SubTotal', 'ExtendedPrice', '_ItemsMatchingQuery', '_ItemsToDisplay', '_LastPage', 'CanSave'] }, self.Order.CurrentOrderItem);
        self.Order.CurrentOrderItem().line(self.Order.OrderItems().length + 1);

        self.Order.CurrentOrderItem()._ItemsPerPage(optionsPerPage);

        // Focus DieType (Run Async)
        setTimeout(() => {
            $("#item_dietype").focus();
            self.Order.CurrentOrderItem()._LoadPrice(true);
        }, 10);
    }


    UpdateDiscountPercentage(): void {
        var self = this;


        // If no rate supplied, then exit out of the routine.
        if (self.NCRRate() == "") {
            return;
        }


        if (self.Order.ncr() || self.Order.ncrs() || self.Order.zeroprice()) {
            $("#in_discountpercentage").attr("placeholder", "100T");
            $("#in_discountpercentage").val("").change();
            $("#in_nitriderate").val("0").change();
            $("#in_steelsurchargerate").val("0").change();
            $("#in_freightrate").val("0").change();
        } else {
            $("#in_discountpercentage").attr("placeholder", "0").change();
            $("#in_nitriderate").val("").change();
            $("#in_steelsurchargerate").val("").change();
            $("#in_freightrate").val("").change();
        }
    }
}


function RoundNumber(input: number, places: number): number {
    return Math.round(input * Math.pow(10, places)) / Math.pow(10, places);
}
// ReSharper restore InconsistentNaming