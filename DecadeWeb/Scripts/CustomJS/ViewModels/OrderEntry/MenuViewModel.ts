/// <reference path="..\..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\..\typings\jQuery.fileDownload\jQuery.fileDownload.d.ts" />
/// <reference path="..\..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\..\DecadeClasses.ts" />
/// <reference path=".\Common.ts" />
/// <reference path="..\..\ExcoCommon.ts" />
class OrderEntryMenuViewModel {
    private OrdersTable = $("#orders");
    private OrdersRows = $("#orders tbody tr");
    private lastSelectedRowIndex: number = -1;

    public Orders: KnockoutObservableArray<MenuOrder> = ko.observableArray<MenuOrder>();
    public ordercount: KnockoutObservable<string> = ko.observable("");
    public title: KnockoutObservable<string> = ko.observable("");
    public numberfield: KnockoutObservable<string> = ko.observable("");
    public datetitle: KnockoutObservable<string> = ko.observable("");
    public datefield: KnockoutObservable<string> = ko.observable("");

    private _CurrentPage: KnockoutObservable<number> = ko.observable(1);
    private _LastPage: KnockoutComputed<number>;
    private _SearchText: KnockoutObservable<string> = ko.observable("");
    public _ItemsMatchingQuery: KnockoutComputed<MenuOrder[]>;
    public _ItemsToDisplay: KnockoutComputed<MenuOrder[]>;
    private _ItemsPerPageOptions: KnockoutObservableArray<number> = ko.observableArray([25, 50, 100, 200]);
    private _ItemsPerPage: KnockoutObservable<number> = ko.observable(this._ItemsPerPageOptions()[0]);
    private _FirstDispPage: KnockoutObservable<number> = ko.observable(1);
    private _LastDispPage: KnockoutObservable<number> = ko.observable(1);

    private _SortingColumn: KnockoutObservable<string> = ko.observable("");
    private _SortingDirection: KnockoutObservable<boolean> = ko.observable(false);


    constructor() {
        var self = this;

        self.init();

        self._CurrentPage.subscribe(function(newValue) {
            var first = newValue - 2,
                last = newValue + 2;

            if (first < 1) {
                first = 1;
                last = self._LastPage() > 5 ? 5 : self._LastPage();
            } else if (last > self._LastPage()) {
                last = self._LastPage();
                first = self._LastPage() > 5 ? self._LastPage() - 4 : 1;
            }

            self._FirstDispPage(first);
            self._LastDispPage(last);
        });

        self._ItemsPerPage.subscribe(function() {
            self._CurrentPage.valueHasMutated();
        });

        ko.applyBindings(self);
    }

    //#region Initalization Functions
    init(): void {
        this.initMenu();
        this.initTable();

        this.LoadData();
    }

    initMenu(): void {
        var self = this;

        $("#neworder").click(function() {
            GoToEditOrder(null, true);
        });

        $("#edit").click(function() {
            if (!GoToEditOrder(self.GetFirstSelectedOrder(), false)) {
                GetSONumber("Enter order number to edit: ", function(order) { GoToEditOrder(order); });
            }
        });

        $("#editbyso").click(function() {
            GetSONumber("Enter order number to edit: ", function(order) { GoToEditOrder(order); });
        });

        $("#print, #printinvoice").click(function() {
            var mode = "Order";

            if ($(this).attr("id").toString().toLowerCase() == "printinvoice")
                mode = "Invoice";

            self.PrintReport(mode);
        });

        $("#pdforder, #pdfinvoice").click(function() {
            var mode = "Order";

            if ($(this).attr("id").toString().toLowerCase() == "pdfinvoice")
                mode = "Invoice";

            self.DownloadReport(mode);
        });

        $("#track").click(function() {
            if (!GoToTrackOrder(self.GetFirstSelectedOrder())) {
                GetSONumber("Enter order number to track: ", function(order) { GoToTrackOrder(order); });
            }
        });

        $("#schedule").click(function() {
            var date = new Date();
            date.setDate(date.getDate() + 8);
            GoToSchedule("Solid", date);
        });

        $("#copyorder").click(function() {
            // Add Functionality
        });

        $("#confirmations").click(function() {
            window.open("/OrderEntry/Confirmations", "Confirmations", "");
        });

        $("#editcustomers").click(function() {
            return;
            // window.location.href = "/OrderEntry/CustomerList";
        });


// Add handlers for filter to reload data
        $("input[type=radio][name='filtertype'], input[type=radio][name='filterduration']").change(function() {
            self.LoadData();
        });
    }

    initTable(): void {
        var self = this;

        //#region Table Functions
        $("#orders > tbody").on("click", "tr", function(e: JQueryEventObject) {
            // Ignore the Click on the Checkbox
            if ($(e.target).prop("tagName").toUpperCase() === 'INPUT')
                return;

            if (e.shiftKey && self.lastSelectedRowIndex != -1) {
                var curIndex = $(this).closest("tr").prevAll().length;

                // Select All Rows in-between
                var start = 0;
                var stop = 0;

                if (curIndex > self.lastSelectedRowIndex) {
                    start = self.lastSelectedRowIndex + 1;
                    stop = curIndex - 1;
                } else {
                    start = curIndex + 1;
                    stop = self.lastSelectedRowIndex - 1;
                }

                var i = 0;
                $("#orders > tbody > tr").each(function() {
                    if (i >= start && i <= stop) {
                        var chkBox = $(this).find("input[type=checkbox]");
                        chkBox.prop("checked", "checked");
                        ko.contextFor(chkBox[0]).$data.Selected(true);
                    }

                    i++;
                });
                // Done Selecting All Rows in-between

                self.lastSelectedRowIndex = curIndex;
            } else {
                if ($(this).closest("tr").find(':checked').length < 1)
                    self.lastSelectedRowIndex = $(this).closest("tr").prevAll().length;
                else
                    self.lastSelectedRowIndex = -1;
            }
        });

        // Enable Double Clicking to Edit Order
        $("#orders > tbody").on("dblclick", "tr", function(e) {
            var oNum = Number($(this).attr('id').toString().replace("row", ""));
            GoToEditOrder(oNum);
        });
        //#endregion

        //#region Searchable Table
        self._ItemsMatchingQuery = ko.computed({
            read: function() {
                var rtnVal: MenuOrder[] = self.Orders().slice(0, self.Orders().length);

                if (self._SearchText() != '') {
                    rtnVal = $.grep(rtnVal, function(value) {
                        var searchText = self._SearchText().toLowerCase();
                        if (value[self.numberfield()]().toString().toLowerCase().indexOf(searchText) > -1)
                            return true;

                        if (value.CustomerName().toLowerCase().indexOf(searchText) > -1)
                            return true;

                        if (value.customercode().toLowerCase().indexOf(searchText) > -1)
                            return true;

                        return false;
                    });
                }

                // Sorting
                if (IsValid(self._SortingColumn(), true)) {
                    var column: string = "";
                    var isAsc: boolean = self._SortingDirection();
                    var ascDir: number = isAsc ? -1 : 1;
                    var descDir: number = isAsc ? 1 : -1;

                    switch (self._SortingColumn()) {
                    default:
                        column = self.numberfield();
                        break;

                    case "code":
                        column = "customercode";
                        break;

                    case "name":
                        column = "CustomerName";
                        break;

                    case "date":
                        column = self.datefield();
                        break;
                    }

                    rtnVal.sort(function(left, right) {
                        left = left[column]();
                        right = right[column]();
                        return left === right ? 0 : left < right ? ascDir : descDir;
                    });
                }

                return rtnVal;
            },
            write: function() {
                // Do Nothing
            }
        });

        self._ItemsToDisplay = ko.computed({
            read: function() {
                var rtnVal: MenuOrder[] = self._ItemsMatchingQuery();

                var startIndex = (self._CurrentPage() - 1) * self._ItemsPerPage();
                rtnVal = rtnVal.slice(startIndex, startIndex + self._ItemsPerPage());

                return rtnVal;
            },
            write: function() {
                // Do Nothing
            }
        });

        self._LastPage = ko.computed({
            read: function() {
                return Math.ceil(self._ItemsMatchingQuery().length / self._ItemsPerPage());
            },
            write: function() {
                // Do Nothing
            }
        });

        self._SearchText.subscribe(function() {
            self._CurrentPage(1);
        });

        self._ItemsPerPage.subscribe(function() {
            self._CurrentPage(1);
        });
        //#endregion
    }

//#endregion

    //#region Pagination
    FirstPage(): void {
        var self = this;
        self._CurrentPage(1);
    }

    NextPage(): void {
        var self = this;

        var curPage = self._CurrentPage();

        if (curPage < self._LastPage())
            self._CurrentPage(curPage + 1);
        else
            self._CurrentPage(self._LastPage());
    }

    GoToPage(data): void {
        var self = this;
        self._CurrentPage(data);
    }

    PrevPage(): void {
        var self = this;

        var curPage = self._CurrentPage();

        if (curPage > 1)
            self._CurrentPage(curPage - 1);
        else
            self._CurrentPage(1);
    }

    LastPage(): void {
        var self = this;
        self._CurrentPage(self._LastPage());
    }

    UpdateDisplay(): void {
        var self = this;

        var rtnVal: MenuOrder[] = self.Orders().slice(0, self.Orders().length - 1);

        if (self._SearchText() != '') {
            rtnVal = $.grep(rtnVal, function(value: any) { // TODO: Figure out what type this is
                return value.chargename().toLowerCase().indexOf(self._SearchText().toLowerCase()) > -1;
            });
        }

        var startIndex = (self._CurrentPage() - 1) * self._ItemsPerPage();
        rtnVal = rtnVal.slice(startIndex, startIndex + self._ItemsPerPage());

        self._ItemsToDisplay(rtnVal);
    }

//#endregion

    LoadData(): void {
        // Remove Orders
        var self = this;

        self.Orders.removeAll();

        // Show Working Animation
        // $("#ordercount").html('<img src="/images/loading.gif" width="20px" height="20px" alt="Loading..." />');

        var filter = $("#orderfilter :radio:checked").attr("value"),
            duration = $("#durationfilter :radio:checked").attr("value");

        // Load in New Content
        $.ajax({
            type: "GET",
            url: "/api/Order/GetOrderListing",
            data: { "orderstatus": filter, "duration": duration },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                self.ordercount(data.OrderCountTitle);
                self.title(data.Title);
                self.numberfield(data.NumberField);
                self.datetitle(data.DateTitle);
                self.datefield(data.DateField);
                ko.mapping.fromJS(data.Orders, { create: function(options) { return new MenuOrder(options); } }, self.Orders);
                self._CurrentPage.valueHasMutated();
            }
        });
    }

    PrintReport(mode: string): void {
        var self = this;
        var data = { "mode": mode, "orders": self.GetAllSelectedOrders() };

        $.ajax({
            type: "POST",
            url: "/api/OrderEntry/PrintOrders",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            success: function(msg) {
                // Clear Selected Orders

                console.log(msg.Orders);

                if (!msg.isValid) {
                    DisplayError("error", "Error Printing (At Server)", msg.message);
                    return;
                }

                self.ClearSelected();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                DisplayError("error", "Error Printing (At Client)", xhr.responseText);
            }
        });
    }

    DownloadReport(mode: string): void {
        var self = this;
        var data = { "mode": mode, "orders": self.GetAllSelectedOrders() };


        $.fileDownload("/api/OrderEntry/DownloadOrders", {
                data: data
            })
            .done(() => {
                alert('File download a success!');
                self.ClearSelected();
            })
            .fail((xhr) => {
                DisplayError("error", "Download Error", "There was an error downloading the file.  " + xhr.responseText);
            });
    }

    GetFirstSelectedOrder(): number {
        var self = this;

        var order = ko.utils.arrayFirst(self.Orders(), function(item) { return item.Selected(); });

        if (IsValid(order))
            return order.ordernumber();

        return -1;
    }

    GetAllSelectedOrders(): number[] {
        var self = this;

        var rtnVal: number[] = [];

        var orders = ko.utils.arrayFilter(self.Orders(), function(item) { return item.Selected(); });

        if (IsValid(orders))
            ko.utils.arrayForEach(orders, function(item) { rtnVal.push(item.ordernumber()); });

        return rtnVal;
    }

    ClearSelected(): void {
        var self = this;

        ko.utils.arrayForEach(self.Orders(), function(item) {
            item.Selected(false);
        });
    }

    SortTable(header): void {
        var self = this;

        if (self._SortingColumn() !== header) {
            self._SortingColumn(header);
            self._SortingDirection(true);
        } else {
            if (self._SortingDirection())
                self._SortingDirection(false);
            else {
                self._SortingColumn("");
                self._SortingDirection(true);
            }
        }
    }
}

class MenuOrder extends d_order {
    public CustomerName: KnockoutObservable<string> = ko.observable("");
    public Selected: KnockoutObservable<boolean> = ko.observable(false);

    constructor(options?) {
        super();

        var self = this;

        if (IsValid(options)) {
            ko.mapping.fromJS(options.data, {}, self);
        }
    }
}