/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
/// <reference path="..\ExcoCommon.ts" />
var TrackingViewModel = (function () {
    function TrackingViewModel(ordernumber) {
        this.CanSubscribe = ko.observable(false);
        this.IsSubscribed = ko.observable(false);
        this.EmailAlerts = ko.observable(false);
        var self = this;
        self.Ordernumber = ordernumber;
        $.ajax({
            type: "GET",
            url: "/api/Order/GetSubscribeInfo",
            data: ko.mapping.toJS({ ordernumber: self.Ordernumber })
        })
            .done(function (data) {
            self.ProcessSubscribeInfo(data);
        });
        ko.applyBindings(self);
    }
    TrackingViewModel.prototype.Subscribe = function (option) {
        var self = this;
        $.ajax({
            type: "GET",
            url: "/api/Order/ManageOrderWatch",
            data: ko.mapping.toJS({ ordernumber: self.Ordernumber, options: option })
        })
            .done(function (data) {
            self.ProcessSubscribeInfo(data);
        });
    };
    TrackingViewModel.prototype.ProcessSubscribeInfo = function (subscribeinfo) {
        var self = this;
        // console.log(subscribeinfo);
        self.CanSubscribe(subscribeinfo.cansubscribe);
        self.IsSubscribed(subscribeinfo.issubscribed);
        self.EmailAlerts(subscribeinfo.emailalerts);
    };
    return TrackingViewModel;
}());
//# sourceMappingURL=TrackingViewModel.js.map