/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
/// <reference path="..\ExcoCommon.ts" />
class TrackingViewModel {
    public CanSubscribe: KnockoutObservable<boolean> = ko.observable(false);
    public IsSubscribed: KnockoutObservable<boolean> = ko.observable(false);
    public EmailAlerts: KnockoutObservable<boolean> = ko.observable(false);

    public Ordernumber: number;

    constructor(ordernumber: number) {
        var self = this;

        self.Ordernumber = ordernumber;

        $.ajax({
                type: "GET",
                url: "/api/Order/GetSubscribeInfo",
                data: ko.mapping.toJS({ ordernumber: self.Ordernumber })
            })
            .done(function(data) {
                self.ProcessSubscribeInfo(data);
            });

        ko.applyBindings(self);
    }

    public Subscribe(option: string): void {
        var self = this;

        $.ajax({
                type: "GET",
                url: "/api/Order/ManageOrderWatch",
                data: ko.mapping.toJS({ ordernumber: self.Ordernumber, options: option })
            })
            .done(function(data) {
                self.ProcessSubscribeInfo(data);
            });
    }

    private ProcessSubscribeInfo(subscribeinfo: any) {
        var self = this;
        // console.log(subscribeinfo);

        self.CanSubscribe(subscribeinfo.cansubscribe);
        self.IsSubscribed(subscribeinfo.issubscribed);
        self.EmailAlerts(subscribeinfo.emailalerts);
    }
}