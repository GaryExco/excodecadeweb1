/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
/// <reference path="..\ExcoCommon.ts" />
var ManageHoldViewModel = (function () {
    function ManageHoldViewModel() {
        this.orders = ko.observable("");
        this.mode = ko.observable("");
        this.IsSubmitting = ko.observable(false);
        var self = this;
        // Used to disable form submission (which reloads the page)
        $("#holdform input").keypress(function (e) {
            return e.keyCode !== 13;
        });
        $("#processhold").click(function () {
            self.Submit();
        });
        self.orders.extend({ required: true });
        self.mode.extend({ required: true });
        self.errors = ko.validation.group(self);
        self.CanSubmit = ko.computed({
            read: function () {
                return !self.IsSubmitting() && self.errors().length === 0;
            },
            owner: self,
            deferEvaluation: true
        });
        ko.applyBindings(self);
    }
    ManageHoldViewModel.prototype.Submit = function () {
        var self = this;
        if (self.errors().length > 0)
            return;
        self.IsSubmitting(true);
        $.ajax({
            type: "GET",
            url: "/api/OrderEntry/ManageOrderHoldStatus",
            data: ko.mapping.toJS(self, { 'ignore': ['IsSubmitting', 'CanSubmit', 'errors', 'isValid', 'Submit', 'isAnyMessageShown'] })
        })
            .done(function (data) {
            if (data.IsValid) {
                DisplayError("success", "Done", "Processed successfully.");
            }
            else {
                for (var i = 0; i < data.errors.length; i++) {
                    DisplayError("error", "Error", data.errors[i], false);
                }
            }
        })
            .fail(function () {
            DisplayError("error", "Error processing", "message");
        })
            .always(function () {
            self.IsSubmitting(false);
            self.orders("");
            self.mode("");
            $("#ordernumbers").focus();
            self.errors.showAllMessages(false);
        });
    };
    return ManageHoldViewModel;
}());
//# sourceMappingURL=ManageHoldViewModel.js.map