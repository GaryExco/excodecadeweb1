/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
class NonConformanceViewModel extends scraplog {
    //#region Variables
    public errors: KnockoutValidationErrors;

//#endregion

    constructor() {
        super();
        // Setup Validation
        var self = this;

        self.sonum.extend({
            required: true,
            minLength: 5,
            ajax: "/api/Order/IsValid"
        });

        self.empnum.extend({
            required: true,
            minLength: 5,
            maxLength: 5,
            ajax: "/api/User/IsValid"
        });

        self.department.extend({
            required: true
        });

        self.part.extend({
            required: true
        });

        self.Errorcode.extend({
            required: true
        });

        self.correctioncode.extend({
            required: true
        });

        self.problem.extend({
            required: true
        });

        self.errors = ko.validation.group(self);

        ko.applyBindings(this);
    }
}