var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
var NonConformanceViewModel = (function (_super) {
    __extends(NonConformanceViewModel, _super);
    //#endregion
    function NonConformanceViewModel() {
        var _this = _super.call(this) || this;
        // Setup Validation
        var self = _this;
        self.sonum.extend({
            required: true,
            minLength: 5,
            ajax: "/api/Order/IsValid"
        });
        self.empnum.extend({
            required: true,
            minLength: 5,
            maxLength: 5,
            ajax: "/api/User/IsValid"
        });
        self.department.extend({
            required: true
        });
        self.part.extend({
            required: true
        });
        self.Errorcode.extend({
            required: true
        });
        self.correctioncode.extend({
            required: true
        });
        self.problem.extend({
            required: true
        });
        self.errors = ko.validation.group(self);
        ko.applyBindings(_this);
        return _this;
    }
    return NonConformanceViewModel;
}(scraplog));
//# sourceMappingURL=NonConformance.js.map