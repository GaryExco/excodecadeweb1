/// <reference path="..\..\typings\jquery\jquery.d.ts" />
/// <reference path="..\..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\..\typings\knockout\knockout.d.ts" />
/// <reference path="..\..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\..\typings\globalize\globalize.d.ts" />
/// <reference path="..\..\typings\ExcoGlobals.ts" />
/// <reference path="..\DecadeClasses.ts" />
class HotListViewModel extends d_ordernotes {
    public hotlistdate: KnockoutObservable<Date> = ko.observable<Date>();

    public errors: KnockoutValidationErrors;

    constructor() {
        super();

        var self = this;

        self.ordernumber.extend({
            required: true,
            minLength: 5,
            maxLength: 6,
            ajax: "/api/Order/IsValid"
        });

        self.errors = ko.validation.group(self);

        ko.applyBindings(this);
    }
}