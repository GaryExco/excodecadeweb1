﻿function EnterAsATab($) {
    var focusable = ":input, a[href]";

    EnterAsTab = function(event) {
        //Get the element that registered the event
        var $target = $(event.target);

        if (isEnterKey(event)) {
            var isTabSuccessful = tab(true, event.shiftKey, $target);

            if (isTabSuccessful) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                return false;
            }
        }
    };

    function RegisterEnterAsTab() {
        //on adds a handler to the object.  In this case it is the document itself
        console.log("Registering EnterAsTab");
        $(document).on("keydown", EnterAsTab);
    }

    function isEnterKey(event) {

        if (!event.altKey && !event.ctrlKey && !event.metaKey && event.keyCode == 13) {
            return true;
        }

        return false;
    }

    function tab(isTab, isReverse, $target) {
        if (isReverse) {
            return performTab($target, -1);
        } else {
            return performTab($target, +1);
        }
    }

    function performTab($from, offset) {
        var $next = findNext($from, offset);
        $next.focus();

        return true;
    }

    function findNext($from, offset) {
        var $focusable = $(focusable).not(":disabled").not(":hidden").not("a[href]:empty");

        var currentIndex = $focusable.index($from);

        var nextIndex = (currentIndex + offset) % $focusable.length;

        var $next = $focusable.eq(nextIndex);

        return $next;
    }

    RegisterEnterAsTab();
}