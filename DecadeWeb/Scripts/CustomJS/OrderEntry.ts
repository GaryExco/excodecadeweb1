/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\typings\knockout\knockout.d.ts" />
/// <reference path="..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\typings\globalize\globalize.d.ts" />
/// <reference path="..\typings\ExcoGlobals.ts" />
/// <reference path="DecadeClasses.ts" />
class Order extends d_order {

}


// Main Order Entry Screen
function showOrderForm(orderToDisp: string): void {
    if (orderToDisp != null) {
        window.location.href = "/OrderEntry/OrderInfo.aspx?ordernumber=" + orderToDisp;
    } else {
        window.location.href = "/OrderEntry/OrderInfo.aspx";
    }
}

function GetFirstSelectedOrder(): string {
    var rtnVal = "";
    var checkboxes: JQuery = $("#orders input:checkbox");
    for (var i = 0; i < checkboxes.length; i++) {
        if ((<HTMLInputElement>checkboxes[i]).checked) {
            rtnVal = $(checkboxes[i]).attr("id").toString().replace("chk", "");
            break;
        }
    }

    return rtnVal;
}

function GetAllSelectedOrders(): string[] {
    var rtnVal = new Array();
    var checkboxes = $("#orders input:checkbox");
    for (var i = 0; i < checkboxes.length; i++) {
        if ((<HTMLInputElement>checkboxes[i]).checked) {
            rtnVal[rtnVal.length] = $(checkboxes[i]).attr("id").toString().replace("chk", "");
        }
    }

    return rtnVal;
}