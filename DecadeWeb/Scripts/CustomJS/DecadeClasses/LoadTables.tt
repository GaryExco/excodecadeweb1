﻿<#@ template language="C#" debug="true" inherits="Microsoft.VisualStudio.TextTemplating.VSHost.ModelingTextTransformation" #>
<#@ assembly name="System.Data" #>
<#@ assembly name="System.Xml" #>
<#@ import namespace="System" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Data" #>
<#@ import namespace="System.Data.Common" #><#+
    // Provider to Use
    string ProviderString = "System.Data.SqlClient";

    // Connection String
    string ConnectionString = "Data Source=192.168.1.7;Initial Catalog=DecadeTest;User Id=jamie;Password=jamie;Application Name=MSET;Timeout=120;";
    
    Dictionary<string, Table> Tables = new Dictionary<string, Table>();

    void LoadTables()
    {
        DbConnection dbConn = null;
        DbCommand dbCmd = null;
        DbDataReader dbReader = null;
        DbDataAdapter dbAdapter = null;
        DataTable dt = new DataTable();

        try
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(ProviderString);
            dbConn = factory.CreateConnection();
            dbCmd = dbConn.CreateCommand();

            dbConn.ConnectionString = ConnectionString;
            dbConn.Open();

            dbCmd.CommandText = "SELECT * FROM sysobjects WHERE (xtype = 'U' OR xtype = 'V' OR xtype = 'IF') ORDER BY name";
            dbAdapter = factory.CreateDataAdapter();
            dbAdapter.SelectCommand = dbCmd;
            dbAdapter.Fill(dt);
            dbAdapter.Dispose();

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Table table = new Table();
                    table.Name = dt.Rows[i]["name"].ToString();

                    if (dt.Rows[i]["xtype"].ToString() == "IF")
                        dbCmd.CommandText = String.Format("SELECT *, 0 AS IS_IDENTITY FROM Information_Schema.Routine_Columns WHERE TABLE_NAME = '{0}' ORDER BY ORDINAL_POSITION", table.Name);
                    else
                        dbCmd.CommandText = String.Format("SELECT *, COLUMNPROPERTY(OBJECT_ID(TABLE_NAME), COLUMN_NAME, 'IsIdentity') AS IS_IDENTITY FROM Information_Schema.Columns WHERE TABLE_NAME = '{0}' ORDER BY ORDINAL_POSITION", table.Name);
                    dbReader = dbCmd.ExecuteReader();

                    if (dbReader.HasRows)
                    {
                        while (dbReader.Read())
                        {
                            Column column = new Column();
                            column.Name = dbReader.GetString(dbReader.GetOrdinal("COLUMN_NAME"));
                            column.HasDefault = !dbReader.IsDBNull(dbReader.GetOrdinal("COLUMN_DEFAULT")) || dbReader.GetInt32(dbReader.GetOrdinal("IS_IDENTITY")) == 1;
                            column.IsNullable = dbReader["IS_NULLABLE"].ToString().Trim().ToUpper() == "YES";
                            column.TypeString = GetDataType(dbReader["DATA_TYPE"], column.IsNullable, column.HasDefault);

                            table.Columns.Add(column.Name.ToLower(), column);
                        }
                    }
                    dbReader.Dispose();

                    Tables.Add(table.Name.ToLower(), table);
                }
            }
        }
        catch (System.Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (dt != null)
                dt.Dispose();

            if (dbAdapter != null)
                dbAdapter.Dispose();

            if (dbReader != null)
                dbReader.Dispose();

            if (dbConn != null)
                dbConn.Dispose();
        }
    }

    class Table
    {
        public bool Ignore { get; set; }

        public string Name { get; set; }

        public Dictionary<string, Column> Columns { get; set; }

        public Table()
        {
            Columns = new Dictionary<string, Column>();
        }

        public override string ToString()
        {
            if (Ignore)
                return "";

            // Build Column Properties
            StringBuilder columnString = new StringBuilder();
            foreach (KeyValuePair<string, Column> curColumn in Columns)
            {
                columnString.Append(curColumn.Value.ToString());
            }

			columnString.Append("\r\n\ttoJSON() {");
			columnString.Append("\r\n\t\tvar copy = ko.toJS(this);");
			columnString.Append("\r\n\t\tif (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {");
			columnString.Append("\r\n\t\t\treturn null;");
			columnString.Append("\r\n\t\t}");
			columnString.Append("\r\n\t\tdelete copy.errors;");
			columnString.Append("\r\n\t\tdelete copy.group;");
			columnString.Append("\r\n\t\tdelete copy.constructor;");
			columnString.Append("\r\n\t\treturn copy;");
			columnString.Append("\r\n\t}");

            return String.Format("\r\nclass {0}" +
            "\r\n{{" +
            "{1}" +
            "\r\n}}\r\n", Name, columnString);
            /*
            if (ColumnType != string.Empty)
                return "public " + ColumnType + " " + ColumnName + " { get; set; }";
            else
                return string.Empty;
            */
        }
    }

    class Column
    {
        public string Name { get; set; }

        public bool Ignore { get; set; }

        public bool IsID { get; set; }

        public bool HasDefault { get; set; }

        public bool IsNullable { get; set; }

        public string TypeString { get; set; }

        public override string ToString()
        {
            if (TypeString == string.Empty)
                return "";

            StringBuilder rtnVal = new StringBuilder();

            rtnVal.Append(String.Format("\r\n\tpublic {0}: KnockoutObservable<{1}> = ko.observable<{1}>();", Name, TypeString));

            return rtnVal.ToString();
        }
    }

    string GetDataType(object colType, bool IsNullable, bool HasDefault)
    {
        string ColumnType = colType.ToString().Trim().ToLower();

        switch (ColumnType)
        {
            case "bigint":
                ColumnType = "number";
                break;
            case "binary":
                ColumnType = "Byte[]";
                break;
            case "bit":
                ColumnType = "boolean";
                break;
            case "char":
                ColumnType = "string";
                break;
            case "datetime":
                ColumnType = "Date";
                break;
            case "decimal":
                ColumnType = "number";
                break;
            case "float":
                ColumnType = "number";
                break;
            case "int":
                ColumnType = "number";
                break;
            case "money":
                ColumnType = "number";
                break;
            case "nchar":
                ColumnType = "string";
                break;
            case "numeric":
                ColumnType = "number";
                break;
            case "nvarchar":
                ColumnType = "string";
                break;
            case "real":
                ColumnType = "number";
                break;
            case "smallint":
                ColumnType = "number";
                break;
            case "text":
                ColumnType = "string";
                break;
            case "tinyint":
                ColumnType = "number";
                break;
            case "varbinary":
                ColumnType = "Byte[]";
                break;
            case "xml":
                ColumnType = "string";
                break;
            case "varchar":
                ColumnType = "string";
                break;
            case "smalldatetime":
                ColumnType = "Date";
                break;
            case "image":
                ColumnType = "byte[]";
                break;
            default:
                ColumnType = string.Empty;
                break;
        }

        if (ColumnType != string.Empty)
        {
            string nullableFlag = (IsNullable ? "?" : String.Empty);

            // Detect If Most Likely an ID field
            if (nullableFlag == String.Empty && HasDefault)
            {
                switch (ColumnType.ToLower())
                {
                    case "int":
                    case "int16":
                    case "int64":
                        nullableFlag = "?";
                        break;
                }
            }

            return ColumnType;
        }
        else
            return string.Empty;
    }
#>