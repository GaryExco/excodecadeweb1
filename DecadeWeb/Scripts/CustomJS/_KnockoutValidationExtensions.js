/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\typings\knockout\knockout.d.ts" />
/// <reference path="..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\typings\globalize\globalize.d.ts" />
/// <reference path=".\ExcoCommon.ts" />
// Rules
ko.validation.rules['greaterThan'] = {
    validator: function (val, otherVal) {
        return Globalize.parseFloat(val.toString()) > Globalize.parseFloat(otherVal.toString());
    },
    message: "The field must be greater than Diameter"
};
ko.validation.rules['isFloat'] = {
    validator: function (val) {
        if (!IsValid(val, true))
            return true;
        return !isNaN(Globalize.parseFloat(val.toString()));
    },
    message: "The field must be a float."
};
ko.validation.rules['isInt'] = {
    validator: function (val) {
        if (!IsValid(val, true))
            return true;
        return !isNaN(Globalize.parseInt(val.toString()));
    },
    message: "The field must be an integer."
};
ko.validation.rules['ajax'] = {
    async: true,
    request: new Array(),
    timeouts: new Array(),
    validator: function (val, parms, callback) {
        var self = this;
        var ajaxKey = 'key';
        var tmpVal = { "id": val };
        var url = parms;
        if (parms != null && typeof (parms) === 'object') {
            url = parms.url;
            tmpVal = parms.val;
        }
        if (parms.field != null) {
            if (parms.id != null) {
                ajaxKey = parms.field + parms.id;
            }
            else {
                ajaxKey = parms.field + '_new';
            }
        }
        if (self.request[ajaxKey] != null) {
            self.request[ajaxKey].abort();
        }
        var ajaxDefaults = {
            url: url,
            type: 'GET',
            success: callback,
            data: tmpVal
        };
        // var options = $.extend(ajaxDefaults, parms);
        clearTimeout(self.timeouts[ajaxKey]);
        self.timeouts[ajaxKey] = setTimeout(function () {
            if (self.request[ajaxKey] != null) {
                self.request[ajaxKey].abort();
            }
            self.request[ajaxKey] = $.ajax(ajaxDefaults);
        }, 500);
    },
    message: "The field is invalid."
};
// Validation Setup
var koSettings = {
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null,
    decorateInputElement: true,
    errorElementClass: 'error'
};
ko.validation.init(koSettings, true);
// Binding handlers
ko.bindingHandlers['datepicker'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var $element = $(element);
        var observable = valueAccessor();
        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            observable($(element).datepicker("getDate"));
        });
        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).datepicker("destroy");
        });
        var val = $element.val();
        if (val !== undefined && val !== null)
            setTimeout(function () { observable(Globalize.parseDate(val)); }, 0);
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var isDate = (value instanceof Date) ? true : false;
        //handle date data coming via json from Microsoft
        if (String(value).indexOf('/Date(') == 0) {
            value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")));
        }
        if (!isDate && value !== undefined && value !== null) {
            value = Globalize.parseDate(value, "yyyy-MM-ddTHH:mm:ss");
        }
        var current = $(element).datepicker("getDate");
        if (!isDate || value - current !== 0) {
            $(element).datepicker("setDate", value);
        }
    }
};
ko.bindingHandlers['valueplaceholder'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var observable = valueAccessor();
        var $element = $(element);
        var dataType = $element.data("type");
        if (!IsValid(dataType)) {
            dataType = "string";
        }
        var GetValue = function () {
            var val;
            if ($element.val() == '')
                val = $element.attr('placeholder');
            else
                val = $element.val();
            return GetAsDataType(val);
        };
        var GetAsDataType = function (val) {
            if (IsValid(val)) {
                if (IsValid(dataType)) {
                    if (dataType.toUpperCase() === "number".toUpperCase()) {
                        val = Globalize.parseFloat(val.toString());
                        if (isNaN(val))
                            val = null;
                    }
                    else if (dataType.toUpperCase() === "int".toUpperCase()) {
                        val = Globalize.parseInt(val.toString());
                        if (isNaN(val))
                            val = null;
                    }
                    else if (dataType.toUpperCase() === "date".toUpperCase()) {
                        val = Globalize.parseDate(val.toString());
                    }
                }
            }
            else {
                if (IsValid(dataType)) {
                    if (dataType.toUpperCase() === "number".toUpperCase() || dataType.toUpperCase() === "int".toUpperCase())
                        val = null;
                }
            }
            return val;
        };
        var interceptor = ko.computed({
            read: function () {
                var val = GetValue();
                var def = GetAsDataType($element.attr('placeholder'));
                /*
                if (IsValid(dataType) && dataType.toLowerCase() === "date") {
                    return Globalize.format(val, "d");
                }

                // This is because on the initial load it was removing values from steelrate
                //   If we can get the code to work and not remove the value then it would be fine.
                return observable();
                */
                var valsAreEqual = false;
                var formatPattern;
                if (val instanceof Date) {
                    formatPattern = Globalize.culture().calendars.standard.patterns.d;
                    valsAreEqual = +val - def === 0; // val.getTime() === def.getTime();
                }
                else {
                    formatPattern = "n2";
                    valsAreEqual = val === def;
                }
                if (!valsAreEqual) {
                    return Globalize.format(val, formatPattern);
                }
                else
                    return null;
            },
            write: function (newValue) {
                var printDebug = false;
                var cur = $element.data('prevvalue');
                var val = GetValue();
                var def = GetAsDataType($element.attr('placeholder'));
                if (!IsValid(val, true)) {
                    val = def;
                }
                // console.log($element.attr("id"));
                if (printDebug) {
                    console.log(val);
                    console.log(def);
                }
                if (val !== cur) {
                    // console.log("Updating Value");
                    observable(val);
                }
                else {
                    if (IsValid(val)) {
                        if (newValue !== val.toString()) {
                            // console.log("Notifying Change");
                            observable.valueHasMutated();
                        }
                    }
                }
                $element.data('prevvalue', val);
            }
        });
        var tmp = GetAsDataType($element.val());
        if (IsValid(tmp)) {
            interceptor(tmp);
        }
        else {
            tmp = GetValue();
            if (IsValid(tmp))
                interceptor(tmp);
        }
        // Use Normal Value binding, but on our generated observable
        ko.applyBindingsToNode(element, { value: interceptor }, viewModel);
        // Reset Default Value
        if (dataType.toLowerCase() !== "date") {
            setTimeout(function () { $element.val(tmp).change(); }, 0);
        }
    }
};
ko.bindingHandlers['datavalue'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var observable = valueAccessor();
        var $element = $(element);
        var GetAsDataType = function (val) {
            var dataType = $element.data("type");
            if (IsValid(val)) {
                if (IsValid(dataType)) {
                    if (!(val instanceof String))
                        val = String(val);
                    if (dataType.toUpperCase() === "number".toUpperCase()) {
                        val = Globalize.parseFloat(val.toString());
                        if (isNaN(val))
                            val = null;
                    }
                    else if (dataType.toUpperCase() === "int".toUpperCase()) {
                        val = Globalize.parseInt(val.toString());
                        if (isNaN(val))
                            val = null;
                    }
                    else if (dataType.toUpperCase() === "date".toUpperCase()) {
                        val = Globalize.parseDate(val.toString());
                    }
                }
            }
            else {
                if (IsValid(dataType)) {
                    if (dataType.toUpperCase() === "number".toUpperCase() || dataType.toUpperCase() === "int".toUpperCase())
                        val = null;
                }
            }
            return val;
        };
        var interceptor = ko.computed({
            read: function () {
                return observable();
            },
            write: function (newValue) {
                var cur = $element.data('prevvalue');
                var val = GetAsDataType(newValue);
                if (!IsValid(val, true))
                    val = newValue;
                if (val !== cur) {
                    observable(val);
                }
                else {
                    if (IsValid(val)) {
                        if (newValue !== val.toString())
                            observable.valueHasMutated();
                    }
                }
            }
        });
        var tmp = GetAsDataType($element.val());
        if (IsValid(tmp)) {
            // console.log("Writing: " + tmp);
            interceptor(tmp);
        }
        // Use Normal Value binding, but on our generated observable
        ko.applyBindingsToNode(element, { value: interceptor }, viewModel);
    }
};
ko.bindingHandlers['currency'] = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var options = allBindingsAccessor().currencyOptions || {};
        var value = ko.utils.unwrapObservable(valueAccessor());
        ko.bindingHandlers['currency']['format'](element, value, options.symbol);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var options = allBindingsAccessor().currencyOptions || {};
        var value = ko.utils.unwrapObservable(valueAccessor());
        ko.bindingHandlers['currency']['format'](element, value, options.symbol);
    },
    symbol: "$",
    format: function (element, value, symbol) {
        // Clone Current Culture
        var culture = ko.toJS(Globalize.culture());
        // Replace current symbol with Symbol requested
        if (IsValid(symbol, true))
            culture.numberFormat.currency.symbol = symbol;
        // Output Currency
        $(element).text(Globalize.format(value, "c", culture));
    }
};
ko.bindingHandlers['date'] = {
    update: function (element, valueAccessor, allBindingAccessor, viewModel) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (value !== undefined && value !== null && !(value instanceof Date)) {
            var tmp = Globalize.parseDate(value);
            if (!(tmp instanceof Date)) {
                // 2013-08-29T16:09:00
                var datePatterns = ["yyyy-MM-ddTHH:mm:ssZ", "yyyy-MM-ddThh:mm:sszzz", "yyyy-MM-ddThh:mm:sszz", "yyyy-MM-ddThh:mm:ssz", "yyyy-MM-ddThh:mm:ss", "yyyy-MM-ddThh:mm", "yyyy-MM-dd"];
                for (var i = 0; i < datePatterns.length; i++) {
                    tmp = Globalize.parseDate(value, datePatterns[i]);
                    if (tmp instanceof Date)
                        break;
                }
                if (!(tmp instanceof Date)) {
                    tmp = Globalize.parseDate(value.substr(0, 19), "yyyy-MM-ddTHH:mm:ss");
                }
            }
            value = tmp;
        }
        $(element).text("{date} {time}".format({ date: Globalize.format(value, "d"), time: Globalize.format(value, "t") }));
    }
};
/*
ko.extenders.datavalue = function (target, type) {
    function GetValue(value, type) {
        var tmp: any = String(value);

        if (IsValid(value) && IsValid(type)) {
            if (type.toUpperCase() === "number".toUpperCase()) {
                tmp = Globalize.parseFloat(tmp);
                if (isNaN(tmp))
                    tmp = value;
            } else if (type.toUpperCase() === "int".toUpperCase()) {
                tmp = Globalize.parseInt(tmp);
                if (isNaN(tmp))
                    tmp = value;
            } else if (type.toUpperCase() === "date".toUpperCase()) {
                tmp = Globalize.parseDate(tmp);
            }
        } else {
            tmp = value;
        }

        return tmp;
    }

    var result = ko.computed({
        read: function () {
            return target();
        },
        write: function (newValue) {
            var current = target(),
                valueToWrite = GetValue(newValue, type),
                currentValue = GetValue(current, type);

            console.log("Current: " + current);
            console.log("Value:   " + valueToWrite);
            console.log("Cur Val: " + currentValue);

            if (valueToWrite !== current) {
                target(valueToWrite);
            } else {
                if (valueToWrite !== currentValue) {
                    target.notifySubscribers(valueToWrite);
                }
            }
        }
    });

    // result(target());

    return result;
};
*/
/*
ko.extenders.datavalue = function (target, type) {
    target['formatted'] = ko.observable<any>();

    function GetValue(value, type) {
        var tmp: any = String(value);

        if (IsValid(value) && IsValid(type)) {
            if (type.toUpperCase() === "number".toUpperCase()) {
                tmp = Globalize.parseFloat(tmp);
                if (isNaN(tmp))
                    tmp = value;
            } else if (type.toUpperCase() === "int".toUpperCase()) {
                tmp = Globalize.parseInt(tmp);
                if (isNaN(tmp))
                    tmp = value;
            } else if (type.toUpperCase() === "date".toUpperCase()) {
                tmp = Globalize.parseDate(tmp);
            }
        } else {
            tmp = value;
        }

        return tmp;
    }

    function toValueType(newValue) {
        var current = target(),
            valueToWrite = GetValue(newValue, type),
            currentValue = GetValue(current, type);

        if (valueToWrite !== current) {
            target['formatted'](valueToWrite);
        } else {
            if (valueToWrite !== currentValue) {
                target['formatted'].notifySubscribers(valueToWrite);
            }
        }
    }

    target.subscribe(toValueType);

    toValueType(target());

    return target;
};
*/
// Update ViewModel on the Fly
// Knockout V3
(function () {
    var getInjectValueUpdate = function (allBindings) {
        return {
            has: function (bindingKey) {
                return (bindingKey == 'valueUpdate') || allBindings.has(bindingKey);
            },
            get: function (bindingKey) {
                var binding = allBindings.get(bindingKey);
                if (bindingKey == 'valueUpdate') {
                    binding = binding ? [].concat(binding, 'afterkeydown') : ['afterkeydown'];
                }
                return binding;
            }
        };
    };
    var valueInitHandler = ko.bindingHandlers.value.init;
    ko.bindingHandlers.value.init = function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        // return valueInitHandler(element, valueAccessor, allBindings, viewModel, bindingContext);
        return valueInitHandler(element, valueAccessor, getInjectValueUpdate(allBindings), viewModel, bindingContext);
    };
}());
function CleanObject(itemToClean) {
    var props = GetObservableProperties(itemToClean);
    return ko.mapping.toJS(itemToClean, { 'include': props });
}
function GetObservableProperties(item, path) {
    if (path == undefined)
        path = "";
    else
        path = path + ".";
    var props = [];
    for (var prop in item) {
        if (item.hasOwnProperty(prop)) {
            if (ko.isObservable(item[prop]) && item[prop].push != undefined) {
                var tmpProps = GetObservableProperties(item[prop], path + prop);
                props.push(path + prop);
                props = ko.utils.arrayPushAll(props, tmpProps);
            }
            else if (ko.isComputed(item[prop]) || ko.isObservable(item[prop])) {
                props.push(path + prop);
            }
        }
    }
    return props;
}
//# sourceMappingURL=_KnockoutValidationExtensions.js.map