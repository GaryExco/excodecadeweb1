//// This file was Auto-Generated and could be overwritten at anytime,
////   use the DecadeExtensions. file to extend functionality.
/// <reference path="..\typings\jquery\jquery.d.ts" />
/// <reference path="..\typings\jqueryui\jqueryui.d.ts" />
/// <reference path="..\typings\knockout\knockout.d.ts" />
/// <reference path="..\typings\knockout.mapping\knockout.mapping.d.ts" />
/// <reference path="..\typings\knockout.validation\knockout.validation.d.ts" />
/// <reference path="..\typings\globalize\globalize.d.ts" />
/// <reference path="..\typings\ExcoGlobals.ts" />
//#region Classes
var d_budget = (function () {
    function d_budget() {
        this.id = ko.observable();
        this.customercode = ko.observable();
        this.estsales = ko.observable();
        this.yearmonth = ko.observable();
    }
    d_budget.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_budget;
}());
var d_custgroup = (function () {
    function d_custgroup() {
        this.id = ko.observable();
        this.name = ko.observable();
    }
    d_custgroup.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_custgroup;
}());
var d_custgroupitems = (function () {
    function d_custgroupitems() {
        this.id = ko.observable();
        this.groupid = ko.observable();
        this.customercode = ko.observable();
    }
    d_custgroupitems.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_custgroupitems;
}());
var d_customer = (function () {
    function d_customer() {
        this.customercode = ko.observable();
        this.name = ko.observable();
        this.onhold = ko.observable();
        this.creditlimit = ko.observable();
        this.gst = ko.observable();
        this.territory = ko.observable();
        this.contact = ko.observable();
        this.accountset = ko.observable();
        this.terms = ko.observable();
        this.freightcost = ko.observable();
        this.fax = ko.observable();
        this.faxconfirm = ko.observable();
        this.nitridecost = ko.observable();
        this.email = ko.observable();
        this.steelcost = ko.observable();
        this.ResPri = ko.observable();
        this.ResSec = ko.observable();
        this.ResTri = ko.observable();
        this.combineprices = ko.observable();
        this.mastercustomer = ko.observable();
        this.ftpremium = ko.observable();
        this.rank = ko.observable();
        this.weighttype = ko.observable();
        this.surcharge = ko.observable();
        this.minnitridecost = ko.observable();
        this.nitridemineffectivedate = ko.observable();
        this.surchargedetaildisplay = ko.observable();
        this.maxnitridecost = ko.observable();
        this.lang = ko.observable();
        this.gstlicense = ko.observable();
        this.suppliercode = ko.observable();
        this.active = ko.observable();
        this.pricelist = ko.observable();
        this.forceupdate = ko.observable();
        this.lastupdated = ko.observable();
        this.taxgroup = ko.observable();
        this.taxcode = ko.observable();
        this.surchargecode = ko.observable();
        this.roundtotals = ko.observable();
        this.confPriceBreakDown = ko.observable();
        this.confIncludeFreight = ko.observable();
        this.confAttachFile = ko.observable();
        this.pricelistMultiplier = ko.observable();
        this.currencyMultiplier = ko.observable();
        this.taxOnFreight = ko.observable();
    }
    d_customer.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_customer;
}());
var d_customer_order_count_total = (function () {
    function d_customer_order_count_total() {
        this.customercode = ko.observable();
        this.ordercount = ko.observable();
        this.ordertotal = ko.observable();
    }
    d_customer_order_count_total.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_customer_order_count_total;
}());
var d_customeraddress = (function () {
    function d_customeraddress() {
        this.customercode = ko.observable();
        this.name = ko.observable();
        this.address1 = ko.observable();
        this.address2 = ko.observable();
        this.address3 = ko.observable();
        this.address4 = ko.observable();
        this.postalcode = ko.observable();
        this.phone = ko.observable();
        this.country = ko.observable();
        this.stateprov = ko.observable();
    }
    d_customeraddress.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_customeraddress;
}());
var d_customermapping = (function () {
    function d_customermapping() {
        this.cmsNumber = ko.observable();
        this.decadeCode = ko.observable();
    }
    d_customermapping.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_customermapping;
}());
var d_customership = (function () {
    function d_customership() {
        this.customercode = ko.observable();
        this.shipvia = ko.observable();
        this.defaultship = ko.observable();
        this.id = ko.observable();
    }
    d_customership.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_customership;
}());
var d_customerwatch = (function () {
    function d_customerwatch() {
        this.customercode = ko.observable();
        this.employeenumber = ko.observable();
    }
    d_customerwatch.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_customerwatch;
}());
var d_dailylimits = (function () {
    function d_dailylimits() {
        this.dietype = ko.observable();
        this.maxitems = ko.observable();
        this.id = ko.observable();
        this.name = ko.observable();
        this.ftable = ko.observable();
        this.ffield = ko.observable();
        this.flogic = ko.observable();
        this.fvalue = ko.observable();
        this.limit = ko.observable();
    }
    d_dailylimits.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_dailylimits;
}());
var d_dailytotals = (function () {
    function d_dailytotals() {
        this.ordernumber = ko.observable();
        this.customercode = ko.observable();
        this.orderdate = ko.observable();
        this.shipdate = ko.observable();
        this.invoicedate = ko.observable();
        this.sales = ko.observable();
        this.nitride = ko.observable();
        this.steelsurcharge = ko.observable();
        this.discountamount = ko.observable();
        this.ncr = ko.observable();
        this.ncrs = ko.observable();
        this.freight = ko.observable();
        this.gst = ko.observable();
        this.prefix = ko.observable();
        this.territory = ko.observable();
        this.accountset = ko.observable();
    }
    d_dailytotals.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_dailytotals;
}());
var d_department = (function () {
    function d_department() {
        this.ID = ko.observable();
        this.name = ko.observable();
        this.finishedtaskcode = ko.observable();
        this.OrderInProcess = ko.observable();
    }
    d_department.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_department;
}());
var d_dietype = (function () {
    function d_dietype() {
        this.prefix = ko.observable();
        this.dietype = ko.observable();
    }
    d_dietype.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_dietype;
}());
var d_exchangerates = (function () {
    function d_exchangerates() {
        this.basecurrency = ko.observable();
        this.destcurrency = ko.observable();
        this.factor = ko.observable();
    }
    d_exchangerates.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_exchangerates;
}());
var d_hotlist = (function () {
    function d_hotlist() {
        this.ordernumber = ko.observable();
        this.customercode = ko.observable();
        this.orderdate = ko.observable();
        this.shopdate = ko.observable();
        this.expshipdate = ko.observable();
        this.hotListDate = ko.observable();
        this.name = ko.observable();
        this.notes = ko.observable();
    }
    d_hotlist.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_hotlist;
}());
var d_mailinglist = (function () {
    function d_mailinglist() {
        this.customercode = ko.observable();
        this.displayname = ko.observable();
        this.email = ko.observable();
        this.confirmations = ko.observable();
        this.forrecords = ko.observable();
        this.openorders = ko.observable();
    }
    d_mailinglist.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_mailinglist;
}());
var d_openorders_inc_ship = (function () {
    function d_openorders_inc_ship() {
        this.ordernumber = ko.observable();
        this.customercode = ko.observable();
        this.employeenumber = ko.observable();
        this.orderdate = ko.observable();
        this.invoicedate = ko.observable();
        this.shopdate = ko.observable();
        this.noshipbefore = ko.observable();
        this.expshipdate = ko.observable();
        this.expreceiveddate = ko.observable();
        this.shipdate = ko.observable();
        this.shipwithorder = ko.observable();
        this.invoicenumber = ko.observable();
        this.onhold = ko.observable();
        this.customerpo = ko.observable();
        this.customerreq = ko.observable();
        this.note = ko.observable();
        this.defaultfreight = ko.observable();
        this.defaultgst = ko.observable();
        this.sales = ko.observable();
        this.freight = ko.observable();
        this.gst = ko.observable();
        this.parts = ko.observable();
        this.baddress1 = ko.observable();
        this.baddress2 = ko.observable();
        this.baddress3 = ko.observable();
        this.baddress4 = ko.observable();
        this.bpostalcode = ko.observable();
        this.saddress1 = ko.observable();
        this.saddress2 = ko.observable();
        this.saddress3 = ko.observable();
        this.saddress4 = ko.observable();
        this.spostalcode = ko.observable();
        this.stock = ko.observable();
        this.fasttrack = ko.observable();
        this.priority = ko.observable();
        this.shipvia = ko.observable();
        this.defaultnitride = ko.observable();
        this.nitride = ko.observable();
        this.ncr = ko.observable();
        this.wr = ko.observable();
        this.defaultsteelsurcharge = ko.observable();
        this.steelsurcharge = ko.observable();
        this.steelrate = ko.observable();
        this.poscanned = ko.observable();
        this.polink = ko.observable();
        this.DWGName = ko.observable();
        this.Sent4Records = ko.observable();
        this.combineprices = ko.observable();
        this.shelf = ko.observable();
        this.zeroprice = ko.observable();
        this.shelforder = ko.observable();
        this.cor = ko.observable();
        this.ncrs = ko.observable();
        this.nitrideweight = ko.observable();
        this.nitridecharge = ko.observable();
        this.roughweight = ko.observable();
        this.freightweight = ko.observable();
        this.freightcharge = ko.observable();
        this.fasttrackcharge = ko.observable();
        this.fasttrackpercentage = ko.observable();
        this.total = ko.observable();
        this.subtotal = ko.observable();
        this.steel = ko.observable();
        this.shipvendorid = ko.observable();
        this.shippingvendor = ko.observable();
        this.trackingnumber = ko.observable();
        this.discountpercentage = ko.observable();
        this.discountamount = ko.observable();
        this.discountontotal = ko.observable();
        this.lastchange = ko.observable();
        this.hasnitridecomputedline = ko.observable();
        this.importedorder = ko.observable();
        this.ForApproval = ko.observable();
        this.htdate = ko.observable();
        this.hotListPriority = ko.observable();
        this.hotListDate = ko.observable();
        this.DesignDone = ko.observable();
        this.DesignType = ko.observable();
    }
    d_openorders_inc_ship.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_openorders_inc_ship;
}());
var d_openorders_no_ship = (function () {
    function d_openorders_no_ship() {
        this.ordernumber = ko.observable();
        this.customercode = ko.observable();
        this.employeenumber = ko.observable();
        this.orderdate = ko.observable();
        this.invoicedate = ko.observable();
        this.shopdate = ko.observable();
        this.noshipbefore = ko.observable();
        this.expshipdate = ko.observable();
        this.expreceiveddate = ko.observable();
        this.shipdate = ko.observable();
        this.shipwithorder = ko.observable();
        this.invoicenumber = ko.observable();
        this.onhold = ko.observable();
        this.customerpo = ko.observable();
        this.customerreq = ko.observable();
        this.note = ko.observable();
        this.defaultfreight = ko.observable();
        this.defaultgst = ko.observable();
        this.sales = ko.observable();
        this.freight = ko.observable();
        this.gst = ko.observable();
        this.parts = ko.observable();
        this.baddress1 = ko.observable();
        this.baddress2 = ko.observable();
        this.baddress3 = ko.observable();
        this.baddress4 = ko.observable();
        this.bpostalcode = ko.observable();
        this.saddress1 = ko.observable();
        this.saddress2 = ko.observable();
        this.saddress3 = ko.observable();
        this.saddress4 = ko.observable();
        this.spostalcode = ko.observable();
        this.stock = ko.observable();
        this.fasttrack = ko.observable();
        this.priority = ko.observable();
        this.shipvia = ko.observable();
        this.defaultnitride = ko.observable();
        this.nitride = ko.observable();
        this.ncr = ko.observable();
        this.wr = ko.observable();
        this.defaultsteelsurcharge = ko.observable();
        this.steelsurcharge = ko.observable();
        this.steelrate = ko.observable();
        this.poscanned = ko.observable();
        this.polink = ko.observable();
        this.DWGName = ko.observable();
        this.Sent4Records = ko.observable();
        this.combineprices = ko.observable();
        this.shelf = ko.observable();
        this.zeroprice = ko.observable();
        this.shelforder = ko.observable();
        this.cor = ko.observable();
        this.ncrs = ko.observable();
        this.nitrideweight = ko.observable();
        this.nitridecharge = ko.observable();
        this.roughweight = ko.observable();
        this.freightweight = ko.observable();
        this.freightcharge = ko.observable();
        this.fasttrackcharge = ko.observable();
        this.fasttrackpercentage = ko.observable();
        this.total = ko.observable();
        this.subtotal = ko.observable();
        this.steel = ko.observable();
        this.shipvendorid = ko.observable();
        this.shippingvendor = ko.observable();
        this.trackingnumber = ko.observable();
        this.discountpercentage = ko.observable();
        this.discountamount = ko.observable();
        this.discountontotal = ko.observable();
        this.lastchange = ko.observable();
        this.hasnitridecomputedline = ko.observable();
        this.importedorder = ko.observable();
        this.ForApproval = ko.observable();
        this.htdate = ko.observable();
        this.hotListPriority = ko.observable();
        this.hotListDate = ko.observable();
        this.DesignDone = ko.observable();
        this.DesignType = ko.observable();
    }
    d_openorders_no_ship.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_openorders_no_ship;
}());
var d_order = (function () {
    function d_order() {
        this.ordernumber = ko.observable();
        this.customercode = ko.observable();
        this.employeenumber = ko.observable();
        this.lastemployeenumber = ko.observable();
        this.orderdate = ko.observable();
        this.invoicedate = ko.observable();
        this.shopdate = ko.observable();
        this.noshipbefore = ko.observable();
        this.expshipdate = ko.observable();
        this.adjustedexpship = ko.observable();
        this.expreceiveddate = ko.observable();
        this.shipdate = ko.observable();
        this.shipwithorder = ko.observable();
        this.invoicenumber = ko.observable();
        this.onhold = ko.observable();
        this.customerpo = ko.observable();
        this.customerreq = ko.observable();
        this.note = ko.observable();
        this.defaultfreight = ko.observable();
        this.defaultgst = ko.observable();
        this.sales = ko.observable();
        this.freight = ko.observable();
        this.gst = ko.observable();
        this.parts = ko.observable();
        this.baddress1 = ko.observable();
        this.baddress2 = ko.observable();
        this.baddress3 = ko.observable();
        this.baddress4 = ko.observable();
        this.bpostalcode = ko.observable();
        this.saddress1 = ko.observable();
        this.saddress2 = ko.observable();
        this.saddress3 = ko.observable();
        this.saddress4 = ko.observable();
        this.spostalcode = ko.observable();
        this.stock = ko.observable();
        this.fasttrack = ko.observable();
        this.priority = ko.observable();
        this.shipvia = ko.observable();
        this.defaultnitride = ko.observable();
        this.nitride = ko.observable();
        this.ncr = ko.observable();
        this.wr = ko.observable();
        this.defaultsteelsurcharge = ko.observable();
        this.steelsurcharge = ko.observable();
        this.steelrate = ko.observable();
        this.poscanned = ko.observable();
        this.polink = ko.observable();
        this.DWGName = ko.observable();
        this.Sent4Records = ko.observable();
        this.combineprices = ko.observable();
        this.shelf = ko.observable();
        this.zeroprice = ko.observable();
        this.shelforder = ko.observable();
        this.cor = ko.observable();
        this.ncrs = ko.observable();
        this.nitrideweight = ko.observable();
        this.nitridecharge = ko.observable();
        this.roughweight = ko.observable();
        this.freightweight = ko.observable();
        this.freightcharge = ko.observable();
        this.fasttrackcharge = ko.observable();
        this.fasttrackpercentage = ko.observable();
        this.total = ko.observable();
        this.subtotal = ko.observable();
        this.steel = ko.observable();
        this.shipvendorid = ko.observable();
        this.shippingvendor = ko.observable();
        this.trackingnumber = ko.observable();
        this.discountpercentage = ko.observable();
        this.defaultdiscountamount = ko.observable();
        this.discountamount = ko.observable();
        this.discountontotal = ko.observable();
        this.lastchange = ko.observable();
        this.hasnitridecomputedline = ko.observable();
        this.importedorder = ko.observable();
        this.ForApproval = ko.observable();
        this.htdate = ko.observable();
        this.hotListPriority = ko.observable();
        this.hotListDate = ko.observable();
        this.DesignDone = ko.observable();
        this.DesignType = ko.observable();
        this.canceled = ko.observable();
        this.countasorder = ko.observable();
        this.pricelistmultiplier = ko.observable();
        this.currencymultiplier = ko.observable();
    }
    d_order.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_order;
}());
var d_order_changelog = (function () {
    function d_order_changelog() {
        this.ordernumber = ko.observable();
        this.revision = ko.observable();
        this.revisiontime = ko.observable();
        this.propertyname = ko.observable();
        this.oldvalue = ko.observable();
        this.newvalue = ko.observable();
    }
    d_order_changelog.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_order_changelog;
}());
var d_orderchecklist = (function () {
    function d_orderchecklist() {
        this.ordernumber = ko.observable();
        this.dienumber = ko.observable();
        this.feeder = ko.observable();
        this.backer = ko.observable();
        this.bolster = ko.observable();
        this.press = ko.observable();
        this.pressnumber = ko.observable();
        this.alloy = ko.observable();
    }
    d_orderchecklist.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_orderchecklist;
}());
var d_orderitem = (function () {
    function d_orderitem() {
        this.id = ko.observable();
        this.ordernumber = ko.observable();
        this.line = ko.observable();
        this.prefix = ko.observable();
        this.suffix = ko.observable();
        this.qty = ko.observable();
        this.description = ko.observable();
        this.location = ko.observable();
        this.dienumber = ko.observable();
        this.note = ko.observable();
        this.price = ko.observable();
        this.chargenitride = ko.observable();
        this.steelcost = ko.observable();
        this.roughweight = ko.observable();
        this.freightweight = ko.observable();
        this.nitrideweight = ko.observable();
        this.steelsurchargeweight = ko.observable();
        this.baseprice = ko.observable();
        this.hasnitride = ko.observable();
        this.tariffcode = ko.observable();
    }
    d_orderitem.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_orderitem;
}());
var d_orderitemcharges = (function () {
    function d_orderitemcharges() {
        this.ordernumber = ko.observable();
        this.line = ko.observable();
        this.chargeline = ko.observable();
        this.chargetype = ko.observable();
        this.chargename = ko.observable();
        this.chargeprice = ko.observable();
        this.chargepercentage = ko.observable();
        this.qty = ko.observable();
        this.price = ko.observable();
    }
    d_orderitemcharges.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_orderitemcharges;
}());
var d_ordernotes = (function () {
    function d_ordernotes() {
        this.ordernumber = ko.observable();
        this.notes = ko.observable();
    }
    d_ordernotes.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_ordernotes;
}());
var d_ordertaxes = (function () {
    function d_ordertaxes() {
        this.ordernumber = ko.observable();
        this.taxline = ko.observable();
        this.taxtype = ko.observable();
        this.rate = ko.observable();
        this.taxamount = ko.observable();
    }
    d_ordertaxes.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_ordertaxes;
}());
var d_orderwatch = (function () {
    function d_orderwatch() {
        this.employeenumber = ko.observable();
        this.ordernumber = ko.observable();
        this.emailalert = ko.observable();
    }
    d_orderwatch.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_orderwatch;
}());
var d_partsteelcut = (function () {
    function d_partsteelcut() {
        this.id = ko.observable();
        this.ordernumber = ko.observable();
        this.part = ko.observable();
        this.NumID = ko.observable();
    }
    d_partsteelcut.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_partsteelcut;
}());
var d_pricelist = (function () {
    function d_pricelist() {
        this.id = ko.observable();
        this.plcode = ko.observable();
        this.description = ko.observable();
    }
    d_pricelist.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_pricelist;
}());
var d_pricelistbaseitems = (function () {
    function d_pricelistbaseitems() {
        this.id = ko.observable();
        this.plcode = ko.observable();
        this.prefix = ko.observable();
        this.suffix = ko.observable();
        this.price = ko.observable();
    }
    d_pricelistbaseitems.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_pricelistbaseitems;
}());
var d_pricelistholecharges = (function () {
    function d_pricelistholecharges() {
        this.id = ko.observable();
        this.plcode = ko.observable();
        this.prefix = ko.observable();
        this.suffix = ko.observable();
        this.holeqty = ko.observable();
        this.holeprice = ko.observable();
    }
    d_pricelistholecharges.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_pricelistholecharges;
}());
var d_pricelistitemcharges = (function () {
    function d_pricelistitemcharges() {
        this.id = ko.observable();
        this.plcode = ko.observable();
        this.prefix = ko.observable();
        this.chargetype = ko.observable();
        this.chargename = ko.observable();
        this.chargeprice = ko.observable();
        this.chargepercentage = ko.observable();
        this.minqty = ko.observable();
        this.maxqty = ko.observable();
        this.mincharge = ko.observable();
        this.maxcharge = ko.observable();
    }
    d_pricelistitemcharges.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_pricelistitemcharges;
}());
var d_scheduling = (function () {
    function d_scheduling() {
        this.ordernumber = ko.observable();
        this.customercode = ko.observable();
        this.customername = ko.observable();
        this.orderdate = ko.observable();
        this.shopdate = ko.observable();
        this.expreceiveddate = ko.observable();
        this.hotlistdate = ko.observable();
        this.hotlistpriority = ko.observable();
        this.onhold = ko.observable();
        this.awaitingapproval = ko.observable();
        this.nitride = ko.observable();
        this.diedia = ko.observable();
        this.dietype = ko.observable();
        this.dienumber = ko.observable();
        this.isinvoiced = ko.observable();
        this.oldShopDate = ko.observable();
        this.ShopDateChanged = ko.observable();
        this.LastChanged = ko.observable();
        this.Ignore = ko.observable();
        this.process = ko.observable();
        this.forcerefresh = ko.observable();
    }
    d_scheduling.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_scheduling;
}());
var d_schedulingcustomer = (function () {
    function d_schedulingcustomer() {
        this.customercode = ko.observable();
        this.HotListLevel = ko.observable();
        this.DateExpires = ko.observable();
    }
    d_schedulingcustomer.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_schedulingcustomer;
}());
var d_schedulingevent = (function () {
    function d_schedulingevent() {
        this.ordernumber = ko.observable();
        this.partcode = ko.observable();
        this.deporder = ko.observable();
        this.department = ko.observable();
        this.duedate = ko.observable();
        this.completed = ko.observable();
    }
    d_schedulingevent.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_schedulingevent;
}());
var d_schedulingitem = (function () {
    function d_schedulingitem() {
        this.ordernumber = ko.observable();
        this.partcode = ko.observable();
        this.reqdepartments = ko.observable();
        this.LastDep = ko.observable();
        this.LastStation = ko.observable();
        this.LastEmployee = ko.observable();
        this.LastTrackTime = ko.observable();
        this.LastScrapTime = ko.observable();
        this.LastChanged = ko.observable();
        this.Ignore = ko.observable();
        this.StartDate = ko.observable();
        this.LeadTime = ko.observable();
    }
    d_schedulingitem.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_schedulingitem;
}());
var d_shelf = (function () {
    function d_shelf() {
        this.id = ko.observable();
        this.shelved = ko.observable();
        this.ordernumber = ko.observable();
        this.employeenumber = ko.observable();
        this.shelftime = ko.observable();
    }
    d_shelf.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_shelf;
}());
var d_ship = (function () {
    function d_ship() {
        this.id = ko.observable();
        this.shipped = ko.observable();
        this.ordernumber = ko.observable();
        this.employeenumber = ko.observable();
        this.readytoship = ko.observable();
        this.shiptime = ko.observable();
    }
    d_ship.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_ship;
}());
var d_shipdatechanged = (function () {
    function d_shipdatechanged() {
        this.ordernumber = ko.observable();
        this.changedate = ko.observable();
    }
    d_shipdatechanged.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_shipdatechanged;
}());
var d_shippingvendors = (function () {
    function d_shippingvendors() {
        this.shipvendorid = ko.observable();
        this.name = ko.observable();
        this.trackingpage = ko.observable();
    }
    d_shippingvendors.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_shippingvendors;
}());
var d_solsync = (function () {
    function d_solsync() {
        this.Name = ko.observable();
        this.LastOccurrence = ko.observable();
    }
    d_solsync.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_solsync;
}());
var d_task = (function () {
    function d_task() {
        this.id = ko.observable();
        this.ordernumber = ko.observable();
        this.employeenumber = ko.observable();
        this.task = ko.observable();
        this.part = ko.observable();
        this.subpart = ko.observable();
        this.station = ko.observable();
        this.tasktime = ko.observable();
        this.flags = ko.observable();
    }
    d_task.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_task;
}());
var d_taskcode = (function () {
    function d_taskcode() {
        this.task = ko.observable();
        this.taskname = ko.observable();
        this.feeder = ko.observable();
        this.solidplate = ko.observable();
        this.solidbacker = ko.observable();
        this.mandrel = ko.observable();
        this.hollowplate = ko.observable();
        this.hollowbacker = ko.observable();
        this.bolster = ko.observable();
    }
    d_taskcode.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_taskcode;
}());
var d_taskcodemapping = (function () {
    function d_taskcodemapping() {
        this.DepartmentID = ko.observable();
        this.task = ko.observable();
    }
    d_taskcodemapping.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_taskcodemapping;
}());
var d_taxinfo = (function () {
    function d_taxinfo() {
        this.taxgroup = ko.observable();
        this.taxratecode = ko.observable();
        this.taxtype = ko.observable();
        this.sequence = ko.observable();
        this.description = ko.observable();
        this.rate = ko.observable();
    }
    d_taxinfo.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_taxinfo;
}());
var d_territories = (function () {
    function d_territories() {
        this.code = ko.observable();
        this.name = ko.observable();
        this.orderinlist = ko.observable();
    }
    d_territories.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_territories;
}());
var d_user = (function () {
    function d_user() {
        this.employeenumber = ko.observable();
        this.lastname = ko.observable();
        this.firstname = ko.observable();
        this.trackpassword = ko.observable();
        this.salt = ko.observable();
        this.hashpassword = ko.observable();
        this.a_vieworders = ko.observable();
        this.a_modorders = ko.observable();
        this.a_viewcustomers = ko.observable();
        this.a_modcustomers = ko.observable();
        this.a_adminreports = ko.observable();
        this.a_shopreports = ko.observable();
        this.a_tracking = ko.observable();
        this.a_admin = ko.observable();
        this.a_god = ko.observable();
        this.a_ship = ko.observable();
        this.startapp = ko.observable();
        this.a_scrap = ko.observable();
        this.a_steel = ko.observable();
        this.a_steelpricing = ko.observable();
        this.a_cadnoteedit = ko.observable();
        this.email = ko.observable();
        this.a_deltasks = ko.observable();
        this.a_deltrack = ko.observable();
        this.a_hotlistview = ko.observable();
        this.a_hotlistadmin = ko.observable();
        this.a_htschedview = ko.observable();
        this.a_depschedadmin = ko.observable();
        this.secondaryemail = ko.observable();
        this.printername = ko.observable();
        this.debugmode = ko.observable();
        this.disabled = ko.observable();
    }
    d_user.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return d_user;
}());
var inv_steel = (function () {
    function inv_steel() {
        this.ID = ko.observable();
        this.TrueDia = ko.observable();
        this.Length = ko.observable();
        this.PrevLength = ko.observable();
        this.NumID = ko.observable();
        this.DateIn = ko.observable();
        this.DateFin = ko.observable();
        this.lastchange = ko.observable();
        this.BarInf = ko.observable();
        this.lbPrice = ko.observable();
        this.vendorid = ko.observable();
        this.heatnumber = ko.observable();
    }
    inv_steel.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return inv_steel;
}());
var inv_steelinf = (function () {
    function inv_steelinf() {
        this.ID = ko.observable();
        this.Material = ko.observable();
        this.Dia = ko.observable();
        this.MinStock = ko.observable();
        this.MaxStock = ko.observable();
    }
    inv_steelinf.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return inv_steelinf;
}());
var inv_vendors = (function () {
    function inv_vendors() {
        this.VendorID = ko.observable();
        this.VendorName = ko.observable();
        this.Address1 = ko.observable();
        this.Address2 = ko.observable();
        this.Address3 = ko.observable();
        this.Address4 = ko.observable();
        this.PostalCode = ko.observable();
        this.Phone = ko.observable();
        this.Fax = ko.observable();
        this.OurAcctNum = ko.observable();
        this.SteelVendor = ko.observable();
    }
    inv_vendors.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return inv_vendors;
}());
var scrapdes = (function () {
    function scrapdes() {
        this.id = ko.observable();
        this.department = ko.observable();
        this.scrapdesc = ko.observable();
        this.errorcode = ko.observable();
    }
    scrapdes.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return scrapdes;
}());
var scraplog = (function () {
    function scraplog() {
        this.date = ko.observable();
        this.part = ko.observable();
        this.department = ko.observable();
        this.sonum = ko.observable();
        this.Errorcode = ko.observable();
        this.empnum = ko.observable();
        this.problem = ko.observable();
        this.correctioncode = ko.observable();
        this.ID = ko.observable();
    }
    scraplog.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return scraplog;
}());
var SteelCuts = (function () {
    function SteelCuts() {
        this.ID = ko.observable();
        this.ordernumber = ko.observable();
        this.PieceName = ko.observable();
        this.SteelGrade = ko.observable();
        this.Thickness = ko.observable();
        this.Diameter = ko.observable();
        this.Plotted = ko.observable();
        this.Deleted = ko.observable();
        this.PlotTime = ko.observable();
    }
    SteelCuts.prototype.toJSON = function () {
        var copy = ko.toJS(this);
        if (copy.hasOwnProperty['errors'] && copy.errors.length > 0) {
            return null;
        }
        delete copy.errors;
        delete copy.group;
        delete copy.constructor;
        return copy;
    };
    return SteelCuts;
}());
//#endregion 
//# sourceMappingURL=DecadeClasses.js.map