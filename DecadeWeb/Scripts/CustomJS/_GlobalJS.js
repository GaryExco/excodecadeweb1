﻿// Ask ASP.NET what culture we prefer, because we stuck it in a meta tag
var data = $("meta[name='accept-language']").attr("content");
var timeFormat = $("meta[name='timeformat']").attr("content");
$.datepicker.setDefaults($.datepicker.regional['']);


// DatePicker Setup
if (data.toLowerCase() != 'en-us' && data.toLowerCase() != 'en' && data.toLowerCase() != 'en-ca') {
    Globalize.culture(data);

    if ($.datepicker.regional.hasOwnProperty(Globalize.culture().name))
        $.datepicker.setDefaults($.datepicker.regional[Globalize.culture().name]);
    else
        $.datepicker.setDefaults($.datepicker.regional[Globalize.culture().language]);


    // Setup Defaults for BootBox
    // bootbox.setLocale(data.substring(0, 2));
    bootbox.setDefaults("locale", data.substring(0, 2));
}


// Apply Theming to TableSorter
$.extend($.tablesorter.themes.bootstrap, {
    table: 'table table-bordered',
    header: 'bootstrap-header',
    footerRow: '',
    footerCells: '',
    icons: '',
    sortNone: 'bootstrap-icon-unsorted',
    sortAsc: 'icon-chevron-up',
    sortDesc: 'icon-chevron-down',
    active: '',
    hover: '',
    filterRow: '',
    even: '',
    odd: ''
});

/*
$.fn.typeahead.Constructor.prototype.blur = function () {
    var that = this;
    setTimeout(function() { that.hide() }, 250);
};
*/

var tmpShow = $.fn.popover.Constructor.prototype.show;
$.fn.popover.Constructor.prototype.show = function() {
    if ($(this.$element).is(":focus")) {
        return false;
    }

    if (this.options.callshow) {
        this.options.callshow();
    }
    tmpShow.call(this);
};

var tmpHide = $.fn.popover.Constructor.prototype.hide;
$.fn.popover.Constructor.prototype.hide = function() {
    if ($(this.$element).is(":focus")) {
        return false;
    }

    if (this.options.callhide) {
        this.options.callhide();
    }
    tmpHide.call(this);
};

$(document).ready(function() {
    //Tell the validator, for example,
    // that we want numbers parsed a certain way!
    $.validator.methods.number = function(value, element) {
        if (Globalize.parseFloat(value.toString())) {
            return true;
        }
        return false;
    };

    $.validator.methods.date = function(value, element) {
        return this.optional(element) || Globalize.parseDate(value, $.datepicker._defaults.dateFormat);
    }; //Fix the range to use globalized methods
    jQuery.extend(jQuery.validator.methods, {
        range: function(value, element, param) {
            //Use the Globalization plugin to parse the value
            var val = Globalize.parseFloat(value.toString());
            return this.optional(element) || (val >= param[0] && val <= param[1]);
        }
    });

    // Setup Datepickers
    $('.datepicker').each(function() {
        var options = {};
        if ($(this).attr('data-date')) {
            $.extend(options, $(this).data('date'));

            if (options.hasOwnProperty('defaultDate')) {
                if (!(options.defaultDate instanceof Date)) {
                    options.defaultDate = Globalize.parseDate(options.defaultDate, "yyyy-MM-dd"); // "yyyy-MM-ddThh:mm:ss"
                }
            }
        }

        $(this).datepicker(options);
    });
    $('.datetimepicker').each(function () {
        var options = {
            timeFormat: timeFormat
        };


        $(this).datetimepicker(options);
    });

    $('.monthpicker').monthpicker();
    // Apply Popovers
    $(".dopopover").popover();

    // Apply keep_val
    $(".keep_val").focus(function() {
        var input = $(this);
        input.val(input.attr("placeholder"));
    }).blur(function() {
        var input = $(this);
        input.attr("placeholder", input.val());
        input.val("");

        setTimeout(function() { input.change(); }, 10);
    });

    // Setup Currency
    $('.currency').each(function() {
        $(this).text(Globalize.format(Globalize.parseFloat($(this).text()), "c"));
    });

    // Customer AutoCompletes
    $('.customerautocomplete').each(function() {
        new CustomerAutoComplete($(this).attr("id"));
    });

    // Employee AutoCompletes
    $('.employeeautocomplete').each(function() {
        new EmployeeAutoComplete($(this).attr("id"));
    });

    // Setup Focus Select All
    $(document).on('focus', '.selectall', function() {
        var self = $(this);
        setTimeout(function() { self.select(); }, 5);
    });

    // Setup Sortable Tables
    $(".sortable").tablesorter({
        theme: "bootstrap",
        widthFixed: true,
        headerTemplate: '{content} {icon}',
        widthFixed: true,
        widgets: ["uitheme", "filter", "zebra"],
        widgetOptions: {
            zebra: ["even", "odd"],
            filter_reset: ".reset"
        }
    });

    // Apply Fading for Page Rendered Div
    $('#pagerendered').on('mouseenter', function() {
        $('#pagerendered').animate({ opacity: 1 }, 400);
    }).on('mouseleave', function() {
        $('#pagerendered').fadeTo(400, .4);
    });

    // Apply Search Button Actions
    $("#searchbar").attr('data-default', $("#searchbar").width());
    $("#searchbar").focus(function() {
        var w = parseFloat($(this).attr('data-default'));
        $(this).animate({ width: w + 75 }, "slow");
        $(this).popover("hide");
    }).blur(function() {
        $(this).animate({ width: $(this).attr('data-default') }, 'slow');
    }).popover({
        trigger: "hover",
        html: true,
        content: "Use this as a quick search.<br />Options<ul><li>Track - Enter &lt;Order Number&gt; (e.g. 670251)</li><li>Order - Enter O &lt;Order Number&gt; (e.g. O 674120)</li><li>Invoice - Enter I &lt;Invoice Number&gt; (e.g. I 770852)</li><li>Die Number - Enter DN &lt;Die Number&gt; (e.g. DN 17705 OR DN *7705 to search all)</li><li>Location Number - Enter LOC &lt;Location Number&gt; (e.g. LOC 85540)</li></ul>",
        // title: "Search Options",
        placement: "bottom",
        delay: { show: 750, hide: 250 }
    });

    // Clear Field on Double Click
    new ClearOnDoubleClick(".clearondblclick");

    // Common Modal Settings
    $(".modal").on("hidden", function() {
        // Clear Modals on Close
        var form = $(this).find('form');
        if (form != undefined) {
            form[0].reset();
        }
    }).on("shown", function() {
        // Select First Input on Shown event
        var input = $(this).find('input[type=text], textarea, select').filter(':visible:first');
        if (input != undefined) {
            input.focus();
        }
    });

    // Blur on Esc (Can't use Mousetrap because it messes with other bindings)
    $(document).keyup(function(e) {
        if (e.keyCode == 27)
            $(":input:focus").blur();
    });

    // Focus the 1st Input
    $('.focus').first().focus();
});