﻿/// <reference path="../jquery/jquery.d.ts" />

interface FileDownloadGlobal {
    update(): void;
    pause(): void;
    resume(): void;
    interval(): number;
    interval(interval: number): void;
}

interface FileDownloadOptionsGlobal {
    preparingMessageHtml?: string;
    failMessageHtml?: string;
    androidPostUnsupportedMessageHtml?: string;
    prepareCallback?: void;
    successCallback?: void;
    failCallback?: void;
    httpMethod?: string;
    data?: Object;
    checkInterval?: Number;
    cookieName?: string;
    cookieValue?: string;
    cookiePath?: string;
    cookieDomain?: string;
}

interface JQueryStatic {
    fileDownload(url: string): JQueryPromise<any>;
    fileDownload(url: string, data: FileDownloadOptionsGlobal): JQueryPromise<any>;
}

interface JQuery {
    fileDownload(url: string): JQuery;
    fileDownload(url: string, data: FileDownloadOptionsGlobal): JQuery;
}