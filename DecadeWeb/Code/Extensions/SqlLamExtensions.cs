﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using LambdaSqlBuilder;



namespace DecadeWeb.Code.Extensions
{
    public static class SqlLamExtensions
    {
        public static int Execute( this SqlLamBase sqlLam, IDbConnection conn, IDbTransaction trans = null )
        {
            return conn.Execute(
                sqlLam.QueryString,
                new[]
                {
                    sqlLam.QueryParameters
                },
                trans );
        }


        public static IEnumerable<T> Query<T>(
            this SqlLamBase sqlLam,
            IDbConnection conn,
            IDbTransaction trans = null,
            bool buffered = true,
            int? commandTimeout = null,
            CommandType? commandType = null )
        {
            return conn.Query<T>(
                sqlLam,
                trans,
                buffered,
                commandTimeout,
                commandType );
        }
    }
}