﻿using System;
using System.Data;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb
{
    public static class EarlyOnTimeLateExtensions
    {
        public static string ToLateEarlyString( this d_order order )
        {
            return order.DiffFromExpShip.ToLateEarlyString();
        }


        public static string ToLateEarlyString<T>( this T daysEarlyOnTimeLate ) where T : IComparable<T>
        {
            if (!daysEarlyOnTimeLate.IsNumeric())
                throw new ArgumentOutOfRangeException( nameof(
                    daysEarlyOnTimeLate ) );

            decimal tmp = Convert.ToDecimal( daysEarlyOnTimeLate );

            if (tmp > -0.001m
                && tmp < 0.001m)
                return "On Time";

            string strDaysEarly = Math.Abs(
                tmp )
                                      .ToString(
                                          "0.##" ),
                   strDayTense = Math.Abs(
                       tmp ) == 1
                                     ? "day"
                                     : "days",
                   strLateEarly = tmp < 0
                                      ? "early"
                                      : "late";

            return $"{strDaysEarly} {strDayTense} {strLateEarly}";
        }
    }
}