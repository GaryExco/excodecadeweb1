﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb.Code.Extensions
{
    public static class OrderExtensions
    {
        public static decimal GetExchange( this IEnumerable<d_order> orders, Func<d_order, decimal> selector, decimal exchange )
        {
            var sum = orders.Where( x => !x.Customer.accountset.EqualsIgnoreCase( Settings.DefaultCurrency ) ).Sum( selector );
            return sum * ( exchange - 1 );
        }


        public static decimal GetExchange( this IEnumerable<d_order> orders, Func<d_order, double> selector, decimal exchange )
        {
            var sum = orders.Where( x => !x.Customer.accountset.EqualsIgnoreCase( Settings.DefaultCurrency ) ).Sum( selector ).To<decimal>();
            return sum * ( exchange - 1 );
        }


        public static decimal ApplyExchange( this IEnumerable<d_order> orders, decimal exchange )
        {
            return orders.ApplyExchange(
                x => x.SubTotal,
                exchange );
        }


        public static decimal ApplyExchange( this IEnumerable<d_order> orders, Func<d_order, decimal> selector, decimal exchange )
        {
            orders = orders.ToList();
            return orders.Sum( selector ) + orders.GetExchange(
                selector,
                exchange );
        }


        public static decimal ApplyExchange( this IEnumerable<d_order> orders, Func<d_order, double> selector, decimal exchange )
        {
            orders = orders.ToList();
            return orders.Sum( selector ).To<decimal>() + orders.GetExchange(
                selector,
                exchange );
        }
    }
}