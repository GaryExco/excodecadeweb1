﻿using System;
using DecadeWeb.Models;



namespace DecadeWeb
{
    public static class DateTimeExtensions
    {
        public static string ToNotifyDate( this DateTime dt )
        {
            return dt.ToString( "yyyy-MM-ddTHH:mm:ss-zzz" );
        }


        public static string ToURLDate( this DateTime dt )
        {
            return dt.TimeOfDay == default(TimeSpan)
                ? dt.ToString( "yyyy-MM-dd" )
                : dt.ToString( "yyyy-MM-dd HH:mm:ss" );
            return dt.ToString( "yyyy-MM-dd" );
        }


        public static string ToTitleDate( this DateTime dt, DayMonthYearEnum Mode )
        {
            switch (Mode)
            {
                case DayMonthYearEnum.Year:
                    return dt.ToString( "MM/yyyy" );

                case DayMonthYearEnum.Month:
                    return dt.ToString( "MMMM yyyy" );

                default:
                    return dt.ToShortDateString();
            }
        }
    }
}