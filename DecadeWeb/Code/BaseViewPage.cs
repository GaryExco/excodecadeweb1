﻿using System.Web.Mvc;



namespace DecadeWeb
{
    public abstract class BaseViewPage : WebViewPage
    {
        public new virtual d_userPrincipal User
        {
            get { return base.User as d_userPrincipal; }
        }


        public virtual d_user DUser
        {
            get { return base.User.Identity as d_user; }
        }
    }


    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public new virtual d_userPrincipal User
        {
            get { return base.User as d_userPrincipal; }
        }


        public virtual d_user DUser
        {
            get { return base.User.Identity as d_user; }
        }
    }
}