﻿using System;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Xml;
using Mset.Extensions;



namespace DecadeWeb
{
    public static partial class MvcExtensions
    {
        public static MvcHtmlString If( this MvcHtmlString value, bool evaluation )
        {
            return evaluation
                       ? value
                       : MvcHtmlString.Empty;
        }


        public static MvcHtmlString LinkIf( this HtmlHelper html, object link, object linkText, bool expression, bool NewWindow = false )
        {
            var strTarget = NewWindow
                                ? " target=\"_blank\""
                                : "";

            return !expression
                       ? new MvcHtmlString( linkText.ToString() )
                       : new MvcHtmlString( $"<a href=\"{link}\"{strTarget}>{linkText}</a>" );
        }


        public static MvcHtmlString OutputIf( this HtmlHelper html, object content, bool expression )
        {
            return !expression
                       ? new MvcHtmlString( "" )
                       : new MvcHtmlString( content.ToString() );
        }


        public static MvcHtmlString PageRendered( this HtmlHelper html )
        {
            return html.Partial( "_PageRendered" );
        }


        public static MvcHtmlString PrintButton( this HtmlHelper html )
        {
            return html.Partial( "PrintButton" );
        }


        public static MvcHtmlString PageTitle( this HtmlHelper html, string title )
        {
            return html.Partial(
                "_PageHeader",
                title );
        }


        public static MvcHtmlString KeepAlive( this HtmlHelper html )
        {
            return html.Partial( "_KeepAlive" );
        }


        public static MvcHtmlString MenuLink( this HtmlHelper html, string linkText, string actionName, string controllerName, object htmlAttributes = null )
        {
            var currentAction = html.ViewContext.RouteData.GetRequiredString( "action" );
            var currentController = html.ViewContext.RouteData.GetRequiredString( "controller" );

            var builder = new TagBuilder( "li" )
            {
                InnerHtml = html.ActionLink(
                    linkText,
                    actionName,
                    controllerName,
                    null,
                    htmlAttributes ).ToHtmlString()
            };

            if (String.Equals(
                controllerName,
                currentController,
                StringComparison.InvariantCultureIgnoreCase )
                && String.Equals(
                    actionName,
                    currentAction,
                    StringComparison.InvariantCultureIgnoreCase ))
                builder.AddCssClass( "active" );

            return new MvcHtmlString( builder.ToString() );
        }


        public static MvcHtmlString MetaAcceptLanguage( this HtmlHelper html )
        {
            return
                new MvcHtmlString(
                    $"<meta name=\"accept-language\" content=\"{Settings.UserCulture}\" />\n<meta name=\"lang\" content=\"{Settings.UserCulture}\" />\n<meta http-equiv=\"content-language\" content=\"{Settings.UserCulture}\" />\n<meta name=\"timeformat\" content=\"{new CultureInfo(Settings.UserCulture).DateTimeFormat.LongTimePattern}\" />" );
        }


        public static MvcHtmlString DisplayModelErrors( this HtmlHelper html )
        {
            return html.Partial( "_DisplayErrors" );
        }
    }
}