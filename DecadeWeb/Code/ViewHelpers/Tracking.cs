﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using DecadeWeb.Models;
using Mset.Exco.Decade;
using Mset.Extensions;
using Resources;
using StackExchange.Profiling;

namespace DecadeWeb
{
    public static partial class MvcExtensions
    {
        public static MvcHtmlString TrackingLink(this UrlHelper url, d_order order)
        {
            return TrackingLink(url, order.ordernumber);
        }

        public static MvcHtmlString TrackingLink(this UrlHelper url, int ordernumber, string part = null)
        {
            string anchor,
                   suffix;

            if (part.IsNotNullOrEmpty())
            {
                anchor = $"#{part}";
                suffix = $" ({part})";
            }
            else
            {
                anchor = "";
                suffix = "";
            }


            return new MvcHtmlString($"<a href=\"{url.TrackByOrder(ordernumber)}{anchor}\">{ordernumber}{suffix}</a>");
        }

        public static MvcHtmlString PrintTrackTable(this d_order order, HtmlHelper html)
        {
            var sb = new StringBuilder();

            #region Design / CAM
            var globalItems = new Dictionary<string, List<string>>
            {
                {
                    "Design", new List<string>
                    {
                        "DD",
                        "DI",
                        "SA",
                        "SI",
                        "SH",
                        "DS",
                        "DO",
                        "DF",
                        "OH",
                        "RH"
                    }
                },
                {
                    "CAM", new List<string>
                    {
                        "CM",
                        "CO",
                        "CI",
                        "CF"
                    }
                },
                {
                    "Layout", new List<string>
                    {
                        "LO"
                    }
                }
            };

            foreach (var curDep in globalItems)
            {
                using (MiniProfiler.Current.Step("Outputting Global Tracks For {0}".FormatWith(curDep.Key)))
                {
                    var tt = new TrackingTable
                    {
                        Title = curDep.Key,
                        Tasks = order.TrackingInfo.Where(x => x.task.In(curDep.Value.ToArray()))
                    };

                    sb.Append(html.Partial("_TrackingTable", tt));
                }
            }
            #endregion

            #region Parts
            foreach (char p in Part.PART_CODE_SORT_ORDER)
            {
                using (MiniProfiler.Current.Step("Outputting Table for {0}".FormatWith(p)))
                {
                    Part part = null;
                    
                    if (order.Parts.IsNotNull())
                        part = order.Parts.FirstOrDefault(x => x.PartCode.EqualsIgnoreCase(p.ToString()));

                    var tt = new TrackingTable
                    {
                        Tasks = order.TrackingInfo.Where(x => x.part.ToUpper() == p.ToString(CultureInfo.InvariantCulture)),
                        PercentComplete = part.IsNotNull()
                            ? part.PercentComplete
                            : 0,
                        DisplayProgressBar = part.IsNotNull(),
                        SteelCut = part.IsNotNull()
                            ? part.BarID
                            : null,
                        PartCode = p.ToString()
                    };

                    switch (char.ToUpper(p))
                    {
                        case 'F':
                            tt.Title = GlobalResources.General_Feeder;
                            break;

                        case 'M':
                            tt.Title = GlobalResources.General_Mandrel;
                            break;

                        case 'P':
                            tt.Title = GlobalResources.General_Plate;
                            break;

                        case 'B':
                            tt.Title = GlobalResources.General_Backer;
                            break;

                        case 'O':
                            tt.Title = GlobalResources.General_Bolster;
                            break;

                        case 'R':
                            tt.Title = GlobalResources.General_Ring;
                            break;

                        default:
                            tt.Title = p.ToString(CultureInfo.InvariantCulture);
                            break;
                    }

                    sb.Append(html.Partial("_TrackingTable", tt));
                }
            }
            #endregion

            #region Inspection
            globalItems.Clear();
            globalItems.Add("Use As Is", new List<string>
            {
                "AS"
            });
            globalItems.Add("Inspection", new List<string>
            {
                "IN",
                "PN",
                "NI",
                "NT",
                "IC",
                "FK"
            });

            foreach (var curDep in globalItems)
            {
                var tt = new TrackingTable
                {
                    Title = curDep.Key,
                    Tasks = order.TrackingInfo.Where(x => curDep.Value.Contains(x.task))
                };

                sb.Append(html.Partial("_TrackingTable", tt));
            }
            #endregion

            return new MvcHtmlString(sb.ToString());
        }

        public static MvcHtmlString PrintErrorsTable(this d_order order, HtmlHelper html)
        {
            return html.Partial("_ErrorTable", order.ScrapInfo);
        }
    }
}