﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DecadeWeb.Models;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb
{
    public static partial class MvcExtensions
    {
        public static MvcHtmlString PrintAdminSchedRow(
            this HtmlHelper html,
            Dictionary<DateTime, List<d_orderitem>> data,
            AdminSchedShowMode dispMode,
            int line,
            out bool moreRows )
        {
            var sb = new StringBuilder();
            var url = new UrlHelper( HttpContext.Current.Request.RequestContext );
            bool outputLine = false;

            sb.Append( "<tr>" );

            foreach (KeyValuePair<DateTime, List<d_orderitem>> curDate in data)
            {
                if (curDate.Value.Count > line)
                {
                    d_orderitem item = curDate.Value[line];

                    outputLine = true;
                    sb.Append( "<td>" );

                    if (item.Order.fasttrack
                        || item.Order.hotListDate.HasValue)
                        sb.Append( "<b>" );

                    sb.Append(
                        "{Line}. <a href=\"{URL}\" class=\"{LinkClass}\">{CurItem}{PieceOnly}{Nitride}{Hold}{Stock}{HT}</a>".FormatWithName(
                            new
                            {
                                // dopopover   data-trigger=\"hover\" data-delay=\"300\" data-title=\"Hello\" data-content=\"Test Content\"
                                Line = line + 1,
                                CurItem = dispMode == AdminSchedShowMode.DieNumber
                                              ? item.dienumber
                                              : item.Order.ordernumber.ToString(),
                                PieceOnly = ( item.Order.OrderItems.Count == 1 && item.Order.OrderItems[0].prefix != "HO" )
                                                ? $"({item.Order.OrderItems[0].prefix})"
                                                : String.Empty,
                                Nitride = item.hasnitride
                                              ? "(N)"
                                              : String.Empty,
                                Hold = item.Order.onhold
                                           ? "(Hold)"
                                           : String.Empty,
                                Stock = item.Order.IsStockJob
                                            ? "(Stock)"
                                            : String.Empty,
                                HT = item.Order.Parts.IsNotNull() && item.Order.Parts.All( x => x.IsHTDone )
                                         ? "(HT)"
                                         : String.Empty,
                                LinkClass = item.Order.onhold || item.Order.IsStockJob
                                                ? " test"
                                                : String.Empty,
                                URL = url.TrackByOrder( item.ordernumber )
                            } ) );

                    if (item.Order.fasttrack
                        || item.Order.hotListDate.HasValue)
                        sb.Append( "</b>" );

                    sb.Append( "</td>" );
                }
                else
                    sb.Append( "<td></td>" );
            }

            sb.Append( "</tr>" );

            moreRows = outputLine;

            return outputLine
                       ? new MvcHtmlString( sb.ToString() )
                       : new MvcHtmlString( "" );
        }
    }
}