﻿using System;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;



namespace DecadeWeb.Code
{
    public class BaseControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance( RequestContext requestContext, Type controllerType )
        {
            try
            {
                var controller = base.GetControllerInstance(
                    requestContext,
                    controllerType );
                requestContext.HttpContext.User = Thread.CurrentPrincipal = new d_userPrincipal();
                return controller;
            }
            catch (Exception)
            {
                return base.GetControllerInstance(
                    requestContext,
                    controllerType );
            }
        }
    }
}