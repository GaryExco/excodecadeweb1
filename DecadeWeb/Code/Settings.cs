﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Mset.Exco;
using Mset.Extensions;

namespace DecadeWeb
{
    public static class Settings
    {
        public static bool UseDev
        {
            get
            {
                bool rtnVal;

                Boolean.TryParse(GetValue("UseDev"), out rtnVal);

                return rtnVal;
            }
        }

        public static string DecadeConn
        {
            get
            {
                return UseDev
                           ? DatabaseInfo.DecadeTestConnStr
                           : DatabaseInfo.DecadeConnStr;
            }
        }

        public static string DefaultCulture
        {
            get
            {
                string rtnVal = GetValue("DefaultCulture");

                if (rtnVal.IsNullOrEmpty())
                    rtnVal = GetValue("PlantLocale");

                if (rtnVal.IsNullOrEmpty())
                    rtnVal = "en-US";

                return rtnVal;
            }
        }

        public static string DefaultCurrency
        {
            get
            {
                string rtnVal = GetValue("DefaultCurrency");

                if (rtnVal.IsNullOrEmpty())
                    rtnVal = "USD";

                return rtnVal;
            }
        }

        public static string UserCulture
        {
            get
            {
                var acceptedCultures = new List<string> { "en-US", "en", "es", "es-CO", "pt", "pt-BR" };
                string[] userLanguages = HttpContext.Current.Request.UserLanguages;

                if (userLanguages == null || userLanguages.Length == 0)
                    return DefaultCulture;

                string foundCulture = String.Empty;

                foreach (string l in userLanguages)
                    if (acceptedCultures.Contains(l, StringComparer.InvariantCultureIgnoreCase))
                    {
                        foundCulture = l;
                        break;
                    }

                // Adjust Found Culture to Actual Culture
                if (foundCulture != String.Empty)
                {
                    switch (foundCulture.Substring(0, 2).ToLower())
                    {
                        case "es":
                            return "es-CO";

                        case "pt":
                            return "pt-BR";

                        default:
                            return "en-US";
                    }
                }

                return DefaultCulture;
            }
        }

        public static string FacilityName { get { return GetValue("FacilityShortName"); } }

        public static List<string> NCRMailingList
        {
            get
            {
                var rtnVal = new List<string>();

                if (UseDev)
                    return rtnVal;

                string tmp = GetValue("NCRMailingList");

                if (!tmp.IsNullOrEmpty())
                {
                    string[] emails = tmp.Split(';');
                    rtnVal.AddRange(emails);
                }

                return rtnVal;
            }
        }

        public static string OrdersEmail { get { return GetValue("OrdersEmail"); } }

        public static string OrdersEmailDisplay { get { return GetValue("OrdersEmailDisplay"); } }

        public static string OrderEntryNCRDiscount { get { return GetValue( "OrderEntryNCRDiscount", "100T" ); } }

        public static string OldDecadeHost { get { return GetValue("OldDecadeHost"); } }

        public static bool PublishEvents { get
        {
            return Convert.ToBoolean(GetValue(
                "PublishEvents",
                "False" ));
        } }


        public static TimeSpan DayStart
        {
            get { return TimeSpan.Parse( GetValue( "DayStart", "05:00:00" ) ); }
        }


        public static string GetValue(string key, string defVal = "")
        {
            string rtnVal = String.Empty;

            try
            {
                rtnVal = ConfigurationManager.AppSettings[key];
            }
                // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                rtnVal = defVal;
            }

            return rtnVal;
        }
    }
}