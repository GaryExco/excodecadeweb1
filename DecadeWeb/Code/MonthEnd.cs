﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using Dapper;
using LambdaSqlBuilder;
using Mset.Database;
using Mset.Exco.Decade;
using Mset.Exco.Decade.Extensions;
using Mset.Extensions;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DecadeWeb.Code
{
    class MonthEnd
    {
        private const string CURRENCY_FORMAT_STRING = "_(\"$\"* #,##0.00_);_(\"$\"* \\(#,##0.00\\);_(\"$\"* \"-\"??_);_(@_)",
                         PERCENT_FORMAT_STRING = "0%",
                         DATE_FORMAT_STRING = "mm-dd-yy",
                         NUMBER_FORMAT_STRING = "#,###0.000";

        readonly MemoryStream _excelStream;
        readonly ExcelPackage _excelPkg;
        readonly ExcelWorksheet _workSheet;

        private int _orderRowStart = -1;
        private int _currentRow = 1;

        private readonly DateTime _cutOffDate;
        private readonly double _laborMultipler;
        private readonly double _overheadMultiplier;
        private readonly double _smallDiaSteelPrice;
        private readonly double _bigDiaSteelPrice;
        private readonly double _exchangeRate;

        public MonthEnd(DateTime? cutOffDate, double? labor, double? overhead, double? smallSteelRate, double? bigSteelRate, double? exchangeRate)
        {
            _cutOffDate = cutOffDate ?? DateTime.Now;
            _laborMultipler = labor / 100 ?? 0;
            _overheadMultiplier = overhead / 100 ?? 0;
            _smallDiaSteelPrice = smallSteelRate ?? 0;
            _bigDiaSteelPrice = bigSteelRate ?? 0;
            _exchangeRate = exchangeRate ?? 1;

            _excelStream = new MemoryStream();
            _excelPkg = new ExcelPackage(_excelStream);
            _workSheet = _excelPkg.Workbook.Worksheets.Add("WIP");
        }

        public MemoryStream Generate(Database db)
        {
            var taskCache = d_taskcode.LoadAll(db).ToList();

            OutputHeader();

            var invoiceCutOffDate = _cutOffDate.DateEnd();

            #region Get Open Orders
            var query = new SqlLam<d_order>()
                .Select(x => new
                {
                    x.ordernumber,
                    x.orderdate,
                    x.expshipdate,
                    x.customerpo,
                    x.customercode,
                    x.nitride,
                    x.steelsurcharge,
                    x.sales,
                    x.shipdate,
                    x.discountamount,
                    x.discountontotal,
                    x.fasttrackcharge,
                    x.discountpercentage,
                    x.ncr
                })
                .BeginLogicGroup()
                .WhereIsOpen()
                .Or()
                .Where(x => x.invoicedate > invoiceCutOffDate)
                .EndLogicGroup()
                .Where(x => x.orderdate < _cutOffDate)
                .OrderByDescending(x => x.ordernumber)
                .JoinCustomerTable()
                .Select(x => new
                {
                    x.territory,
                    x.accountset,
                    x.name
                });

            var openOrders = db.Connection.Query<d_order, d_customer, d_order>(
                query,
                (order, customer) =>
                {
                    order.Customer = customer;
                    return order;
                },
                splitOn: "territory");
            #endregion

            _orderRowStart = _currentRow;

            #region Loop Orders
            foreach (d_order curOrder in openOrders)
            {
                if (curOrder.discountontotal && curOrder.discountpercentage.HasValue && curOrder.discountpercentage.Value == 100)
                    continue;

                curOrder.LoadOrderItems(db);
                curOrder.LoadParts(db);

                // Check if material has been cut
                curOrder.OrderItems.ForEach(x => x.LoadSawCut(db, endDate: _cutOffDate));

                // Load tracking info
                foreach (var part in curOrder.Parts)
                    part.LoadLastTrack(db, curOrder.IsHollow, taskCache, endDate: _cutOffDate);

                OutputLine(curOrder);
            }
            #endregion

            OutputSummary();

            ApplyFormatting();


            _excelPkg.Save();
            _excelStream.Seek(0, SeekOrigin.Begin);

            return _excelStream;
        }

        void OutputHeader()
        {
            _workSheet.Cells[_currentRow, 1].Value = "WIP Input";
            _workSheet.Cells[_currentRow, 2].Value = "Labor %";
            _workSheet.Cells[_currentRow, 3].Value = _laborMultipler; // 0.06

            _workSheet.Cells[_currentRow, 5].Value = "Standard Cost";
            _workSheet.Cells[_currentRow, 7].Value = "Per Lb.";

            _workSheet.Cells[_currentRow, 9].Value = "Exchange Rates";
            _workSheet.Cells[_currentRow, 10].Value = "USD -> Local";
            _workSheet.Cells[_currentRow, 11].Value = _exchangeRate;

            _currentRow++;

            _workSheet.Cells[_currentRow, 2].Value = "Overhead%";
            _workSheet.Cells[_currentRow, 3].Value = _overheadMultiplier; // 0.57

            _workSheet.Cells[_currentRow, 5].Value = "Average Steel Price";
            _workSheet.Cells[_currentRow, 6].Value = "<= 24\"";
            _workSheet.Cells[_currentRow, 7].Value = _smallDiaSteelPrice; // 1.49

            _currentRow++;

            _workSheet.Cells[_currentRow, 6].Value = "> 24\"";
            _workSheet.Cells[_currentRow, 7].Value = _bigDiaSteelPrice; // 2.02

            _currentRow++;

            _currentRow++;

            _workSheet.Cells[_currentRow, 1].Value = "Order#";
            _workSheet.Cells[_currentRow, 2].Value = "Order Date";
            _workSheet.Cells[_currentRow, 3].Value = "Customer";
            _workSheet.Cells[_currentRow, 4].Value = "Currency";
            _workSheet.Cells[_currentRow, 5].Value = "Description";
            _workSheet.Cells[_currentRow, 6].Value = "Diameter";
            _workSheet.Cells[_currentRow, 7].Value = "Steel Weight";
            _workSheet.Cells[_currentRow, 8].Value = "Selling Price";
            _workSheet.Cells[_currentRow, 9].Value = "Adj. Selling Price";
            _workSheet.Cells[_currentRow, 10].Value = "% Complete";
            _workSheet.Cells[_currentRow, 11].Value = "Steel Price";
            _workSheet.Cells[_currentRow, 12].Value = "Apply Steel Cost";
            _workSheet.Cells[_currentRow, 13].Value = "Steel Cost";
            _workSheet.Cells[_currentRow, 14].Formula = "=CONCATENATE(\"Labor ( \",$C$1*100,\" %)\")";
            _workSheet.Cells[_currentRow, 15].Formula = "=CONCATENATE(\"Overhead ( \",$C$2*100,\" %)\")";
            _workSheet.Cells[_currentRow, 16].Value = "Total WIP Incl. Steel";

            _currentRow++;

            _workSheet.View.FreezePanes(_currentRow, 1);
        }

        void OutputLine(d_order order)
        {
            bool outputOrderNumber = true;

            string curCell = ExcelCellBase.GetAddress(_currentRow, 4);

            decimal discount = (order.discountamount ?? 0) / order.OrderItems.Count;

            foreach (var item in order.OrderItems)
            {
                double percentComplete = 0;

                if (order.Parts.Any())
                {
                    var tmp = order.Parts.GetPartFromPrefix(item.prefix).ToList();

                    if (tmp.IsNotNull()
                        && tmp.Any())
                        percentComplete = tmp.Select(x => x.PercentComplete).Average() / 100;
                }



                // If an NCR, force percent complete to 100%
                if (order.ncr)
                    percentComplete = 100;


                // Adjust Apply Steel Field
                var applySteel = ( ( item.IsSawCut || percentComplete > 0.1005 ) && ( item.steelcost ?? 0 ) > 0.005m ).ToString().ToUpper();


                if (item.Charges.Any( x => x.chargetype.EqualsIgnoreCase( "STEEL" ) ))
                    applySteel = "CUSTOMER";



                if (outputOrderNumber)
                {
                    _workSheet.Cells[_currentRow, 1].Value = order.ordernumber;
                    _workSheet.Cells[_currentRow, 2].Value = order.orderdate.ToShortDateString();
                    _workSheet.Cells[_currentRow, 3].Value = order.Customer.name;
                    _workSheet.Cells[_currentRow, 4].Value = order.Customer.accountset.Trim().ToUpper();
                }

                _workSheet.Cells[_currentRow, 5].Value = item.description;
                _workSheet.Cells[_currentRow, 6].Value = item.ImperialDiameter;
                _workSheet.Cells[_currentRow, 7].Value = item.steelsurchargeweight;
                _workSheet.Cells[_currentRow, 8].Value = Convert.ToDecimal(item.price) + (item.steelcost ?? 0) - discount;
                _workSheet.Cells[_currentRow, 9].Formula = "=IF({0}=\"USD\", {1}, 1)*{2}".FormatWith(
                    curCell,
                    ExcelCellBase.GetAddress(1, 11, true),
                    ExcelCellBase.GetAddress(_currentRow, 8));
                _workSheet.Cells[_currentRow, 10].Value = percentComplete;
                _workSheet.Cells[_currentRow, 11].Formula = "=IF({curDiameter}<=24,{smallDiameter},{bigDiameter})".FormatWithName(new
                {
                    curDiameter = ExcelCellBase.GetAddress(_currentRow, 6),
                    smallDiameter = ExcelCellBase.GetAddress(2, 7, true),
                    bigDiameter = ExcelCellBase.GetAddress(3, 7, true)
                });
                _workSheet.Cells[_currentRow, 12].Value = applySteel;
                _workSheet.Cells[_currentRow, 13].Formula = "=IF({chargesteel}=\"TRUE\",{price}*{weight},0)".FormatWithName(new
                {
                    chargesteel = ExcelCellBase.GetAddress(_currentRow, 12),
                    percent = ExcelCellBase.GetAddress(_currentRow, 10),
                    sales = ExcelCellBase.GetAddress(_currentRow, 9),
                    price = ExcelCellBase.GetAddress(_currentRow, 11),
                    weight = ExcelCellBase.GetAddress(_currentRow, 7)
                });
                _workSheet.Cells[_currentRow, 14].Formula = "={price}*{percentComplete}*{laborRate}".FormatWithName(new
                {
                    price = ExcelCellBase.GetAddress(_currentRow, 9),
                    percentComplete = ExcelCellBase.GetAddress(_currentRow, 10),
                    laborRate = ExcelCellBase.GetAddress(1, 3, true)
                });
                _workSheet.Cells[_currentRow, 15].Formula = "={price}*{percentComplete}*{overheadRate}".FormatWithName(new
                {
                    price = ExcelCellBase.GetAddress(_currentRow, 9),
                    percentComplete = ExcelCellBase.GetAddress(_currentRow, 10),
                    overheadRate = ExcelCellBase.GetAddress(2, 3, true)
                });
                _workSheet.Cells[_currentRow, 16].Formula = "={steel}+{labor}+{overhead}".FormatWithName(new
                {
                    steel = ExcelCellBase.GetAddress(_currentRow, 13),
                    labor = ExcelCellBase.GetAddress(_currentRow, 14),
                    overhead = ExcelCellBase.GetAddress(_currentRow, 15)
                });


                if (item.prefix == "MI"
                    || item.prefix == "RI")
                {
                    _workSheet.Cells[_currentRow, 1, _currentRow, 16].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    _workSheet.Cells[_currentRow, 1, _currentRow, 16].Style.Fill.BackgroundColor.SetColor(Color.Tomato);
                }


                _currentRow++;
                outputOrderNumber = false;
            }
        }

        void OutputSummary()
        {
            int lastOrderRow = _currentRow;

            _currentRow++;

            _workSheet.Cells[_currentRow, 1].Value = "Summary";
            _workSheet.Cells[_currentRow, 8].Value = "Adj. Selling Price";
            _workSheet.Cells[_currentRow, 13].Value = "Steel Cost";
            _workSheet.Cells[_currentRow, 14].Value = "Labor";
            _workSheet.Cells[_currentRow, 15].Value = "Overhead";
            _workSheet.Cells[_currentRow, 16].Value = "Total";

            _currentRow++;

            _workSheet.Cells[_currentRow, 8].Formula = "=SUM({0}:{1})".FormatWith(ExcelCellBase.GetAddress(_orderRowStart, 8), ExcelCellBase.GetAddress(lastOrderRow, 8));
            _workSheet.Cells[_currentRow, 13].Formula = "=SUM({0}:{1})".FormatWith(ExcelCellBase.GetAddress(_orderRowStart, 13), ExcelCellBase.GetAddress(lastOrderRow, 13));
            _workSheet.Cells[_currentRow, 14].Formula = "=SUM({0}:{1})".FormatWith(ExcelCellBase.GetAddress(_orderRowStart, 14), ExcelCellBase.GetAddress(lastOrderRow, 14));
            _workSheet.Cells[_currentRow, 15].Formula = "=SUM({0}:{1})".FormatWith(ExcelCellBase.GetAddress(_orderRowStart, 15), ExcelCellBase.GetAddress(lastOrderRow, 15));
            _workSheet.Cells[_currentRow, 16].Formula = "=SUM({0}:{1})".FormatWith(ExcelCellBase.GetAddress(_orderRowStart, 16), ExcelCellBase.GetAddress(lastOrderRow, 16));
        }

        void ApplyFormatting()
        {
            _workSheet.Cells[1, 3].Style.Numberformat.Format = PERCENT_FORMAT_STRING;
            _workSheet.Cells[2, 3].Style.Numberformat.Format = PERCENT_FORMAT_STRING;

            _workSheet.Cells[2, 7].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            _workSheet.Cells[3, 7].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            _workSheet.Cells[1, 11].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;

            _workSheet.Cells[_orderRowStart, 6, _currentRow, 7].Style.Numberformat.Format = NUMBER_FORMAT_STRING;
            _workSheet.Cells[_orderRowStart, 8, _currentRow, 9].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            _workSheet.Cells[_orderRowStart, 10, _currentRow, 10].Style.Numberformat.Format = PERCENT_FORMAT_STRING;
            _workSheet.Cells[_orderRowStart, 11, _currentRow, 16].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            _workSheet.Cells[_orderRowStart, 12, _currentRow, 12].Style.Numberformat.Format = "@";
        }
    }
}