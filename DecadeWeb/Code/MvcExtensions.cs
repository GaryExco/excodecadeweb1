﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using BundlingAndMinifyingInlineCssJs.ResponseFilters;



namespace DecadeWeb
{
    public static partial class MvcExtensions
    {
        public static void MapRouteWithOptionalFormat( this RouteCollection routes, string name, string url, object defaults, object constraints = null )
        {
            Route explicitRoute = routes.MapRoute(
                name + "-ExplicitFormat",
                url + ".{format}",
                defaults,
                constraints );

            if (!explicitRoute.Constraints.ContainsKey( "format" ))
            {
                explicitRoute.Constraints.Add(
                    "format",
                    "html|xlsx" );
            }

            Route implicitRoute = routes.MapRoute(
                name,
                url,
                defaults,
                constraints );

            implicitRoute.Defaults.Add(
                "format",
                String.Empty );
        }
    }


    public abstract class ModelStateTempDataTransfer : ActionFilterAttribute
    {
        protected static readonly string Key = typeof (ModelStateTempDataTransfer).FullName;
    }


    public class ExportModelStateToTempData : ModelStateTempDataTransfer
    {
        public override void OnActionExecuted( ActionExecutedContext filterContext )
        {
            //Only export when ModelState is not valid
            if (!filterContext.Controller.ViewData.ModelState.IsValid)
            {
                //Export if we are redirecting
                if (( filterContext.Result is RedirectResult )
                    || ( filterContext.Result is RedirectToRouteResult ))
                    filterContext.Controller.TempData[Key] = filterContext.Controller.ViewData.ModelState;
            }

            base.OnActionExecuted( filterContext );
        }
    }


    public class ImportModelStateFromTempData : ModelStateTempDataTransfer
    {
        public override void OnActionExecuted( ActionExecutedContext filterContext )
        {
            var modelState = filterContext.Controller.TempData[Key] as ModelStateDictionary;

            if (modelState != null)
            {
                //Only Import if we are viewing
                if (filterContext.Result is ViewResult)
                    filterContext.Controller.ViewData.ModelState.Merge( modelState );
                else
                {
                    //Otherwise remove it.
                    filterContext.Controller.TempData.Remove( Key );
                }
            }

            base.OnActionExecuted( filterContext );
        }
    }


    public class BundleMinifyInlineCssJsAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting( ActionExecutingContext filterContext )
        {
            filterContext.HttpContext.Response.Filter = new BundleAndMinifyResponseFilter( filterContext.HttpContext.Response.Filter );
        }


        public override void OnResultExecuting( ResultExecutingContext filterContext )
        {
            if (_bundleMimeTypes.Any(
                x => x.Equals(
                    filterContext.HttpContext.Response.ContentType,
                    StringComparison.InvariantCultureIgnoreCase ) ))
                return;

            filterContext.HttpContext.Response.Filter = null;
        }


        private readonly HashSet<string> _bundleMimeTypes = new HashSet<string>
        {
            "text/html",
            "text/css",
            "text/plain",
            "text/javascript",
            "text/ecmascript",
            "application/json"
        };
    }
}