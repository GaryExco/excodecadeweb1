﻿namespace DecadeWeb.Code
{
    public class MonthEndFunctions
    {
        #region Old Report
        /*
        private const int COLUMN_COUNT = 23;

        public static MemoryStream GenerateReport(Database db, DateTime date)
        {
            var excelStream = new MemoryStream();
            var excelPkg = new ExcelPackage(excelStream);
            ExcelWorksheet workSheet = excelPkg.Workbook.Worksheets.Add("WIP");

            DateTime cutOffDate = date.ToFirstDayOfMonth();

            cutOffDate = cutOffDate.AddMonths(1);

            int row = 2,
                itemRow = 0;

            try
            {
                // Lookup Exchange Table
                var exchangeRates = db.Connection.Query<d_exchangerates>("SELECT * FROM d_exchangerates ORDER BY basecurrency, destcurrency").ToList();

                #region Get Open Orders
                IEnumerable<d_order> openOrders = db.Connection.Query<d_order, d_customer, d_order>(@"
                    SELECT d_order.ordernumber, d_order.orderdate, d_order.expshipdate, d_order.customerpo, d_order.customercode, d_order.nitride, 
                        d_order.steelsurcharge, d_order.sales, d_order.shipdate, d_order.discountamount, d_order.discountontotal, d_order.fasttrackcharge, 
                        d_order.discountpercentage, 
                        d_customer.territory, d_customer.accountset 
                    FROM d_order
                        INNER JOIN d_customer ON d_order.customercode = d_customer.customercode 
                    WHERE (invoicedate IS NULL OR invoicedate > @cutoffdate) 
                        AND orderdate < @cutoffdate 
                    ORDER BY ordernumber",
                    (order, customer) => { order.Customer = customer; return order; },
                    new { cutoffdate = cutOffDate },
                    splitOn: "territory"
                );
                #endregion

                #region Output Table Header
                workSheet.Cells[1, Columns.Ordernumber].Value = "Order #";
                workSheet.Cells[1, Columns.OrderDate].Value = "Order Date";
                workSheet.Cells[1, Columns.ExpShipDate].Value = "Exp. Ship";
                workSheet.Cells[1, Columns.DieType].Value = "Die Type";
                workSheet.Cells[1, Columns.Diameter].Value = "Diameter";
                workSheet.Cells[1, Columns.Thickness].Value = "Thickness";
                workSheet.Cells[1, Columns.PartCode].Value = "Part Code";
                workSheet.Cells[1, Columns.Quantity].Value = "Qty";
                workSheet.Cells[1, Columns.Price].Value = "Price";
                workSheet.Cells[1, Columns.Breakdown].Value = "Breakdown";
                workSheet.Cells[1, Columns.Extended].Value = "Extended";
                workSheet.Cells[1, Columns.SteelSurcharge].Value = "Steel Surcharge";
                workSheet.Cells[1, Columns.LastStation].Value = "Last Station";
                workSheet.Cells[1, Columns.PercentComplete].Value = "% Complete";
                workSheet.Cells[1, Columns.Weight].Value = "Weight";
                workSheet.Column(Columns.Weight).Hidden = true;
                workSheet.Cells[1, Columns.PricePerLB].Value = "$/lb.";
                workSheet.Column(Columns.PricePerLB).Hidden = true;
                workSheet.Cells[1, Columns.ApplyMaterialCost].Value = "Apply Material?";
                workSheet.Cells[1, Columns.Material].Value = "Mat'l";
                workSheet.Cells[1, Columns.DirectLabor].Value = "D. Labor";
                workSheet.Column(Columns.DirectLabor).Hidden = true;
                workSheet.Cells[1, Columns.LaborHours].Value = "Labor Hours";
                workSheet.Column(Columns.LaborHours).Hidden = true;
                workSheet.Cells[1, Columns.OverHead].Value = "Overhead";
                workSheet.Column(Columns.OverHead).Hidden = true;
                workSheet.Cells[1, Columns.LaborTotal].Value = "Labor Total";
                workSheet.Cells[1, Columns.WIPTotal].Value = "WIP Total";

                workSheet.Cells[2, COLUMN_COUNT + 1].Value = "Direct Labor Percentage:";
                workSheet.Cells[2, COLUMN_COUNT + 2].Value = ".15";
                workSheet.Cells[3, COLUMN_COUNT + 1].Value = "Cost per hour";
                workSheet.Cells[3, COLUMN_COUNT + 2].Value = ConvertToDefaultCurrency(exchangeRates, "USD", 24.71);
                workSheet.Cells[4, COLUMN_COUNT + 1].Value = "OH Rate";
                workSheet.Cells[4, COLUMN_COUNT + 2].Value = ConvertToDefaultCurrency(exchangeRates, "USD", 39.22);

                workSheet.View.FreezePanes(2, 1);
                #endregion

                #region Loop Orders
                foreach (d_order curOrder in openOrders)
                {
                    var orderParts = new List<string>();

                    if (curOrder.discountontotal && curOrder.discountpercentage.HasValue && curOrder.discountpercentage.Value == 100)
                        continue;

                    // Used to track if the full discount has been applied
                    decimal discount = curOrder.discountamount.HasValue ? curOrder.discountamount.Value : 0;

                    double orderCompletePercentage = 0;
                    int orderPercentCount = 0;

                    if (row > 2)
                    {
                        workSheet.Row(row).OutlineLevel = 0;

                        workSheet.Cells[row, 1, row, COLUMN_COUNT].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        workSheet.Cells[row, 1, row, COLUMN_COUNT].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSteelBlue);

                        row++;
                    }

                    #region Load Order Items
                    IEnumerable<d_orderitem> orderItems = db.Connection.Query<d_orderitem>(@"
                        SELECT qty, price, baseprice, dienumber, prefix, description, line, suffix, steelcost 
                        FROM d_orderitem 
                        WHERE ordernumber = @ordernum 
                        ORDER BY line",
                        new { ordernum = curOrder.ordernumber }
                    );
                    #endregion

                    #region Output Order Info
                    workSheet.Cells[row, Columns.Ordernumber].Value = curOrder.ordernumber;
                    workSheet.Cells[row, Columns.OrderDate].Value = curOrder.orderdate.ToShortDateString();
                    workSheet.Cells[row, Columns.ExpShipDate].Value = curOrder.expshipdate.ToShortDateString();
                    #endregion

                    #region Loop Order Items
                    foreach (d_orderitem curItem in orderItems)
                    {
                        var partCodes = new List<string>();
                        var taskCache = new List<d_task>();
                        curItem.ParseSuffix();

                        itemRow = row;

                        #region Load Charges
                        curItem.Charges = db.Connection.Query<d_orderitemcharges>(
                            "SELECT chargename, qty, price, chargepercentage " +
                            "FROM d_orderitemcharges " +
                            "WHERE ordernumber = @ordernum AND line = @line " +
                            "ORDER BY chargeline",
                            new {
                                ordernum = curOrder.ordernumber,
                                curItem.line }
                        ).ToList();
                        #endregion

                        if (!curItem.ImperialFlag)
                        {
                            curItem.Diameter /= 25.4;
                            curItem.Thickness /= 25.4;
                        }

                        #region Part Detection
                        string partCode = Mset.Exco.DecadeHelpers.GetPartCodeFromPrefix(curItem.prefix);

                        if (partCode != String.Empty)
                        {
                            partCodes.Add(partCode);
                        }
                        else
                        {
                            if (curItem.prefix == "HO")
                            {
                                partCodes = curItem.Order.GetParts(db, partsToFind: "MPB");
                            }
                            else
                            {
                                partCodes.Add("!");
                            }
                        }
                        #endregion

                        #region Output Order Item Information
                        double tmpPrice = curItem.baseprice.HasValue ? curItem.baseprice.Value : curItem.price;

                        if (discount >= Convert.ToDecimal(tmpPrice * curItem.qty))
                        {
                            discount -= Convert.ToDecimal(tmpPrice * curItem.qty);
                            tmpPrice = 0;
                        }
                        else if (discount > 0)
                        {
                            tmpPrice = tmpPrice - Convert.ToDouble(discount) / curItem.qty;
                            discount = 0;
                        }

                        workSheet.Cells[row, Columns.DieType].Value = Mset.Exco.Pricing.PrefixToDieType(curItem.prefix).ToString();
                        workSheet.Cells[row, Columns.Diameter].Value = curItem.Diameter;
                        workSheet.Cells[row, Columns.Thickness].Value = curItem.Thickness;
                        workSheet.Cells[row, Columns.Quantity].Value = curItem.qty;

                        workSheet.Cells[row, Columns.Price].Value = ConvertToDefaultCurrency(exchangeRates, curOrder.Customer.accountset, tmpPrice);
                        #endregion

                        orderParts.AddRange(partCodes);

                        #region Output Part Pricing
                        foreach (string p in partCodes)
                        {
                            FormatCells(workSheet.Cells, row);

                            if (curItem.prefix == "RI")
                                ApplyRowStyle(workSheet.Cells, row, RowStyle.InputNeeded);

                            workSheet.Row(row).OutlineLevel = 1;

                            string station = String.Empty;
                            double maxPercent = 0;

                            double thickness = 0,
                                   diameter = 0,
                                   lbPrice = 0,
                                   priceBreakdown,
                                   price = ConvertToDefaultCurrency(exchangeRates, curOrder.Customer.accountset, tmpPrice);

                            decimal tmpSteelSurcharge;

                            #region Calculate Hollow Pricing Breakdown

                            double mandrelMultiplier = 1,
                                   plateMultiplier = 1,
                                   backerMultiplier = 1;

                            if (curItem.prefix == "HO")
                            {
                                if (partCodes.Contains("M") && partCodes.Contains("P") && partCodes.Contains("B"))
                                {
                                    mandrelMultiplier = .5;
                                    plateMultiplier = .35;
                                    backerMultiplier = .15;
                                }
                                else if (partCodes.Contains("M") && partCodes.Contains("P") && !partCodes.Contains("B"))
                                {
                                    mandrelMultiplier = .6;
                                    plateMultiplier = .4;
                                    backerMultiplier = 0;
                                }
                                else if (partCodes.Contains("M") && !partCodes.Contains("P") && !partCodes.Contains("B"))
                                {
                                    mandrelMultiplier = 1;
                                    plateMultiplier = 0;
                                    backerMultiplier = 0;
                                }
                                else if (!partCodes.Contains("M") && partCodes.Contains("P") && !partCodes.Contains("B"))
                                {
                                    mandrelMultiplier = 0;
                                    plateMultiplier = 1;
                                    backerMultiplier = 0;
                                }
                                else if (!partCodes.Contains("M") && !partCodes.Contains("P") && partCodes.Contains("B"))
                                {
                                    mandrelMultiplier = 0;
                                    plateMultiplier = 0;
                                    backerMultiplier = 1;
                                }
                            }
                            #endregion

                            if (p != "!")
                            {
                                #region Calculate Material
                                #region Get Diameter From Bar
                                d_partsteelcut partCut = db.Connection.Query<d_partsteelcut>(
                                    "SELECT TOP 1 NumID " +
                                    "FROM d_partsteelcut " +
                                    "WHERE ordernumber = @ordernum " +
                                    "AND part = @partcode " +
                                    "ORDER BY id DESC",
                                    new { ordernum = curOrder.ordernumber, partcode = p }
                                    ).SingleOrDefault();

                                if (partCut != null)
                                {
                                    inv_steel steelInv = db.Connection.Query<inv_steel>(
                                        "SELECT TrueDia, lbPrice " +
                                        "FROM inv_steel " +
                                        "WHERE NumID = @barid",
                                        new { barid = partCut.NumID }
                                        ).SingleOrDefault();

                                    if (steelInv != null)
                                    {
                                        diameter = steelInv.TrueDia;
                                        if (steelInv.lbPrice.HasValue)
                                            lbPrice = steelInv.lbPrice.Value;
                                    }
                                }

                                #region Get Average Price (And Average Diameter if None Found)
                                if (Math.Abs(lbPrice) < 0.0005 || Math.Abs(diameter) < 0.0005)
                                {
                                    IEnumerable<inv_steel> lastTenBars = db.Connection.Query<inv_steel>(
                                        "SELECT TOP 10 TrueDia, lbPrice " +
                                        "FROM inv_steel INNER JOIN inv_steelinf ON inv_steel.BarInf = inv_steelinf.ID " +
                                        "WHERE Dia = @dieDia " +
                                        "AND DateIn < @cutoffdate " +
                                        "ORDER BY DateIn DESC",
                                        new { dieDia = Math.Ceiling(curItem.Diameter), cutoffdate = cutOffDate }
                                        );

                                    int barCnt = 0,
                                        priceCnt = 0;
                                    double dia = 0,
                                           avgPrice = 0;

                                    foreach (inv_steel curBar in lastTenBars)
                                    {
                                        dia += curBar.TrueDia;
                                        barCnt++;

                                        if (!curBar.lbPrice.HasValue)
                                            continue;

                                        avgPrice += curBar.lbPrice.Value;
                                        priceCnt++;
                                    }

                                    if (Math.Abs(diameter) < 0.0005 && dia > 0.01)
                                    {
                                        if (barCnt > 0)
                                            diameter = dia / barCnt;
                                        else
                                            diameter = curItem.Diameter * 1.01;
                                    }

                                    if (Math.Abs(lbPrice) < 0.0005)
                                        lbPrice = avgPrice / priceCnt;
                                }
                                #endregion
                                #endregion

                                #region Get Thickness from Turning
                                SteelCuts pieceThick = db.Connection.Query<SteelCuts>(@"
                                    SELECT TOP 1 Thickness
                                    FROM SteelCuts
                                    WHERE ordernumber = @ordernum
                                        AND PieceName = @partcode
                                        AND Deleted = '0'
                                    ORDER BY Thickness DESC",
                                    new { ordernum = curOrder.ordernumber, partcode = p })
                                    .SingleOrDefault();

                                thickness = pieceThick.IsNotNull()
                                                ? pieceThick.Thickness
                                                : curItem.Thickness;

                                // Add Stock to Thickness for Saw/Lathe
                                if (diameter <= 10)
                                    thickness += .12;
                                else if (diameter <= 14)
                                    thickness += .15;
                                else if (diameter <= 18)
                                    thickness += .18;
                                else if (diameter <= 22)
                                    thickness += .21;
                                else if (diameter > 22)
                                    thickness += .24;
                                #endregion
                                #endregion

                                #region Lookup Tracking Information
                                // Look up Last Scrap Time
                                d_task scrapTask = db.Connection.Query<d_task>(@"
                                    SELECT TOP 1 d_task.tasktime 
                                    FROM d_task 
                                    WHERE ordernumber = @ordernum 
                                    AND task = 'SR' 
                                    AND (part = @partcode OR part = 'G')
                                    AND tasktime < @cutoffdate 
                                    ORDER BY tasktime DESC",
                                    new { ordernum = curOrder.ordernumber, partcode = p, cutoffdate = cutOffDate }
                                    ).SingleOrDefault();

                                IEnumerable<d_task> tasks = db.Connection.Query<d_task, d_taskcode, d_task>(@"
                                    SELECT d_task.task, d_task.part, d_task.station, d_taskcode.feeder, d_taskcode.solidplate, d_taskcode.solidbacker,
                                           d_taskcode.mandrel, d_taskcode.hollowplate, d_taskcode.hollowbacker, d_taskcode.bolster 
                                    FROM d_task
                                        INNER JOIN d_taskcode ON d_task.task = d_taskcode.task 
                                    WHERE d_task.ordernumber = @ordernum 
                                        AND (d_task.part = @partcode OR d_task.part = 'G') 
                                        AND tasktime < @cutoffdate 
                                        AND tasktime > @mindate 
                                    ORDER BY tasktime DESC",
                                    (task, taskcode) => { task.TaskCode = taskcode; return task; },
                                    new
                                    {
                                        ordernum = curOrder.ordernumber,
                                        partcode = p,
                                        cutoffdate = cutOffDate,
                                        mindate = (scrapTask.IsNotNull() ? scrapTask.tasktime : new DateTime(1900, 1, 1))
                                    },
                                    splitOn: "feeder"
                                    )
                                    .ToList();

                                taskCache.AddRange(tasks);
                                #endregion

                                maxPercent = GetMaxPercentDone(tasks, curItem.prefix, p);

                                d_task tmpTask = GetMaxPercentDoneTask(tasks, curItem.prefix, p);
                                if (tmpTask != null)
                                    station = tmpTask.station;

                                orderCompletePercentage += maxPercent;
                                orderPercentCount += 1;
                            }

                            lbPrice = ConvertToDefaultCurrency(exchangeRates, "USD", lbPrice);

                            #region Get Price Breakdown
                            if (partCodes.Count <= 1)
                            {
                                priceBreakdown = price;
                                tmpSteelSurcharge = (curItem.steelcost.HasValue ? curItem.steelcost.Value : 0);
                            }
                            else
                            {
                                switch (p)
                                {
                                    case "M":
                                        priceBreakdown = price * mandrelMultiplier;
                                        tmpSteelSurcharge = (curItem.steelcost.HasValue ? curItem.steelcost.Value : 0) * Convert.ToDecimal(mandrelMultiplier);
                                        break;

                                    case "P":
                                        priceBreakdown = price * plateMultiplier;
                                        tmpSteelSurcharge = (curItem.steelcost.HasValue ? curItem.steelcost.Value : 0) * Convert.ToDecimal(plateMultiplier);
                                        break;

                                    case "B":
                                        priceBreakdown = price * backerMultiplier;
                                        tmpSteelSurcharge = (curItem.steelcost.HasValue ? curItem.steelcost.Value : 0) * Convert.ToDecimal(backerMultiplier);
                                        break;

                                    default:
                                        priceBreakdown = price;
                                        tmpSteelSurcharge = (curItem.steelcost.HasValue ? curItem.steelcost.Value : 0);
                                        break;
                                }
                            }
                            #endregion

                            workSheet.Cells[row, Columns.Breakdown].Value = priceBreakdown;
                            workSheet.Cells[row, Columns.Extended].Formula = String.Format("={0}*{1}", ExcelCellBase.GetAddress(row, Columns.Breakdown), ExcelCellBase.GetAddress(itemRow, Columns.Quantity));
                            workSheet.Cells[row, Columns.SteelSurcharge].Value = ConvertToDefaultCurrency(exchangeRates, curOrder.Customer.accountset, tmpSteelSurcharge);
                            workSheet.Cells[row, Columns.LastStation].Value = station;
                            workSheet.Cells[row, Columns.PercentComplete].Value = maxPercent / 100;

                            if (maxPercent > 10)
                                workSheet.Cells[row, Columns.ApplyMaterialCost].Value = "T";

                            if (p != "!")
                            {
                                workSheet.Cells[row, Columns.PartCode].Value = p;
                                workSheet.Cells[row, Columns.Weight].Value = diameter * diameter * thickness * .224;
                                workSheet.Cells[row, Columns.PricePerLB].Value = lbPrice;
                            }
                            else
                            {
                                ApplyRowStyle(workSheet.Cells, row, RowStyle.InputNeeded);
                            }

                            ApplyFormulas(workSheet.Cells, row);

                            row++;
                        }
                        #endregion

                        #region Output Charges
                        foreach (d_orderitemcharges c in curItem.Charges)
                        {
                            workSheet.Row(row).OutlineLevel = 1;

                            FormatCells(workSheet.Cells, row);
                            ApplyRowStyle(workSheet.Cells, row, RowStyle.Charge);

                            workSheet.Cells[row, Columns.Diameter, row, Columns.Thickness].Merge = true;
                            workSheet.Cells[row, Columns.Diameter].Value = c.chargename;
                            workSheet.Cells[row, Columns.Quantity].Value = c.qty;
                            workSheet.Cells[row, Columns.Price].Value = ConvertToDefaultCurrency(exchangeRates, curOrder.Customer.accountset, c.price / c.qty);
                            workSheet.Cells[row, Columns.Breakdown].Value = ConvertToDefaultCurrency(exchangeRates, curOrder.Customer.accountset, c.price / c.qty);
                            workSheet.Cells[row, Columns.Extended].Formula = "={0}*{1}*{2}".FormatWith(
                                ExcelCellBase.GetAddress(row, Columns.Breakdown),
                                ExcelCellBase.GetAddress(row, Columns.Quantity),
                                ExcelCellBase.GetAddress(itemRow, Columns.Quantity));
                            workSheet.Cells[row, Columns.Weight].Value = 0;
                            workSheet.Cells[row, Columns.PricePerLB].Value = 0;
                            workSheet.Cells[row, Columns.Material].Value = 0;

                            #region Calculate Percent Complete for the charge
                            double chargePercentage = 0;
                            string chargeName = c.chargename;
                            bool doAverage = true;

                            #region Parse Charges
                            if (Contains(chargeName, "Bolts"))
                            {
                                chargePercentage = GetPercentDone(taskCache, new List<string> { "IN" }, "");
                                doAverage = false;
                            }
                            else if (Contains(chargeName, "Pocket") || Contains(chargeName, "Camara") || Contains(chargeName, "PKT") || Contains(chargeName, "Clearance"))
                            {
                                chargePercentage = GetPercentDone(taskCache, new List<string> { "NC", "NF", "NO", "NS" }, "P");
                                doAverage = false;
                            }
                            else if (Contains(chargeName, "C Route") || Contains(chargeName, "CRoute") || Contains(chargeName, "C-Route") || Contains(chargeName, "Choke") || Contains(chargeName, "Hilo") || Contains(chargeName, "Taper") || Contains(chargeName, "Triple Skim") || Contains(chargeName, "Wire") || Contains(chargeName, "Zero Bearing") || Contains(chargeName, "Heat Sink") || Contains(chargeName, "Heatsink") || Contains(chargeName, "Heat-Sink"))
                            {
                                chargePercentage = GetPercentDone(taskCache, new List<string> { "PC", "PR", "RC", "RF", "RR", "WD", "WE", "WF", "WR" }, "P");
                                doAverage = false;
                            }
                            else if (Contains(chargeName, "N2"))
                            {
                                string tmpPartCode = "B";

                                if (curItem.prefix != "HO" || (curItem.prefix == "HO" && partCodes.Contains("B")))
                                    tmpPartCode = "B";
                                else if (curItem.prefix == "HO" && !partCodes.Contains("B"))
                                    tmpPartCode = "P";
                                else if (curItem.prefix == "BO")
                                    tmpPartCode = "O";

                                chargePercentage = GetPercentDone(taskCache, new List<string> { "NC", "NF", "NO", "NS" }, tmpPartCode);
                                doAverage = false;
                            }
                            else if (Contains(chargeName, "Nitri"))
                            {
                                chargePercentage = GetPercentDone(taskCache, new List<string> { "IN", "NT", "NI" }, "");
                                doAverage = false;
                            }
                            else if (curItem.prefix == "HO" && (Contains(chargeName, "Bridge") || Contains(chargeName, "Butterfly") || Contains(chargeName, "Centre") || Contains(chargeName, "Extended") || Contains(chargeName, "Fake") || Contains(chargeName, "Mandrel") || Contains(chargeName, "Mandril") || Contains(chargeName, "Mandrl") || Contains(chargeName, "Bridge")))
                            {
                                chargePercentage = GetMaxPercentDone(taskCache, curItem.prefix, "M");
                                doAverage = false;
                            }
                            #endregion

                            if (doAverage)
                            {
                                int cnt = 0;
                                double percentages = 0;

                                foreach (string curPart in partCodes)
                                {
                                    percentages += GetMaxPercentDone(taskCache, curItem.prefix, curPart);
                                    cnt++;
                                }

                                if (cnt > 0)
                                    chargePercentage = percentages / cnt;
                            }

                            workSheet.Cells[row, Columns.PercentComplete].Value = (chargePercentage / 100);
                            #endregion

                            ApplyFormulas(workSheet.Cells, row);

                            row++;
                        }
                        #endregion
                    }
                    #endregion

                    #region Output Fasttrack Charge
                    if (curOrder.fasttrackcharge.HasValue && curOrder.fasttrackcharge.Value > 0)
                    {
                        workSheet.Row(row).OutlineLevel = 1;

                        FormatCells(workSheet.Cells, row);
                        ApplyRowStyle(workSheet.Cells, row, RowStyle.Charge);

                        workSheet.Cells[row, Columns.Diameter, row, Columns.Thickness].Merge = true;
                        workSheet.Cells[row, Columns.Diameter].Value = "Fasttrack Up-Charge";
                        workSheet.Cells[row, Columns.Quantity].Value = 1;
                        workSheet.Cells[row, Columns.Price].Value = ConvertToDefaultCurrency(exchangeRates, curOrder.Customer.accountset, curOrder.fasttrackcharge.Value);
                        workSheet.Cells[row, Columns.Breakdown].Value = ConvertToDefaultCurrency(exchangeRates, curOrder.Customer.accountset, curOrder.fasttrackcharge.Value);
                        workSheet.Cells[row, Columns.Extended].Formula = "={0}*{1}*{2}".FormatWith(
                            ExcelCellBase.GetAddress(row, Columns.Breakdown),
                            ExcelCellBase.GetAddress(row, Columns.Quantity),
                            ExcelCellBase.GetAddress(itemRow, Columns.Quantity));
                        workSheet.Cells[row, Columns.Weight].Value = 0;
                        workSheet.Cells[row, Columns.PricePerLB].Value = 0;
                        workSheet.Cells[row, Columns.Material].Value = 0;

                        workSheet.Cells[row, Columns.PercentComplete].Value = (orderCompletePercentage / orderPercentCount / 100);
                    }
                    #endregion
                }
                #endregion

                #region Output Summary Line
                workSheet.Cells[row, 1, row, COLUMN_COUNT].Style.Fill.PatternType = ExcelFillStyle.Solid;
                workSheet.Cells[row, 1, row, COLUMN_COUNT].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(79, 129, 189));
                workSheet.Cells[row, 1, row, COLUMN_COUNT].Style.Font.Color.SetColor(System.Drawing.Color.White);
                FormatCells(workSheet.Cells, row);

                workSheet.Cells[row, Columns.Price].Formula = "=SUM({0}:{1})".FormatWith(
                    ExcelCellBase.GetAddress(2, Columns.Price),
                    ExcelCellBase.GetAddress(row - 1, Columns.Price));

                workSheet.Cells[row, Columns.Breakdown].Formula = "=SUM({0}:{1})".FormatWith(
                    ExcelCellBase.GetAddress(2, Columns.Breakdown),
                    ExcelCellBase.GetAddress(row - 1, Columns.Breakdown));

                workSheet.Cells[row, Columns.Extended].Formula = "=SUM({0}:{1})".FormatWith(
                    ExcelCellBase.GetAddress(2, Columns.Extended),
                    ExcelCellBase.GetAddress(row - 1, Columns.Extended));

                workSheet.Cells[row, Columns.Material].Formula = "=SUM({0}:{1})".FormatWith(
                    ExcelCellBase.GetAddress(2, Columns.Material),
                    ExcelCellBase.GetAddress(row - 1, Columns.Material));

                workSheet.Cells[row, Columns.DirectLabor].Formula = "=SUM({0}:{1})".FormatWith(
                    ExcelCellBase.GetAddress(2, Columns.DirectLabor),
                    ExcelCellBase.GetAddress(row - 1, Columns.DirectLabor));

                workSheet.Cells[row, Columns.OverHead].Formula = "=SUM({0}:{1})".FormatWith(
                    ExcelCellBase.GetAddress(2, Columns.OverHead),
                    ExcelCellBase.GetAddress(row - 1, Columns.OverHead));

                workSheet.Cells[row, Columns.LaborTotal].Formula = "=SUM({0}:{1})".FormatWith(
                    ExcelCellBase.GetAddress(2, Columns.LaborTotal),
                    ExcelCellBase.GetAddress(row - 1, Columns.LaborTotal));

                workSheet.Cells[row, Columns.WIPTotal].Formula = "=SUM({0}:{1})".FormatWith(
                    ExcelCellBase.GetAddress(2, Columns.WIPTotal),
                    ExcelCellBase.GetAddress(row - 1, Columns.WIPTotal));
                #endregion

                workSheet.Cells.AutoFitColumns(0);
                workSheet.Column(Columns.DirectLabor).Hidden = true;
                workSheet.Column(Columns.LaborHours).Hidden = true;
                workSheet.Column(Columns.OverHead).Hidden = true;

                excelPkg.Save();
                excelStream.Seek(0, SeekOrigin.Begin);
            }
                // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                // Response.Write(ex.ToString());
            }

            return excelStream;
        }

        private static void FormatCells(ExcelRange cells, int row)
        {
            cells[row, Columns.OrderDate].Style.Numberformat.Format = DATE_FORMAT_STRING;
            cells[row, Columns.ExpShipDate].Style.Numberformat.Format = DATE_FORMAT_STRING;
            cells[row, Columns.Diameter].Style.Numberformat.Format = NUMBER_FORMAT_STRING;
            cells[row, Columns.Thickness].Style.Numberformat.Format = NUMBER_FORMAT_STRING;
            cells[row, Columns.Price].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.Breakdown].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.Extended].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.SteelSurcharge].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.PercentComplete].Style.Numberformat.Format = PERCENT_FORMAT_STRING;
            cells[row, Columns.Weight].Style.Numberformat.Format = NUMBER_FORMAT_STRING;
            cells[row, Columns.PricePerLB].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.Material].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.Material].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.DirectLabor].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.LaborHours].Style.Numberformat.Format = NUMBER_FORMAT_STRING;
            cells[row, Columns.OverHead].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.LaborTotal].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
            cells[row, Columns.WIPTotal].Style.Numberformat.Format = CURRENCY_FORMAT_STRING;
        }

        private static void ApplyRowStyle(ExcelRange cells, int row, RowStyle style)
        {
            switch (style)
            {
                case RowStyle.Charge:
                    cells[row, 1, row, COLUMN_COUNT].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    cells[row, 1, row, COLUMN_COUNT].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(255, 255, 204));
                    break;

                case RowStyle.InputNeeded:
                    cells[row, 1, row, COLUMN_COUNT].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    cells[row, 1, row, COLUMN_COUNT].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(255, 204, 153));
                    cells[row, 1, row, COLUMN_COUNT].Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(63, 63, 118));
                    break;
            }
        }

        private static void ApplyFormulas(ExcelRange cells, int row)
        {
            cells[row, Columns.Material].Formula = String.Format("=IF(OR({0}=\"Y\", {0}=\"T\", {0}=1, {0}=\"TRUE\", {0}=\"YES\"),{1}*{2},0)", ExcelCellBase.GetAddress(row, Columns.ApplyMaterialCost), ExcelCellBase.GetAddress(row, Columns.Weight), ExcelCellBase.GetAddress(row, Columns.PricePerLB));
            cells[row, Columns.DirectLabor].Formula = String.Format("=({0}+{1})*{2}*{3}", ExcelCellBase.GetAddress(row, Columns.Extended), ExcelCellBase.GetAddress(row, Columns.SteelSurcharge), ExcelCellBase.GetAddress(row, Columns.PercentComplete), ExcelCellBase.GetAddress(2, COLUMN_COUNT + 2, true));
            cells[row, Columns.LaborHours].Formula = String.Format("={0}/{1}", ExcelCellBase.GetAddress(row, Columns.DirectLabor), ExcelCellBase.GetAddress(3, COLUMN_COUNT + 2, true));
            cells[row, Columns.OverHead].Formula = String.Format("={0}*{1}", ExcelCellBase.GetAddress(row, Columns.LaborHours), ExcelCellBase.GetAddress(4, COLUMN_COUNT + 2, true));
            cells[row, Columns.LaborTotal].Formula = String.Format("={0}+{1}", ExcelCellBase.GetAddress(row, Columns.DirectLabor), ExcelCellBase.GetAddress(row, Columns.OverHead));
            cells[row, Columns.WIPTotal].Formula = String.Format("={0}+{1}", ExcelCellBase.GetAddress(row, Columns.Material), ExcelCellBase.GetAddress(row, Columns.LaborTotal));
        }

        private static bool Contains(string name, string search)
        {
            return System.Globalization.CultureInfo.InvariantCulture.CompareInfo.IndexOf(name, search, System.Globalization.CompareOptions.OrdinalIgnoreCase) >= 0;
        }

        private static double GetPercentDone(IEnumerable<d_task> tasks, IEnumerable<string> tasknames, string partcode)
        {
            double rtnVal = 0;

            var lookForTask = tasks.Where(t => tasknames.Contains(t.task)).ToList();

            if (partcode != String.Empty)
                lookForTask = lookForTask.Where(t => t.part.EqualsIgnoreCase(partcode) || t.part.EqualsIgnoreCase("G")).ToList();

            if (lookForTask.Count > 0)
                rtnVal = 100;

            return rtnVal;
        }

        private static double GetMaxPercentDone(IEnumerable<d_task> tasks, string prefix, string partcode)
        {
            d_task tmp = GetMaxPercentDoneTask(tasks, prefix, partcode);

            if (tmp.IsNull())
                return 0;

            switch (partcode.ToUpper())
            {
                case "F":
                    return tmp.TaskCode.feeder;

                case "M":
                    return tmp.TaskCode.mandrel;

                case "P":
                    return prefix != "HO"
                               ? tmp.TaskCode.solidplate
                               : tmp.TaskCode.hollowplate;

                case "B":
                    return prefix != "HO"
                               ? tmp.TaskCode.solidbacker
                               : tmp.TaskCode.hollowbacker;

                case "O":
                    return tmp.TaskCode.bolster;
            }

            return 0;
        }

        private static d_task GetMaxPercentDoneTask(IEnumerable<d_task> tasks, string prefix, string partcode)
        {
            d_task rtnVal = null;

            var tmpTasks = tasks.Where(t => t.part.ToUpper() == partcode || t.part.ToUpper() == "G").ToList();

            if (!tmpTasks.Any())
                return null;

            switch (partcode.ToUpper())
            {
                case "F":
                    rtnVal = tmpTasks.MaxBy(t => t.TaskCode.feeder);
                    break;

                case "M":
                    rtnVal = tmpTasks.MaxBy(t => t.TaskCode.mandrel);
                    break;

                case "P":
                    rtnVal = prefix != "HO"
                                 ? tmpTasks.MaxBy(t => t.TaskCode.solidplate)
                                 : tmpTasks.MaxBy(t => t.TaskCode.hollowplate);
                    break;

                case "B":
                    rtnVal = prefix != "HO"
                                 ? tmpTasks.MaxBy(t => t.TaskCode.solidbacker)
                                 : tmpTasks.MaxBy(t => t.TaskCode.hollowbacker);
                    break;

                case "O":
                    rtnVal = tmpTasks.MaxBy(t => t.TaskCode.bolster);
                    break;
            }

            return rtnVal;
        }

        private static double ConvertToDefaultCurrency(IEnumerable<d_exchangerates> exchRates, string baseCurrency, double num)
        {
            return ConvertToDefaultCurrency(exchRates, baseCurrency, Settings.DefaultCurrency, num);
        }

        private static double ConvertToDefaultCurrency(IEnumerable<d_exchangerates> exchRates, string baseCurrency, string destCurrency, double num)
        {
            if (destCurrency == String.Empty)
                destCurrency = Settings.DefaultCurrency;

            if (String.Equals(baseCurrency, destCurrency, StringComparison.OrdinalIgnoreCase))
                return num;

            decimal rate = exchRates.Where(c => String.Equals(c.basecurrency, baseCurrency, StringComparison.OrdinalIgnoreCase) && String.Equals(c.destcurrency, destCurrency, StringComparison.OrdinalIgnoreCase)).Select(c => c.factor).Max();

            return num * Convert.ToDouble(rate);
        }

        private static double ConvertToDefaultCurrency(IEnumerable<d_exchangerates> exchRates, string baseCurrency, decimal num)
        {
            return ConvertToDefaultCurrency(exchRates, baseCurrency, Convert.ToDouble(num));
        }

        static class Columns
        {
            // ReSharper disable InconsistentNaming
            public const int Ordernumber = 1,
                             OrderDate = 2,
                             ExpShipDate = 3,
                             DieType = 4,
                             Diameter = 5,
                             Thickness = 6,
                             PartCode = 7,
                             Quantity = 8,
                             Price = 9,
                             Breakdown = 10,
                             Extended = 11,
                             SteelSurcharge = 12,
                             LastStation = 13,
                             PercentComplete = 14,
                             Weight = 15,
                             PricePerLB = 16,
                             ApplyMaterialCost = 17,
                             Material = 18,
                             DirectLabor = 19,
                             LaborHours = 20,
                             OverHead = 21,
                             LaborTotal = 22,
                             WIPTotal = 23;
            // ReSharper restore InconsistentNaming
        }

        enum RowStyle
        {
            Charge,
            InputNeeded
        }
        */
        #endregion
    }
}