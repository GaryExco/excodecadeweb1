﻿using System.Text;
using System.Web;
using System.Web.Mvc;
using Mset.Extensions;
using Newtonsoft.Json;



namespace DecadeWeb
{
    public class JsonNetResult : ContentResult
    {
        public new Encoding ContentEncoding { get; set; }


        public new string ContentType { get; set; }


        public object Data { get; set; }


        public JsonSerializerSettings SerializerSettings { get; set; }


        public Formatting Formatting { get; set; }


        public JsonNetResult()
        {
            SerializerSettings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Error
            };
        }


        public override void ExecuteResult( ControllerContext context )
        {
            context.ThrowIfNull();

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = !ContentType.IsNullOrEmpty()
                                       ? ContentType
                                       : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data != null)
            {
                var writer = new JsonTextWriter( response.Output )
                {
                    Formatting = Formatting
                };

                JsonSerializer serializer = JsonSerializer.Create( SerializerSettings );
                serializer.Serialize(
                    writer,
                    Data );

                writer.Flush();
            }
        }
    }
}