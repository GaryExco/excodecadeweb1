﻿using System;
using System.Web;
using Mset.Database;
using Mset.Exco;
using StackExchange.Profiling;
using StackExchange.Profiling.Data;



namespace DecadeWeb
{
    public static class Helpers
    {
        private const string DB_APPLICATION_NAME = ";Application Name=DecadeWeb;Timeout=240;";


        public static bool IsPOST
        {
            get
            {
                return HttpContext.Current.Request.HttpMethod.Equals(
                    "POST",
                    StringComparison.InvariantCultureIgnoreCase );
            }
        }


        public static string CustomerCodeRegexPattern
        {
            get { return "[A-Z0-9]{1}[0-9]{5}"; }
        }


        public static Database GetDatabase()
        {
            var db = new Database( Settings.DecadeConn + DB_APPLICATION_NAME );
            db.Connection = new ProfiledDbConnection(
                db.Connection,
                MiniProfiler.Current );

            return db;
        }


        public static Database GetCustNotesDatabase()
        {
            var db = new Database( DatabaseInfo.CustNotesStr + DB_APPLICATION_NAME );
            db.Connection = new ProfiledDbConnection(
                db.Connection,
                MiniProfiler.Current );

            return db;
        }
    }
}