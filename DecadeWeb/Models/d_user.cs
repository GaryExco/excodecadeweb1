﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Web.Security;
using Mset.Database;



namespace DecadeWeb
{
    public class d_userPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }


        public bool IsInRole( string Role )
        {
            var user = Identity as d_user;

            if (user == null)
                return false;

            var roles = Role.Split( ',' ).Select( x => x.Trim() );
            foreach (var role in roles)
            {
                switch (role.ToUpper())
                {
                    case "GOD":
                        return user.a_god;

                    case "ADMIN":
                        return user.a_admin;

                    case "MODORDERS":
                        return user.a_modorders;

                    case "VIEWORDERS":
                        return user.a_vieworders || user.a_modorders;

                    case "MODCUSTOMERS":
                        return user.a_modcustomers;

                    case "VIEWCUSTOMERS":
                        return user.a_viewcustomers || user.a_modcustomers;

                    case "REPORTSADMIN":
                        return user.a_adminreports;

                    case "REPORTSSHOP":
                        return user.a_shopreports;

                    case "SHIPPING":
                        return user.a_ship;

                    case "STEEL":
                        return user.a_steel || user.a_steelpricing;

                    case "STEELPRICING":
                        return user.a_steelpricing;

                    case "TRACKING":
                        return user.a_tracking;

                    case "HOTLISTADMIN":
                        return user.a_hotlistadmin;

                    case "HOTLISTVIEW":
                        return user.a_hotlistview || user.a_hotlistadmin;

                    case "DEPSCHEDADMIN":
                        return user.a_depschedadmin;

                    case "HTSCHEDVIEW":
                        return user.a_htschedview;

                    case "NONCONF":
                        return user.a_scrap;
                }
            }

            return false;
        }


        public d_userPrincipal()
        {
            d_user user = d_user.GetUserByEmployeeNumber( Membership.GetUser().UserName );
            if (user == null
                || user.employeenumber == 0)
                Identity = new d_user();
            else
                Identity = user;
        }


        public d_userPrincipal( d_user user )
        {
            Identity = user;
        }
    }


    public class d_user : Mset.Exco.Decade.d_user, IIdentity
    {
        public static d_user GetUserByEmployeeNumber( object employeenumber )
        {
            var rtnVal = new d_user();
            Database db = Helpers.GetDatabase();

            try
            {
                db.Open();
                Mset.Exco.Decade.d_user tmpUser = GetUserByEmployeeNumber(
                    db,
                    employeenumber );

                FieldInfo[] fields = tmpUser.GetType().GetFields( BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance );
                foreach (FieldInfo fi in fields)
                {
                    fi.SetValue(
                        rtnVal,
                        fi.GetValue( tmpUser ) );
                }
            }
                // ReSharper disable once EmptyGeneralCatchClause
            catch { }
            finally
            {
                if (db != null)
                    db.Dispose();
            }

            return rtnVal;
        }


        public string AuthenticationType
        {
            get { return "Custom"; }
        }


        public bool IsAuthenticated
        {
            get { return _employeenumber_ValueSet; }
        }


        public new string Name
        {
            get
            {
                return _employeenumber_ValueSet
                           ? employeenumber.ToString( CultureInfo.InvariantCulture )
                           : String.Empty;
            }
        }
    }
}