﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class OrdersListModel
    {
        public IEnumerable<d_order> Orders { get; set; }


        public string Title { get; set; }


        public bool IsLateList { get; set; }


        public OrdersListModel() { }


        public OrdersListModel( string title, IEnumerable<d_order> orders, bool isLateList = false )
        {
            Title = title;
            Orders = orders;
            IsLateList = isLateList;
        }
    }
}