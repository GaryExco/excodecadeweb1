﻿using System;
using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class OnTimeModel
    {
        public DateTime Date { get; set; }


        public IEnumerable<d_order> Orders { get; set; }


        public DayMonthYearEnum Mode { get; set; }


        public string CustomerCode { get; set; }


        public OnTimePerformance Performance { get; set; }


        public OnTimeModel() { }


        public OnTimeModel( DateTime date, IEnumerable<d_order> orders, DayMonthYearEnum mode, string customercode = null )
        {
            Date = date;
            Orders = orders;
            Mode = mode;
            CustomerCode = customercode;
        }
    }


    public enum OnTimePerformance
    {
        All,


        Normal,


        FastTrack,


        HotList
    }
}