﻿using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class TrackOtherModel
    {
        public int OrderNumber { get; set; }


        public TrackOtherViewType ViewType { get; set; }


        public d_order Order { get; set; }


        public TrackOtherModel( int ordernumber, TrackOtherViewType viewType ) : this( ordernumber,
            viewType,
            null ) { }


        public TrackOtherModel( int orderNumber, TrackOtherViewType viewType, d_order order )
        {
            OrderNumber = orderNumber;
            ViewType = viewType;
            Order = order;
        }
    }


    public enum TrackOtherViewType
    {
        Track,


        Order
    }
}