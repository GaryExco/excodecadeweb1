﻿using System;
using System.Collections.Generic;
using Mset.Exco;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class AdminSchedModel
    {
        public DateTime StartDate { get; set; }


        public DateTime EndDate { get; set; }


        public Dictionary<DateTime, List<d_orderitem>> Orders { get; set; }


        public AdminSchedShowMode DispMode { get; set; }


        public DieType DieType { get; set; }


        public bool CheckHTDone { get; set; }
    }


    public enum AdminSchedShowMode
    {
        Ordernumber,


        DieNumber
    }
}