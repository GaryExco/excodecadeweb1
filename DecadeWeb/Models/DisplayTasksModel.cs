﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class DisplayTasksModel
    {
        public string Title { get; set; }


        public string SubTitle { get; set; }


        public IEnumerable<d_task> Tasks { get; set; }


        public bool ForceDisplayTaskList { get; set; }


        public DisplayTasksModel( string title, string subtitle, IEnumerable<d_task> tasks, bool forceDisplayTaskList = false )
        {
            Title = title;
            SubTitle = subtitle;
            Tasks = tasks;
            ForceDisplayTaskList = forceDisplayTaskList;
        }
    }
}