﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mset.Exco.Decade;
using Mset.Extensions;


namespace DecadeWeb.Models
{
    public class ScheduleOrderLineModel
    {
        public d_dailyorderlimits Limit { get; set; }


        public Func<d_order, double> CounterFunction { get; set; }


        public string LimitString
        {
            get
            {
                var tmpLimit = Limit.limit > 0
                                   ? Limit.limit.ToString(Limit.formatstring)
                                   : "-";

                return $"({tmpLimit})";
            }
        }


        public string Title => $"{LimitString} {Limit.name}:";


        public double Value( List<d_order> orders )
        {
            // Pre-Filter our Orders Before Our Counter Function
            //   Ignore MI Only Orders
            var filteredItems = orders
                .Where( x => !x.OrderItems.First().prefix.EqualsIgnoreCase( "MI" ) )
                .Select( x => new
                {
                    Order = x,
                    Diameter = ( x.OrderItems.FirstOrDefault( y => y.prefix.EqualsIgnoreCase( Limit.code ) ) ?? x.OrderItems.First() ).ImperialDiameter
                } )
                .ToList();


            // Filter our Orders on Their Diameter
            var filteredOrders = filteredItems
                .WhereIf( Limit.mindiameter >= 0, x => x.Diameter >= Limit.mindiameter )
                .WhereIf( Limit.maxdiameter >= 0, x => x.Diameter <= Limit.maxdiameter )
                .Select( x => x.Order )
                .ToList();


            return filteredOrders.Sum(x => CounterFunction(x));
        }
    }
}
