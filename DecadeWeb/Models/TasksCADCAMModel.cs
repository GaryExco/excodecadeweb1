﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb
{
    public class TasksCADCAMModel
    {
        public string Department { get; set; }


        public IEnumerable<d_task> Tasks { get; set; }
    }
}