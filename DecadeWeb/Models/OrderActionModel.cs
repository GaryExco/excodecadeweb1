﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class OrderActionModel
    {
        public string Title { get; set; }


        public IEnumerable<d_order> Orders { get; set; }
    }
}