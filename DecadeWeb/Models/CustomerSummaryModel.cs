﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class CustomerSummaryModel
    {
        public IDictionary<d_order, IEnumerable<AttentionReason>> AttentionOrders { get; set; }


        public IEnumerable<d_order> OpenOrders { get; set; }


        public IEnumerable<d_customer_order_count_total> ThisYear { get; set; }
    }


    public enum AttentionReason
    {
        Late,


        NoPO,


        OnHold
    }
}