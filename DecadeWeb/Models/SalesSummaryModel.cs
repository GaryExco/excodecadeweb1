﻿using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class SalesSummaryModel
    {
        public d_customer Customer { get; set; }


        public d_customer_order_count_total LastYearSales { get; set; }


        public d_customer_order_count_total ThisYearSales { get; set; }


        public d_customer_order_count_total ThisMonthSales { get; set; }


        public bool Output
        {
            get
            {
                return LastYearSales.ordertotal.HasValue && LastYearSales.ordertotal.Value != 0 ||
                       ThisYearSales.ordertotal.HasValue && ThisYearSales.ordertotal.Value != 0;
            }
        }


        public SalesSummaryModel(
            d_customer customer,
            d_customer_order_count_total lastYearSales,
            d_customer_order_count_total thisYearSales,
            d_customer_order_count_total thisMonthSales )
        {
            Customer = customer;
            LastYearSales = lastYearSales;
            ThisYearSales = thisYearSales;
            ThisMonthSales = thisMonthSales;
        }
    }
}