﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class AdminConfigModel
    {
        public IEnumerable<d_dailylimits> DailyLimits { get; set; }


        public IEnumerable<d_exchangerates> ExchangeRates { get; set; }


        public IEnumerable<Mset.Exco.Decade.d_user> Users { get; set; }
    }
}