﻿using System;
using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class ScheduleOrderModel
    {
        public int Days { get; set; }


        public Dictionary<int, List<d_order>> Orders { get; set; }


        public List<ScheduleOrderLineModel> Rows { get; set; }
    }
}
