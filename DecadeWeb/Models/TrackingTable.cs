﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class TrackingTable
    {
        public d_partsteelcut SteelCut { get; set; }


        public IEnumerable<d_task> Tasks { get; set; }


        public string Title { get; set; }


        public double PercentComplete { get; set; }


        public bool DisplayProgressBar { get; set; }


        public string PartCode { get; set; }


        public IEnumerable<d_inprocessinspection> IPIChecks { get; set; }
    }
}