﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class OrderEntryOrderModel
    {
        public d_order Order { get; set; }


        public IEnumerable<d_shippingvendors> ShippingVendors { get; set; }


        public OrderEntryOrderModel( d_order order, IEnumerable<d_shippingvendors> shipVendors )
        {
            Order = order;
            ShippingVendors = shipVendors;
        }
    }
}