﻿using System;
using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class DailyTotalsModel
    {
        public IEnumerable<d_territories> Territories { get; set; }


        public IEnumerable<d_order> Orders { get; set; }


        public DateTime StartDate { get; set; }


        public DateTime EndDate { get; set; }


        public decimal ExchangeRate { get; set; }


        public DailyTotalsMode Mode { get; set; }
    }
}