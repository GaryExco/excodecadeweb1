﻿using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class OrderEntryJsonModel
    {
        public d_order Order { get; set; }


        public string Print { get; set; }


        public bool Geninvoicenumber { get; set; }
    }
}