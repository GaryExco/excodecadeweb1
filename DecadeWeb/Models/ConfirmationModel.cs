﻿using System;
using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class ConfirmationModel
    {
        public bool IsOrderConfirmation { get; set; }


        public DateTime ConfirmationDate { get; set; }


        public d_customer Customer { get; set; }


        public IEnumerable<d_mailinglist> Contacts { get; set; }


        public IEnumerable<d_order> Orders { get; set; }


        public string ConfirmationTitle
        {
            get
            {
                return IsOrderConfirmation
                           ? "Order Confirmation"
                           : "Shipping Confirmation";
            }
        }


        public string DateTitle
        {
            get
            {
                return IsOrderConfirmation
                           ? "Order Date"
                           : "Shipping Date";
            }
        }
    }
}