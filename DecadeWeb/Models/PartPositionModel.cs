﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Models
{
    public class PartPositionModel
    {
        public PartPositionType PositionType { get; set; }


        public string PageTitle { get; set; }


        public IEnumerable<d_order> Orders { get; set; }


        public PartPositionModel() { }


        public PartPositionModel( PartPositionType positionType, string pageTitle, IEnumerable<d_order> orders )
        {
            PositionType = positionType;
            PageTitle = pageTitle;
            Orders = orders;
        }
    }


    public class PartPositionStubModel
    {
        public PartPositionType PositionType { get; set; }


        public d_order Order { get; set; }


        public PartPositionStubModel() { }


        public PartPositionStubModel( PartPositionType positionType, d_order order )
        {
            PositionType = positionType;
            Order = order;
        }
    }


    public enum PartPositionType
    {
        HotList,


        ByCustomer,


        ByShopDate,


        FastTrack,


        Orders
    }
}