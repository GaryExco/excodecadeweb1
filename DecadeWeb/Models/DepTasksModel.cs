﻿using System.Collections.Generic;
using Mset.Exco.Decade;



namespace DecadeWeb.Areas.Reports.Models
{
    public class DepTasksModel
    {
        public IEnumerable<d_task> Tasks { get; set; }
    }
}