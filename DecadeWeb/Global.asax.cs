﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using DecadeWeb.App_Start;
using MassTransit;
using Mset.Exco;
using Mset.Exco.Decade;
using StackExchange.Profiling;
using Mset.Extensions;
using Newtonsoft.Json.Serialization;

namespace DecadeWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        private FileSystemWatcher _settingsFSW;


        protected void Application_Start()
        {
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            // Add our Ignore Paths First
            IgnoreRoutesConfig.RegisterRoutes(RouteTable.Routes);

            LegacyConfig.RegisterRoutes(RouteTable.Routes);

            // Map Custom Routes
            TrackingConfig.RegisterRoutes(RouteTable.Routes);
            ScheduleConfig.RegisterRoutes(RouteTable.Routes);
            TasksConfig.RegisterRoutes(RouteTable.Routes);
            SteelInventoryConfig.RegisterRoutes(RouteTable.Routes);
            ReportsConfig.RegisterRoutes(RouteTable.Routes);
            OnTimeConfig.RegisterRoutes(RouteTable.Routes);
            OrderConfig.RegisterRoutes(RouteTable.Routes);
            OrdersConfig.RegisterRoutes(RouteTable.Routes);
            PartPositionConfig.RegisterRoutes(RouteTable.Routes);
            SalesConfig.RegisterRoutes(RouteTable.Routes);
            NonConformanceConfig.RegisterRoutes(RouteTable.Routes);
            SearchConfig.RegisterRoutes(RouteTable.Routes);
            AdminConfig.RegisterRoutes(RouteTable.Routes);
            WebNotesConfig.RegisterRoutes(RouteTable.Routes);

            // Map Default Routes
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            DefaultRouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Map Custom API Routes
            CustomerApiConfig.Register(GlobalConfiguration.Configuration);
            SteelInventoryApiConfig.Register(GlobalConfiguration.Configuration);
            OrderApiConfig.Register(GlobalConfiguration.Configuration);
            SessionConfig.Register(GlobalConfiguration.Configuration);


            // Set the Default Connection String
            DecadeTable.CONNECTION_STRING = Settings.DecadeConn;


            // Setup Event publishing
            ConfigureBus();


            // Used to Set the User Identity automatically
            var controllerFactory = new Code.BaseControllerFactory();
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            // Clear all View Engines
            ViewEngines.Engines.Clear();

            // Register Razor
            ViewEngines.Engines.Add(new RazorViewEngine());

            // JSON Settings
            var resolver = new DefaultContractResolver
            {
                SerializeCompilerGeneratedMembers = false,
                DefaultMembersSearchFlags =
                    System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.GetProperty
                    | System.Reflection.BindingFlags.GetField
            };

            var config = GlobalConfiguration.Configuration;
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = resolver;


            // Get access to the config file so we can see what the name of the external settings is
            var cfg = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration( "~" );


            // Create a FileSystemWatcher to watch for changes to the external settings file
            //  and reload the settings if they change while running.
            _settingsFSW = new FileSystemWatcher( Path.GetDirectoryName( cfg.FilePath ) );
            _settingsFSW.Changed += ( s, e ) =>
            {
                if (!e.FullPath.ContainsIgnoreCase( cfg.AppSettings.File ))
                    return;
                
                ConfigurationManager.RefreshSection( "appSettings" );
            };
            _settingsFSW.EnableRaisingEvents = true;
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.Url.AbsolutePath.StartsWith("/api/Session", StringComparison.InvariantCultureIgnoreCase))
                return;

            HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        }

        protected void Application_BeginRequest()
        {
            #if DEBUG
            /*
            // Disable Caching in DEBUG Mode.
            HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
            */
            #endif

            // Setup Language
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.UserCulture);
            Response.AddHeader("Content-Language", Settings.UserCulture);
        }

        protected void Application_EndRequest()
        {
            if (!MiniProfiler.Current.IsNull())
                MiniProfiler.Stop();

            var context = new HttpContextWrapper(Context);

            if (FormsAuthentication.IsEnabled && context.Response.StatusCode == 302 && context.Request.IsAjaxRequest())
            {
                context.Response.Clear();
                context.Response.StatusCode = 401;
            }
        }


        protected void Application_End()
        {
            if (Settings.PublishEvents)
                Bus.Shutdown();


            if (_settingsFSW != null)
                _settingsFSW.Dispose();
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            return custom.Equals("User", StringComparison.InvariantCultureIgnoreCase)
                       ? User.Identity.Name
                       : base.GetVaryByCustomString(context, custom);
        }


        private void ConfigureBus()
        {
            if (!Settings.PublishEvents)
                return;

            Bus.Initialize( 
                ( sbc ) =>
                {
                    var uri = new Uri(ExcoVars.RabbitURL);

                    sbc.UseRabbitMq(
                    q => q.ConfigureHost(
                        uri,
                        h =>
                        {
                            if (ExcoVars.RabbitUser.IsNotNullOrEmpty())
                                h.SetUsername(ExcoVars.RabbitUser);


                            if (ExcoVars.RabbitPass.IsNotNullOrEmpty())
                                h.SetPassword(ExcoVars.RabbitPass);
                        }));
                    sbc.ReceiveFrom(new UriBuilder(uri.Scheme, uri.Host, 0, "DecadeWeb").Uri);
                    /*
                    sbc.Subscribe(
                        subs =>
                        {
                            subs.Consumer(() => this)
                                .Permanent();
                        });
                    */
                });
        }
    }
}