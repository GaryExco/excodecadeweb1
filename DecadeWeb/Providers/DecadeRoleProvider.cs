﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;



namespace DecadeWeb.Providers
{
    public class DecadeRoleProvider : RoleProvider
    {
        public override bool IsUserInRole( string username, string roleName )
        {
            return false;
        }


        public override bool RoleExists( string roleName )
        {
            throw new NotImplementedException();
        }


        public override string[] GetRolesForUser( string username )
        {
            var rtnVal = new string[]
            { };
            return rtnVal;
            // throw new NotImplementedException();
        }


        #region Unused Methods
        public override void AddUsersToRoles( string[] usernames, string[] roleNames )
        {
            throw new NotImplementedException();
        }


        public override string ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }


        public override void CreateRole( string roleName )
        {
            throw new NotImplementedException();
        }


        public override bool DeleteRole( string roleName, bool throwOnPopulatedRole )
        {
            throw new NotImplementedException();
        }


        public override string[] FindUsersInRole( string roleName, string usernameToMatch )
        {
            throw new NotImplementedException();
        }


        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }


        public override string[] GetUsersInRole( string roleName )
        {
            throw new NotImplementedException();
        }


        public override void RemoveUsersFromRoles( string[] usernames, string[] roleNames )
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}

namespace DecadeWeb
{
    /// <summary>
    ///     If a Flag is added here, need to add it to Models.d_user as well what to check for.
    /// </summary>
    [Flags]
    public enum DecadeRole
    {
        God = 1,


        Admin = 2,


        ModOrders = 4,


        ViewOrders = 8,


        ModCustomers = 16,


        ViewCustomers = 32,


        ReportsAdmin = 64,


        ReportsShop = 128,


        Steel = 256,


        SteelPricing = 512,


        Tracking = 1024,


        HotListAdmin = 2048,


        HotListView = 4096,


        DepSchedAdmin = 8192,


        HTSchedView = 16384,


        NonConf = 32768,


        Shipping = 65536
    }


    [AttributeUsage( AttributeTargets.Class | AttributeTargets.Method )]
    public class DecadeAuthorizeAttribute : AuthorizeAttribute
    {
        public DecadeRole DecadeRole { get; set; }


        /*
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = new JsonResult
                    {
                        Data = new { Message = "No Valid Session" },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    filterContext.HttpContext.Response.StatusCode = 401;
                    filterContext.HttpContext.Response.StatusDescription = "Unauthorized";
                }
            }

            // base.HandleUnauthorizedRequest(filterContext);
        }
        */


        public override void OnAuthorization( AuthorizationContext filterContext )
        {
            #region Ajax Authorization
            /*
            if (
                filterContext.HttpContext.Request.IsAjaxRequest()
                && !filterContext.HttpContext.User.Identity.IsAuthenticated
                && (filterContext.ActionDescriptor.GetCustomAttributes(typeof(AuthorizeAttribute), true).Count() > 0 || filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(AuthorizeAttribute), true).Count() > 0)
            )
            {
                filterContext.HttpContext.SkipAuthorization = true;
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                filterContext.Result = new HttpUnauthorizedResult("Unauthorized");
                filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                filterContext.HttpContext.Response.End();
            }
            */
            #endregion


            if (DecadeRole != 0)
                Roles = DecadeRole.ToString();

            base.OnAuthorization( filterContext );

            if (filterContext.HttpContext.User.Identity.IsAuthenticated
                && filterContext.ActionDescriptor.IsDefined(
                    typeof (DecadeAuthorizeAttribute),
                    true )
                || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(
                    typeof (DecadeAuthorizeAttribute),
                    true ))
            {
                var attributes = new List<object>(
                    filterContext.ActionDescriptor.GetCustomAttributes(
                        typeof (DecadeAuthorizeAttribute),
                        true ) );
                attributes.AddRange(
                    filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(
                        typeof (DecadeAuthorizeAttribute),
                        true ) );

                if (!attributes.OfType<DecadeAuthorizeAttribute>().Any( authAttribute => filterContext.HttpContext.User.IsInRole( Roles ) ))
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                area = "",
                                controller = "Main",
                                action = "AccessDenied"
                            } ) );
                }

                /*
                foreach (var authAttribute in attributes.OfType<DecadeAuthorizeAttribute>().Where(authAttribute => !filterContext.HttpContext.User.IsInRole(Roles)))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { area = "", controller = "Main", action = "AccessDenied" }));
                    break;
                }
                */
            }
        }
    }


    public static class DecadeAuthorizeExtensions
    {
        public static bool IsInRole( this IPrincipal user, DecadeRole appRole )
        {
            return user.IsInRole( appRole.ToString() );
            /*
            var roles = appRole.ToString().Split(',').Select(x => x.Trim());
            foreach (var role in roles)
            {
                if (user.IsInRole(role))
                    return true;
            }

            return false;
            */
        }
    }
}