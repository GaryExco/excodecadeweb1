﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb.Controllers.Services
{
    public class SearchController : BaseApiController
    {
        //
        // GET: /api/Customer/
        [AcceptVerbs( "POST", "GET" )]
        public dynamic Get( string q )
        {
            var options = new List<dynamic>();

            var match = q.Match( "[0-9]{5,6}" );

            if (match.Success)
            {
                if (d_order.OrderExists(
                    DB,
                    Convert.ToInt32( match.Value ) ))
                {
                    options.Add(
                        new
                        {
                            name = $"{match.Value} (Tracking)",
                            link = $"/Tracking/{match.Value}"
                        } );
                    options.Add(
                        new
                        {
                            name = $"{match.Value} (Order Page)",
                            link = $"/Order/{match.Value}"
                        } );
                }

                if (d_order.InvoiceExists(
                    DB,
                    Convert.ToInt32( match.Value ) ))
                {
                    options.Add(
                        new
                        {
                            name = $"{match.Value} (Invoice)",
                            link = $"/Tracking/Invoice/{match.Value}"
                        } );
                }
            }

            /*
            if (query.IsNullOrEmpty())
                return Redirect(Request.UrlReferrer.AbsolutePath);

            if (query.StartsWith("LOC", StringComparison.InvariantCultureIgnoreCase))
                return Redirect(Url.TrackByLoc(query.Substring(3).Trim()));

            if (query.StartsWith("DN", StringComparison.InvariantCultureIgnoreCase))
                return Redirect(Url.TrackByDie(query.Substring(2).Trim()));

            if (query.StartsWith("O", StringComparison.InvariantCultureIgnoreCase) && Regex.IsMatch(query, "[0-9]{5,6}"))
                return Redirect(Url.DisplayOrder(Convert.ToInt32(Regex.Match(query, "[0-9]{5,6}").Value)));

            if (query.StartsWith("I", StringComparison.InvariantCultureIgnoreCase) && Regex.IsMatch(query, "[0-9]{5,6}"))
                return Redirect(Url.TrackByInvoice(Regex.Match(query, "[0-9]{5,6}").Value));

            if (Regex.IsMatch(query, "[0-9]{5,6}"))
                return Redirect(Url.TrackByOrder(Regex.Match(query, "[0-9]{5,6}").Value));
            */
            return new
            {
                options
            };
        }
    }
}