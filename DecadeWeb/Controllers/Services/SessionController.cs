﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Mset.Extensions;



namespace DecadeWeb.Controllers.Services
{
    [AllowAnonymous]
    public class SessionController : BaseApiController
    {
        [System.Web.Http.AcceptVerbs( "POST", "GET" )]
        public dynamic KeepAlive()
        {
            HttpSessionState session = HttpContext.Current.Session;

            if (!session.IsNull())
                session["KeepAlive"] = DateTime.Now;

            return DateTime.Now;
        }
    }
}