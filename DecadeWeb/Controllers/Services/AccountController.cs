﻿using System.Web.Http;



namespace DecadeWeb.Controllers.Services
{
    public class AccountController : ApiController
    {
        [AllowAnonymous]
        [AcceptVerbs( "POST", "GET" )]
        public dynamic Login( string username, string password )
        {
            using (var ac = new Controllers.AccountController())
            {
                var isValid = ac.ValidateLogin(
                    username,
                    password );

                return new
                {
                    isValid
                };
            }
        }
    }
}