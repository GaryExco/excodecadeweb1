﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using DecadeWeb.Code.Extensions;
using LambdaSqlBuilder;
using Microsoft.Ajax.Utilities;
using Mset.Exco;
using Mset.Exco.Decade;
using Mset.Extensions;
using Mset.IO;
using Mset.Report;
using Newtonsoft.Json.Linq;
using Dapper;
using System.Net.Mail;
using KellermanSoftware.CompareNetObjects;
using MassTransit;
using Mset.Exco.Decade.Messages;
using RazorEngine;
using Resources;
using Pricing = Mset.Exco.Decade.Pricing;

namespace DecadeWeb.Controllers.Services
{
    public class OrderEntryController : BaseApiController
    {
        #region Order Entry Screen
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic Schedule(DateTime shopdate)
        {
            var limits = d_dailylimits.GetAllLimits(DB).ToList();
            var orders = d_order.GetOrdersByShopDate(DB, shopdate).ToList();

            List<string> solidPrefixes = d_orderitem.SolidPrefixes.ToList(),
                         hollowPrefixes = new List<string>
                         { "HO", "MA", "PL" };

            return new
            {
                ShopClosed = ShopClosed.Dates.Contains(shopdate.Date),
                Solid = new
                {
                    Count = orders.Count(x => x.prefix.In(solidPrefixes)),
                    Limit = limits.Where(x => x.dietype.EqualsIgnoreCase("SOLIDS")).Select(x => x.maxitems).Single()
                },
                Hollow = new
                {
                    Count = orders.Count(x => x.prefix.In(hollowPrefixes)),
                    Limit = limits.Where(x => x.dietype.EqualsIgnoreCase("HOLLOWS")).Select(x => x.maxitems).Single()
                },
                Bolster = new
                {
                    Count = orders.Count(x => x.prefix.EqualsIgnoreCase("BO")),
                    Limit = limits.Where(x => x.dietype.EqualsIgnoreCase("BOLSTERS")).Select(x => x.maxitems).Single()
                },
                Ring = new
                {
                    Count = orders.Count(x => x.prefix.EqualsIgnoreCase("RI")),
                    Limit = limits.Where(x => x.dietype.EqualsIgnoreCase("RINGS")).Select(x => x.maxitems).Single()
                }
            };
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic FileSearch(string mode, string customercode, string filename)
        {
            string dirPath = String.Empty,
                   nameSearch = "*{filename}*.dwg".FormatWithName(new { filename });
            // Remove First Character
            if (customercode.Length == 6)
                customercode = customercode.Substring(1);

            #region Get Directory
            switch (mode.ToLower())
            {
                case "dienumber":
                    dirPath = ExcoVars.Variables["FileSys.MDrive"] + "{sep}{cust}{sep}";
                    break;

                case "backer":
                    dirPath = ExcoVars.Variables["FileSys.SStd"] + "{sep}{cust}{sep}BA{sep}";
                    break;

                case "bolster":
                    dirPath = ExcoVars.Variables["FileSys.SStd"] + "{sep}{cust}{sep}BO{sep}";
                    break;

                case "feeder":
                    dirPath = ExcoVars.Variables["FileSys.SStd"] + "{sep}{cust}{sep}FE{sep}";
                    break;
            }

            dirPath = dirPath.FormatWithName(new { cust = customercode, sep = Path.DirectorySeparatorChar });
            #endregion

            return Directory.Exists(dirPath)
                       ? FastDirectoryEnumerator.GetFiles(dirPath, nameSearch, SearchOption.TopDirectoryOnly).Select(x => Path.GetFileNameWithoutExtension(x.Name)).Take(25)
                       : new string[0];
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic GetCharges(string plcode, string prefix, short pricelistMultiplier, decimal currencyMultiplier)
        {
            return d_pricelistitemcharges.GetCharges(DB, plcode, prefix).ToList();
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic GetPriceInfo(string customercode, string plcode, string prefix, double diameter, double thickness, double cavid, short pricelistMultiplier, decimal currencyMultiplier)
        {
            var dieType = Pricing.PrefixToDieType(prefix);
            if (dieType == DieType.Misc)
                return new { isValid = true };

            bool usedEstimated,
                 priceFound = true;
            decimal price;
            double freightWeight,
                   nitrideWeight = 0,
                   steelSurchargeWeight;
            byte weightType = 0;

            string description;

            #region Lookup Customer
            d_customer cust = d_customer.GetCustomerByCode(DB, customercode);
            if (cust.IsNotNull())
                weightType = cust.weighttype;
            #endregion

            #region Get Price
            if (dieType == DieType.Ring)
            {
                if (!Pricing.GetPrice(DB, customercode, plcode, dieType, diameter, thickness, cavid, out usedEstimated, out price, pricelistMultiplier, currencyMultiplier))
                    priceFound = false;
            }
            else
            {
                if (!Pricing.GetPrice(DB, customercode, plcode, dieType, diameter, thickness, Convert.ToInt32(cavid), out usedEstimated, out price, pricelistMultiplier, currencyMultiplier))
                    priceFound = false;
            }
            #endregion

            #region Get Weights
            // Freight
            if (dieType == DieType.Ring)
            {
                if (Pricing.GetFreightWeight(customercode, dieType, diameter, thickness, cavid, out freightWeight))
                    freightWeight = Pricing.RoundDouble(freightWeight, 2);
            }
            else
            {
                if (Pricing.GetFreightWeight(customercode, dieType, diameter, thickness, out freightWeight))
                    freightWeight = Pricing.RoundDouble(freightWeight, 2);
            }

            // Nitride (Only on DI or HO)
            if (dieType == DieType.Solid || dieType == DieType.Hollow)
                if (Pricing.GetFreightWeight(customercode, dieType, diameter, thickness, out nitrideWeight))
                    nitrideWeight = Pricing.RoundDouble(nitrideWeight, 4);

            // Steel Surcharge
            if (Pricing.GetSurchargeWeight(weightType, dieType, diameter, thickness, 1, out steelSurchargeWeight))
                steelSurchargeWeight = Pricing.RoundDouble(steelSurchargeWeight, 4);
            #endregion

            #region Description

            // Store the current UI Culture so we can restore it later
            var uiCulture = Thread.CurrentThread.CurrentUICulture;

            // Change the current UI culture to the plant default so that the description generated is correct.
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.DefaultCulture);

            var dieTypeDesc = dieType.ToString();

            switch (dieType)
            {
                case DieType.Hollow:
                    description = Resources.OrderEntry.Hollow_Solid_Description;
                    dieTypeDesc = GlobalResources.General_Hollow;
                    break;
                case DieType.Solid:
                    description = Resources.OrderEntry.Hollow_Solid_Description;
                    dieTypeDesc = GlobalResources.General_Solid;
                    break;

                case DieType.Ring:
                    description = Resources.OrderEntry.Ring_Description;
                    dieTypeDesc = GlobalResources.General_Ring;
                    break;

                case DieType.Backer:
                    description = Resources.OrderEntry.Backer_Feeder_Bolster_Description;
                    dieTypeDesc = GlobalResources.General_Backer;
                    break;

                case DieType.Feeder:
                    description = Resources.OrderEntry.Backer_Feeder_Bolster_Description;
                    dieTypeDesc = GlobalResources.General_Feeder;
                    break;

                case DieType.Bolster:
                    description = Resources.OrderEntry.Backer_Feeder_Bolster_Description;
                    dieTypeDesc = GlobalResources.General_Bolster;
                    break;

                default:
                    description = Resources.OrderEntry.Backer_Feeder_Bolster_Description;
                    dieTypeDesc = dieType.ToString();
                    break;
            }

            // Change dieType for language Change
            description = description.FormatWithName(new { 
                dietype = dieTypeDesc.ToUpper(),
                diameter = diameter.ToString("0.###"),
                thickness = thickness.ToString("0.###"),
                cavid = cavid.ToString("0.###"),
                units = (diameter > Pricing.MAX_IMPERIAL_DIA ? GlobalResources.General_Millimeter : GlobalResources.General_Inch).ToUpper()
            });

            // Restore the UI culture
            Thread.CurrentThread.CurrentUICulture = uiCulture;
            #endregion

            string suffix = dieType == DieType.Ring
                                ? d_orderitem.EncodeSuffix(diameter, thickness, innerDiameter: cavid)
                                : d_orderitem.EncodeSuffix(diameter, thickness, cavities: Convert.ToInt32(cavid));

            var tariffCode = dieType != DieType.Misc
                ? "8207.20.00"
                : String.Empty;

            return new { isValid = true, UsedEstimated = usedEstimated, PriceFound = priceFound, suffix, price, freightWeight, nitrideWeight, steelSurchargeWeight, Description = description, TariffCode = tariffCode };
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic GetWeight(string weightMode, string customercode, string prefix, double diameter, double thickness, double innerDiameter = -1, int pieceCount = 1)
        {
            double weight;

            if (weightMode.Equals("steelsurcharge", StringComparison.InvariantCultureIgnoreCase))
            {
                byte weightType = 0;

                d_customer cust = d_customer.GetCustomerByCode(DB, customercode);
                if (cust != null)
                    weightType = cust.weighttype;

                if (Pricing.GetSurchargeWeight(weightType, Pricing.PrefixToDieType(prefix), diameter, thickness, pieceCount, out weight))
                    return new { isValid = true, weight };
            }
            else
            {
                if (Pricing.GetFreightWeight(customercode, Pricing.PrefixToDieType(prefix), diameter, thickness, out weight))
                    return new { isValid = true, weight };
            }

            return new { isValid = false, weight = 0 };
        }

        [System.Web.Http.AcceptVerbs("POST", "GET")]
        public dynamic SaveOrder([System.Web.Http.FromBody] dynamic json)
        {
            bool isValid;
            var order = ((JObject)json.order).ToObject<d_order>();
            string print = json.print;
            bool genInvoiceNumber = json.geninvoicenumber;
            bool isNewOrder = order.ordernumber < 0;
            int orderCopies = json.order.OrderCopies;
            var changes = new List<d_order_changelog>();
            int firstOrdernumber = 0;
            d_order oldOrder = null;

            using (DbTransaction trans = DB.Connection.BeginTransaction())
            {
                try
                {
                    if (isNewOrder)
                    {
                        order.employeenumber = Convert.ToInt32(User.Identity.Name);
                    }
                    else
                    {
                        #region Comparison
                        // Run a comparison of what we got and what exists in the DB
                        //   Save a change list to d_order_changelog (This will give me the visibility to see what changes were made on each revision)

                        var compare = new CompareLogic
                        {
                            Config =
                            {
                                MaxDifferences = Int32.MaxValue
                            }
                        };
                        foreach (PropertyInfo pInfo in typeof(d_order).GetProperties().Where(pInfo => !d_order.FieldsToTrackChanges.Contains(pInfo.Name, StringComparer.InvariantCultureIgnoreCase)))
                        {
                            compare.Config.MembersToIgnore.Add(pInfo.Name);
                        }

                        oldOrder = d_order.GetOrderByOrderNumber(DB, order.ordernumber, trans);
                        oldOrder.LoadOrderItems(DB, trans: trans);
                        oldOrder.LoadCheckList(DB, trans);

                        foreach (d_orderitem item in order.OrderItems)
                            item.ParseSuffix();

                        var diffs = compare.Compare(oldOrder, order);

                        if (diffs.Differences.Count > 0)
                        {
                            DateTime revisionTime = DateTime.Now;
                            int currentRevision = d_order_changelog.GetLastRevisionForOrder(DB, order.ordernumber, trans) + 1;

                            foreach (var diff in diffs.Differences)
                            {
                                var change = new d_order_changelog
                                {
                                    ordernumber = order.ordernumber,
                                    revision = currentRevision,
                                    revisiontime = revisionTime,
                                    propertyname = diff.PropertyName,
                                    oldvalue = diff.Object1Value,
                                    newvalue = diff.Object2Value
                                };

                                changes.Add(change);

                                change.Insert(DB, trans);
                            }
                        }
                        #endregion

                        #region Merge Order w/ Old Order Info
                        foreach (PropertyInfo pInfo in order.GetType().GetProperties())
                        {
                            if (d_order.FieldsToTrackChanges.Contains(pInfo.Name, StringComparer.InvariantCultureIgnoreCase))
                                continue;

                            try
                            {
                                if (order.GetType().GetProperty(pInfo.Name).GetCustomAttributes(typeof(IsDBField), false).Length > 0)
                                    order.GetType().GetProperty(pInfo.Name).SetValue(order, oldOrder.GetType().GetProperty(pInfo.Name).GetValue(oldOrder, null), null);
                            }
                                // ReSharper disable once EmptyGeneralCatchClause
                            catch { }
                        }
                        #endregion
                    }

                    order.importedorder = 2;
                    order.lastemployeenumber = Convert.ToInt32(User.Identity.Name);
                    order.lastchange = DateTime.Now;

                    var orders = new List<int>();

                    // Update Order
                    for (var ordercount = 0; ordercount < orderCopies; ordercount++)
                    {
                        #region Get Order Number
                        if (isNewOrder || ordercount > 0)
                        {
                            order.ordernumber = d_order.GetNextOrderNumber(DB, trans);
                        }
                        #endregion

                        #region Get Invoice Number
                        if ((order.importedorder.HasValue && order.importedorder.Value == 2 && order.invoicedate.HasValue && !order.invoicenumber.HasValue) ||
                            (genInvoiceNumber && order.invoicedate.HasValue && !order.invoicenumber.HasValue))
                        {
                            order.invoicenumber = d_order.GetNextInvoiceNumber(DB, trans);
                        }
                        #endregion

                        if (ordercount == 0)
                            firstOrdernumber = order.ordernumber;


                        // Add our order number to a list to track
                        orders.Add(order.ordernumber);


                        // Update the order in the database
                        order.Update(DB, trans, true);

                        order.CheckList.ordernumber = order.ordernumber;
                        order.CheckList.Update(DB, trans, true);

                        // Remove OrderItems / Charges
                        new SqlLam<d_orderitem>(SqlLamQueryType.Delete)
                            .Where(x => x.ordernumber == order.ordernumber)
                            .Execute(DB.Connection, trans);

                        new SqlLam<d_orderitemcharges>(SqlLamQueryType.Delete)
                            .Where(x => x.ordernumber == order.ordernumber)
                            .Execute(DB.Connection, trans);

                        new SqlLam<d_ordertaxes>(SqlLamQueryType.Delete)
                            .Where(x => x.ordernumber == order.ordernumber)
                            .Execute(DB.Connection, trans);

                        foreach (d_orderitem oi in order.OrderItems)
                        {
                            oi.ordernumber = order.ordernumber;
                            oi.Insert(DB, trans);

                            for (int j = 0; j < oi.Charges.Count; j++)
                            {
                                oi.Charges[j].ordernumber = order.ordernumber;
                                oi.Charges[j].line = oi.line;
                                oi.Charges[j].chargeline = j + 1;
                                oi.Charges[j].Insert(DB, trans);
                            }
                        }

                        foreach (d_ordertaxes tax in order.TaxItems)
                        {
                            tax.ordernumber = order.ordernumber;
                            tax.Insert(DB, trans);
                        }
                    }

                    #region Update Exp. Ship/Recv on Ship With Orders
                    order.LoadShipWithOrders(DB, trans);

                    foreach (int ordernumber in order.ShipWithOrders)
                    {
                        System.Diagnostics.Debug.WriteLine(ordernumber);
                        d_order shipWithOrder = d_order.GetOrderByOrderNumber(DB, ordernumber, trans);

                        if (shipWithOrder == null
                            || shipWithOrder.invoicedate.HasValue
                            || (shipWithOrder.expshipdate == order.expshipdate && shipWithOrder.expreceiveddate == order.expreceiveddate))
                            continue;

                        shipWithOrder.expshipdate = order.expshipdate;
                        shipWithOrder.expreceiveddate = order.expreceiveddate;
                        shipWithOrder.Update(DB, trans, false);
                    }
                    #endregion

                    trans.Commit();


                    // Push the event to Rabbit
                    if (Settings.PublishEvents)
                    {
                        foreach (var o in orders)
                        {
                            // Publish our OrderModified Event
                            Bus.Instance.Publish(
                                new OrderModifiedEvent(
                                    o,
                                    isNewOrder ),
                                ( m ) =>
                                {
                                    m.SetDeliveryMode(
                                        DeliveryMode.Persistent );
                                } );


                            // Determine if the ShipDate has changed
                            if (oldOrder != null
                                && oldOrder.shipdate != order.shipdate)
                            {
                                Bus.Instance.Publish(
                                    new OrderShippedEvent(
                                        o,
                                        order.shipdate.HasValue
                                            ? ShippedStatus.Shipped
                                            : ShippedStatus.NotShipped ) );
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    // Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    throw;
                }

                isValid = true;
            }

            if (!print.IsNullOrEmpty() && !print.EqualsIgnoreCase("None"))
            {
                order.LoadCustomerName(DB);
                PrintOrderHelper(print, new List<d_order> { order });
            }

            return new { isValid, Changes = changes, Order = order, ordernumber = firstOrdernumber, order.invoicenumber };
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic PrintOrders([System.Web.Http.FromBody] dynamic json)
        {
            string mode = json.mode;
            List<int> orders = json.orders.ToObject<List<int>>();

            var tmpOrders = new List<d_order>();

            foreach (d_order order in orders.Select(o => d_order.GetOrderByOrderNumber(DB, o)).Where(order => !order.IsNull()))
            {
                order.LoadCustomerName(DB);
                order.LoadOrderItems(DB);
                order.LoadCheckList(DB);
                order.LoadTaxItems(DB, true);
                order.LoadTerms(DB);
                order.LoadCurrency(DB);
                order.LoadSupplierCode(DB);

                foreach (d_ordertaxes curTax in order.TaxItems)
                    curTax.LoadTaxName(DB);

                tmpOrders.Add(order);
            }

            // Setup for Colation
            tmpOrders.Reverse();

            bool printed;
            string message = String.Empty;
            try
            {
                printed = PrintOrderHelper(mode, tmpOrders);
            }
            catch (System.Exception ex)
            {
                printed = false;
                message = ex.ToString();
            }

            return new { isValid = printed, message };
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public HttpResponseMessage DownloadOrders(string mode, [FromUri] int[] orders)
        {
            // string mode = json.mode;
            // List<int> orders = json.orders.ToObject<List<int>>();

            var tmpOrders = new List<d_order>();

            foreach (d_order order in orders.Select(o => d_order.GetOrderByOrderNumber(DB, o)).Where(order => !order.IsNull()))
            {
                order.LoadCustomerName(DB);
                order.LoadOrderItems(DB);
                order.LoadCheckList(DB);
                order.LoadTaxItems(DB, true);
                order.LoadTerms(DB);
                order.LoadCurrency(DB);
                order.LoadSupplierCode(DB);

                foreach (d_ordertaxes curTax in order.TaxItems)
                    curTax.LoadTaxName(DB);

                tmpOrders.Add(order);
            }

            // Setup for Colation
            // tmpOrders.Reverse();

            var response = new HttpResponseMessage();

            PrintOrderHelper(mode, tmpOrders, response);

            var cookie = new CookieHeaderValue("fileDownload", "true")
            {
                Expires = DateTimeOffset.Now.AddDays(1),
                Domain = Request.RequestUri.Host,
                Path = "/"
            };

            response.Headers.AddCookies(new [] { cookie });

            return response;
        }

        #region Validation
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic ValidateShipWithOrder(string customercode, int shipwithorder)
        {
            d_order order = d_order.GetOrderByOrderNumber(DB, shipwithorder);

            if (order == null)
                return new { isValid = false, message = "No order found" };

            if (order.shipdate.HasValue || order.invoicedate.HasValue)
                return new { isValid = false, message = "Order has already shipped" };

            if (!order.customercode.EqualsIgnoreCase(customercode))
                return new { isValid = false, message = "Customer does not match" };

            return new
            {
                isValid = true,
                order.shipvia,
                shopdate = order.shopdate.ToURLDate(),
                expshipdate = order.expshipdate.ToURLDate(),
                expreceiveddate = order.expreceiveddate.ToURLDate(),
                noshipbefore = order.noshipbefore.HasValue
                    ? order.noshipbefore.Value.ToURLDate()
                    : String.Empty
            };
        }
        #endregion

        private bool PrintOrderHelper(string mode, List<d_order> orders, HttpResponseMessage response = null)
        {
            if (orders.Count < 1)
                return true;

            string reportName = "Order";

            if (mode.EqualsIgnoreCase("Invoice"))
                reportName = "Invoice";


            #region Add HT Date if Not Exist
            foreach (d_order tmpOrder in orders.Where(tmpOrder => !tmpOrder.htdate.HasValue).Where(tmpOrder => tmpOrder.OrderItems.Any()))
            {
                tmpOrder.OrderItems[0].ParseSuffix();
                double dia = tmpOrder.OrderItems[0].Diameter;
                DieType dieType = Mset.Exco.Pricing.PrefixToDieType(tmpOrder.OrderItems[0].prefix);

                DateTime inDate = dieType == DieType.Ring
                                      ? tmpOrder.orderdate
                                      : tmpOrder.shopdate;

                tmpOrder.htdate = HTDate.CalculateHTDate(inDate, dieType, dia);
            }
            #endregion


            var reportPath = HttpContext.Current.Server.MapPath(
                "~/PrintReports/{reportname}.rpt".FormatWithName(
                    new
                    {
                        reportname = reportName
                    }));

            // Create a reference to our Report Generator
            var report = new ReportGenerator<d_order>(
                reportPath,
                orders,
                Settings.DefaultCulture);


            if (response != null)
            {
                var stream = new MemoryStream();
                report.SaveToPDF(stream: stream);

                response.Content = new StreamContent(stream);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "{0}-{1}.pdf".FormatWith(mode, orders.First().ordernumber);
            }
            else
            {
                // Used as a workaround for printing through ASP.Net, temporairly removes the impersonation to print and then reapplies.
                //  If we upgrade from Windows 2003 we should be able to remove this.
                var id = new System.Web.Configuration.IdentitySection();
                System.Security.Principal.WindowsImpersonationContext wic = null;

                if (id.Impersonate)
                    wic = System.Security.Principal.WindowsIdentity.Impersonate(IntPtr.Zero);

                try
                {
                    using (var opd = report.Print("Order - {0}".FormatWith(orders[0].ordernumber)))
                    {
                        opd.PrinterSettings.PrinterName = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(DB, User.Identity.Name).printername;
                        opd.PrinterSettings.Duplex = Duplex.Simplex;

                        opd.DefaultPageSettings.Landscape = false;

                        opd.DefaultPageSettings.Margins.Left = 30;
                        opd.DefaultPageSettings.Margins.Right = 30;
                        opd.DefaultPageSettings.Margins.Top = 30;
                        opd.DefaultPageSettings.Margins.Bottom = 30;

                        opd.OriginAtMargins = true;
                        opd.Print();
                    }
                }
                finally
                {
                    if (wic != null)
                    {
                        wic.Undo();
                        wic.Dispose();
                    }
                }
            }

            return true;
        }
        #endregion

        #region Confirmations
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic GenerateConfirmations(string confType, DateTime confDate, bool confSend)
        {
            bool isOrderConf = !confType.ContainsIgnoreCase("ship");

            var messages = new List<MailMessage>();

            var ordersToSend = d_order.GetConfirmationOrders(DB, isOrderConf, confDate).ToList();
            var contacts = d_mailinglist.GetConfirmationContacts(DB, ordersToSend.Select(x => x.customercode).Distinct()).ToList();
            var shippingVendors = !isOrderConf ? d_shippingvendors.GetAll(DB).ToList() : null;
            var customers = d_customer.GetCustomersByCode(DB, ordersToSend.Select(x => x.customercode).Distinct()).ToList();

            string confirmationTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailTemplates/_ConfirmationEmail.cshtml"));
            var ts = new RazorEngine.Templating.TemplateService(new RazorEngine.Configuration.TemplateServiceConfiguration());
            Razor.SetTemplateService(ts);
            Razor.Compile(confirmationTemplate, "ConfirmationEmail");

            #region Load Extra Order Information
            var breakdownCustomers = customers.Where(x => x.confPriceBreakDown).Select(x => x.customercode).Distinct().ToList();

            foreach (d_order order in ordersToSend)
            {
                order.GetDieNumbers(DB);

                if (isOrderConf && breakdownCustomers.Contains(order.customercode, StringComparer.InvariantCultureIgnoreCase))
                    order.LoadOrderItems(DB);

                if (!isOrderConf && order.shipvendorid.HasValue)
                    order.ShipVendor = shippingVendors.FirstOrDefault(x => x.shipvendorid == order.shipvendorid.Value);

            }
            #endregion

            foreach (string currentCustomer in ordersToSend.Select(x => x.customercode).Distinct())
            {
                d_customer customer = customers.First(x => x.customercode == currentCustomer);
                IEnumerable<d_order> customerOrders = ordersToSend.Where(x => x.customercode == currentCustomer);
                List<d_mailinglist> customerContacts = contacts.Where(x => x.customercode == currentCustomer).ToList();

                var email = new MailMessage
                {
                    IsBodyHtml = true,
                    Body = Razor.Run("ConfirmationEmail", new Models.ConfirmationModel
                    {
                        ConfirmationDate = confDate,
                        IsOrderConfirmation = isOrderConf,
                        Customer = customer,
                        Contacts = customerContacts,
                        Orders = customerOrders
                    })
                };

                /*
                email.Body = Razor.Parse(ConfirmationTemplate, new Models.ConfirmationModel() {
                    ConfirmationDate = confDate,
                    IsOrderConfirmation = IsOrderConf,
                    Customer = Customer,
                    Contacts = CustomerContacts,
                    Orders = CustomerOrders
                });
                */

                #region Setup Email
                if (!confSend)
                    customerContacts.Clear();

                if (customerContacts.Count == 0)
                    customerContacts.Add(new d_mailinglist
                    { displayname = Settings.OrdersEmailDisplay, email = Settings.OrdersEmail });
                else
                    email.CC.Add(new MailAddress(Settings.OrdersEmail, Settings.OrdersEmailDisplay));

                foreach (d_mailinglist contact in customerContacts)
                    email.To.Add(new MailAddress(contact.email, contact.displayname));

                email.To.Clear();
                email.Bcc.Clear();

                email.To.Add(new MailAddress("markwallace@excomich.com"));
                #endregion

                messages.Add(email);
            }

            using (SmtpClient smtp = Email.CreateSmtpClient())
            {
                /*
                foreach (MailMessage email in Messages)
                    Mset.Exco.Email.SendMessage(smtp, email, true);
                */
            }

            return new { isValid = false };
        }
        #endregion

        #region Hold
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic ManageOrderHoldStatus(string orders, string mode)
        {
            return ManageOrderHoldStatus(d_order.ParseOrderNumbers(orders), mode);
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public dynamic ManageOrderHoldStatus(IEnumerable<int> orders, string mode)
        {
            string station = "HOLD",
                   taskCode = "OH";
            bool hold = true;
            DateTime holdTime = DateTime.Now;

            #region Parse Mode
            if (mode.EqualsIgnoreCase("RELEASE"))
            {
                station = "RELS";
                taskCode = "RH";
                hold = false;
            }
            else if (mode.EqualsIgnoreCase("REMOVE"))
            {
                station = "DELT";
                taskCode = "RH";
                hold = false;
            }
            #endregion

            /*
            return new
            {
                IsValid = true,
                station,
                taskCode,
                hold,
                holdTime
            };
            */

            DbTransaction trans = DB.Connection.BeginTransaction();

            var errors = new List<string>();
            var tasks = new List<d_task>();

            #region Process Orders
            foreach (var ordernumber in orders)
            {
                #region Process Database
                d_order order = d_order.GetOrderByOrderNumber(DB, ordernumber, trans);

                if (order.IsNull())
                {
                    errors.Add("{0} does not exist.".FormatWith(ordernumber));
                    continue;
                }

                if (order.invoicedate.HasValue)
                {
                    errors.Add("{0} has already been invoiced.".FormatWith(ordernumber));
                    continue;
                }

                order.onhold = hold;
                order.Update(DB, trans);

                var task = new d_task
                {
                    ordernumber = ordernumber,
                    employeenumber = Convert.ToInt32(User.Identity.Name),
                    task = taskCode,
                    part = "G",
                    subpart = "G",
                    station = station,
                    tasktime = holdTime,
                    flags = 0
                };

                task.Insert(DB, trans);

                tasks.Add(
                    task );
                #endregion

                #region Process Filesystem
                if (!Settings.UseDev)
                {
                    var programDirectories = new Dictionary<string, string>
                    {
                        { ExcoVars.Variables["FileSys.CurJobs"], ExcoVars.Variables["FileSys.HoldPrograms"] + @"mill\" },
                        { ExcoVars.Variables["FileSys.Dramet"], ExcoVars.Variables["FileSys.HoldPrograms"] + @"dramet\" },
                        { ExcoVars.Variables["FileSys.Wire"], ExcoVars.Variables["FileSys.HoldPrograms"] + @"wire\" },
                        { ExcoVars.Variables["FileSys.Flame"], ExcoVars.Variables["FileSys.HoldPrograms"] + @"flame\" }
                    };

                    foreach (var dir in programDirectories)
                    {
                        switch (mode.Trim().ToUpper())
                        {
                            case "RELEASE":
                                foreach (var curFile in Directory.GetFiles(dir.Value, "*{0}.*".FormatWith(ordernumber)))
                                    File.Move(curFile, dir.Key + Path.DirectorySeparatorChar + Path.GetFileName(curFile));
                                break;

                            case "REMOVE":
                                foreach (var curFile in Directory.GetFiles(dir.Value, "*{0}.*".FormatWith(ordernumber)))
                                    File.Delete(curFile);
                                break;

                            default:
                                foreach (var curFile in Directory.GetFiles(dir.Key, "*{0}.*".FormatWith(ordernumber)))
                                    File.Move(curFile, dir.Value + Path.DirectorySeparatorChar + Path.GetFileName(curFile));
                                break;
                        }
                    }
                }
                #endregion
            }
            #endregion

            // Save the changes to the database
            trans.Commit();


            // Push the events through to the Bus
            foreach (var task in tasks)
                Bus.Instance.Publish( new TrackingEvent(task) );

            return new
            {
                IsValid = errors.Count == 0,
                errors
            };
        }
        #endregion
    }
}
