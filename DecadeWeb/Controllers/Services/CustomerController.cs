﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MoreLinq;
using Mset.Database;
using Mset.Exco;
using Mset.Exco.Decade;
using Mset.Exco.Decade.Extensions;
using Mset.Extensions;
using Newtonsoft.Json;



namespace DecadeWeb.Controllers.Services
{
    public class CustomerController : BaseApiController
    {
        //
        // GET: /api/Customer/
        [AcceptVerbs( "POST", "GET" )]
        public dynamic Get( string id )
        {
            var customer = d_customer.GetCustomerByCode(
                DB,
                id );

            if (customer == null)
            {
                return new
                {
                    isValid = false,
                    message = "No customer Found."
                };
            }

            customer.LoadAddresses( DB );
            customer.LoadShipVia( DB );
            customer.LoadTaxInfo( DB );

            using (var custNotes = new Database( DatabaseInfo.CustNotesStr ))
            {
                custNotes.Open();
                customer.LoadPresses( custNotes );
            }

            return new
            {
                isValid = true,
                Customer = customer,
                message = ""
            };
        }


        [AcceptVerbs( "POST", "GET" )]
        public dynamic GetCustomerByAny( string term )
        {
            var customers = d_customer.GetCustomers(
                DB,
                customercodelike: term,
                namelike: term ).ToList();

            if (customers.Any())
            {
                return JsonConvert.SerializeObject(
                    customers.Select(
                        c => new
                        {
                            c.customercode,
                            c.name
                        } ) );
            }

            return new List<string>();
        }


        [AcceptVerbs( "POST", "GET" )]
        public dynamic GetCustomerDashboard( string customercode )
        {
            var openOrders = d_order.GetCustomerOrders(
                DB,
                customercode ).ToList();
            var oneYearOfOrders = d_order.GetCustomerOrders(
                DB,
                customercode,
                true,
                minOrderDate: DateTime.Now.ToFirstDayOfMonth().AddYears( -1 ) )
                                         .GroupBy( x => x.orderdate.Date.ToFirstDayOfMonth() )
                                         .ToList();

            var taskCache = d_taskcode.LoadAll( DB ).ToList();

            openOrders.ForEach(
                x =>
                {
                    x.LoadOrderItems( DB );
                    x.LoadParts( DB );
                    x.LoadPredictedShipDate(
                        DB,
                        taskCodes: taskCache );
                    MoreEnumerable.ForEach(
                        x.Parts,
                        y => y.LoadLastTrack(
                            DB,
                            x.IsHollow,
                            taskCache ) );
                } );

            // Sorted Data
            var ncr = oneYearOfOrders.Select( x => x.Count( y => y.ncr ) ).ToList();
            var ncrs = oneYearOfOrders.Select( x => x.Count( y => y.ncrs ) ).ToList();
            var zeroPrice = oneYearOfOrders.Select( x => x.Count( y => y.zeroprice ) ).ToList();
            var sales = oneYearOfOrders.Select( x => x.Sum( y => y.SubTotal ) );
            var paidJobs = oneYearOfOrders.Select(
                x => x.Count(
                    y =>
                    y.countasorder
                    && !( y.ncr )
                    && !( y.ncrs )
                    && !( y.zeroprice )
                         ) ).ToList();
            var ncrDies = oneYearOfOrders.Select(
                x => x.Where(
                    y =>
                    y.ncr
                    || y.ncrs
                    || y.zeroprice ) )
                                         .ToList();

            return new
            {
                series = new dynamic[]
                {
                    new
                    {
                        name = "NCR",
                        data = ncr
                    },
                    new
                    {
                        name = "NCRS",
                        data = ncrs
                    },
                    new
                    {
                        name = "ZeroPrice",
                        data = zeroPrice
                    },
                    new
                    {
                        name = "PaidJobs",
                        data = paidJobs
                    },
                    new
                    {
                        name = "Sales",
                        data = sales
                    }
                }
            };
        }
    }
}