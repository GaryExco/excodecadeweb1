﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using LambdaSqlBuilder;
using Newtonsoft.Json;
using Mset.Exco;
using Mset.Exco.Decade;
using Mset.Extensions;
using Dapper;
using System.Text;
using MassTransit;
using Mset.Exco.Decade.Messages;



namespace DecadeWeb.Controllers.Services
{
    public class NonConformanceController : BaseApiController
    {
        [AcceptVerbs("POST", "GET")]
        public dynamic PostNewError([FromBody] string json)
        {
            scraplog error;
            using (DbTransaction trans = DB.Connection.BeginTransaction())
            {
                dynamic Error = JsonConvert.DeserializeObject(json);

                #region Enter NCR into System
                error = new scraplog
                {
                    sonum = Error["sonum"],
                    empnum = Error["empnum"],
                    date = DateTime.Now,
                    part = Error["part"],
                    department = Error["department"],
                    Errorcode = Error["Errorcode"],
                    problem = Error["problem"],
                    correctioncode = Error["correctioncode"]
                };
                error.Insert(DB, trans);
                #endregion

                #region Enter Tracking Information
                var task = new d_task
                {
                    ordernumber = error.sonum,
                    employeenumber = error.empnum,
                    subpart = "G",
                    station = "NCFL",
                    tasktime = error.date,
                    flags = 0
                };
                // task.part = "G";                        // Fix This
                // task.task = "AS";                       // Fix Thix

                #region Map Part Code
                string tmpPart = error.part;
                switch (tmpPart.ToUpper())
                {
                    case "FDR":
                        task.part = "F";
                        break;

                    case "HSG":
                        task.part = "M";
                        break;

                    case "T-DIE":
                    case "S-DIE":
                        task.part = "P";
                        break;

                    case "BCK":
                        task.part = "B";
                        break;

                    case "BOL":
                        task.part = "O";
                        break;

                    default:
                        task.part = "Z";
                        break;
                }
                #endregion

                #region Map Correction/Task Code
                switch (error.correctioncode.ToUpper())
                {
                    case "REWORK":
                        task.task = "WK";
                        break;

                    case "WELD":
                    case "REPAIR":
                        task.task = "WL";
                        break;

                    case "SCRAP":
                        task.task = "SR";
                        break;

                    default:
                        task.task = "AS";
                        break;
                }
                #endregion

                task.Insert(DB, trans);
                #endregion

                #region Grab ID from Error Entered
                var query = new SqlLam<scraplog>()
                    .Where(x => x.sonum == error.sonum)
                    .Where(x => x.empnum == error.empnum)
                    .Where(x => x.department == error.department)
                    .Where(x => x.part == error.part)
                    .Where(x => x.date == error.date)
                    .Where(x => x.correctioncode == error.correctioncode);

                error = DB.Connection.Query<scraplog>(query, trans).SingleOrDefault();
                #endregion

                trans.Commit();

                // Publish our TrackingEvent for the NCR
                if (Settings.PublishEvents)
                {
                    Bus.Instance.Publish( new TrackingEvent(task), (m) => m.SetDeliveryMode( DeliveryMode.Persistent ) );
                }
            }

            #region Send Email to Alert everyone internally
            if (error.IsNotNull())
            {
                d_order order = d_order.GetOrderByOrderNumber(DB, error.sonum);
                order.GetDieNumbers(DB);
                order.LoadCustomerName(DB);
                order.LoadTrackPageInfo(DB);

                Mset.Exco.Decade.d_user user = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(DB, error.empnum);

                var email = new MailMessage
                {
                    From = new MailAddress(Email.DefaultMailAddress.Address, "Non-Conformance ({PlantCode})".FormatWithName(new
                    {
                        PlantCode = Settings.FacilityName
                    }))
                };
                foreach (string ncrMailAddress in Settings.NCRMailingList)
                    email.To.Add(new MailAddress(ncrMailAddress));

                bool adminInTo = email.To.Count <= 0;

                foreach (MailAddress mailAddress in Email.GetAdminEmailAddress())
                    if (adminInTo)
                        email.To.Add(mailAddress);
                    else
                        email.Bcc.Add(mailAddress);

                email.Subject = "{department} Non-Conformance for Die: {dienumber} Cust: {customer}".FormatWithName(new { department = error.department, dienumber = order.DieNumbers.Count > 0 ? order.DieNumbers[0] : String.Empty, customer = order.CustomerName });

                var body = new StringBuilder();

                body.Append(@"
                    <html>
                    <head>
                    </head>
                    <body>
                    S.O.: <a href=""{host}/Tracking/{ordernumber}"">{ordernumber}</a><br/>
                    Die Number: {dienumber}<br/>
                    Customer Name: {customername}<br/>
                    Expected Receive Date: {exprecvdate}<br/>
                    Part Code: {partcode}<br/>
                    Method of Correction: {correctioncode}<br/>
                    Description of Problem: {problem}<br/>
                    Employee: {employeename}<br/>".FormatWithName(new
                    {
                        host = HttpContext.Current.Request.Url.Scheme + System.Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host +(HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port),
                        ordernumber = error.sonum,
                        dienumber = order.DieNumbers.Count > 0 ? order.DieNumbers[0] : String.Empty,
                        customername = order.CustomerName,
                        exprecvdate = order.expreceiveddate.ToShortDateString(),
                        partcode = error.part,
                        correctioncode = error.correctioncode,
                        problem = error.problem.ToUpper(),
                        employeename = user != null ? user.Name : error.empnum.ToString()
                    })
                );

                bool tasksOutput = false;

                foreach (char c in "FMPBORSG")
                {
                    IEnumerable<d_task> tasks = order.TrackingInfo.Where(t => t.part == c.ToString(CultureInfo.InvariantCulture) || t.part == "G").OrderBy(t => t.tasktime);

                    if (tasks.All(t => t.part != c.ToString(CultureInfo.InvariantCulture)))
                        continue;

                    if (!tasks.Any()
                        || (c == 'G' && (c != 'G' || tasksOutput)))
                        continue;

                    body.Append("<br/>");
                    body.Append("<table border=\"1\" bgcolor=\"silver\">");
                    body.Append("<tr>");
                    body.Append("<td colspan=\"2\"><center>{piecename}</center></td>".FormatWithName(new { piecename = c }));
                    body.Append("</tr>");

                    foreach (d_task task in tasks)
                    {
                        body.Append("<tr>");
                        body.Append("<td width=\"300\"><center>{tasktime}</center></td>".FormatWithName(new { tasktime = task.tasktime }));
                        body.Append("<td width=\"150\">{taskname} ({taskcode})</td>".FormatWithName(new { taskname = task.TaskCode.taskname, taskcode = task.task }));
                        body.Append("</tr>");
                    }

                    body.Append("</table>");

                    tasksOutput = true;
                }

                body.Append("</body>");
                body.Append("</html>");

                email.IsBodyHtml = true;
                email.Body = body.ToString();

                Email.SendMessage(email, true);

                return new { isValid = true, message = "Entered successfuly", errorid = error.ID };
            }
            #endregion

            return new { isValid = false, message = "Error while saving." };
        }
    }
}