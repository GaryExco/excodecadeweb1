﻿using System.Web.Http;
using System.Web.Http.Controllers;
using Mset.Database;
using Mset.Extensions;
using StackExchange.Profiling;



namespace DecadeWeb.Controllers.Services
{
    [DecadeAuthorize]
    public class BaseApiController : ApiController
    {
        protected Database DB;


        protected override void Initialize( HttpControllerContext controllerContext )
        {
            base.Initialize( controllerContext );


            #region Profiling
            var user = User.Identity as d_user;
            if (user.IsNotNull()
                && user.debugmode)
            {
                MiniProfiler.Start();
                MiniProfiler.Settings.PopupRenderPosition = RenderPosition.Right;
            }
            #endregion


            DB = Helpers.GetDatabase();
            DB.OpenAndSet();
        }


        protected override void Dispose( bool disposing )
        {
            if (disposing)
            {
                if (DB != null)
                    DB.Dispose();
            }

            base.Dispose( disposing );
        }
    }
}