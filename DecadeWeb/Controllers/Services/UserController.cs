﻿using System;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json;



namespace DecadeWeb.Controllers.Services
{
    [DecadeAuthorize]
    public class UserController : BaseApiController
    {
        [AcceptVerbs( "POST", "GET" )]
        public string GetUserByAny( string term )
        {
            var users = Mset.Exco.Decade.d_user.GetUsers(
                DB,
                term,
                term ).ToList();

            return users.Any()
                       ? JsonConvert.SerializeObject(
                           users.Select(
                               u => new
                               {
                                   u.employeenumber,
                                   name = u.Name
                               } ) )
                       : "";
        }


        [AcceptVerbs( "POST", "GET" )]
        public dynamic IsValid( string id )
        {
            int tmpEmpNum;

            if (Int32.TryParse(
                id,
                out tmpEmpNum ))
            {
                Mset.Exco.Decade.d_user user = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(
                    DB,
                    tmpEmpNum );

                if (user != null)
                {
                    return new
                    {
                        isValid = true,
                        message = "Valid Employee"
                    };
                }
            }

            return new
            {
                isValid = false,
                message = "Invalid Employee Number"
            };
        }
    }
}