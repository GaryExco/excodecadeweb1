﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web.Http;
using Dapper;
using LambdaSqlBuilder;
using Mset.Exco.Decade;
using Mset.Extensions;
using Newtonsoft.Json;



namespace DecadeWeb.Controllers.Services
{
    public class SteelInventoryController : BaseApiController
    {
        [AcceptVerbs( "POST", "GET" )]
        public dynamic InsertBars( [FromBody] string json )
        {
            using (DbTransaction trans = DB.Connection.BeginTransaction())
            {
                dynamic bars = JsonConvert.DeserializeObject( json );
                var curDate = DateTime.Now;

                foreach (dynamic Bar in bars)
                {
                    if (Bar == null
                        || !String.IsNullOrEmpty( Bar["NumID"].ToString().Trim() ))
                        continue;

                    double diameter = -1;
                    if (String.IsNullOrEmpty( Bar["Diameter"].ToString().Trim() )
                        || !Double.TryParse(
                            Bar["Diameter"].ToString(),
                            out diameter ))
                        continue;
                    // Valid Bar
                    string material = Bar["Material"].ToString();


                    #region Get Bar Group Info
                    var query = new SqlLam<inv_steelinf>()
                        .Where( x => x.Material == material )
                        .Where( x => x.Dia == diameter );

                    var barGroup = DB.Connection.Query<inv_steelinf>(
                        query,
                        trans ).SingleOrDefault();

                    if (barGroup.IsNull())
                    {
                        barGroup = new inv_steelinf
                        {
                            Material = material,
                            Dia = diameter
                        };
                        barGroup.Update(
                            DB,
                            trans,
                            true );

                        // Get the ID of the newly inserted Group
                        barGroup = DB.Connection.Query<inv_steelinf>(
                            query,
                            trans ).SingleOrDefault();
                    }
                    #endregion


                    #region Insert Bar Into Database
                    var strMatCode = barGroup.Material.Equals("HOT VAR")
                        ? barGroup.Material.Substring(0, 2)
                        : barGroup.Material.Substring(0, 1);

                    var bar = new inv_steel
                    {
                        BarInf = barGroup.ID.Value,
                        DateIn = curDate,
                        lastchange = curDate,
                        Length = Convert.ToDouble( Bar["Length"] ),
                        PrevLength = Convert.ToDouble( Bar["Length"] ),
                        TrueDia = Convert.ToDouble( Bar["TrueDia"] ),
                        lbPrice = Convert.ToDouble( Bar["lbPrice"] ),
                        vendorid = Convert.ToInt32( Bar["vendorid"] ),
                        heatnumber = Bar["heatnumber"],
                        NumID = $"{strMatCode}-{barGroup.Dia}-"
                    };


                    #region Get Next BarID (NumID)
                    double tmpDia = Convert.ToDouble( Bar["Diameter"] );
                    var barIDQuery = new SqlLam<inv_steel>()
                        .Top( 1 )
                        .Select( x => x.NumID )
                        .OrderByDescending(
                            x => new
                            {
                                x.DateIn,
                                x.NumID
                            } )
                        .JoinInner<inv_steelinf>( ( inv_steel, inv_steelinf ) => inv_steel.BarInf == inv_steelinf.ID )
                        .Select( x => x.Dia )
                        .Where( x => x.Material == material )
                        .Where( x => x.Dia == tmpDia );

                    var tmpBar = DB.Connection.Query<inv_steel, inv_steelinf, inv_steel>(
                        barIDQuery,
                        ( inv_steel, inv_steelinf ) =>
                        {
                            inv_steel.SteelInformation = inv_steelinf;
                            return inv_steel;
                        },
                        trans,
                        splitOn: "dia" ).SingleOrDefault();

                    if (tmpBar != null)
                    {
                        int barNum = Convert.ToInt32(
                            tmpBar.NumID.Substring(
                                tmpBar.NumID.LastIndexOf(
                                    "-",
                                    StringComparison.Ordinal ) + 1 ) ) + 1;
                        bar.NumID += barNum;
                    }
                    else
                        bar.NumID += "100";
                    #endregion


                    Bar.NumID = bar.NumID;
                    bar.Update(
                        DB,
                        trans,
                        true );
                    #endregion
                }

                trans.Commit();

                return bars;
            }
        }


        [AcceptVerbs( "POST", "GET" )]
        public dynamic CheckBarID( string id )
        {
            inv_steel bar = inv_steel.GetBarByID(
                DB,
                id );

            if (bar != null
                && !bar.DateFin.HasValue)
            {
                return new
                {
                    isValid = true,
                    message = "Valid Bar"
                };
            }

            return new
            {
                isValid = false,
                message = "Invalid Bar ID."
            };
        }


        [AcceptVerbs( "POST", "GET" )]
        public dynamic CheckBarLength( string val, double newLength )
        {
            inv_steel bar = inv_steel.GetBarByID(
                DB,
                val );

            if (bar != null
                && !bar.DateFin.HasValue)
            {
                return newLength < bar.PrevLength
                           ? new
                           {
                               isValid = true,
                               message = "Valid Length"
                           }
                           : new
                           {
                               isValid = false,
                               message = $"Bar should be less than Previous length ({bar.PrevLength})."
                           };
            }

            return new
            {
                isValid = false,
                message = "Invalid Bar ID."
            };
        }


        [AcceptVerbs( "POST", "GET" )]
        public dynamic PostNewBarLengths( [FromBody] string json )
        {
            var errors = new List<string>();
            var changeDate = DateTime.Now;

            using (DbTransaction trans = DB.Connection.BeginTransaction())
            {
                dynamic bars = JsonConvert.DeserializeObject( json );

                foreach (dynamic bar in bars)
                {
                    if (Extension.IsNull( bar )
                        || bar["NumID"] == null
                        || bar["NewLength"] == null)
                        continue;

                    double barLength = -1;
                    if (!String.IsNullOrEmpty( bar["NumID"].ToString().Trim() )
                        && Double.TryParse(
                            bar["NewLength"].ToString(),
                            out barLength ))
                    {
                        DB.Connection.Execute(
                            "UPDATE inv_steel SET PrevLength = @newLength " +
                            ", lastchange = @changeDate " +
                            ( barLength <= 0
                                  ? ", DateFin = @datefin "
                                  : String.Empty ) +
                            "WHERE NumID = @barid",
                            new
                            {
                                barid = bar["NumID"].ToString(),
                                newLength = barLength,
                                datefin = changeDate,
                                changeDate
                            },
                            trans
                            );
                    }
                    else
                    {
                        errors.Add(
                            $"Invald information for Bar {bar["NumID"]}." );
                    }
                }

                trans.Commit();
            }

            return new
            {
                error = errors.Count != 0,
                errors
            };
        }
    }
}