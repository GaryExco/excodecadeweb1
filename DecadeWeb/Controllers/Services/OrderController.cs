﻿using System;
using System.Linq;
using System.Web.Http;
using Dapper;
using MassTransit;
using Mset.Exco;
using Mset.Exco.Decade;
using Mset.Exco.Decade.Messages;
using Mset.Extensions;
using Newtonsoft.Json;

namespace DecadeWeb.Controllers.Services
{
    [DecadeAuthorize(DecadeRole = DecadeRole.ViewOrders)]
    public class OrderController : BaseApiController
    {
        // GET api/order/5
        [AcceptVerbs("POST", "GET")]
        public d_order Get(int id)
        {
            return d_order.GetOrderByOrderNumber(DB, id);
        }

        [AcceptVerbs("POST", "GET")]
        public dynamic IsValid(int id)
        {
            d_order order = d_order.GetOrderByOrderNumber(DB, id);

            if (order.IsNotNull() && !order.invoicedate.HasValue)
                return new { isValid = true, message = "Valid SO" };

            return new { isValid = false, message = "Invalid SO or Order has been closed." };
        }

        [AcceptVerbs("POST", "GET")]
        public dynamic GetHotListInfo(int id)
        {
            d_order order = d_order.GetOrderByOrderNumber(DB, id);

            // Order doesn't exist or is closed
            if (order.IsNull() || order.invoicedate.HasValue)
                return new { error = true, message = "No valid order found." };

            // Order not on hot list
            if (!order.hotListDate.HasValue)
                return new { error = false, hotlistdate = order.shopdate, hotlistpriority = order.hotListPriority, hotlistnotes = String.Empty };

            order.LoadHotListNote(DB);

            return new { error = false, hotlistdate = order.hotListDate.Value, hotlistpriority = order.hotListPriority, hotlistnotes = order.OrderNotes.notes };
        }

        [AcceptVerbs("POST", "GET"), DecadeAuthorize(DecadeRole = DecadeRole.HotListAdmin)]
        public dynamic PostHotListOrder([FromBody] string json)
        {
            try
            {
                dynamic hotListOrder = JsonConvert.DeserializeObject(json);

                int ordernumber = Convert.ToInt32(hotListOrder["ordernumber"]);
                string strNotes = hotListOrder["notes"];
                DateTime? hotlistdate = null;
                // 2013-08-05T04:00:00.000Z
                if (hotListOrder["hotlistdate"] != null)
                    hotlistdate = DateTime.Parse(hotListOrder["hotlistdate"].ToString());

                #region Update Order
                d_order order = d_order.GetOrderByOrderNumber(DB, Convert.ToInt32(hotListOrder["ordernumber"]));
                if (hotlistdate.HasValue)
                {
                    order.hotListPriority = 5;
                    order.hotListDate = hotlistdate.Value;
                }
                else
                {
                    order.hotListPriority = 0;
                    order.hotListDate = null;
                }

                order.Update(DB, null, false);
                #endregion

                #region Update Notes
                d_ordernotes notes = d_ordernotes.GetNotesForOrder(DB, ordernumber);
                if (notes.IsNull())
                {
                    notes = new d_ordernotes
                    {
                        ordernumber = ordernumber
                    };
                }

                notes.notes = strNotes;
                notes.Update(DB, null, true);
                #endregion


                // Push an event to tell scheduling to update this order
                Bus.Instance.Publish( new OrderModifiedEvent(ordernumber, false) );


                return new { isValid = true, message = "Entered Successfully." };
            }
            catch (Exception ex)
            {
                return new { isValid = false, message = ex.Message };
            }
        }

        [AcceptVerbs("POST", "GET"), DecadeAuthorize(DecadeRole = DecadeRole.ModOrders)]
        public dynamic GetOrderListing(string orderstatus, string duration)
        {
            string title,
                   numberField,
                   dateTitle,
                   dateField;
            var orders = d_order.GetOrderEntryListing(DB, orderstatus, duration).ToList();

            switch (orderstatus.ToLower())
            {
                case "shippedorders":
                    title = "Order #";
                    numberField = "ordernumber";
                    dateTitle = "Ship Date";
                    dateField = "shipdate";
                    break;

                case "closedorders":
                    title = "Invoice #";
                    numberField = "invoicenumber";
                    dateTitle = "Invoice Date";
                    dateField = "invoicedate";
                    break;

                default:
                    title = "Order #";
                    numberField = "ordernumber";
                    dateTitle = "Order Date";
                    dateField = "orderdate";
                    break;
            }

            return new {
                OrderCountTitle = "{Count} Orders".FormatWithName(new { Count = orders.Count() }),
                Title = title,
                NumberField = numberField,
                DateTitle = dateTitle,
                DateField = dateField,
                Orders = orders.Select(s => new
                {
                    s.ordernumber,
                    s.customercode,
                    CustomerName = s.Customer.name,
                    s.orderdate,
                    s.invoicenumber,
                    s.invoicedate,
                    s.shipdate
                })
            };
        }

        [AcceptVerbs("POST", "GET")]
        public dynamic GetSubscribeInfo(int ordernumber)
        {
            bool canSubscribe = false,
                 isSubscribed = false,
                 emailAlerts = false;

            try
            {
                var order = d_order.GetOrderByOrderNumber(DB, ordernumber);
                var user = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(DB, User.Identity.Name);

                canSubscribe = !(order.shipdate.HasValue || order.invoicedate.HasValue) && !user.email.IsNullOrEmpty();

                if (canSubscribe)
                {
                    var orderToWatch = d_orderwatch.GetWatchedOrders(DB, ordernumber, user.employeenumber).SingleOrDefault();

                    if (orderToWatch.IsNotNull())
                    {
                        isSubscribed = true;
                        emailAlerts = orderToWatch.emailalert;
                    }
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {
                // Do Nothing
                // throw;
            }

            return ReturnSubscribeInfo(canSubscribe, isSubscribed, emailAlerts);
        }

        [AcceptVerbs("POST", "GET")]
        public dynamic ManageOrderWatch(int ordernumber, string options)
        {
            bool canSubscribe = false,
                 isSubscribed = false,
                 emailAlerts = false;

            try
            {
                var order = d_order.GetOrderByOrderNumber(DB, ordernumber);
                var user = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(DB, User.Identity.Name);

                canSubscribe = !(order.shipdate.HasValue || order.invoicedate.HasValue) && !user.email.IsNullOrEmpty();

                if (canSubscribe)
                {
                    switch (options.ToUpper())
                    {
                        case "REMOVE":
                            DB.Connection.Execute("DELETE FROM d_orderwatch WHERE ordernumber = @ordernumber AND employeenumber = @employeenumber",
                                new
                                {
                                    ordernumber,
                                    user.employeenumber
                                });

                            isSubscribed = false;
                            emailAlerts = false;
                            break;

                        default:
                            var watchOrder = new d_orderwatch
                            {
                                employeenumber = user.employeenumber,
                                ordernumber = ordernumber,
                                emailalert = options.ContainsIgnoreCase("email")
                            };

                            watchOrder.Update(DB, null, true);

                            isSubscribed = true;
                            emailAlerts = watchOrder.emailalert;
                            break;
                    }
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {
                // Do Nothing
            }

            return ReturnSubscribeInfo(canSubscribe, isSubscribed, emailAlerts);
        }

        private dynamic ReturnSubscribeInfo(bool canSubscribe = false, bool isSubscribed = false, bool emailAlerts = false)
        {
            return new
            {
                cansubscribe = canSubscribe,
                issubscribed = isSubscribed,
                emailalerts = emailAlerts
            };
        }
    }
}