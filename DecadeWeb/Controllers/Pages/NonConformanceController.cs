﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DecadeWeb.Controllers.Pages;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb.Controllers
{
    [DecadeAuthorize( DecadeRole = DecadeRole.NonConf )]
    public class NonConformanceController : BasePageController
    {
        //
        // GET: /NonConformance/
        // [OutputCache(Duration = Int32.MaxValue, Location = System.Web.UI.OutputCacheLocation.ServerAndClient, VaryByParam = "*", VaryByCustom = "User")]
        [ImportModelStateFromTempData]
        public ActionResult Index()
        {
            return View();
        }


        [ImportModelStateFromTempData]
        public ActionResult ListForm( DateTime? startDate, DateTime? endDate, string department, int? employeenumber )
        {
            if (startDate.HasValue
                && endDate.HasValue)
            {
                return Redirect(
                    Url.ListNonConformance(
                        startDate.Value,
                        endDate.Value,
                        department,
                        employeenumber ) );
            }

            return View( "_ListForm" );
        }


        [ExportModelStateToTempData]
        public ActionResult List( DateTime startDate, DateTime endDate, string department, int? employeenumber )
        {
            IEnumerable<scraplog> errors = scraplog.GetAllFrom(
                DB,
                startDate,
                endDate,
                department,
                employeenumber );

            if (!errors.Any())
            {
                ModelState.AddModelError(
                    "NON_CONF_NONE_FOUND",
                    "Nothing found." );
                return RedirectToAction( "ListForm" );
            }

            return View( errors );
        }


        [ExportModelStateToTempData]
        public ActionResult Add()
        {
            return View( scrapdes.GetErrorCodes( DB ) );
        }


        [ExportModelStateToTempData]
        public ActionResult Print( int id )
        {
            scraplog error = scraplog.GetErrorByID(
                DB,
                id );

            if (error.IsNull())
            {
                ModelState.AddModelError(
                    "NON_CONF_ID_NOT_FOUND",
                    "Not found." );
                return RedirectToAction( "Index" );
            }

            return View( error );
        }
    }
}