﻿using System.IO;
using System.Web.Mvc;
using Mset.Exco;



namespace DecadeWeb.Controllers.Pages
{
    public class FileSystemController : BaseController
    {
        public ActionResult RDrive( string allinfo )
        {
            string path = Path.Combine(
                ExcoVars.FileSys_CADFiles,
                allinfo );
            return File(
                path,
                "application/acad" );
        }
    }
}