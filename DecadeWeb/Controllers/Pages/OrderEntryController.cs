﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DecadeWeb.Models;
using Mset.Database;
using Mset.Exco;
using Mset.Exco.Decade;
using Mset.Exco.Extensions;
using Mset.Extensions;



namespace DecadeWeb.Controllers.Pages
{
    [DecadeAuthorize( DecadeRole = DecadeRole.ModOrders )]
    public class OrderEntryController : BasePageController
    {
        //
        // GET: /OrderEntry/
        [ImportModelStateFromTempData]
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Add()
        {
            var order = new d_order
            {
                ordernumber = 0,
                orderdate = DateTime.Now,
                shopdate = DateTime.Now.ToWorkDay( 9 ),
                expshipdate = DateTime.Now.ToWorkDay( 12 ),
                expreceiveddate = DateTime.Now.ToWorkDay( 13 ),
                Customer = new d_customer(),
                defaultdiscountamount = true,
                defaultfreight = true,
                defaultgst = true,
                defaultnitride = true,
                defaultsteelsurcharge = true,
                discountamount = 0,
                shipvendorid = 0,
                ncr = false,
                ncrs = false,
                stock = false,
                fasttrack = false,
                priority = false,
                combineprices = false,
                shelf = false,
                zeroprice = false,
                shelforder = false,
                cor = false,
                nitrideweight = 0,
                roughweight = 0,
                freightweight = 0,
                subtotal = 0,
                steel = 0
            };

            return View(
                "Order",
                new OrderEntryOrderModel(
                    order,
                    d_shippingvendors.GetAll( DB ) ) );
        }


        [ExportModelStateToTempData]
        public ActionResult Edit( int id )
        {
            d_order order = d_order.GetOrderByOrderNumber(
                DB,
                id );

            if (order == null)
            {
                ModelState.AddModelError(
                    "NOT_FOUND",
                    $"Order {id} not found.");
                return RedirectToAction( "Index" );
            }

            order.Customer = d_customer.GetCustomerByCode(
                DB,
                order.customercode );
            order.Customer.LoadAddresses( DB );
            order.Customer.LoadShipVia( DB );

            using (var custNotes = new Database( DatabaseInfo.CustNotesStr ))
            {
                custNotes.Open();
                order.Customer.LoadPresses( custNotes );
            }

            order.LoadCustomerName( DB );
            order.LoadCheckList( DB );
            order.LoadOrderItems( DB );
            order.LoadTaxItems( DB );

            if (order.CheckList.IsNull())
                order.CheckList = new d_orderchecklist();

            if (order.OrderItems.IsNull()
                || !order.OrderItems.Any())
                order.OrderItems = new List<d_orderitem>();

            foreach (d_orderitem item in order.OrderItems)
            {
                // Read Suffix to have those available for JS
                item.ParseSuffix();

                // Clear out the order reference since we want to decrease bytes going across the wire
                item.Order = null;
            }


            return View(
                "Order",
                new OrderEntryOrderModel(
                    order,
                    d_shippingvendors.GetAll( DB ) ) );
        }
    }
}