﻿using System;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using DecadeWeb.Controllers.Pages;
using Mset.Extensions;



namespace DecadeWeb
{
    public class SearchController : BasePageController
    {
        //
        // GET: /Search/

        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "Main" );
        }


        public ActionResult Query( string query )
        {
            if (query.IsNullOrEmpty())
                return Redirect( Request.UrlReferrer.AbsolutePath );

            if (query.StartsWith(
                "LOC",
                StringComparison.InvariantCultureIgnoreCase ))
                return Redirect( Url.TrackByLoc( query.Substring( 3 ).Trim() ) );

            if (query.StartsWith(
                "DN",
                StringComparison.InvariantCultureIgnoreCase ))
                return Redirect( Url.TrackByDie( query.Substring( 2 ).Trim() ) );

            if (query.StartsWith(
                "O",
                StringComparison.InvariantCultureIgnoreCase )
                && Regex.IsMatch(
                    query,
                    "[0-9]{5,6}" ))
            {
                return Redirect(
                    Url.DisplayOrder(
                        Convert.ToInt32(
                            Regex.Match(
                                query,
                                "[0-9]{5,6}" ).Value ) ) );
            }

            if (query.StartsWith(
                "I",
                StringComparison.InvariantCultureIgnoreCase )
                && Regex.IsMatch(
                    query,
                    "[0-9]{5,6}" ))
            {
                return Redirect(
                    Url.TrackByInvoice(
                        Regex.Match(
                            query,
                            "[0-9]{5,6}" ).Value ) );
            }

            Match match = query.Match( "[0-9]{5,6}" );

            return Redirect(
                match.Success
                    ? Url.TrackByOrder( match.Value )
                    : Request.UrlReferrer.AbsolutePath );
        }
    }
}