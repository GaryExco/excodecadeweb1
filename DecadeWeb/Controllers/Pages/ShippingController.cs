﻿using System.Web.Mvc;
using Dapper;
using DecadeWeb.Controllers.Pages;
using LambdaSqlBuilder;
using Mset.Exco.Decade;
using Mset.Exco.Decade.Extensions;



namespace DecadeWeb.Controllers
{
    [DecadeAuthorize( DecadeRole = DecadeRole.Shipping )]
    public class ShippingController : BasePageController
    {
        //
        // GET: /Shipping/

        public ActionResult Index()
        {
            var query = new SqlLam<d_order>()
                .WhereIsOpen()
                .WhereNotShipped()
                .OrderBy( x => x.customercode )
                .OrderByDescending( x => x.expshipdate )
                .OrderBy( x => x.ordernumber );

            var openOrders = DB.Connection.Query<d_order>( query );
            return View( openOrders );
        }
    }
}