﻿using System;
using System.Linq;
using System.Web.Mvc;
using DecadeWeb.Controllers.Pages;
using DecadeWeb.Models;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb.Controllers
{
    [DecadeAuthorize( DecadeRole = DecadeRole.ReportsShop )]
    public class OnTimeController : BasePageController
    {
        //
        // GET: /OnTime/

        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "Reports" );
        }


        #region Detailed
        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult DetailedForm( string mode, DateTime? date, string customer )
        {
            if (!String.IsNullOrEmpty( mode )
                && date.HasValue)
            {
                return Redirect(
                    Url.DetailedOnTime(
                        mode,
                        date.Value,
                        customer ) );
            }

            return View( "_DetailedForm" );
        }


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult Detailed( string mode, DateTime date, string customer )
        {
            var tmpMode = DayMonthYearEnum.Day;
            DateTime startDate = date,
                     endDate = date.AddDays( 1 );

            if (mode.EqualsIgnoreCase( "Month" ))
            {
                startDate = new DateTime(
                    startDate.Year,
                    startDate.Month,
                    1 );
                endDate = date.AddMonths( 1 );
                tmpMode = DayMonthYearEnum.Month;
            }

            return View(
                new OnTimeModel(
                    startDate,
                    d_order.GetDetailedOnTime(
                        DB,
                        startDate,
                        endDate,
                        customer ),
                    tmpMode,
                    customer ) );
        }
        #endregion


        #region Performance
        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult PerformanceForm( string dayMode, DateTime? date, string orderFilter )
        {
            if (!dayMode.IsNullOrEmpty()
                && !orderFilter.IsNullOrEmpty()
                && date.HasValue)
            {
                return Redirect(
                    Url.PerformanceOnTime(
                        dayMode,
                        orderFilter,
                        date.Value ) );
            }

            return View( "_PerformanceForm" );
        }


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult Performance( string dayMode, DateTime date, string orderFilter )
        {
            var mode = DayMonthYearEnum.Day;
            DateTime startDate = date,
                     endDate = date.AddDays( 1 );

            if (String.Equals(
                dayMode,
                "month",
                StringComparison.InvariantCultureIgnoreCase ))
            {
                mode = DayMonthYearEnum.Month;
                startDate = new DateTime(
                    startDate.Year,
                    startDate.Month,
                    1 );
                endDate = date.AddMonths( 1 );
            }

            var model = new OnTimeModel(
                startDate,
                d_order.GetPerformanceOnTime(
                    DB,
                    startDate,
                    endDate,
                    orderFilter ),
                mode );

            if (String.Equals(
                orderFilter,
                "normal" ))
                model.Performance = OnTimePerformance.Normal;
            else if (String.Equals(
                orderFilter,
                "fasttrack" ))
                model.Performance = OnTimePerformance.FastTrack;
            else if (String.Equals(
                orderFilter,
                "hotlist" ))
                model.Performance = OnTimePerformance.HotList;
            else
                model.Performance = OnTimePerformance.All;

            model.Orders = model.Performance == OnTimePerformance.HotList
                               ? model.Orders.OrderByDescending( x => x.DiffFromHotList )
                               : model.Orders.OrderByDescending( x => x.DiffFromExpShip );

            return View( model );
        }
        #endregion


        #region Month By Cust
        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult MonthByCustForm( DateTime? date )
        {
            if (date.HasValue)
                return Redirect( Url.MonthOnTimeByCust( date.Value ) );

            return View( "_MonthByCustForm" );
        }


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult MonthByCust( DateTime date )
        {
            DateTime startDate = new DateTime(
                date.Year,
                date.Month,
                1 ),
                     endDate = startDate.AddMonths( 1 );

            return View(
                new OnTimeModel(
                    startDate,
                    d_order.GetMonthOnTimeByCust(
                        DB,
                        startDate,
                        endDate ),
                    DayMonthYearEnum.Month ) );
        }
        #endregion


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult YTD()
        {
            return View( d_order.YTDOnTime( DB ) );
        }
    }
}