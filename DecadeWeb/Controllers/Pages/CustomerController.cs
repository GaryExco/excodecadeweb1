﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DecadeWeb.Models;
using Mset.Exco.Decade;
using Mset.Exco.Extensions;
using Mset.Extensions;



namespace DecadeWeb.Controllers.Pages
{
    public class CustomerController : BasePageController
    {
        public ActionResult Dashboard()
        {
            return View( d_customer.GetCustomers( DB ) );
        }


        public ActionResult Summary( string id )
        {
            var model = new CustomerSummaryModel
            {
                OpenOrders = d_order.GetOpenOrdersByCustomer(
                    DB,
                    id )
            };

            var attentionOrders = new Dictionary<d_order, IEnumerable<AttentionReason>>();
            foreach (var order in model.OpenOrders)
            {
                var reasons = new List<AttentionReason>();

                if (order.customerpo.IsNullOrEmpty()
                    && order.expshipdate <= DateTime.Now.Date.ToWorkDay( -3 ))
                    reasons.Add( AttentionReason.NoPO );

                if (order.onhold)
                    reasons.Add( AttentionReason.OnHold );

                if (order.expshipdate < DateTime.Now.Date)
                    reasons.Add( AttentionReason.Late );

                if (reasons.Count > 0)
                {
                    attentionOrders.Add(
                        order,
                        reasons );
                }
            }

            model.AttentionOrders = attentionOrders;

            return View( model );
        }
    }
}