﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DecadeWeb.Controllers.Pages;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb.Controllers
{
    [DecadeAuthorize( DecadeRole = DecadeRole.Steel | DecadeRole.SteelPricing )]
    public class SteelInventoryController : BasePageController
    {
        //
        // GET: /SteelInventory/

        // [OutputCache(Duration = Int32.MaxValue, Location = System.Web.UI.OutputCacheLocation.ServerAndClient, VaryByParam = "*", VaryByCustom = "User")]
        [ImportModelStateFromTempData]
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Display( string filter, double? diameter )
        {
            return View(
                inv_steel.GetSteelInventory(
                    DB,
                    filter,
                    diameter ) );
        }


        [DecadeAuthorize( DecadeRole = DecadeRole.SteelPricing )]
        public ActionResult Add( string json )
        {
            if (!json.IsNullOrEmpty())
                return View( "Inserted" );

            IEnumerable<inv_vendors> Vendors = inv_vendors.GetVendors( DB );

            if (!Vendors.Any())
                Vendors = new List<inv_vendors>();

            return View( Vendors );
        }


        public ActionResult Edit()
        {
            return View( inv_steel.GetSteelInventory( DB ) );
        }


        [ImportModelStateFromTempData]
        public ActionResult ViewBarForm( string barid )
        {
            if (!barid.IsNullOrEmpty())
                return Redirect( Url.SteelInventoryViewBar( barid ) );

            return View( "_ViewBarForm" );
        }


        [ExportModelStateToTempData]
        public ActionResult ViewBar( string barid )
        {
            inv_steel bar = inv_steel.GetBarByID(
                DB,
                barid );

            if (bar == null)
            {
                ModelState.AddModelError(
                    "STEEL_INV_NO_BAR",
                    $"Bar {barid} not found." );
                return RedirectToAction( "ViewBarForm" );
            }

            return View( bar );
        }
    }
}