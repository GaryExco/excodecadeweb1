﻿using System.IO;
using System.Web.Mvc;
using DecadeWeb.Controllers.Pages;
using Mset.Exco;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb.Controllers
{
    [DecadeAuthorize( DecadeRole = DecadeRole.ViewOrders )]
    public class OrderController : BasePageController
    {
        //
        // GET: /Order/
        [ImportModelStateFromTempData]
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "Tracking" );
        }


        [ExportModelStateToTempData]
        public ActionResult DisplayOrder( int ordernumber )
        {
            if (Helpers.IsPOST)
                return Redirect( Url.DisplayOrder( ordernumber ) );

            d_order order = d_order.GetOrderByOrderNumber(
                DB,
                ordernumber );

            if (order == null)
            {
                ModelState.AddModelError(
                    "DISPLAY_ORDER_NOT_FOUND",
                    $"Order {ordernumber} not found.");
                return RedirectToAction(
                    "Index",
                    "Tracking" );
            }

            order.LoadOrderItems( DB );
            order.LoadShipVendor( DB );
            return View(
                "_OrderDisplay",
                order );
        }


        public ActionResult DisplayInvoice( int invoicenumber )
        {
            if (Helpers.IsPOST)
                return Redirect( Url.DisplayOrderByInvoice( invoicenumber ) );

            d_order order = d_order.GetOrderByInvoiceNumber(
                DB,
                invoicenumber );
            order.LoadOrderItems( DB );
            order.LoadShipVendor( DB );
            return View(
                "_OrderDisplay",
                order );
        }


        public ActionResult YDrive( string allinfo )
        {
            string path = Path.Combine(
                ExcoVars.Variables["FileSys.YDrive"],
                allinfo );
            return File(
                path,
                "image/jpeg" );
        }
    }
}