﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Dapper;
using DecadeWeb.Code;
using DecadeWeb.Controllers.Pages;
using DecadeWeb.Models;

using LambdaSqlBuilder;
using MoreLinq;
using Mset.Exco;
using Mset.Exco.Decade;
using Mset.Exco.Decade.Extensions;
using Mset.Exco.Extensions;
using Mset.Extensions;
// using WebGrease.Css.Extensions;

namespace DecadeWeb.Controllers
{
    [DecadeAuthorize(DecadeRole = DecadeRole.ReportsShop)]
    public class ScheduleController : BasePageController
    {
        //
        // GET: /Schedule/

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Reports");
        }

        #region Admin
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult AdminForm(string mode, DateTime? date, string dietype, string checkht = null)
        {
            if (!String.IsNullOrEmpty(mode) && date.HasValue && !String.IsNullOrEmpty(dietype))
            {
                var tmpDieType = Mset.Exco.DieType.Solid;
                var tmpMode = AdminSchedShowMode.Ordernumber;

                if (String.Equals(dietype, "hollow", StringComparison.InvariantCultureIgnoreCase))
                    tmpDieType = Mset.Exco.DieType.Hollow;

                if (mode.IndexOf("die", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    tmpMode = AdminSchedShowMode.DieNumber;

                bool checkHTDone = !checkht.IsNullOrEmpty() && checkht.EqualsIgnoreCase("HT");

                return Redirect(Url.AdminSchedule(tmpDieType, tmpMode, date.Value, checkHTDone));
            }

            return View("_AdminForm");
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult Admin(string dispmode, DateTime? date, string dietype, string checkHT = null)
        {
            var asm = new AdminSchedModel
            {
                DispMode = dispmode.EqualsIgnoreCase("die")
                               ? AdminSchedShowMode.DieNumber
                               : AdminSchedShowMode.Ordernumber,
                StartDate = date.HasValue
                                ? date.Value.StartOfWeek()
                                : DateTime.Now.StartOfWeek(),
                Orders = new Dictionary<DateTime, List<d_orderitem>>(),
                DieType = dietype.EqualsIgnoreCase("Solid")
                    ? Mset.Exco.DieType.Solid
                    : Mset.Exco.DieType.Hollow,
                CheckHTDone = !checkHT.IsNullOrEmpty() && checkHT.EqualsIgnoreCase("HT")
            };

            asm.EndDate = asm.StartDate.AddDays(5);

            // Fill in Dates on Schedule
            var tmpDate = asm.StartDate.Date;
            while (tmpDate < asm.EndDate)
            {
                asm.Orders.Add(tmpDate, new List<d_orderitem>());

                tmpDate = tmpDate.AddDays(1);
            }

            var orders = d_order.GetAdminScheduleItems(DB, asm.StartDate, asm.EndDate, dietype, null).ToList();

            if (!orders.Any())
                return View(asm);

            if (asm.CheckHTDone)
            {
                // Load Parts
                orders.Select(x => x.Order).ForEach(x => x.LoadParts(DB));

                // Check if HT is done
                orders.Select(x => x.Order).ForEach(x => x.Parts.ForEach(y => y.LoadHTDoneInfo(DB)));
            }

            #region Load Prefixes for Order Items
            IEnumerable<int> orderNumbers = orders.Select(o => o.ordernumber).Distinct();
            IEnumerable<d_orderitem> orderItems = d_order.GetPrefixesForOrders(DB, orderNumbers);

            foreach (d_orderitem order in orderItems)
            {
                d_orderitem tmp = orders.First(x => x.ordernumber == order.ordernumber);
                tmp.Order.OrderItems.Add(order);
            }
            #endregion

            var orderDict = new Dictionary<DateTime, List<d_orderitem>>();

            // Sort Into Days
            foreach (var curDate in asm.Orders)
            {
                var tmp = orders.Where(o => o.Order.shopdate == curDate.Key);
                tmp = asm.DispMode == AdminSchedShowMode.DieNumber
                    ? tmp.OrderBy(o => o.dienumber)
                    : tmp.OrderBy(o => o.ordernumber);

                orderDict.Add(curDate.Key, tmp.ToList());

                // asm.Orders[curDate.Key] = tmp.ToList();
            }

            asm.Orders = orderDict;

            return View(asm);
        }
        #endregion


        #region HT
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult HTForm(DateTime? startdate, DateTime? enddate)
        {
            if (startdate.HasValue && enddate.HasValue)
                return Redirect(Url.HTSchedule(startdate.Value, enddate.Value));

            return View("_HTForm");
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult HT(DateTime startdate, DateTime enddate)
        {
            IEnumerable<d_order> orders = d_order.GetOrdersByHTDate(DB, startdate, enddate);
            var taskCache = d_taskcode.LoadAll(DB).ToList();

            foreach (d_order order in orders)
            {
                order.LoadParts(DB);

                foreach (Part part in order.Parts)
                {
                    part.LoadHTDoneInfo(DB);

                    if (!part.IsHTDone)
                        part.LoadLastTrack(DB, order.IsHollow, taskCache);
                }
            }

            return View(orders);
        }
        #endregion


        #region Order
        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult Order(int? days)
        {
            if (!days.HasValue)
                days = 12;

            if (days <= 0)
                days = 1;

            if (days > 31)
                days = 31;


            // Find the End Date to find all of our orders
            var endDate = DateTime.Today.ToWorkDay( days.Value );


            // Build our Query to select the orders
            var query = new SqlLam<d_order>()
                    .WhereIsOpen()
                    // .Where( x => x.shopdate >= startDate )
                    .Where( x => x.shopdate <= endDate );


            var orders = DB.Connection.Query<d_order>( query )
                                      .ToList();


            // Load all of the Order Items / Charges for these orders
            orders.LoadOrderItems(DB);


            var orderDict = new Dictionary<int, List<d_order>>();


            for (var i = -1; i < days.Value; i++)
            {
                var tmpDate = DateTime.Today.ToWorkDay( i );
                var tmpOrders = orders
                    .WhereIf( i == -1, x => x.shopdate.Date <= tmpDate.Date )
                    .WhereIf( i == -1, x => !x.onhold && !x.IsStockJob )
                    .WhereIf( i >= 0, x => x.shopdate.Date == tmpDate.Date )
                    .ToList();

                orderDict.Add(i, tmpOrders);
            }


            var limits = d_dailyorderlimits.GetLimits(DB);


            var modelLimits = limits.Select(GetScheduleOrderLineModel)
                                    .ToList();


            var model = new ScheduleOrderModel
            {
                Days = days.Value,
                Orders = orderDict,
                Rows = modelLimits
            };


            return View(model);
        }


        private ScheduleOrderLineModel GetScheduleOrderLineModel( d_dailyorderlimits limit )
        {
            var rtnVal = new ScheduleOrderLineModel
            {
                Limit = limit
            };


            switch (limit.code)
            {
                case "DI":
                case "BA":
                case "FE":
                case "HO":
                case "BO":
                case "RI":
                    rtnVal.CounterFunction = (o) => o.OrderItems.Count(x => x.prefix.EqualsIgnoreCase(limit.code));
                    break;


                case "MC":
                    rtnVal.CounterFunction = (o) => o.OrderItems.SelectMany(x => x.Charges)
                                                                .Count(x => x.chargename.Replace(" ", "").Replace("-", "").ContainsIgnoreCase("MULTIMANDREL"));
                    break;


                case "OP":
                    rtnVal.CounterFunction = (o) => o.OrderItems.SelectMany(x => x.Charges)
                                                                .Count(x => x.chargename.Replace(" ", "").Replace("-", "").ContainsIgnoreCase("ONEPIECE"));
                    break;


                case "FT":
                    rtnVal.CounterFunction = (o) => o.fasttrack ? 1 : 0;
                    break;


                case "NI":
                    rtnVal.CounterFunction = (o) => o.OrderItems.Any(x => x.hasnitride) ? 1 : 0;
                    break;


                case "NW":
                    rtnVal.CounterFunction = ( o ) =>
                    {
                        if (!o.OrderItems.Any(x => x.hasnitride))
                            return 0;

                        return Convert.ToDouble(o.nitrideweight ?? 0);
                    };
                    break;


                case "SA":
                    rtnVal.CounterFunction = ( o ) => Convert.ToDouble(o.sales);
                    break;


                case "DQ":
                    rtnVal.CounterFunction = ( o ) => 1;
                    break;


                default:
                    rtnVal.CounterFunction = ( o ) => 0;
                    break;
            }


            return rtnVal;
        }
        #endregion
    }
}
