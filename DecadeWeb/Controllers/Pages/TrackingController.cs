﻿using System.Linq;
using System.Web.Mvc;
using Dapper;
using DecadeWeb.Controllers.Pages;
using LambdaSqlBuilder;
using MoreLinq;
using Mset.Exco.Decade;
using Mset.Exco.Decade.Extensions;
using Mset.Extensions;



// using WebGrease.Css.Extensions;

namespace DecadeWeb.Controllers
{
    [DecadeAuthorize( DecadeRole = DecadeRole.Tracking )]
    public class TrackingController : BasePageController
    {
        //
        // GET: /Tracking/
        [ImportModelStateFromTempData]
        //, OutputCache(Duration = Int32.MaxValue, Location = System.Web.UI.OutputCacheLocation.ServerAndClient, VaryByParam = "*", VaryByCustom = "User")
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult TrackHelper( d_order order )
        {
            order.LoadCustomerName( DB );
            order.LoadTrackPageInfo( DB );
            order.LoadScrapInfo( DB );
            order.LoadOrderItems( DB );
            order.GetDieNumbers( DB );
            order.LoadParts( DB );
            order.LoadShipWithOrders( DB );
            order.LoadPredictedShipDate( DB );
            order.LoadProcessNumbers( DB );
            order.LoadDesignList( CustNotesDB );

            order.Parts.ForEach(
                x =>
                {
                    x.LoadLastTrack(
                        DB,
                        x.Order.IsHollow );
                    x.LoadBarID( DB );
                } );

            // Adjust % complete for shipped orders who were > 50% complete
            if (order.shipdate.HasValue)
                order.Parts.Where( x => x.PercentComplete > 50 ).ForEach( x => x.PercentComplete = 100 );


            var ipiQuery = new SqlLam<d_inprocessinspection>()
                .Where( x => x.ordernumber == order.ordernumber );

            order.InProcessInspection = DB.Connection.Query<d_inprocessinspection>( ipiQuery )
                                          .ToList();

            return View(
                "Track",
                order );
        }


        [ExportModelStateToTempData]
        public ActionResult TrackByOrder( int ordernumber, string format )
        {
            if (d_order.OrderExists(
                DB,
                ordernumber ))
            {
                return Helpers.IsPOST
                           ? Redirect( Url.TrackByOrder( ordernumber ) )
                           : TrackHelper(
                               d_order.GetOrderByOrderNumber(
                                   DB,
                                   ordernumber ) );
            }

            ModelState.AddModelError(
                "ORDER_NOT_FOUND",
                $"Order {ordernumber} not found.");
            return RedirectToAction(
                "Index",
                "Tracking" );
        }


        public ActionResult TrackByInvoice( int invoicenumber )
        {
            if (d_order.InvoiceExists(
                DB,
                invoicenumber ))
            {
                return Helpers.IsPOST
                           ? Redirect( Url.TrackByInvoice( invoicenumber ) )
                           : TrackHelper(
                               d_order.GetOrderByInvoiceNumber(
                                   DB,
                                   invoicenumber ) );
            }

            ModelState.AddModelError(
                "INVOICE_NOT_FOUND",
                $"Invoice {invoicenumber} not found." );
            return RedirectToAction(
                "Index",
                "Tracking" );
        }


        [ExportModelStateToTempData]
        public ActionResult TrackByDie( string dienumber, string customercode )
        {
            // Dienumber is empty redirect to track page
            if (dienumber.IsNullOrEmpty())
            {
                ModelState.AddModelError(
                    "TRACK_NO_DIE_NUM",
                    "No dienumber specified." );
                return RedirectToAction( "Index" );
            }

            return Request.HttpMethod == "POST"
                       ? (ActionResult) Redirect(
                           Url.TrackByDie(
                               dienumber,
                               customercode ) )
                       : View(
                           "TrackBy",
                           d_order.GetOrdersByDieNumber(
                               DB,
                               dienumber,
                               customercode ) );
        }


        public ActionResult TrackByCust( string customercode, string timefilter, string orderstatus )
        {
            if (Request.HttpMethod == "POST")
            {
                return Redirect(
                    Url.TrackByCust(
                        customercode,
                        timefilter,
                        orderstatus ) );
            }

            return View(
                "TrackBy",
                d_order.GetCustomerOrdersWithDieNumber(
                    DB,
                    customercode,
                    timefilter,
                    orderstatus ) );
        }


        public ActionResult TrackByLoc( string location )
        {
            return View(
                "TrackBy",
                d_order.GetOrdersByLocNumber(
                    DB,
                    location ) );
        }
    }
}