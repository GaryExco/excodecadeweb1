﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using LambdaSqlBuilder;
using MoreLinq;
using Mset.Exco.Decade;
using Mset.Extensions;
// using WebGrease.Css.Extensions;

namespace DecadeWeb.Controllers.Pages
{
    public class WebNotesController : BaseController
    {
        //
        // GET: /WebNotes/

        public ActionResult Index(string sortMode)
        {
            if (sortMode.IsNullOrEmpty())
                sortMode = "ByName";

            IEnumerable<d_customer> customers = d_customer.GetCustomerList(DB, sortMode);

            return View(model: customers);
        }

        public ActionResult Display(string customercode, string noteType, string filter = null)
        {
            if (noteType.ContainsIgnoreCase("press"))
                return View("Press");

            return View("Display");
        }

        private ActionResult DisplayPress(string customercode)
        {
            var presses = Press.LoadPressesForCustomer(CustNotesDB, customercode);

            // Load Presses and related info
            presses.ForEach(x =>
            {
                x.LoadDieRingInfo(CustNotesDB);
                x.DieRings.ForEach(y => y.LoadDieSizes(CustNotesDB));
            });

            return View("Press", presses);
        }

        private ActionResult DisplayNote(string customercode, string noteType, string filter = null)
        {
            // var notes = Notes.GetNotes(CustNotesDB, customercode,)

            return View("Display");
        }
    }
}
