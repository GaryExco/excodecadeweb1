﻿using System.Web.Mvc;
using DecadeWeb.Controllers.Pages;
using Mset.Extensions;



namespace DecadeWeb.Controllers
{
    public class MainController : BasePageController
    {
        //
        // GET: /Main/

        [ImportModelStateFromTempData]
        //, OutputCache(Duration = Int32.MaxValue, Location = System.Web.UI.OutputCacheLocation.ServerAndClient, VaryByParam = "*", VaryByCustom = "User")
        public ActionResult Index()
        {
            // If this is requested, Redirect them to their starting app

            var user = User.Identity as d_user;

            return Redirect(
                user.IsNotNull()
                    ? user.startapp
                    : "/Tracking" );
        }


        [ExportModelStateToTempData]
        public ActionResult AccessDenied()
        {
            ModelState.AddModelError(
                "ACCESS_DENIED",
                "Access is Denied." );
            return RedirectToAction( "Index" );
        }
    }
}