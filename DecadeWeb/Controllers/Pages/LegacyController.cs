﻿using System;
using System.Web.Mvc;
using Mset.Extensions;



namespace DecadeWeb.Controllers.Pages
{
    public class LegacyController : BaseController
    {
        public ActionResult ProcLogin( string employee, string goTo, string id )
        {
            if (goTo.IsNotNullOrEmpty()
                && goTo.EqualsIgnoreCase( "track" ))
            {
                var accountController = new AccountController();
                var user = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(
                    DB,
                    employee );

                if (user.IsNotNull())
                {
                    return accountController.Login(
                        employee,
                        user.trackpassword,
                        Url.TrackByOrder( id ) );
                }
            }

            return Legacy( "" );
        }


        public ActionResult Tracking( int? id, int? ordernumber, int? invoicenumber )
        {
            if (id.HasValue)
                return Redirect( Url.TrackByOrder( id.Value ) );

            if (ordernumber.HasValue)
                return Redirect( Url.TrackByOrder( ordernumber.Value ) );

            if (invoicenumber.HasValue)
                return Redirect( Url.TrackByInvoice( invoicenumber.Value ) );

            return Legacy( "" );
        }


        public ActionResult Legacy( string path )
        {
            // Try to redirect to the user to their start app
            if (User.Identity.IsNotNullOrEmpty())
            {
                Mset.Exco.Decade.d_user user = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(
                    DB,
                    User.Identity.Name );
                if (user.IsNotNull()
                    && !String.IsNullOrEmpty( user.startapp )
                    && !user.startapp.ContainsIgnoreCase( ".asp" ))
                    return Redirect( user.startapp );
            }

            // Fail to the default page
            return RedirectToAction(
                "Index",
                "Main" );
        }
    }
}