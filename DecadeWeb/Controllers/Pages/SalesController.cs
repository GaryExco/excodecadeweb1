﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Dapper;
using DecadeWeb.Controllers.Pages;
using DecadeWeb.Models;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb.Controllers
{
    [DecadeAuthorize( DecadeRole = DecadeRole.ReportsAdmin )]
    public class SalesController : BasePageController
    {
        //
        // GET: /Sales/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Summary()
        {
            DateTime lastFiscalStart = new DateTime(
                DateTime.Now.Month >= 10
                    ? DateTime.Now.Year - 1
                    : DateTime.Now.Year - 2,
                10,
                1 ),
                     thisFiscalStart = lastFiscalStart.AddYears( 1 ),
                     nextFiscalStart = thisFiscalStart.AddYears( 1 ),
                     thisMonthStart = DateTime.Now.Date.ToFirstDayOfMonth(),
                     nextMonthStart = thisMonthStart.AddMonths( 1 );

            var model = new List<SalesSummaryModel>();
            IEnumerable<d_customer> customers = d_customer.GetCustomers( DB );

            foreach (d_customer customer in customers)
            {
                const string sql = @"
                    SELECT * FROM d_customer_order_count_total(@customercode, @lastFiscalStart, @thisFiscalStart)
                    SELECT * FROM d_customer_order_count_total(@customercode, @thisFiscalStart, @nextFiscalStart)
                    SELECT * FROM d_customer_order_count_total(@customercode, @thisMonthStart, @nextMonthStart)
                ";

                using (var multi = DB.Connection.QueryMultiple(
                    sql,
                    new
                    {
                        customer.customercode,
                        lastFiscalStart,
                        thisFiscalStart,
                        nextFiscalStart,
                        thisMonthStart,
                        nextMonthStart
                    } ))
                {
                    d_customer_order_count_total lastYear = multi.Read<d_customer_order_count_total>().SingleOrDefault();
                    d_customer_order_count_total thisYear = multi.Read<d_customer_order_count_total>().SingleOrDefault();
                    d_customer_order_count_total thisMonth = multi.Read<d_customer_order_count_total>().SingleOrDefault();

                    if (lastYear == null)
                    {
                        lastYear = new d_customer_order_count_total
                        {
                            ordercount = 0,
                            ordertotal = 0
                        };
                    }

                    if (thisYear == null)
                    {
                        thisYear = new d_customer_order_count_total
                        {
                            ordercount = 0,
                            ordertotal = 0
                        };
                    }

                    if (thisMonth == null)
                    {
                        thisMonth = new d_customer_order_count_total
                        {
                            ordercount = 0,
                            ordertotal = 0
                        };
                    }

                    model.Add(
                        new SalesSummaryModel(
                            customer,
                            lastYear,
                            thisYear,
                            thisMonth ) );
                }
            }

            return View( model );
        }
    }
}