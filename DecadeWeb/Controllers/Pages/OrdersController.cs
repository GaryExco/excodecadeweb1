﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DecadeWeb.Controllers.Pages;
using Mset.Exco.Decade;
using Mset.Exco.Decade.Extensions;
using Mset.Exco.Extensions;
using DecadeWeb.Models;
using MoreLinq;
using Mset.Extensions;
using LambdaSqlBuilder;
using Dapper;

namespace DecadeWeb.Controllers
{
    [DecadeAuthorize(DecadeRole = DecadeRole.ReportsShop)]
    public class OrdersController : BasePageController
    {
        //
        // GET: /Orders/

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Reports");
        }

        #region List Jobs
        public ActionResult Idle()
        {
            return ListJobs(new OrdersListModel("Idle Orders Since {Date}".FormatWithName(new { Date = DateTime.Now.AddDays(-1.5) }), d_order.GetIdleOrders(DB)));
        }

        public ActionResult OnHold()
        {
            return ListJobs(new OrdersListModel("On Hold Orders", d_order.GetOnHoldOrders(DB)));
        }

        public ActionResult Stock()
        {
            return ListJobs(new OrdersListModel("Stock Orders", d_order.GetStockOrders(DB)));
        }

        public ActionResult NCR()
        {
            return ListJobs(new OrdersListModel("NCR Orders", d_order.GetNCROrders(DB)));
        }
        
        public ActionResult FastTrack()
        {
            return ListJobs(new OrdersListModel("Fast Track Orders", d_order.GetFasttrackOrders(DB)));
        }

        public ActionResult Late()
        {
            return ListJobs(new OrdersListModel("Late Orders", d_orderextensions.GetLateOrders(DB, DateTime.Now.ToWorkDay(3)), true));
        }
        
        public ActionResult HotList()
        {
            return View();
        }

        private ActionResult ListJobs(OrdersListModel model)
        {
            foreach (d_order order in model.Orders)
                order.GetDieNumbers(DB);

            return View("_List", model);
        }
        #endregion

        #region Daily Totals
        public ActionResult DailyTotalsForm(DateTime? startdate, DateTime? enddate, string mode, decimal exchangerate = 1)
        {
            if (!startdate.HasValue || String.IsNullOrEmpty(mode))
                return View("_DailyTotalsForm");


            return Redirect(Url.DailyTotals(startdate.Value, mode, exchangerate, enddate));
        }


        public ActionResult DailyTotals(DateTime startdate, DateTime? enddate, string mode, decimal exchangerate)
        {
            DailyTotalsMode tmpMode;

            switch (mode.ToLower())
            {
                case "invoice":
                    tmpMode = DailyTotalsMode.Invoice;
                    break;

                case "shipped":
                    tmpMode = DailyTotalsMode.Shipped;
                    break;

                default:
                    tmpMode = DailyTotalsMode.Order;
                    break;
            }

            var model = new DailyTotalsModel
            {
                StartDate = startdate.ToFirstDayOfMonth(),
                EndDate = (enddate ?? startdate).AddMonths( 1 ),
                Mode = tmpMode,
                ExchangeRate = exchangerate
            };
            // model.EndDate = model.StartDate.AddMonths(1);

            model.Territories = d_territories.GetTerritories(DB);
            model.Orders = d_order.GetDailyTotals(DB, model.Mode, model.StartDate, model.EndDate);
            return View(model);
        }
        #endregion

        #region Invoice Audit
        [ImportModelStateFromTempData]
        public ActionResult InvoiceAuditForm(DateTime? date)
        {
            if (date.HasValue)
                return Redirect(Url.InvoiceAudit(date.Value));

            return View("_InvoiceAuditForm");
        }

        [ExportModelStateToTempData]
        public ActionResult InvoiceAudit(DateTime date)
        {
            IEnumerable<d_order> orders = d_order.GetInvoiceAuditOrders(DB, date);

            if (!orders.Any())
            {
                ModelState.AddModelError("INVOICE_AUDIT_NO_ORDERS", "No orders found with invoice date of {date}.".FormatWithName(new { date = date.ToShortDateString() }));
                return RedirectToAction("InvoiceAuditForm");
            }

            return View(orders);
        }
        #endregion

        #region Order Action
        [ImportModelStateFromTempData]
        public ActionResult OrderActionForm(int? minorder, int? maxorder, string customercode, DateTime? startdate, DateTime? enddate)
        {
            if (minorder.HasValue && maxorder.HasValue)
                return Redirect(Url.OrderAction(minorder.Value, maxorder.Value, customercode));

            if (startdate.HasValue && enddate.HasValue)
                return Redirect(Url.OrderAction(startdate.Value, enddate.Value, customercode));

            var daysIterator = 0;
            d_order firstOrder = null;

            while (firstOrder.IsNull())
            {
                if (daysIterator >= 7)
                    break;

                firstOrder = d_order.GetFirstOrderOnDate(DB, DateTime.Now.Date.ToWorkDay(daysIterator * -1));
                daysIterator++;
            }

            if (firstOrder == null)
            {
                firstOrder = new d_order
                {
                    ordernumber =  0,
                    orderdate = DateTime.Now
                };
            }

            return View("_OrderActionForm", firstOrder);
        }

        /*
        [ExportModelStateToTempData]
        public ActionResult OrderAction(int? minorder, int? maxorder, string customercode, DateTime? startdate, DateTime? enddate)
        {
            IEnumerable<d_order> orders = new List<d_order>();

            if (minorder.HasValue && maxorder.HasValue)
                orders = d_order.GetOrderAction(DB, minorder.Value, maxorder.Value, customercode);
            else if (startdate.HasValue && enddate.HasValue)
                orders = d_order.GetOrderAction(DB, startdate.Value, enddate.Value, customercode);

            if (!orders.Any())
            {
                ModelState.AddModelError("ORDER_ACTION_NO_ORDERS", "No orders found between {minorder} and {maxorder}.".FormatWithName(new { minorder, maxorder }));
                return RedirectToAction("OrderActionForm");
            }

            return View(orders);
        }
        */

        [ExportModelStateToTempData]
        public ActionResult OrderActionByRange(string datefield, int minorder, int maxorder, string customercode)
        {
            return OrderActionHelper("Orders between {minorder} - {maxorder}".FormatWithName(
                new
                {
                    minorder,
                    maxorder
                }),
                d_order.GetOrderAction(DB, datefield, minorder, maxorder, customercode));
        }

        [ExportModelStateToTempData]
        public ActionResult OrderActionByDateRange(string datefield, DateTime startdate, DateTime enddate, string customercode)
        {
            var mode = "Order Date";

            if (datefield.ContainsIgnoreCase("ship"))
                mode = "Ship Date";
            else if (datefield.ContainsIgnoreCase("invoice"))
                mode = "Invoice Date";

            return OrderActionHelper("{datename} between {startdate} - {enddate}".FormatWithName(
                new
                {
                    datename = mode,
                    startdate = startdate.ToShortDateString(),
                    enddate = enddate.ToShortDateString()
                }),
                d_order.GetOrderAction(DB, datefield, startdate, enddate, customercode));
        }

        [ExportModelStateToTempData]
        public ActionResult OrderActionHelper(string title, IEnumerable<d_order> orders)
        {
            if (!orders.Any())
            {
                ModelState.AddModelError("ORDER_ACTION_NONE_FOUND", "No orders found.");
                return RedirectToAction("OrderActionForm");
            }
            return View("OrderAction", new OrderActionModel{ Orders = orders, Title = title });
        }
        #endregion

        #region Open Orders
        public ActionResult OpenOrdersForm(int? minorder, int? maxorder, string customercode, DateTime? startdate, DateTime? enddate)
        {
            if (minorder.HasValue && maxorder.HasValue)
                return Redirect(Url.OpenOrdersByRange(minorder.Value, maxorder.Value, customercode));

            if (startdate.HasValue && enddate.HasValue)
                return Redirect(Url.OpenOrdersByDateRange(startdate.Value, enddate.Value, customercode));

            if (!customercode.IsNullOrEmpty())
                return Redirect(Url.OpenOrdersByCustomer(customercode));

            var daysIterator = 0;
            d_order firstOrder = null;

            while (firstOrder.IsNull())
            {
                if (daysIterator >= 7)
                    break;

                firstOrder = d_order.GetFirstOrderOnDate(DB, DateTime.Now.Date.ToWorkDay(daysIterator * -1));
                daysIterator++;
            }

            if (firstOrder == null)
            {
                firstOrder = new d_order
                {
                    ordernumber = 0,
                    orderdate = DateTime.Now
                };
            }

            return View("_OpenOrdersForm", firstOrder);
        }

        public ActionResult OpenOrdersByRange(int minorder, int maxorder, string customercode)
        {
            return OpenOrdersHelper(d_order.GetOpenOrdersByRange(DB, minorder, maxorder, customercode));
        }

        public ActionResult OpenOrdersByCustomer(string customercode)
        {
            return OpenOrdersHelper(d_order.GetOpenOrdersByCustomer(DB, customercode));
        }

        public ActionResult OpenOrdersByDateRange(DateTime startdate, DateTime enddate, string customercode)
        {
            return OpenOrdersHelper(d_order.GetOpenOrdersByDateRange(DB, startdate, enddate, customercode));
        }

        private ActionResult OpenOrdersHelper(IEnumerable<d_order> orders)
        {
            orders.ForEach(x => x.LoadOrderItems(DB));
            return View("OpenOrders", orders);
        }
        #endregion

        #region Manage Hold
        public ActionResult ManageHold()
        {
            return View();
        }
        #endregion

        #region Surcharge Calculator
        public ActionResult SurchargeCalculator()
        {
            return View();
        }
        #endregion

        #region ZeroDollarDies
        public ActionResult ZeroDollarDiesForm(DateTime? startDate, DateTime? endDate)
        {
            if (startDate.IsNull() || endDate.IsNull())
                return View();

            return Redirect(Url.ZeroDollarDies(startDate.Value,endDate.Value));
        }

        public ActionResult ZeroDollarDies(DateTime startDate, DateTime endDate)
        {

            /*
             	strQuery = "SELECT DISTINCT customercode, d_order.ordernumber, customerpo, orderdate,invoicedate, description, d_orderitem.dienumber "&_
				"FROM d_order, d_orderitem WHERE d_order.ordernumber = d_orderitem.ordernumber AND customerpo not like '%cancel%' "&_
				"AND (sales = 0 or d_order.discountontotal ='1') and total='0' AND shipdate <> '' AND invoicedate <> '' "&_
				"AND exists (select * from d_orderitem where (description <> '' or (note not like '%cancel%' and note not like '%duplicat%')) "&_
				"and d_orderitem.ordernumber = d_order.ordernumber) AND invoicedate >= '"&fromdate&"' AND invoicedate < '"&todate&"' "&_
				"ORDER BY d_orderitem.dienumber"
             */
            var query = new SqlLam<d_order>()
                .Select(x => new { x.customercode, x.ordernumber, x.customerpo, x.orderdate, x.invoicedate })
                // .Where(x => x.invoicedate != null)
                // .Where(x=> x.)
                .Where(x => !x.canceled)
                .Where(x => x.total == 0 || (x.discountpercentage == 100 && x.discountontotal))
                .Where(x => x.invoicedate >= startDate && x.invoicedate <= endDate)
                .JoinOrderItemTable()
                .Select(x => new { x.description, x.dienumber })
                .Where(x => x.description != "")
                .OrderBy(x => x.Order.ordernumber)
                .OrderBy(x => x.line);

            var orders = DB.Connection.Query<d_order, d_orderitem, d_orderitem>(
                query,
                (o, oi) =>
                {
                    oi.Order = o;
                    return oi;
                },
                splitOn: "description"
            ).ToList();

            return View( orders);
        }

        #endregion
    }
}
