﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Dapper;
using DecadeWeb.Controllers.Pages;
using DecadeWeb.Models;
using LambdaSqlBuilder;
using Mset.Exco;
using Mset.Exco.Decade;
using Mset.Extensions;



namespace DecadeWeb.Controllers
{
    [DecadeAuthorize( DecadeRole = DecadeRole.ReportsShop )]
    public class PartPositionController : BasePageController
    {
        //
        // GET: /PartPosition/
        [ImportModelStateFromTempData]
        public ActionResult Index()
        {
            return View(
                "_Form",
                d_customer.GetCustomers(
                    DB,
                    true ) );
        }


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult HotList()
        {
            return List(
                new PartPositionModel(
                    PartPositionType.HotList,
                    "Hot List",
                    d_order.GetHotListOrders( DB ) ) );
        }


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult ByShopDateForm( string dietype, DateTime? date )
        {
            if (!String.IsNullOrEmpty( dietype )
                && date.HasValue)
            {
                return Redirect(
                    Url.PartPositionByShopDate(
                        dietype,
                        date.Value ) );
            }

            return RedirectToAction( "Index" );
        }


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult ByShopDate( string dietype, DateTime date )
        {
            var dieType = DieType.Hollow;
            if (dietype.EqualsIgnoreCase( "Solid" ))
                dieType = DieType.Solid;

            return List(
                new PartPositionModel(
                    PartPositionType.ByShopDate,
                    $"{dieType}'s with shop date {date.ToShortDateString()}",
                    d_order.GetOrdersByShopDateAndDieType(
                        DB,
                        dieType,
                        date ) ) );
        }


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        [ExportModelStateToTempData]
        public ActionResult ByCustomer( string[] customers )
        {
            if (customers == null
                || customers.Length <= 0)
                return RedirectToAction( "Index" );

            var orders = d_order.GetCustomersOrders(
                DB,
                customers ).ToList();

            if (!orders.Any())
            {
                ModelState.AddModelError(
                    "PART_POSITIONS_NONE_FOUND",
                    "No Orders Returned" );
                return RedirectToAction( "Index" );
            }

            var rtnOrders = orders
                .OrderBy( x => x.Customer.name )
                .ThenBy( x => x.shopdate )
                .ThenBy( x => x.ordernumber );

            return List(
                new PartPositionModel(
                    PartPositionType.ByCustomer,
                    $"Part Positions For Customer(s) {customers.JoinWith(",")}",
                    rtnOrders ) );
        }


        [AcceptVerbs( HttpVerbs.Post | HttpVerbs.Get )]
        public ActionResult FastTrack()
        {
            return List(
                new PartPositionModel(
                    PartPositionType.FastTrack,
                    "Fast Track Part Positions",
                    d_order.GetFasttrackOrders( DB ) ) );
        }


        [ExportModelStateToTempData]
        public ActionResult WatchList()
        {
            var orders = d_orderwatch.GetWatchedOrdersWithInfo(
                DB,
                Convert.ToInt32( User.Identity.Name ) ).ToList();

            if (!orders.Any())
            {
                ModelState.AddModelError(
                    "PART_POSITIONS_NONE_FOUND",
                    "Nothing found on watchlist." );
                return RedirectToAction( "Index" );
            }

            var rtnOrders = orders
                .OrderBy( x => x.Customer.name )
                .ThenBy( x => x.shopdate )
                .ThenBy( x => x.ordernumber );

            return List(
                new PartPositionModel(
                    PartPositionType.ByCustomer,
                    "Part Positions For Watchlist",
                    rtnOrders ) );
        }


        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult ByOrders( string ordernumbers )
        {
            var o = ordernumbers.Split( ',' )
                                .Cast<object>()
                                .ToList();

            var query = new SqlLam<d_order>()
                .WhereIsIn(x => x.ordernumber, o);


            var orders = DB.Connection.Query<d_order>( query );


            return List( new PartPositionModel( PartPositionType.Orders, "Part Positions for Orders", orders ) );
        }


        private ActionResult List( PartPositionModel model )
        {
            foreach (d_order order in model.Orders)
            {
                order.LoadOrderItems(
                    DB,
                    false );
                order.GetDieNumbers( DB );

                List<string> parts = order.GetParts( DB );

                foreach (string part in parts)
                {
                    d_task task = d_task.LoadTracksForOrder(
                        DB,
                        order.ordernumber,
                        part,
                        true ).SingleOrDefault();

                    if (task.IsNull())
                        continue;

                    if (order.TrackingInfo.IsNull())
                        order.TrackingInfo = new List<d_task>();

                    task.PartCode = part;

                    ( (List<d_task>) order.TrackingInfo ).Add( task );
                }
            }

            return View(
                "_List",
                model );
        }
    }
}