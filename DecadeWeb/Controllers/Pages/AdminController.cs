﻿using System.Web.Mvc;
using DecadeWeb.Models;
using Mset.Exco.Decade;



namespace DecadeWeb.Controllers.Pages
{
    [DecadeAuthorize( DecadeRole = DecadeRole.Admin )]
    public class AdminController : BasePageController
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            var model = new AdminConfigModel
            {
                DailyLimits = d_dailylimits.GetAllLimits( DB ),
                ExchangeRates = d_exchangerates.GetAllRates( DB ),
                Users = Mset.Exco.Decade.d_user.GetUsers( DB )
            };
            return View( model );
        }
    }
}