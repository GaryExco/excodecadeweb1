﻿using System;
using System.Web.Mvc;
using DecadeWeb.Code;
using DecadeWeb.Controllers.Pages;
using Mset.Extensions;



namespace DecadeWeb.Controllers
{
    public class ReportsController : BasePageController
    {
        //
        // GET: /Reports/

        public ActionResult Index()
        {
            return View();
        }


        [DecadeAuthorize( DecadeRole = DecadeRole.ReportsShop )]
        public ActionResult Shop()
        {
            return View();
        }


        [DecadeAuthorize( DecadeRole = DecadeRole.ReportsAdmin )]
        public ActionResult Admin()
        {
            return View();
        }


        [DecadeAuthorize( DecadeRole = DecadeRole.ReportsAdmin )]
        public ActionResult Order()
        {
            return View();
        }


        /*
        [DecadeAuthorize(DecadeRole = DecadeRole.ReportsAdmin), ImportModelStateFromTempData]
        public ActionResult MonthEndForm(DateTime? date, double? labor = null, double? overhead = null, double? smallSteelRate = null, double? bigSteelRate = null,
            double? exchangeRate = null)
        {
            return date.HasValue
                       ? MonthEnd(date.Value, labor, overhead, smallSteelRate, bigSteelRate, exchangeRate)
                       : View("_MonthEndForm");
        }
        */


        [DecadeAuthorize( DecadeRole = DecadeRole.ReportsAdmin )]
        [ExportModelStateToTempData]
        public ActionResult MonthEnd(
            DateTime? date,
            double? labor = null,
            double? overhead = null,
            double? smallSteelRate = null,
            double? bigSteelRate = null,
            double? exchangeRate = null )
        {
            if (!date.HasValue)
                return View( "_MonthEndForm" );

            Response.ContentType = "application/vnd.xls";

            // Push the frist day of the month to next month to encompass the whole month
            var reportDate = date.Value.AddMonths( 1 );

            var excelFile = new MonthEnd(
                reportDate,
                labor,
                overhead,
                smallSteelRate,
                bigSteelRate,
                exchangeRate )
                .Generate( DB );

            // excelFile = Code.MonthEndFunctions.GenerateReport(DB, date);

            if (excelFile == null)
            {
                ModelState.AddModelError(
                    "MONTH_END_ERROR",
                    "" );
                return RedirectToAction( "MonthEnd" );
            }

            return File(
                excelFile,
                "application/vnd.xls",
                $"MonthEnd_{Settings.FacilityName}_{date.Value.ToString("yyyy-MM")}.xlsx" );
        }
    }
}