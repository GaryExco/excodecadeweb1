﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DecadeWeb.Controllers.Pages;
using Mset.Exco.Decade;
using Mset.Extensions;
using DecadeWeb.Models;

namespace DecadeWeb.Controllers
{
    [DecadeAuthorize(DecadeRole = DecadeRole.ReportsShop)]
    public class TasksController : BasePageController
    {
        //
        // GET: /Tasks/

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Reports");
        }

        #region Department
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get), ImportModelStateFromTempData]
        public ActionResult DepartmentForm(DateTime? startDate, DateTime? endDate, string options, string[] departments)
        {
            if (startDate.HasValue && endDate.HasValue && departments.Length > 0)
                return Redirect(Url.DepartmentTasks(startDate.Value, endDate.Value, options, departments.ToList()));

            return View("_DepartmentForm", d_department.GetDepartments());
        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get), ExportModelStateToTempData]
        public ActionResult Department(DateTime startDate, DateTime endDate, string options, string departments)
        {
            if (departments.IsNullOrEmpty())
            {
                ModelState.AddModelError("DEPARTMENT_TASKS_NONE_SELECTED", "No department selected.");
                return DepartmentForm(null, null, null, null);
            }

            var departmentsIDs = new List<string>(departments.Split('/'));

            IEnumerable<d_task> tasks = d_task.LoadTracksFromTo(
                DB,
                startDate,
                endDate,
                null,
                d_taskcodemapping.GetTaskMappings(DB, departmentsIDs).Select(x => x.task).ToList()
            ).ToList();

            return View("_DisplayTasks", new DisplayTasksModel(
                "Department Tasks",
                "From {startdate} to {enddate}".FormatWithName(new { startDate, endDate }),
                tasks,
                options.EqualsIgnoreCase("List")));
        }
        #endregion

        #region Employee
        public ActionResult EmployeeForm(int? employeenumber, DateTime? startDate, DateTime? endDate)
        {
            if (startDate.HasValue && endDate.HasValue && employeenumber.HasValue)
                return Redirect(Url.EmployeeTasks(employeenumber.Value, startDate.Value, endDate.Value));

            return View("_EmployeeForm", Mset.Exco.Decade.d_user.GetUsers(DB));
        }

        public ActionResult Employee(int employeenumber, DateTime startDate, DateTime endDate)
        {
            IEnumerable<d_task> tasks = d_task.LoadTracksFromTo(DB, startDate, endDate, employeenumber).ToList();
            return View("_DisplayTasks", new DisplayTasksModel("Employee Tasks", "For {employeenumber} from {startdate} to {enddate}".FormatWithName(new { employeenumber, startDate, endDate }), tasks));
        }
        #endregion

        #region Station
        public ActionResult StationForm(string station, DateTime? startDate, DateTime? endDate)
        {
            if (!station.IsNullOrEmpty() && startDate.HasValue && endDate.HasValue)
                return Redirect(Url.StationTasks(station, startDate.Value, endDate.Value));

            return View("_StationForm", d_task.GetStationCodes(DB));
        }

        public ActionResult Station(string station, DateTime startDate, DateTime endDate)
        {
            IEnumerable<d_task> tasks = d_task.LoadTracksFromTo(DB, startDate, endDate, station).ToList();
            return View("_DisplayTasks", new DisplayTasksModel("Station Tasks", "For {station} from {startdate} to {enddate}".FormatWithName(new { station = Mset.Exco.StationAliases.GetStationAlias(station, true), startDate, endDate }), tasks));
        }
        #endregion

        #region CADCAM
        public ActionResult CADCAMForm(string department, DateTime? startDate, DateTime? endDate)
        {
            if (!String.IsNullOrEmpty(department) && startDate.HasValue && endDate.HasValue)
                return Redirect(Url.CADCAMTasks(department, startDate.Value, endDate.Value));

            return View("_CADCAMForm");
        }

        public ActionResult CADCAM(string department, DateTime startDate, DateTime endDate)
        {
            var model = new TasksCADCAMModel
            {
                Department = department
            };

            var tasks = new List<string>();

            switch (model.Department.ToUpper())
            {
                case "CAD":
                    tasks.Add("DF");
                    tasks.Add("DD");
                    tasks.Add("DS");
                    break;

                default:
                    tasks.Add("CM");
                    tasks.Add("CF");
                    break;
            }

            model.Tasks = d_task.LoadTracksFromTo(DB, startDate, endDate, null, tasks)
                .OrderBy(x => x.User.lastname)
                .ToList();
            // model.Tasks = model.Tasks.OrderBy(x => x.User.lastname);
            return View(model);
        }
        #endregion
    }
}