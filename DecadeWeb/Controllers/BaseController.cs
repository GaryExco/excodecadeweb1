﻿using System.Web.Mvc;
using System.Web.Routing;
using Mset.Database;
using Mset.Extensions;
using StackExchange.Profiling;
// using BootstrapSupport;



namespace DecadeWeb.Controllers
{
#if !DEBUG
    [HandleError]
    // [BundleMinifyInlineCssJsAttribute]
    // [OutputCache(Location = System.Web.UI.OutputCacheLocation.ServerAndClient, Duration = 30, VaryByParam = "*", VaryByCustom = "User")]
    #endif


    public class BaseController : Controller
    {
        protected Database DB;


        protected Database CustNotesDB;


        protected override void Initialize( RequestContext requestContext )
        {
            base.Initialize( requestContext );


            #region Profiling
            var user = User.Identity as d_user;
            if (user.IsNotNull()
                && user.debugmode)
            {
                if (!requestContext.HttpContext.Request.Browser.VBScript)
                {
                    MiniProfiler.Start();
                    MiniProfiler.Settings.PopupRenderPosition = RenderPosition.Right;
                }
            }
            #endregion


            DB = Helpers.GetDatabase();
            DB.OpenAndSet();

            CustNotesDB = Helpers.GetCustNotesDatabase();
            CustNotesDB.OpenAndSet();
        }


        protected override void Dispose( bool disposing )
        {
            if (disposing)
            {
                if (CustNotesDB != null)
                    CustNotesDB.Dispose();

                if (DB != null)
                    DB.Dispose();
            }

            base.Dispose( disposing );
        }


        protected Mset.Exco.Decade.d_user _DUser;


        protected virtual Mset.Exco.Decade.d_user DUser
        {
            get
            {
                if (_DUser.IsNull())
                {
                    _DUser = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(
                        DB,
                        HttpContext.User.Identity.Name );
                    ViewData["DUser"] = _DUser;
                }

                return _DUser;
            }
        }


        #region BootStrap Helpers
        /*
        public void Attention(string message)
        {
            TempData.Add(Alerts.ATTENTION, message);
        }

        public void Success(string message)
        {
            TempData.Add(Alerts.SUCCESS, message);
        }

        public void Information(string message)
        {
            TempData.Add(Alerts.INFORMATION, message);
        }

        public void Error(string message)
        {
            TempData.Add(Alerts.ERROR, message);
        }
        */
        #endregion
    }
}