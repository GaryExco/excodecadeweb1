﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using DecadeWeb.Providers;



namespace DecadeWeb.Controllers
{
    public class AccountController : BaseController
    {
        private readonly DecadeMemberProvider _provider = (DecadeMemberProvider) Membership.Provider;


        //
        // GET: /Account/

        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "Main" );
        }


        [AllowAnonymous]
        [OutputCache( Duration = Int32.MaxValue, VaryByParam = "*", VaryByCustom = "User" )]
        public ActionResult Login()
        {
            return View();
        }


        [AcceptVerbs( HttpVerbs.Post )]
        [AllowAnonymous]
        [OutputCache( Location = OutputCacheLocation.None )]
        public ActionResult Login( string username, string password, string returnUrl )
        {
            if (!ValidateLogin(
                username,
                password ))
                return View();

            // This should be set in the ValidateLogin() call above.
            // FormsAuthentication.SetAuthCookie(username, true, "/");

            if (!String.IsNullOrEmpty( returnUrl )
                && returnUrl != "/")
                return Redirect( returnUrl );

            Mset.Exco.Decade.d_user user = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(
                DB,
                username );
            if (!String.IsNullOrEmpty( user.startapp ))
                return Redirect( user.startapp );

            return RedirectToAction(
                "Index",
                "Main" );
        }


        [AllowAnonymous]
        [OutputCache( Location = OutputCacheLocation.None )]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();

            // clear authentication cookie
            var cookie1 = new HttpCookie(
                FormsAuthentication.FormsCookieName,
                "" )
            {
                Expires = DateTime.Now.AddYears( -1 )
            };
            Response.Cookies.Add( cookie1 );

            var cookie2 = new HttpCookie(
                "ASP.NET_SessionId",
                "" )
            {
                Expires = DateTime.Now.AddYears( -1 )
            };
            Response.Cookies.Add( cookie2 );

            // Remove the Cache on Client
            Response.Cache.SetCacheability( HttpCacheability.NoCache );
            Response.Cache.SetNoStore();

            FormsAuthentication.RedirectToLoginPage();
            return RedirectToAction( "Login" );
        }


        [DecadeAuthorize( DecadeRole = DecadeRole.Admin )]
        public ActionResult Create()
        {
            return View();
        }


        [DecadeAuthorize]
        [ExportModelStateToTempData]
        public ActionResult Edit( int? id )
        {
            // Make sure User is Authorised to Edit Other Users
            if (id.HasValue
                && !User.IsInRole( DecadeRole.Admin ))
            {
                ModelState.AddModelError(
                    "",
                    "" );
                return RedirectToAction(
                    "Index",
                    "Main" );
            }

            Mset.Exco.Decade.d_user user = Mset.Exco.Decade.d_user.GetUserByEmployeeNumber(
                DB,
                id.HasValue
                    ? id.Value.ToString()
                    : User.Identity.Name );

            return View( user );
        }


        [ExportModelStateToTempData]
        //, OutputCache(Location = System.Web.UI.OutputCacheLocation.None)
        public bool ValidateLogin( string username, string password )
        {
            if (String.IsNullOrEmpty( username ))
            {
                ModelState.AddModelError(
                    "username",
                    "You must specify an employee number." );
            }

            if (String.IsNullOrEmpty( password ))
            {
                ModelState.AddModelError(
                    "password",
                    "You must specify a password." );
            }

            if (!_provider.ValidateUser(
                username,
                password ))
            {
                ModelState.AddModelError(
                    "_FORM",
                    "Incorrect username and password combination." );
            }

            return ModelState.IsValid;
        }
    }
}