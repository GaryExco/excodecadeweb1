﻿using System.Web.Mvc;
using System.Web.Routing;



namespace DecadeWeb.App_Start
{
    public class IgnoreRoutesConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.IgnoreRoute( "{resource}.axd/{*pathInfo}" );

            routes.IgnoreRoute(
                "{*favicon}",
                new
                {
                    favicon = @"(.*/)?favicon\.ico(/.*)?"
                } );
        }
    }
}