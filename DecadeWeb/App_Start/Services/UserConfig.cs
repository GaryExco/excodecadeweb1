﻿using System.Web.Http;



namespace DecadeWeb
{
    public static class UserApiConfig
    {
        public static void Register( HttpConfiguration config )
        {
            config.Routes.MapHttpRoute(
                "GetUsersApi",
                "api/{controller}/{action}/{*catchall}",
                new
                {
                    catchall = RouteParameter.Optional
                }
                );
        }
    }
}