﻿using System.Web.Http;



namespace DecadeWeb.App_Start.Services
{
    public class NonConformanceConfig
    {
        public static void Register( HttpConfiguration config )
        {
            config.Routes.MapHttpRoute(
                "NonConformancePostNew",
                "api/{controller}/{action}/{*json}",
                new
                {
                    controller = "NonConformance",
                    action = "PostNewError"
                }
                );
        }
    }
}