﻿using System.Web.Http;



namespace DecadeWeb
{
    public class SessionConfig
    {
        public static void Register( HttpConfiguration config )
        {
            config.Routes.MapHttpRoute(
                "SessionKeepAlive",
                "api/{controller}/{action}",
                new
                {
                    controller = "Session",
                    action = "KeepAlive"
                }
                );
        }
    }
}