﻿using System.Web.Http;



namespace DecadeWeb
{
    public class SteelInventoryApiConfig
    {
        public static void Register( HttpConfiguration config )
        {
            config.Routes.MapHttpRoute(
                "SteelInventoryInsert",
                "api/{controller}/{action}/{*json}",
                new
                {
                    controller = "SteelInventory",
                    action = "InsertBars"
                }
                );

            config.Routes.MapHttpRoute(
                "SteelInventoryCheckBarLength",
                "api/{controller}/{action}/{id}/{newLength}",
                new
                {
                    controller = "SteelInventory",
                    action = "CheckBarLength"
                }
                );
        }
    }
}