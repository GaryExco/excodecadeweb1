﻿using System.Linq;
using System.Web.Http;



namespace DecadeWeb
{
    public static class WebApiConfig
    {
        public static void Register( HttpConfiguration config )
        {
            config.Routes.MapHttpRoute(
                "DefaultApiWithAction",
                "api/{controller}/{action}/{id}",
                new
                {
                    id = RouteParameter.Optional
                }
                );

            config.Routes.MapHttpRoute(
                "DefaultApiWithActionJson",
                "api/{controller}/{action}/{*json}",
                new
                {
                    json = RouteParameter.Optional
                }
                );

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new
                {
                    id = RouteParameter.Optional
                }
                );

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault( t => t.MediaType == "application/xml" );
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove( appXmlType );
        }
    }
}