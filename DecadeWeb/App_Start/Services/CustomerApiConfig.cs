﻿using System.Web.Http;



namespace DecadeWeb
{
    public static class CustomerApiConfig
    {
        public static void Register( HttpConfiguration config )
        {
            config.Routes.MapHttpRoute(
                "TempApi",
                "api/{controller}/{action}/{*catchall}",
                new
                {
                    catchall = RouteParameter.Optional
                }
                );
        }
    }
}