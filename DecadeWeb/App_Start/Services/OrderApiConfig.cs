﻿using System.Web.Http;



namespace DecadeWeb
{
    public class OrderApiConfig
    {
        public static void Register( HttpConfiguration config )
        {
            config.Routes.MapHttpRoute(
                "OrderGetOrderList",
                "api/{controller}/{action}/{orderstatus}/{duration}",
                new
                {
                    controller = "Order",
                    action = "GetOrderListing"
                }
                );
        }
    }
}