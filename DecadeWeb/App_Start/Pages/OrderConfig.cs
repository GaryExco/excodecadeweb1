﻿using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;



namespace DecadeWeb
{
    public class OrderConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            #region Detailed
            routes.MapRoute(
                "DisplayOrder",
                "Order/View/{ordernumber}",
                new
                {
                    controller = "Order",
                    action = "DisplayOrder"
                },
                new
                {
                    ordernumber = "[0-9]{5,6}"
                }
                );

            routes.MapRoute(
                "DisplayOrderByInvoice",
                "Order/View/Invoice/{invoicenumber}",
                new
                {
                    controller = "Order",
                    action = "DisplayInvoice"
                },
                new
                {
                    invoicenumber = "[0-9]{5,6}"
                }
                );

            routes.MapRoute(
                "EditOrder",
                "Order/Edit/{ordernumber}",
                new
                {
                    controller = "Order",
                    action = "Edit"
                },
                new
                {
                    ordernumber = "[0-9]{5,6}"
                }
                );

            // Change This Route.
            routes.MapRoute(
                "PreEditOrder",
                "Order/Entry",
                new
                {
                    controller = "Order",
                    action = ""
                }
                );

            routes.MapRoute(
                "NewOrder",
                "Order/New",
                new
                {
                    controller = "Order",
                    action = "New"
                }
                );
            #endregion


            routes.MapRoute(
                "YDrive",
                "ydrive/{*allinfo}",
                new
                {
                    controller = "Order",
                    action = "YDrive",
                    allinfo = UrlParameter.Optional
                }
                );

            routes.MapRoute(
                "RDrive",
                "rdrive/{*allinfo}",
                new
                {
                    controller = "FileSystem",
                    action = "RDrive",
                    allinfo = UrlParameter.Optional
                }
                );
        }
    }


    public static class OrderExtensions
    {
        public static string DisplayOrder( this UrlHelper url, int ordernumber )
        {
            return $"/Order/View/{ordernumber}";
        }


        public static string DisplayOrderByInvoice( this UrlHelper url, int invoicenumber )
        {
            return $"/Order/View/Invoice/{invoicenumber}";
        }


        public static string EditOrder( this UrlHelper url, int ordernumber )
        {
            return $"/OrderEntry/Edit/{ordernumber}";
        }
    }
}