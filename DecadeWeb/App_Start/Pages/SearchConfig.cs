﻿using System.Web.Mvc;
using System.Web.Routing;



namespace DecadeWeb
{
    public class SearchConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.MapRoute(
                "SearchQuery",
                "Search/{*query}",
                new
                {
                    controller = "Search",
                    action = "Query"
                }
                );
        }
    }
}