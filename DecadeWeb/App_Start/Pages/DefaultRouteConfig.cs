﻿using System.Web.Mvc;
using System.Web.Routing;



namespace DecadeWeb
{
    public class DefaultRouteConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.IgnoreRoute( "{resource}.axd/{*pathInfo}" );

            routes.IgnoreRoute(
                "{*favicon}",
                new
                {
                    favicon = @"(.*/)?favicon\.ico(/.*)?"
                } );
            routes.IgnoreRoute(
                "{folder}/{*pathInfo}",
                new
                {
                    folder = "Content"
                } );
            routes.IgnoreRoute(
                "{folder}/{*pathInfo}",
                new
                {
                    folder = "Scripts"
                } );
            routes.IgnoreRoute(
                "{folder}/{*pathInfo}",
                new
                {
                    folder = "fonts"
                } );
            // routes.IgnoreRoute("{folder}/{*pathInfo}", new { folder = "ydrive" });

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new
                {
                    controller = "Main",
                    action = "Index",
                    id = UrlParameter.Optional
                }
                );

            /*
            routes.MapRoute(
                name: "DefaultArgs",
                url: "{controller}/{action}/{*allsegs}",
                defaults: new { controller = "Main", action = "Index", allsegs = UrlParameter.Optional }
            );
            */
        }
    }
}