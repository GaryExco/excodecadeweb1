﻿using System.Web.Mvc;
using System.Web.Routing;



namespace DecadeWeb
{
    public class SalesConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.MapRoute(
                "SalesSummary",
                "Sales/Summary",
                new
                {
                    controller = "Sales",
                    action = "Summary"
                }
                );

            /*
            routes.MapRoute(
                name: "SalesSummaryForm",
                url: "Sales/Summary",
                defaults: new { controller = "Sales", action = "SummaryForm" }
            );
            */
        }
    }


    public static class SalesExtensions
    {
        /*
        public static string SalesSummary(this UrlHelper url, DateTime date)
        {
            return "/Sales/Summary/{date}".FormatWithName(new { date = date.ToURLDate() });
        }
        */
    }
}