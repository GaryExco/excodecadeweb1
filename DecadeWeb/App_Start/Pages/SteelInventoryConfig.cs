﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;



namespace DecadeWeb
{
    public class SteelInventoryConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            #region Display
            routes.MapRoute(
                "SteelInventoryDisplayFilterDiameter",
                "SteelInventory/Display/{filter}/{diameter}",
                new
                {
                    controller = "SteelInventory",
                    action = "Display"
                },
                new
                {
                    filter = "open|closed|all",
                    diameter = @"[0-9]+(\.[0-9]+)?"
                }
                );

            routes.MapRoute(
                "SteelInventoryDisplayFilter",
                "SteelInventory/Display/{filter}",
                new
                {
                    controller = "SteelInventory",
                    action = "Display"
                },
                new
                {
                    filter = "open|closed|all"
                }
                );

            routes.MapRoute(
                "SteelInventoryDisplayByDiameter",
                "SteelInventory/Display/{diameter}",
                new
                {
                    controller = "SteelInventory",
                    action = "Display"
                },
                new
                {
                    diameter = @"[0-9]+(\.[0-9]+)?"
                }
                );

            routes.MapRoute(
                "SteelInventoryDisplay",
                "SteelInventory/Display/",
                new
                {
                    controller = "SteelInventory",
                    action = "Display"
                }
                );
            #endregion


            routes.MapRoute(
                "SteelInventoryAddPost",
                "SteelInventory/Add/{*json}",
                new
                {
                    controller = "SteelInventory",
                    action = "Add"
                }
                );

            routes.MapRoute(
                "SteelInventoryAdd",
                "SteelInventory/Add",
                new
                {
                    controller = "SteelInventory",
                    action = "Add"
                }
                );

            routes.MapRoute(
                "SteelInventoryModify",
                "SteelInventory/Modify/{barid}",
                new
                {
                    controller = "SteelInventory",
                    action = "Modify",
                    barid = UrlParameter.Optional
                },
                new
                {
                    barid = "[H2DQO]{1-2}-[0-9]{1-2}-[0-9]{3-4}"
                }
                );

            routes.MapRoute(
                "SteelInventoryViewBar",
                "SteelInventory/ViewBar/{barid}",
                new
                {
                    controller = "SteelInventory",
                    action = "ViewBar"
                }
                );

            routes.MapRoute(
                "SteelInventoryViewBarForm",
                "SteelInventory/ViewBar",
                new
                {
                    controller = "SteelInventory",
                    action = "ViewBarForm"
                }
                );
        }
    }


    public static class SteelInventoryExtensions
    {
        public static string SteelInventory( this UrlHelper url, string filter = "open", double? diameter = null )
        {
            var strDia = diameter.HasValue
                             ? "/" + diameter
                             : "";

            return $"/SteelInventory/Display/{filter}{strDia}";
        }


        public static string SteelInventoryAdd( this UrlHelper url )
        {
            return "/SteelInventory/Add";
        }


        public static string SteelInventoryModify( this UrlHelper url, string barid = null )
        {
            var strBarID = barid.IsNotNullOrEmpty()
                               ? "/" + barid
                               : "";

            return $"/SteelInventory/Modify{strBarID}";
        }


        public static string SteelInventoryViewBar( this UrlHelper url, string barid )
        {
            return $"/SteelInventory/ViewBar/{barid}";
        }
    }
}