﻿using System.Web.Mvc;
using System.Web.Routing;



namespace DecadeWeb
{
    public class WebNotesConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.MapRoute(
                "WebNotesMainWithSort",
                "WebNotes/{sortMode}",
                new
                {
                    controller = "WebNotes",
                    action = "Index"
                },
                new
                {
                    sortMode = "ByName|ByNumber"
                }
                );
        }
    }


    public static class WebNotesExtensions
    {
        /*
        public static string CustomerByCode(this UrlHelper url, string customercode)
        {
            return "/Customer/ByCode/{0}".FormatWith(customercode);
        }

        public static string CustomerByName(this UrlHelper url, string name)
        {
            return "/Customer/ByName/{0}".FormatWith(name);
        }
        */
    }
}