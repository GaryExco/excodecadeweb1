﻿using System.Web.Mvc;
using System.Web.Routing;



namespace DecadeWeb
{
    public class LegacyConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            /*
            routes.MapRoute(
                name: "LegacyWith",
                url: "{*path}",
                defaults: new { controller = "Legacy", action = "Legacy" },
                constraints: new { path = @".*\.asp.*" }
            );
            */

            // ProcLogin for quick access for Design
            routes.MapRoute(
                "LegacyProcLogin",
                "proclogin.asp",
                new
                {
                    controller = "Legacy",
                    action = "ProcLogin"
                }
                );

            // Hard link the old tracking link to the new one
            routes.MapRoute(
                "LegacyTracking",
                "tracking/track.asp",
                new
                {
                    controller = "Legacy",
                    action = "Tracking"
                }
                );

            // Dynamic route for all *.asp/*.aspx routes to go to the users startapp
            routes.MapRoute(
                "Legacy",
                "{*path}",
                new
                {
                    controller = "Legacy",
                    action = "Legacy"
                },
                new
                {
                    path = @".*\.asp.*"
                }
                );
        }
    }
}