﻿using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;



namespace DecadeWeb
{
    public class AdminConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.MapRoute(
                "AdminManageDailyLimits",
                "Admin/Manage/DailyLimits",
                new
                {
                    controller = "Admin",
                    action = "ManageDailyLimits"
                },
                new
                {
                    customercode = Helpers.CustomerCodeRegexPattern
                }
                );

            routes.MapRoute(
                "AdminManageExchangeRates",
                "Admin/Manage/ExchangeRates",
                new
                {
                    controller = "Admin",
                    action = "ManageExchangeRates"
                }
                );
        }
    }


    public static class AdminExtensions
    {
        public static string CustomerByCode( this UrlHelper url, string customercode )
        {
            return $"/Customer/ByCode/{customercode}";
        }


        public static string CustomerByName( this UrlHelper url, string name )
        {
            return $"/Customer/ByName/{name}";
        }
    }
}