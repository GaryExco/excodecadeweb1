﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Mset.Exco.Decade;
using Mset.Extensions;

namespace DecadeWeb
{
    public class ScheduleConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "AdminSchedule",
                url: "Schedule/Admin/{dietype}/{dispmode}/{date}/{checkht}",
                defaults: new { controller = "Schedule", action = "Admin", date = UrlParameter.Optional, checkht = UrlParameter.Optional },
                constraints: new { dietype = "solid|hollow", dispmode = "ordernumber|dienumber|orders|dies|order|die" }
            );

            routes.MapRoute(
                name: "AdminScheduleForm",
                url: "Schedule/Admin",
                defaults: new { controller = "Schedule", action = "AdminForm" }
            );

            routes.MapRoute(
                name: "HTSchedule",
                url: "Schedule/HT/{startdate}/{enddate}",
                defaults: new { controller = "Schedule", action = "HT" }
            );

            routes.MapRoute(
                name: "HTScheduleForm",
                url: "Schedule/HT",
                defaults: new { controller = "Schedule", action = "HTForm" }
            );


            routes.MapRoute(
                name: "DepartmentSchedule",
                url: "Schedule/Department/{department}/{dietypes}/{parttypes}/{side}",
                defaults: new
                {
                    controller = "Schedule",
                    action = "Department"
                },
                constraints: new
                {
                    side = "front|back|all"
                });


            routes.MapRoute(
                name: "DepartmentScheduleForm",
                url: "Schedule/Department",
                defaults: new
                {
                    controller = "Schedule",
                    action = "DepartmentForm"
                } );


            routes.MapRoute(
                name: "OrderScheduleWithDays",
                url: "Schedule/Order/{days}",
                defaults: new
                {
                    controller = "Schedule",
                    action = "Order"
                },
                constraints: new
                {
                    days = "\\d{1,2}"
                } );

            routes.MapRoute(
                name: "OrderSchedule",
                url: "Schedule/Order",
                defaults: new
                {
                    controller = "Schedule",
                    action = "Order"
                });
        }
    }

    public static class ScheduleExtensions
    {
        public static string AdminSchedule(this UrlHelper url, Mset.Exco.DieType dieType, Models.AdminSchedShowMode mode, DateTime date, bool checkHTDone = false)
        {
            return AdminSchedule(url, dieType, mode, new DateTime?(date), checkHTDone);
        }

        public static string AdminSchedule(this UrlHelper url, Mset.Exco.DieType dieType, Models.AdminSchedShowMode mode, DateTime? date = null, bool checkHTDone = false)
        {
            if (!date.HasValue)
                date = DateTime.Now;

            var tmpMode = mode == Models.AdminSchedShowMode.DieNumber
                              ? "Die"
                              : "Order";

            var htFlag = checkHTDone
                             ? "/HT"
                             : "";

            return $"/Schedule/Admin/{dieType}/{tmpMode}/{date.Value.ToURLDate()}{htFlag}";
        }

        public static string HTSchedule(this UrlHelper url, DateTime startdate, DateTime enddate)
        {
            return $"/Schedule/HT/{startdate.ToURLDate()}/{enddate.ToURLDate()}";
        }


        public static string DepartmentSchedule( this UrlHelper url, string department, string[] dietypes, string[] parttypes, string side )
        {
            return $"/Schedule/Department/{department}/{dietypes.JoinWith( "," )}/{parttypes.JoinWith( "," )}/{side}";
        }
    }
}