﻿using System.Web.Mvc;
using System.Web.Routing;



namespace DecadeWeb
{
    public class ReportsConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.MapRoute(
                "ShopReports",
                "Reports/Shop",
                new
                {
                    controller = "Reports",
                    action = "Shop"
                }
                );

            routes.MapRoute(
                "AdminReports",
                "Reports/Admin",
                new
                {
                    controller = "Reports",
                    action = "Admin"
                }
                );

            routes.MapRoute(
                "OrderReports",
                "Reports/Order",
                new
                {
                    controller = "Reports",
                    action = "Order"
                }
                );

            /*
            routes.MapRoute(
                name: "MonthEnd",
                url: "Reports/MonthEnd/{date}",
                defaults: new { controller = "Reports", action = "MonthEnd" }
            );

            routes.MapRoute(
                name: "MonthEndForm",
                url: "Reports/MonthEnd",
                defaults: new { controller = "Reports", action = "MonthEndForm" }
            );
            */
        }
    }
}