﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;



namespace DecadeWeb
{
    public class PartPositionConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.MapRoute(
                "PartPositionsByShopDateDieType",
                "PartPosition/ByShopDate/{dietype}/{date}",
                new
                {
                    controller = "PartPosition",
                    action = "ByShopDate"
                },
                new
                {
                    dietype = "solid|hollow"
                }
                );


            routes.MapRoute(
                "PartPositionsByOrders",
                "PartPosition/ByOrders",
                new
                {
                    controller = "PartPosition",
                    action = "ByOrders"
                } );
        }
    }


    public static class PartPositionExtensions
    {
        public static string PartPositionByShopDate( this UrlHelper url, string dietype, DateTime shopdate )
        {
            return $"/PartPosition/ByShopDate/{dietype}/{shopdate.ToURLDate()}";
        }


        public static string PartPositionByOrders( this UrlHelper url, IEnumerable<int> ordernumbers )
        {
            return $"/PartPosition/ByOrders?ordernumbers={ordernumbers.JoinWith( "," )}";
        }
    }
}