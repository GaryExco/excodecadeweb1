﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;

namespace DecadeWeb
{
    public class OrdersConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "DailyTotals",
                url: "Orders/DailyTotals/{mode}/{exchangerate}/{startdate}",
                defaults: new { controller = "Orders", action = "DailyTotals" },
                constraints: new { mode = "invoice|shipped|order" }
            );

            routes.MapRoute(
                name: "DailyTotalsWithEndDate",
                url: "Orders/DailyTotals/{mode}/{exchangerate}/{startdate}/{enddate}",
                defaults: new { controller = "Orders", action = "DailyTotals" },
                constraints: new { mode = "invoice|shipped|order" }
            );

            routes.MapRoute(
                name: "DailyTotalsForm",
                url: "Orders/DailyTotals",
                defaults: new { controller = "Orders", action = "DailyTotalsForm" }
            );

            routes.MapRoute(
                name: "InvoiceAudit",
                url: "Orders/InvoiceAudit/{date}",
                defaults: new { controller = "Orders", action = "InvoiceAudit" }
            );

            routes.MapRoute(
                name: "InvoiceAuditForm",
                url: "Orders/InvoiceAudit",
                defaults: new { controller = "Orders", action = "InvoiceAuditForm" }
            );

            routes.MapRoute(
                name: "OrderActionByDateRangeByCustomer",
                url: "Orders/Action/{datefield}/{startdate}/{enddate}/{customercode}",
                defaults: new { controller = "Orders", action = "OrderActionByDateRange" },
                constraints: new
                {
                    datefield = "order|invoice|shipped",
                    startdate = "[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}",
                    enddate = "[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}"
                }
            );

            routes.MapRoute(
                name: "OrderActionByDateRange",
                url: "Orders/Action/{datefield}/{startdate}/{enddate}",
                defaults: new { controller = "Orders", action = "OrderActionByDateRange" },
                constraints: new
                {
                    datefield = "order|invoice|shipped",
                    startdate = "[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}",
                    enddate = "[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}"
                }
            );

            routes.MapRoute(
                name: "OrderActionByCustomer",
                url: "Orders/Action/{datefield}/{minorder}/{maxorder}/{customercode}",
                defaults: new { controller = "Orders", action = "OrderActionByRange" },
                constraints: new { datefield = "order|invoice|shipped" }
            );

            routes.MapRoute(
                name: "OrderAction",
                url: "Orders/Action/{datefield}/{minorder}/{maxorder}",
                defaults: new { controller = "Orders", action = "OrderActionByRange" },
                constraints: new { datefield = "order|invoice|shipped" }
            );

            routes.MapRoute(
                name: "OrderActionForm",
                url: "Orders/Action",
                defaults: new { controller = "Orders", action = "OrderActionForm" }
            );

            routes.MapRoute(
                name: "OpenOrdersByDateRangeByCustomer",
                url: "Orders/Open/ByDateRange/{startdate}/{enddate}/{customercode}",
                defaults: new { controller = "Orders", action = "OpenOrdersByDateRange" }
            );

            routes.MapRoute(
                name: "OpenOrdersByDateRange",
                url: "Orders/Open/ByDateRange/{startdate}/{enddate}",
                defaults: new { controller = "Orders", action = "OpenOrdersByDateRange" }
            );

            routes.MapRoute(
                name: "OpenOrdersByRangeByCustomer",
                url: "Orders/Open/ByOrderRange/{minorder}/{maxorder}/{customercode}",
                defaults: new { controller = "Orders", action = "OpenOrdersByRange" }
            );

            routes.MapRoute(
                name: "OpenOrdersByRange",
                url: "Orders/Open/ByOrderRange/{minorder}/{maxorder}",
                defaults: new { controller = "Orders", action = "OpenOrdersByRange" }
            );

            routes.MapRoute(
                name: "OpenOrdersByCustomer",
                url: "Orders/Open/ByCustomer/{customercode}",
                defaults: new { controller = "Orders", action = "OpenOrdersByCustomer" },
                constraints: new { customercode = Helpers.CustomerCodeRegexPattern }
            );

            routes.MapRoute(
                name: "OpenOrdersForm",
                url: "Orders/Open",
                defaults: new { controller = "Orders", action = "OpenOrdersForm" }
            );

            routes.MapRoute(
                name : "ZeroDollarDiesForm",
                url: "Orders/ZeroDollarDies",
                defaults: new { controller = "Orders", action = "ZeroDollarDiesForm" }
            );

            routes.MapRoute(
                name : "ZeroDollarDies",
                url : "Orders/ZeroDollarDies/{startDate}/{endDate}",
                defaults : new { controller = "Orders", action = "ZeroDollarDies" }

                );
        }
    }

    public static class OrdersExtensions
    {
        public static string DailyTotals(this UrlHelper url, DateTime date, string mode, decimal exchangeRate, DateTime? endDate = null)
        {
            return "/Orders/DailyTotals/{mode}/{exchange}/{date}{enddate}".FormatWithName(
                new
                {
                    mode = mode,
                    exchange = exchangeRate,
                    date = date.ToURLDate(),
                    enddate = endDate.HasValue ? "/" + endDate.Value.ToURLDate() : ""
                } );
        }

        public static string InvoiceAudit(this UrlHelper url, DateTime date)
        {
            return "/Orders/InvoiceAudit/{date}".FormatWithName(new { date = date.ToURLDate() });
        }

        public static string OrderAction(this UrlHelper url, int minorder, int maxorder, string customercode = null, string datefield = null)
        {
            if (datefield.IsNullOrEmpty())
                datefield = "order";

            return "/Orders/Action/{datefield}/{minorder}/{maxorder}{customercode}".FormatWithName(new
            {
                datefield,
                minorder,
                maxorder,
                customercode = customercode.IsNotNullOrEmpty() ? "/" + customercode : ""
            });
        }

        public static string OrderAction(this UrlHelper url, DateTime startdate, DateTime enddate, string customercode = null, string datefield = null)
        {
            if (datefield.IsNullOrEmpty())
                datefield = "order";

            return "/Orders/Action/{datefield}/{startdate}/{enddate}{customercode}".FormatWithName(new
            {
                datefield,
                startdate = startdate.ToURLDate(),
                enddate = enddate.ToURLDate(),
                customercode = customercode.IsNotNullOrEmpty() ? "/" + customercode : ""
            });
        }

        public static string OpenOrdersByRange(this UrlHelper url, int minorder, int maxorder, string customercode = null)
        {
            return "/Orders/Open/ByOrderRange/{minorder}/{maxorder}{customercode}".FormatWithName(new
            {
                minorder,
                maxorder,
                customercode = customercode.IsNotNullOrEmpty() ? "/" + customercode : ""
            });
        }

        public static string OpenOrdersByCustomer(this UrlHelper url, string customercode)
        {
            return "/Orders/Open/ByCustomer/{customercode}".FormatWithName(new { customercode });
        }

        public static string OpenOrdersByDateRange(this UrlHelper url, DateTime startDate, DateTime endDate, string customercode = null)
        {
            return "/Orders/Open/ByDateRange/{startdate}/{enddate}{customercode}".FormatWithName(new
            {
                startdate = startDate.ToURLDate(),
                enddate = endDate.ToURLDate(),
                customercode = customercode.IsNotNullOrEmpty() ? "/" + customercode : ""
            });
        }

        public static string ZeroDollarDies(this UrlHelper url, DateTime startDate, DateTime endDate)
        {
            return $"/Orders/ZeroDollarDies/{startDate.ToURLDate()}/{endDate.ToURLDate()}";
        }
    }
}