﻿using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;



namespace DecadeWeb
{
    public class CustomerConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.MapRoute(
                "CustomerByCode",
                "Customer/ByCode/{customercode}",
                new
                {
                    controller = "Customer",
                    action = "ByCode"
                },
                new
                {
                    customercode = Helpers.CustomerCodeRegexPattern
                }
                );

            routes.MapRoute(
                "CustomerByName",
                "Customer/ByName/{name}",
                new
                {
                    controller = "Customer",
                    action = "ByName"
                }
                );
        }
    }


    public static class CustomerExtensions
    {
        public static string CustomerByCode( this UrlHelper url, string customercode )
        {
            return $"/Customer/ByCode/{customercode}";
        }


        public static string CustomerByName( this UrlHelper url, string name )
        {
            return $"/Customer/ByName/{name}";
        }
    }
}