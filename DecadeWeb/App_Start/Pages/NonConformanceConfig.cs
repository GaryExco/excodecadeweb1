﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;



namespace DecadeWeb
{
    public class NonConformanceConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            const string validDepartments = "shop|cam|cad|all";

            routes.MapRoute(
                "NonConformanceListDepartmentEmployee",
                "NonConformance/List/{startdate}/{enddate}/{department}/{employeenumber}",
                new
                {
                    controller = "NonConformance",
                    action = "List"
                },
                new
                {
                    employeenumber = "[0-9]{5}",
                    department = validDepartments
                }
                );

            routes.MapRoute(
                "NonConformanceListEmployee",
                "NonConformance/List/{startdate}/{enddate}/{employeenumber}",
                new
                {
                    controller = "NonConformance",
                    action = "List"
                },
                new
                {
                    employeenumber = "[0-9]{5}"
                }
                );

            routes.MapRoute(
                "NonConformanceListDepartment",
                "NonConformance/List/{startdate}/{enddate}/{department}",
                new
                {
                    controller = "NonConformance",
                    action = "List"
                },
                new
                {
                    department = validDepartments
                }
                );

            routes.MapRoute(
                "NonConformanceListRange",
                "NonConformance/List/{startdate}/{enddate}",
                new
                {
                    controller = "NonConformance",
                    action = "List"
                }
                );

            routes.MapRoute(
                "NonConformanceListForm",
                "NonConformance/List",
                new
                {
                    controller = "NonConformance",
                    action = "ListForm"
                }
                );

            routes.MapRoute(
                "NonConformanceAdd",
                "NonConformance/Add",
                new
                {
                    controller = "NonConformance",
                    action = "Add"
                }
                );

            routes.MapRoute(
                "NonConformancePrint",
                "NonConformance/Print/{id}",
                new
                {
                    controller = "NonConformance",
                    action = "Print"
                }
                );


        }
    }


    public static class NonConformanceExtensions
    {
        public static string ListNonConformance( this UrlHelper url, DateTime startDate, DateTime endDate, string department = null, int? employeenumber = null )
        {
            var strDep = !department.IsNullOrEmpty()
                                 ? $"/{department}"
                                 : String.Empty;
            var strEmp = employeenumber.HasValue
                             ? $"/{employeenumber.Value}"
                             : String.Empty;


            return $"/NonConformance/List/{startDate.ToURLDate()}/{endDate.ToURLDate()}{strDep}{strEmp}";
        }
    }
}