﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;



namespace DecadeWeb
{
    public class OnTimeConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            #region Detailed
            routes.MapRoute(
                "DetailedOnTimeCustomer",
                "OnTime/Detailed/{mode}/{date}/{customer}",
                new
                {
                    controller = "OnTime",
                    action = "Detailed"
                },
                new
                {
                    mode = "month|day",
                    customer = Helpers.CustomerCodeRegexPattern
                }
                );

            routes.MapRoute(
                "DetailedOnTime",
                "OnTime/Detailed/{mode}/{date}",
                new
                {
                    controller = "OnTime",
                    action = "Detailed"
                },
                new
                {
                    mode = "month|day"
                }
                );

            routes.MapRoute(
                "DetailedOnTimeForm",
                "OnTime/Detailed",
                new
                {
                    controller = "OnTime",
                    action = "DetailedForm"
                }
                );
            #endregion


            #region Performance
            routes.MapRoute(
                "PerformanceOnTime",
                "OnTime/Performance/{daymode}/{orderfilter}/{date}",
                new
                {
                    controller = "OnTime",
                    action = "Performance"
                },
                new
                {
                    daymode = "month|day",
                    orderfilter = "all|normal|fasttrack|hotlist"
                }
                );

            routes.MapRoute(
                "PerformanceOnTimeForm",
                "OnTime/Performance",
                new
                {
                    controller = "OnTime",
                    action = "PerformanceForm"
                }
                );
            #endregion


            #region Month OnTime By Cust
            routes.MapRoute(
                "MonthOnTimeByCust",
                "OnTime/MonthByCust/{date}",
                new
                {
                    controller = "OnTime",
                    action = "MonthByCust"
                }
                );

            routes.MapRoute(
                "MonthOnTimeByCustForm",
                "OnTime/MonthByCust",
                new
                {
                    controller = "OnTime",
                    action = "MonthByCustForm"
                }
                );
            #endregion


            #region YTD
            routes.MapRoute(
                "OnTimeYTD",
                "OnTime/YTD",
                new
                {
                    controller = "OnTime",
                    action = "YTD"
                }
                );

            /*
            routes.MapRoute(
                name: "OnTimeYTDForm",
                url: "OnTime/YTD",
                defaults: new { controller = "OnTime", action = "YTDForm" }
            );
            */
            #endregion
        }
    }


    public static class OnTimeExtensions
    {
        public static string DetailedOnTime( this UrlHelper url, string dayMode, DateTime date, string customercode = null )
        {
            if (!String.IsNullOrEmpty( customercode ))
                customercode = "/" + customercode;
            else
                customercode = String.Empty;

            return $"/OnTime/Detailed/{dayMode}/{date.ToURLDate()}{customercode}";
        }


        public static string PerformanceOnTime( this UrlHelper url, string dayMode, string orderFilter, DateTime date )
        {
            return $"/OnTime/Performance/{dayMode}/{orderFilter}/{date.ToURLDate()}";
        }


        public static string MonthOnTimeByCust( this UrlHelper url, DateTime date )
        {
            return $"/OnTime/MonthByCust/{date.ToURLDate()}";
        }
    }
}