﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;

namespace DecadeWeb
{
    public class TrackingConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute(
                name: "TrackByDieAndCust",
                url: "Tracking/TrackBy/Die/{dienumber}/{customercode}",
                defaults: new { controller = "Tracking", action = "TrackByDie" },
                constraints: new { customercode = Helpers.CustomerCodeRegexPattern }
            );

            routes.MapRoute(
                name: "TrackByDie",
                url: "Tracking/TrackBy/Die/{dienumber}",
                defaults: new { controller = "Tracking", action = "TrackByDie" }
            );

            routes.MapRoute(
                name: "TrackByCust",
                url: "Tracking/TrackBy/Cust/{customercode}/{orderstatus}/{timefilter}",
                defaults: new { controller = "Tracking", action = "TrackByCust" },
                constraints: new { customercode = Helpers.CustomerCodeRegexPattern, orderstatus = "all|open", timefilter = "all|[0-9]{1,2}" }
            );

            routes.MapRoute(
                name: "TrackByCustPre",
                url: "Tracking/TrackBy/Cust",
                defaults: new { controller = "Tracking", action = "TrackByCust" }
            );

            routes.MapRoute(
                name: "TrackByLoc",
                url: "Tracking/TrackBy/Loc/{location}",
                defaults: new { controller = "Tracking", action = "TrackByLoc" }
            );

            #region Track Order
            routes.MapRouteWithOptionalFormat(
                name: "TrackByOrder",
                url: "Tracking/TrackBy/Order/{ordernumber}",
                defaults: new { controller = "Tracking", action = "TrackByOrder" },
                constraints: new { ordernumber = "[0-9]{5-6}" }
            );

            routes.MapRouteWithOptionalFormat(
                name: "TrackOrder1",
                url: "Tracking/Order/{ordernumber}",
                defaults: new { controller = "Tracking", action = "TrackByOrder" },
                constraints: new { ordernumber = "[0-9]{5,6}" }
            );

            routes.MapRouteWithOptionalFormat(
                name: "TrackOrder2",
                url: "Tracking/{ordernumber}",
                defaults: new { controller = "Tracking", action = "TrackByOrder" },
                constraints: new { ordernumber = "[0-9]{5,6}" }
            );

            routes.MapRouteWithOptionalFormat(
                name: "TrackOrder3",
                url: "Tracking/Track/{ordernumber}",
                defaults: new { controller = "Tracking", action = "TrackByOrder" },
                constraints: new { ordernumber = "[0-9]{5,6}" }
            );
            #endregion

            routes.MapRoute(
                name: "TrackByInvoice",
                url: "Tracking/Invoice/{invoicenumber}",
                defaults: new { controller = "Tracking", action = "TrackByInvoice" },
                constraints: new { invoicenumber = "[0-9]{5,6}" }
            );
        }
    }

    public static class TrackingExtensions
    {
        public static string TrackByDie(this UrlHelper url, string dienumber, string customercode = null)
        {
            return "/Tracking/TrackBy/Die/{dienumber}{customercode}".FormatWithName(new {
                dienumber = dienumber,
                customercode = !customercode.IsNullOrEmpty() ? "/" + customercode : String.Empty
            });
        }

        public static string TrackByCust(this UrlHelper url, string customercode, string timefilter, string orderstatus)
        {
            return $"/Tracking/TrackBy/Cust/{customercode}/{orderstatus}/{timefilter}";
        }

        public static string TrackByLoc(this UrlHelper url, string location)
        {
            return $"/Tracking/TrackBy/Loc/{location}";
        }

        public static string TrackByOrder(this UrlHelper url, object ordernumber, string part = null)
        {
            if (part.IsNotNullOrEmpty())
                part = $"#{part}";

            return $"/Tracking/Order/{ordernumber}{part}";
        }

        public static string TrackByInvoice(this UrlHelper url, object invoicenumber)
        {
            return $"/Tracking/Invoice/{invoicenumber}";
        }
    }
}