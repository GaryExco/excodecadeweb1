﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Mset.Extensions;



namespace DecadeWeb
{
    public class TasksConfig
    {
        public static void RegisterRoutes( RouteCollection routes )
        {
            routes.MapRoute(
                "DepartmentTasksWithOptions",
                "Tasks/Department/{startdate}/{enddate}/{options}/{*departments}",
                new
                {
                    controller = "Tasks",
                    action = "Department"
                },
                new
                {
                    options = "List|NoList"
                }
                );

            /*
            routes.MapRoute(
                name: "DepartmentTasks",
                url: "Tasks/Department/{startdate}/{enddate}/{*departments}",
                defaults: new { controller = "Tasks", action = "Department" }
            );
            */

            routes.MapRoute(
                "DepartmentTasksForm",
                "Tasks/Department",
                new
                {
                    controller = "Tasks",
                    action = "DepartmentForm"
                }
                );

            routes.MapRoute(
                "EmployeeTasks",
                "Tasks/Employee/{employeenumber}/{startdate}/{enddate}",
                new
                {
                    controller = "Tasks",
                    action = "Employee",
                    enddate = UrlParameter.Optional
                },
                new
                {
                    employeenumber = "[0-9]{5}"
                }
                );

            routes.MapRoute(
                "EmployeeTasksForm",
                "Tasks/Employee",
                new
                {
                    controller = "Tasks",
                    action = "EmployeeForm"
                }
                );

            routes.MapRoute(
                "StationTasks",
                "Tasks/Station/{startdate}/{enddate}/{station}",
                new
                {
                    controller = "Tasks",
                    action = "Station",
                    enddate = UrlParameter.Optional
                }
                );

            routes.MapRoute(
                "StationTasksForm",
                "Tasks/Station",
                new
                {
                    controller = "Tasks",
                    action = "StationForm"
                }
                );

            routes.MapRoute(
                "CADCAMTasks",
                "Tasks/CADCAM/{department}/{startdate}/{enddate}",
                new
                {
                    controller = "Tasks",
                    action = "CADCAM"
                },
                new
                {
                    department = "CAD|CAM"
                }
                );

            routes.MapRoute(
                "CADCAMTasksForm",
                "Tasks/CADCAM",
                new
                {
                    controller = "Tasks",
                    action = "CADCAMForm"
                }
                );
        }
    }


    public static class TasksExtensions
    {
        public static string DepartmentTasks( this UrlHelper url, DateTime startDate, DateTime endDate, string options, List<string> departments )
        {
            var strOptions = options.IsNotNullOrEmpty()
                                 ? options
                                 : "NoList";

            return $"/Tasks/Department/{startDate.ToURLDate()}/{endDate.ToURLDate()}/{strOptions}/{departments.JoinWith( "/" )}";
        }


        public static string EmployeeTasks( this UrlHelper url, int employeenumber, DateTime startDate, DateTime? endDate )
        {
            var strEndDate = endDate.HasValue
                                 ? "/" + endDate.Value.ToURLDate()
                                 : "";

            return $"/Tasks/Employee/{employeenumber}/{startDate.ToURLDate()}{strEndDate}";
        }


        public static string StationTasks( this UrlHelper url, string station, DateTime startDate, DateTime endDate )
        {
            return $"/Tasks/Station/{startDate.ToURLDate()}/{endDate.ToURLDate()}/{station}";
        }


        public static string CADCAMTasks( this UrlHelper url, string department, DateTime startDate, DateTime endDate )
        {
            return $"/Tasks/CADCAM/{department}/{startDate.ToURLDate()}/{endDate.ToURLDate()}";
        }
    }
}