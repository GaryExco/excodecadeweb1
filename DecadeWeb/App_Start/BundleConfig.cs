﻿using System.Web.Optimization;

namespace DecadeWeb
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            // CSS Bundle
            var cssBundle = new StyleBundle( "~/Content/css" );
            cssBundle.Include( "~/Content/bootstrap.css" );
            cssBundle.Include( "~/Content/bootstrap-responsive.css" );
            cssBundle.Include( "~/Content/font-awesome.css" );
            cssBundle.Include( "~/Content/jquery.pnotify.css" );
            cssBundle.Include( "~/Content/jquery.pnotify.icons.css" );
            cssBundle.Include( "~/Content/themes/custom-theme/jquery-ui-{version}.custom.css" );
            cssBundle.Include( "~/Content/themes/custom-theme/jquery.ui.{version}.ie.css" );
            cssBundle.Include( "~/Content/theme.bootstrap.css" );
            cssBundle.Include( "~/Content/CustomStyle.css" );
            cssBundle.Include( "~/Content/jquery-ui-timepicker-addon.css" );
#if DEBUG
            cssBundle.Transforms.Clear();
#endif
            bundles.Add(cssBundle);

            // JS Bundle
            var jsBundle = new ScriptBundle("~/Scripts/js");
            jsBundle.Include("~/Scripts/modernizr-*");
            jsBundle.Include("~/Scripts/jquery-{version}.js");
            jsBundle.Include("~/Scripts/jquery-ui-{version}.js");
            jsBundle.Include("~/Scripts/jquery.validate.js");
            jsBundle.Include("~/Scripts/jquery.validate.js");
            jsBundle.Include("~/Scripts/bootstrap.js");
            jsBundle.Include("~/Scripts/globalize/globalize.js");
            jsBundle.Include("~/Scripts/jquery-ui-i18n.js");
            jsBundle.Include("~/Scripts/jquery.pnotify.js");
            jsBundle.Include("~/Scripts/jquery.fileDownload.js");
            jsBundle.Include("~/Scripts/moment.js");
            jsBundle.Include("~/Scripts/livestamp.js");
            jsBundle.Include("~/Scripts/jquery.mtz.monthpicker.js");
            jsBundle.Include("~/Scripts/jquery.tablesorter.js");
            jsBundle.Include("~/Scripts/jquery.tablesorter.widgets.js");
            jsBundle.Include("~/Scripts/jquery.tablesorter.widgets-filter.js");
            jsBundle.Include("~/Scripts/knockout-{version}.js");
            jsBundle.Include("~/Scripts/knockout.validation.js");
            jsBundle.Include("~/Scripts/knockout.mapping-latest.js");
            jsBundle.Include("~/Scripts/mousetrap.js");
            jsBundle.Include("~/Scripts/mousetrap-global.js");
            jsBundle.Include("~/Scripts/persist.js");
            jsBundle.Include("~/Scripts/bootbox.js");
            jsBundle.Include("~/Scripts/highcharts.js");
            jsBundle.Include("~/Scripts/linq.js");
            jsBundle.Include("~/Scripts/linq.jquery.js");
            jsBundle.Include("~/Scripts/jquery-ui-timepicker-addon.js");
            // jsBundle.Include("~/Scripts/linq.rx.js");
            bundles.Add(jsBundle);

            // Custom JS Bundle
            var customJSBundle = new ScriptBundle("~/Scripts/custjs");
            customJSBundle.IncludeDirectory("~/Scripts/CustomJS", "*.js", true);
            bundles.Add(customJSBundle);

            #if DEBUG
            BundleTable.EnableOptimizations = false;
            #else
            BundleTable.EnableOptimizations = true;
            #endif
        }
    }
}